<?php

namespace MediaWiki\Skin\Yongheng;

use SkinTemplate;

/**
 * SkinTemplate class for the Yongheng skin
 *
 * @ingroup Skins
 */
class SkinYongheng extends SkinTemplate {
	/**
	 * @inheritDoc
	 */
	public function __construct(
		array $options = []
	) {
		$out = $this->getOutput();

		// Basic IE support without flexbox
		$out->addStyle( 'Yongheng/resources/IE9fixes.css', 'screen', 'IE' );

		parent::__construct( $options );
	}
}
