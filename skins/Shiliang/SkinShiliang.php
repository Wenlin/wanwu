<?php
/**
 * Shiliang - Version of Vector skin customized for Wenlin.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Skins
 */

/**
 * SkinTemplate class for Shiliang skin
 * @ingroup Skins
 */
class SkinShiliang extends SkinTemplate {
	public $skinname = 'shiliang';
	public $stylename = 'Shiliang';
	public $template = 'ShiliangTemplate';
	/**
	 * @var Config
	 */
	private $shiliangConfig;

	public function __construct() {
		$this->shiliangConfig = ConfigFactory::getDefaultInstance()->makeConfig( 'shiliang' );
	}

	/**
	 * Initializes output page and sets up skin-specific parameters
	 * @param OutputPage $out Object to initialize
	 */
	public function initPage( OutputPage $out ) {
		parent::initPage( $out );

		if ( $this->shiliangConfig->get( 'ShiliangResponsive' ) ) {
			$out->addMeta( 'viewport', 'width=device-width, initial-scale=1' );
			$out->addModuleStyles( 'skins.shiliang.styles.responsive' );
		}

		$out->addModules( 'skins.shiliang.js' );
	}

	/**
	 * Loads skin and user CSS files.
	 * @param OutputPage $out
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		/* Wenlin note: 'mediawiki.skinning.interface' is a standard MediaWiki thing,
			defined in resources/Resources.php, to load these three stylesheets:
				'resources/src/mediawiki.skinning/elements.css'
				'resources/src/mediawiki.skinning/content.css'
				'resources/src/mediawiki.skinning/interface.css'
			They include some styles we want to change, such as the
			color #0645ad for "a" tags. */
		$styles = [ 'mediawiki.skinning.interface', 'skins.shiliang.styles' ];
		Hooks::run( 'SkinShiliangStyleModules', [ $this, &$styles ] );
		$out->addModuleStyles( $styles );
	}

	/**
	 * Override to pass our Config instance to it
	 */
	public function setupTemplate( $classname, $repository = false, $cache_dir = false ) {
		return new $classname( $this->shiliangConfig );
	}
}
