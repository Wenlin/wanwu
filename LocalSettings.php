<?php
/* Customized for wenlin.co/wow, wenlin.biz/wow, and localhost (MacBook) by tbishop starting 2015-6-4.
	See includes/DefaultSettings.php for all configurable settings and their default values, but don't forget
	to make changes in _this_ file, not there. See <https://www.mediawiki.org/wiki/Manual:Configuration_settings>. */

if ( !defined( 'MEDIAWIKI' ) ) {
	exit; // Protect against web entry
}
$wgSitename = "Wenlin Dictionaries";
$wgMetaNamespace = "Project";
$wgScriptPath = "/wanwu";
$wgArticlePath = "/wow/$1";
$wgUsePathInfo = true; # Enable use of pretty URLs
$wgLogo = "{$wgScriptPath}/images_wenlin/logo.png";
$wgStylePath = "$wgScriptPath/skins"; // same as default, maybe not needed here?
$wgResourceBasePath = $wgScriptPath; // same as default, maybe not needed here?

######################
### Email:
$wgEmergencyContact = "wenlin@wenlin.com";
$wgPasswordSender = "wenlin@wenlin.com";

#############################
### Database settings:
# Wenlin moved wgDBpassword, wgDBuser, wgDBname, wgDBserver, wgDBtype, wgServer,
# and also wgSecretKey, to a file in a folder "private_inc" outside of public_html.
# This enables having LocalSettings.php in version control accessible by people without password,
# and also enables different values for "https://wenlin.biz", "https://wenlin.co", and "http://localhost" (MacBook),
# while we use this same LocalSettings.php for all of them. Only wow_db_settings.php needs to differ.
# See <http://www.mediawiki.org/wiki/Manual:Securing_database_passwords>.
# Important: $wgServer and $wgDBserver are different, and are both in wow_db_settings.php.
require_once(__DIR__ . '/../../private_inc/wow_db_settings.php');

# MySQL table options to use during installation or update
# Note for Wenlin: CHARSET=binary is important for Unicode above U+FFFF, don't want mysql bogus utf-8.
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";

##########################
## Shared memory settings:
// $wgMainCacheType = CACHE_NONE; // default is CACHE_NONE
$wgMemCachedServers = array(); // default is [ '127.0.0.1:11211' ], but probably no effect if CACHE_NONE, no need to change?

##########################
### Images, uploads:
## To enable image uploads, make sure the 'images' directory is writable, then set this to true:
$wgEnableUploads = true;
$wgUseImageMagick = true;
# Note: on Mac OS X, convert might be located at /opt/local/bin/convert or /usr/local/bin/convert, while on
# Linux (webserver) it's typically at "/usr/bin/convert". Let's use file_exists to try all locations here.
# Also, might need "chmod -R 777 images/" so wanwu/images folder is writable by Apache.
if (!file_exists($wgImageMagickConvertCommand)) { // DefaultSettings.php has '/usr/bin/convert'
	$convertLocations = array('/opt/local/bin/convert', '/usr/local/bin/convert');
	$found = false;
	foreach ($convertLocations as $c) {
		if (file_exists($c)) {
			$wgImageMagickConvertCommand = $c;
			$found = true;
			break;
		}
	}
	if (!$found) {
		error_log("Warning: in LocalSettings.php, $wgImageMagickConvertCommand not found"
				. ", wgImageMagickConvertCommand may fail");
	}
}

##########################
### Skins:
# The Shiliang skin is new for Wenlin.
$wgDefaultSkin = "shiliang"; /* Wenlin: shǐliàng 矢量 n. vector */
wfLoadSkin( 'Vector' );
wfLoadSkin( 'Shiliang' );
# wfLoadSkin( 'Monobook' );
# wfLoadSkin( 'Timeless' );

# Enable MediaWiki:Common.css for log-in and preferences pages:
$wgAllowSiteCSSOnRestrictedPages = true;

##########################
### Extensions:
# Most extensions are enabled by including the base extension file here
# but check specific extension documentation for more details.

# ImageMap and ParserFunctions were automatically enabled by the installer.
wfLoadExtension( 'ImageMap' );
wfLoadExtension( 'ParserFunctions' );

# For Wenlin we add WikiEditor, and turn on usebetatoolbar, to facilitate insertion of IPA and other symbols.
wfLoadExtension( 'WikiEditor' );
$wgDefaultUserOptions['usebetatoolbar'] = 1;
$wgDefaultUserOptions['usebetatoolbar-cgd'] = 1;
$wgDefaultUserOptions['wikieditor-preview'] = 1;

# For Wenlin we also add ReplaceText, and enable 'replacetext' for sysop
wfLoadExtension( 'ReplaceText' );
$wgGroupPermissions['sysop']['replacetext'] = true;

# See also wfLoadExtension('Wenlin') elsewhere in this file.

# Inhibit "Jump to: navigation, search" at top of each page, which started after upgrade
# to MediaWiki 1.32.0.
$wgDefaultUserOptions['showjumplinks'] = 0;

#######################################
### Namespaces:
define("NS_WL_CI",        3180);
define("NS_WL_CI_TALK",   3181);
define("NS_WL_ZI",        3182);
define("NS_WL_ZI_TALK",   3183);
define("NS_WL_EN",        3184);
define("NS_WL_EN_TALK",   3185);
define("NS_WL_CDL",       3186);
define("NS_WL_CDL_TALK",  3187);
define("NS_WL_JYUT",      3188);
define("NS_WL_JYUT_TALK", 3189);
$wgExtraNamespaces[NS_WL_CI]        = "Ci";
$wgExtraNamespaces[NS_WL_CI_TALK]   = "Ci_talk";
$wgExtraNamespaces[NS_WL_ZI]        = "Zi";
$wgExtraNamespaces[NS_WL_ZI_TALK]   = "Zi_talk";
$wgExtraNamespaces[NS_WL_EN]        = "En";
$wgExtraNamespaces[NS_WL_EN_TALK]   = "En_talk";
$wgExtraNamespaces[NS_WL_CDL]       = "Cdl";
$wgExtraNamespaces[NS_WL_CDL_TALK]  = "Cdl_talk";
$wgExtraNamespaces[NS_WL_JYUT]      = "Jyut";
$wgExtraNamespaces[NS_WL_JYUT_TALK] = "Jyut_talk";
$wgContentNamespaces = array(NS_MAIN, NS_WL_CI, NS_WL_ZI, NS_WL_EN, NS_WL_CDL, NS_WL_JYUT);

/* To prevent circumvention of access restrictions, set wgNonincludableNamespaces to same as
	wgContentNamespaces. Maybe we can find a way to loosen this restriction eventually, supposing
	there might be benefits to supporting inclusion/transclusion in our wiki.
		Setting $wgNonincludableNamespaces here does not prevent using {{...}} templates in pages in
	our content namespaces, if the templates themselves are in the Template namespace (NS_TEMPLATE),
	which is not in $wgContentNamespaces. References:
		https://www.mediawiki.org/wiki/Security_issues_with_authorization_extensions
		https://www.mediawiki.org/wiki/Manual:$wgNonincludableNamespaces */
$wgNonincludableNamespaces = $wgContentNamespaces;

/* By default, search all our extra namespaces except NS_WL_CDL. MediaWiki default is only NS_MAIN.
	TODO: turn off NS_WL_JYUT for users without Jyut access. Should that happen in a hook? */
foreach (array_keys($wgExtraNamespaces) as $n) {
	if ($n !== NS_WL_CDL && $n !== NS_WL_CDL_TALK) {
		$wgNamespacesToBeSearchedDefault[$n] = true;
	}
}

###########################
#### Rights
# Restrict editing Main and Cdl namespaces to those with the 'editinterface' right (i.e., sysops).
$wgNamespaceProtection[NS_MAIN] = array('editinterface');
$wgNamespaceProtection[NS_WL_CDL] = array('editinterface');

# Turn off syndication feeds (RSS/Atom), due to permission issues and dubious utility.
# Setting $wgFeed = false would be sufficient, but we also set $wgFeedClasses and/or $wgAdvertisedFeedTypes
# to empty arrays as a workaround for a bug: https://phabricator.wikimedia.org/T116145
$wgFeed = false;
$wgFeedClasses = [];
$wgAdvertisedFeedTypes = [];

# Wenlin-specific rights and groups:
# Some entries are accessible even to anonymous users; others require
# registration without subscription payment; still others require registration
# and subscription payment.
$wenlinSpecificRights = [
	# 'WenlinRightToViewAllZi' is specific to namespace 'Zi' (zidian, cf. NS_WL_ZI).
	# Likewise for namespaces 'Ci', 'En', 'Jyut'.
	'WenlinRightToViewAllZi',
	'WenlinRightToViewAllCi',
	'WenlinRightToViewAllEn',
	'WenlinRightToViewAllJyut'
];
/* Establish the existence of 'WenlinGroupFreeBasic', even though we
	assign it none of the wenlinSpecificRights; currently anyone logged in
	is assumed to have at least the rights of 'WenlinGroupFreeBasic'.
	Adding to $wgGroupPermissions makes the group name appear in
	various special pages; assigning 'read' right here might otherwise
	be redundant. Same for 'WenlinGroupFreeIntro'. */
$wgGroupPermissions['WenlinGroupFreeBasic']['read'] = true;
$wgGroupPermissions['WenlinGroupFreeIntro']['read'] = true;

/* Our groups are:
	'WenlinGroupFullSubscriber' -- can view everything that's possible by subscribing
	'WenlinGroupCantoneseDeluxe' -- can view complete EC/CE/Zidian/Cantonese
	'WenlinGroupDeluxe' -- can view complete EC/CE/Zidian (not Cantonese)
	'WenlinGroupFreeBasic' -- can view abridged EC/CE/Zidian, maybe abridged Cantonese
	'WenlinGroupFreeIntro' -- can view complete EC/CE/Zidian/Cantonese, no editing
	Currently 'WenlinGroupFullSubscriber' and 'WenlinGroupCantoneseDeluxe' have almost
	identical rights; main (only?) exception is 'writeapi'.
*/
foreach ($wenlinSpecificRights as $r) {
	$wgAvailableRights[] = $r;
	$wgGroupPermissions['WenlinGroupFullSubscriber'][$r] = true;
	$wgGroupPermissions['WenlinGroupCantoneseDeluxe'][$r] = true;
	$wgGroupPermissions['WenlinGroupFreeIntro'][$r] = true;
	if ($r != 'WenlinRightToViewAllJyut') {
		$wgGroupPermissions['WenlinGroupDeluxe'][$r] = true;
	}
}

/* TODO: add 3 pages for each of our custom WenlinGroups:
	MediaWiki:Group-<group-name> (content: Name of the group)
	MediaWiki:Group-<group-name>-member (content: Name of a member of the group)
	MediaWiki:Grouppage-<group-name> (content: Name of the group page, like "Project:Users")
	Reference:
		See https://www.mediawiki.org/wiki/Manual:User_rights
		See https://www.mediawiki.org/wiki/Manual:$wgAvailableRights
*/
/* Some of these rights are typically on by default for most users in default MediaWiki.
	For Wenlin, most of them are conditional on group membership. For a list
	of groups and rights, see https://wenlin.co/wow/Special:ListGroupRights and
	https://www.mediawiki.org/wiki/Manual:User_rights
*/
$wenlinRestrictedRights = [
	'applychangetags', // Apply tags along with one's changes
	'changetags', // Add and remove arbitrary tags on individual revisions and log entries
	'createaccount', // Create new user accounts
	'createpage', // Create pages (which are not discussion pages)
	'createtalk', // Create discussion pages
	'edit', // Edit pages
	'editcontentmodel', // Edit the content model of a page
	'minoredit', // Mark edits as minor
	'move-categorypages', // Move category pages
	'movefile', // Move files
	'move', // Move pages
	'move-subpages', // Move pages with their subpages
	'move-rootuserpages', // Move root user pages
	'reupload-shared', // Override files on the shared media repository locally
	'reupload', // Overwrite existing files
	'purge', // Purge the site cache for a page without confirmation
	'read', // Read pages
	'sendemail', // Send email to other users
	'upload', // Upload files
	'writeapi', // Use of the write API
];
// Turn off restricted rights for users with limited access
foreach ($wenlinRestrictedRights as $r) {
	# User '*' means everybody; 'user' means logged-in users.
	$wgGroupPermissions['*'][$r] = false; // possibly overridden below
	if ($r != 'read') {
		$wgGroupPermissions['user'][$r] = false; // possibly overridden below
	}
}

// Turn the following on for deluxe/full subscribers.
$wenlinDeluxeRights = [
	'applychangetags', // Apply tags along with one's changes
	'changetags', // Add and remove arbitrary tags on individual revisions and log entries
	'createpage', // Create pages (which are not discussion pages)
	'createtalk', // Create discussion pages
	'edit', // Edit pages
	'minoredit', // Mark edits as minor
	'move-categorypages', // Move category pages
	'movefile', // Move files
	'move', // Move pages
	'move-subpages', // Move pages with their subpages
	'move-rootuserpages', // Move root user pages
	'reupload-shared', // Override files on the shared media repository locally
	'reupload', // Overwrite existing files
	'purge', // Purge the site cache for a page without confirmation
	'read', // Read pages
	'sendemail', // Send email to other users
	'upload', // Upload files
];
foreach ($wenlinDeluxeRights as $r) {
	$wgGroupPermissions['WenlinGroupFullSubscriber'][$r] = true;
	$wgGroupPermissions['WenlinGroupCantoneseDeluxe'][$r] = true;
	$wgGroupPermissions['WenlinGroupDeluxe'][$r] = true;
}
$wgGroupPermissions['WenlinGroupFullSubscriber']['writeapi'] = true; // special for staff, web-sync

# Starting 2017-5-25, even anonymous users can read a subset of pages.
$wgGroupPermissions['*']['read'] = true;

##############################
### Load the Wenlin Extension
# Does it make any difference where in LocalSettings.php we do this?
# Pending further study, safest to assume that it should be below at least some of our
# customized settings above, like $wgGroupPermissions. It would be more orderly to have
# all the calls to wfLoadExtension in the same part of LocalSettings.php. And maybe some
# Wenlin-extension-specific code should be moved to a function in our extension, rather
# than occupying so much space in LocalSettings.php.
wfLoadExtension('Wenlin');

# For the pages STATS_NS_Zi, STATS_NS_Ci, STATS_NS_En to work, we need
# to set these values for wgArticleCountMethod and wgAllowSlowParserFunctions.
$wgArticleCountMethod = "any";
$wgAllowSlowParserFunctions = true;

##############################
### Password policies
# Default MinimalPasswordLength is 1. Change it to 8.
$wgPasswordPolicy['policies']['default']['MinimalPasswordLength'] = 8;

###############################
### CapitalLinks:
# $wgCapitalLinks: for Wenlin (and dictionaries in general) we want it false, as on Wiktionary; default is true as on Wikipedia.
# See <https://en.wiktionary.org/wiki/Wiktionary:Capitalization>. Also <http://www.mediawiki.org/wiki/Manual:$wgCapitalLinks>.
# The $wgCapitalLinks setting affects all namespaces except Special, MediaWiki and User.
$wgCapitalLinks = false;

###############################
### Timezone
# To avoid "PHP Warning: date_default_timezone_get()...", see documentation at
# <https://www.mediawiki.org/wiki/Manual:Timezone>.
# <https://www.mediawiki.org/wiki/Manual:Errors_and_symptoms#Strict_Standards:_date_default_timezone_get.28.29:_It_is_not_safe_to_rely_on_the_system.27s_timezone_settings.>
# For simplicity, for now:
$wgLocaltimezone = "UTC";

###############################
### Recent Changes:
# For sync-from-web (ch_web_dict.c), we want the "recent changes" api to cover the complete history.
# Cf. rebuildrecentchanges.php, <https://www.mediawiki.org/wiki/Manual_talk:$wgRCMaxAge>.
// $wgRCMaxAge = 31556908800; // 1000 years = 86400 * 365.242 * 1000 seconds
$wgRCMaxAge = time(); // no larger, since negative values (for before 1970) may be problematic and the wiki's not that old
// Round down to a whole number of days to avoid fraction in Watchlist "Period of time to display".
$wgRCMaxAge -= ($wgRCMaxAge % 86400);

###############################
### Searching:
$wgAdvancedSearchHighlighting = true;

###############################
### SETTINGS FOR DEBUGGING
# IMPORTANT: some of the debugging and error reporting settings should be changed, for
# security, when we make the wiki available to the general public.

# Note: setting $wgDebugLogFile may trigger "PHP Warning: file_put_contents ... Permission denied".
# Also, the log file may grow very large even with $wgDebugComments = false.
# The images folder is generally writable. Currently wfDebug works for wanwu/images but not
# for wanwu itself or the home folder.
if ($wgServer === 'http://localhost') { // Note: $wgServer is defined in wow_db_settings.php, included above.
	error_reporting(E_ALL);
	ini_set( 'display_errors', 1 );
	$wgDebugLogFile = __DIR__ . '/images/wow_wiki_log.txt';
	## $wgDebugLogFile = __DIR__ . '/../../wow_wiki_log.txt';
	# $wgDebugComments = true; # true may cause large log file, e.g., 535 MB
	$wgShowExceptionDetails = true;
	$wgDebugPrintHttpHeaders = true;
	$wgShowDBErrorBacktrace = true;
	// $wgEnableParserCache = false;
	// $wgCachePages = false;
	// wfDebug( "LocalSettings.php: testing 你好\n" );

	$wgDebugToolbar = true;
	MWDebug::init();
	MWDebug::log("LocalSettings.php: testing MWDebug::log, 真棒!\n");
}

# At one time we thought these two lines might help with a problem importing dictionary
# entries into empty database. It's not clear if they ever helped.
# $wgEnableParserCache = false;
# $wgCachePages = false;

# temporary debugging -- see comment in WenlinXuke.php where $wgParserCacheType prevents strange bug
# $wgParserCacheType = CACHE_NONE;
# $wgResourceLoaderDebug = true;
