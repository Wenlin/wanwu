This repository contains a customized and extended version of the MediaWiki
software, including the Wenlin extension, the Shiliang skin, and our customized
LocalSettings.php.

Modifications from the original MediaWiki are copyright (c) 2017 Wenlin
Institute, Inc. SPC. The Wenlin extension (extensions/Wenlin) is offered for
licensing under the AGPL v.3 license. See extensions/Wenlin/LICENSE.txt.

To contact Wenlin Institute, please see https://wenlin.com/contact

Please see also MEDIAWIKI_README.txt, which is the file that was named
README in the original MediaWiki distribution. Thanks to MediaWiki!!!
