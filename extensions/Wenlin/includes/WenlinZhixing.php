<?php
/* Copyright (c) 2018 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/
/**
	WenlinZhixing.php for the Wenlin MediaWiki extension.

	Execute commands, generally received from SpecialWenlin.php.

	'Zhixing' means zhíxíng 执行 'execute' or:
		¹zhī-xíng 知行 {F} n. knowing and doing; knowledge and action
		²zhīxíng 枝形 {F} attr. branch-shaped
		zhīxìng 知性 n. intellect
		zhíxīng 值星 v.o. 〈mil.〉 be on duty for the week
		¹zhíxíng* 執行[执-] {B} v. carry out the execution; carry out; execute
			‖ Mìnglìng lìkè jiù bèi ∼ le. 命令立刻就被∼了。
			The command was carried out at once. ◆attr. executive
		²zhíxíng 直行 v. go straight ahead See also ¹zhíháng
		zhíxìng 直性 See zhíxìngzi
		¹zhǐxíng 紙型[纸-] n. 〈print.〉 paper mold/matrix
		²zhǐxíng 指形 attr. finger; finger-type
		¹zhì-xíng 志行 n. one's aspirations and conduct; purpose/ambition and behavior
		²zhìxíng 至行 n. most virtuous conduct
		³zhìxíng 治行 v. prepare for a journey ◆n. results of an administration
		¹zhìxìng 至性 n. ①natural disposition to love one's family ②great capacity for love
		²zhìxìng 智性 attr. enlightening; stimulating

	@file
	@ingroup Extensions
*/

class WenlinZhixing
{
	private $command; // a string defining a Wenlin-specific command
	private $webRequest; // type is WebRequest
	private $outputPage; // type is OutputPage
	private $outputString; // a string, used for output if outputPage is null (monosyllabic Hanzi strings)
	private $wlParser; // type is WenlinParser
	private $listNextPrev; // a string: 'next', 'prev', or null
	private $user;

	/** __construct:
		Construct a WenlinZhixing and execute its command.

		@param $command a string defining a Wenlin-specific command (may be null for listMonosyllabicEntriesForZiEntry).
		@param WebRequest $webRequest the container for the URL query (may be null for listMonosyllabicEntriesForZiEntry).
		@param OutputPage $outputPage where we send the output to be displayed (may be null for listMonosyllabicEntriesForZiEntry).
		@param WenlinParser $wlParser for parsing wiki text into html.
	*/
	public function __construct($command, $webRequest, $outputPage, $wlParser)
	{
		$this->command = $command;
		$this->webRequest = $webRequest;
		$this->outputPage = $outputPage;
		$this->outputString = '';
		$this->wlParser = $wlParser;
		$this->user = RequestContext::getMain()->getUser();
		if ($command !== null && $webRequest !== null) { // See listMonosyllabicEntriesForZiEntry for which null $command is normal.
			$this->adjustForQQQuery();
			$this->executeCommand();
		}
	} // __construct

	/** executeCommand:
		Execute a command.
	*/
	private function executeCommand()
	{
		switch ($this->command) {
		case 'a':
			$this->listWordsAlphabetically();
			return;
		case 'bz':
			$this->listCharactersContaining();
			return;
		case 'create':
			$this->createNewEntry();
			return;
		case 'py':
			$this->describePinyinSyllable();
			return;
		case 'pyzi':
			$this->listCharactersByPinyin();
			return;
		case 's':
			$this->lookup();
			return;
		case 'test':
			$this->test();
			return;
		case 'zc':
			$this->listWordsContaining();
			return;
		case 'zifreq':
			$this->listCharactersByFrequency();
			return;
		}
		$this->putParagraph("The command [$this->command] isn't recognized.");
	} // executeCommand

	/** adjustForQQQuery:
		Adjust query for 'QQ' syntax: get around the problem of embedding queries in [[wiki links]], and using Title::newFromText(),
		since MediaWiki URL-encodes the symbols (?, &) rather than making them into query. However, only
		do this if the command is preceded by 'QQ' since doing it in general would prevent the inclusion of
		question marks in queries where the URL-encoding is appropriate. So the QQ is specifically for use in wiki links like:
		"[[Special:Wenlin/QQa?a=a&ns=En|List English entries alphabetically]]".
		An awful alternative is like this:
		<span class="plainlinks">[http://localhost/wow/Special:Wenlin/a?a=a&amp;ns=En List English entries alphabetically]</span>
	*/
	private function adjustForQQQuery()
	{
		/* If we match 'QQ' but not '?', simply strip the 'QQ'. This is in case MediaWiki behavior changes,
			or in case we use 'QQ' for a command without arguments. */
		if (preg_match("/^QQ(.+)$/u", $this->command, $matches)) {
			$this->command = $matches[1];
			if (preg_match("/^(.+)\?(.+)$/u", $this->command, $matches)) {
				$this->command = $matches[1];
				$query = $matches[2];
				$array = explode('&', $query);
				foreach ($array as $keyVal) {
					if (preg_match("/^(.+)=(.+)$/u", $keyVal, $matches)) {
						$this->webRequest->setVal($matches[1], $matches[2]);
					}
				}
			}
		}
	} // adjustForQQQuery

	/** lookup:
		Given a term (vocabulary item) to look up, show any matching dictionary item(s).
		For a single Hanzi, make a title like "Zi:字" where 字 is the Hanzi.
		For a word in English or Chinese (pinyin or Hanzi), use our custom database tables to
		find one or more matching entries, and get their serial numbers to construct the title
		like "Ci:$ser" or "En:$ser".

		Compare UULookupVocabulary in ch_uses.c, especially for how to distinguish Pinyin from English.
		However, our method for the wiki is different: we generally look for matches in all dictionaries,
		regardless of whether the term looks more like Pinyin, Jyutping, or English. If it starts with a Hanzi,
		however, we skip English.
	*/
	private function lookup()
	{
		$term = $this->webRequest->getText('s');
		$term = trim($term); // remove any surrounding whitespace
		$term = str_replace('_', ' ', $term); // replace underscores with spaces
		$term = str_replace('​', '', $term); // remove U+200B ZERO WIDTH SPACE
		$this->setTitle($term);
		$zin = WenlinUtf::ziNumber($term);
		if (WenlinFenlei::zinIsCJK($zin)) {
			if (WenlinUtf::zinToStr($zin) == $term) { // single Hanzi
				$this->redirect("Zi:$term"); // eventually calls listMonosyllabicEntriesForZiEntry
			}
			else {
				$this->lookupHanziString($term, false /* not monosyllabic */, WenlinDictionaryPreferences::singleton());
			}
		}
		else {
			$this->lookupAlphabeticalString($term);
		}
	} // lookup

	/** listMonosyllabicEntriesForZiEntry:
		Get all monosyllabic Ci/Jyut entries for the given Hanzi, to be displayed
		within the page for the Zi entry.

		@param zi the head character (Hanzi) of the Zidian entry.
		@param WenlinParser wlParser, used for parseWikiToHtml.
		@param WenlinDictionaryPreferences $dictPref.
		@return the text, zero or more lines, one line for each monosyllabic entry, as HTML.

		Called by WenlinRender::renderZidian.
		This function is static, so it creates a new WenlinZhixing.
	*/
	public static function listMonosyllabicEntriesForZiEntry($zi, $wlParser, $dictPref)
	{
		$zhixing = new WenlinZhixing(null /* command */, null /* $webRequest */, null /* $outputPage */, $wlParser);
		$zhixing->lookupHanziString($zi, true /*monosyllabic */, $dictPref);
		return $zhixing->outputString;
	} // listMonosyllabicEntriesForZiEntry

	/** lookupHanziString:
		Given a Chinese vocabulary item as a string of one or more Hanzi, output any matching dictionary item(s).

		In general this function may search more than one namespace: Jyut and/or Ci.

		@param $term the vocabulary item (Hanzi string) to be looked up.
		@param boolean $isSingleHanzi true for monosyllable (single Hanzi), false for polysyllable (two or more Hanzi).
		@param $dictPref the WenlinDictionaryPreferences.
		@return the output text if $isSingleHanzi; else null (for polysyllables, use outputPage).

		Called by lookup ($isSingleHanzi = false), and by listMonosyllabicEntriesForZiEntry ($isSingleHanzi = true).
	*/
	private function lookupHanziString($term, $isSingleHanzi, $dictPref)
	{
		/* Depending on user rights and preferences, use an array of namespaces
			like ['Jyut', 'Ci'], ['Ci', 'Jyut'], ['Jyut'], or ['Ci']. */
		$namespaceArray = $dictPref->getPreferredChineseNamespaces();
		$totalCount = 0;
		$dbr = wfGetDB(DB_REPLICA);
		foreach ($namespaceArray as $namespaceStr) {
			$res[$namespaceStr] = $this->reallyLookupHanziString($term, $namespaceStr, $dbr);
			$count[$namespaceStr] = ($res[$namespaceStr] == null) ? 0 : $res[$namespaceStr]->numRows();
			$totalCount += $count[$namespaceStr];
		}
		if ($totalCount == 0) {
			$isHanziString = true;
			$namespaceStrForWebSearch = 'Zi'; // 'Zi' here equiv. to anything but 'En', used only for Google Translate
			$this->putZeroEntries($term, $isSingleHanzi, $isHanziString, $namespaceArray, $namespaceStrForWebSearch);
		}
		else {
			$bandContext = new WenlinBandContext();
			if ($isSingleHanzi) {
				$bandContext->singleLine = true; // redundant?
				$bandContext->ssw = true;
			}
			$queryOptions['bandContext'] = $bandContext;
			$queryOptions['mustList'] = $isSingleHanzi || ($totalCount > 1);
			foreach ($namespaceArray as $namespaceStr) {
				$n = $count[$namespaceStr];
				if ($n > 0) {
					/* TODO: put number of entries BEFORE the list instead of after it. Already done for monosyllabic (single Hanzi) entries,
						want to do the same for all kinds of entries. */
					if ($isSingleHanzi) {
						$link = WenlinGongju::makeNamespaceLink($namespaceStr);
						$header = ($n == 1) ? "One monosyllabic entry in $link:" : "$n monosyllabic entries in $link:";
						$this->putStyledParagraph('wenlin-small-header', $header);
					}
					$this->loadEntriesFromQueryResult($res[$namespaceStr], $namespaceStr, $queryOptions, $dbr, $dictPref);
				}
			}
			/* Even though some entries do exist in some namespaces, provide buttons for adding new
				entries in all compatible namespaces. It might be more elegant to have a single link to another
				page giving choice of namespaces, but this is simpler and easier to implement at the moment.
				Compare identical code in lookupAlphabeticalString.
				Don't append the create-entry links, however, if $isSingleHanzi -- too disruptive, and there
				are buttons available with the same effect by clicking on any of the existing monosyllabic entries. */
			if (!$isSingleHanzi) {
				$this->putParagraph(WenlinGongju::makeCreateEntryLinksWithNsArray($term, $namespaceArray));
			}
		}
		return ($this->outputPage !== null) ? null : $this->outputString;
	} // lookupHanziString

	/** lookupAlphabeticalString:
		Given a string of alphabetical characters, output any matching dictionary item(s).

		In general this function may search more than one namespace.

		This function is nearly identical to lookupHanziString; merge?

		@param $term the vocabulary item to be looked up.
	*/
	private function lookupAlphabeticalString($term)
	{
		/* Depending on user rights and preferences, use an array of namespaces
			like ['Jyut', 'Ci'], ['Ci', 'Jyut'], ['Jyut'], or ['Ci']. Then always add 'En'. */
		$dictPref = WenlinDictionaryPreferences::singleton();
		$namespaceArray = $dictPref->getPreferredChineseNamespaces();
		$namespaceArray[] = 'En';
		$dbr = wfGetDB(DB_REPLICA);
		$totalCount = 0;
		foreach ($namespaceArray as $namespaceStr) {
			$res[$namespaceStr] = $this->reallyLookupAlphabeticalString($term, $namespaceStr, $dbr);
			$count[$namespaceStr] = ($res[$namespaceStr] == null) ? 0 : $res[$namespaceStr]->numRows();
			$totalCount += $count[$namespaceStr];
		}
		if ($totalCount == 0) {
			$isHanziString = false;
			$isMonosyllabic = false;
			$namespaceStrForWebSearch = 'Ci'; // 'Ci' here equiv. to anything but 'En', used only for Google Translate
			$this->putZeroEntries($term, $isMonosyllabic, $isHanziString, $namespaceArray, $namespaceStrForWebSearch);
			return;
		}
		$queryOptions['mustList'] = ($totalCount > 1);
		foreach ($namespaceArray as $namespaceStr) {
			if ($count[$namespaceStr] > 0) {
				/* TODO: put number of entries BEFORE the list instead of after it; currently loadEntriesFromQueryResult calls summarizeList at the end. */
				$this->loadEntriesFromQueryResult($res[$namespaceStr], $namespaceStr, $queryOptions, $dbr, $dictPref);
			}
		}
		/* Even though some entries do exist in some namespaces, provide buttons for adding new
			entries in all compatible namespaces. It might be more elegant to have a single link to another
			page giving choice of namespaces, but this is simpler and easier to implement at the moment.
			Compare identical code in lookupHanziString. */
		$this->putParagraph(WenlinGongju::makeCreateEntryLinksWithNsArray($term, $namespaceArray));
	} // lookupAlphabeticalString

	/** putZeroEntries:
		Output appropriate messages, links, etc., given that no matching entries were found for certain namespace(s).

		@param $term the vocabulary item to be looked up.
		@param $isSingleHanzi true if look-up term is single Hanzi like "字", else false.
		@param $isHanziString true if look-up term is a Hanzi string like "字" or "词典" (not like "cídiǎn"), else false.
		@param $namespaceArray the array of namespaces (as strings) that were searched.
		@param $namespaceStrForWebSearch the namespace, as a string, used by makeWebSearchButtons, to customize the buttons,
			but not always identical to the namespace where we were looking for an entry.
	*/
	private function putZeroEntries($term, $isSingleHanzi, $isHanziString, $namespaceArray, $namespaceStrForWebSearch)
	{
		$nsListString = '';
		foreach ($namespaceArray as $namespaceStr) {
			if ($nsListString !== '') {
				$nsListString .= '/';
			}
			$nsListString .= $namespaceStr;
		}
		if ($isSingleHanzi) { // $namespaceArray typically ['Ci', 'Jyut']
			$this->putStyledParagraph('wenlin-small-header', "No monosyllabic words currently in namespace(s) $nsListString.");
			$this->putParagraph(WenlinGongju::makeCreateEntryLinksWithNsArray($term, $namespaceArray));
			return;
		}
		$this->putParagraph("Zero entries match ‘" . $term . "’ as headword in namespace(s) $nsListString.");
		if ($isHanziString) {
			// $this->putParagraph("words with ‘玩、万’"); // TODO: see UU_ZC_ZZ in C code
		}
		else { // alphabetical
			$this->putParagraph(WenlinGongju::makeAdjAlphaLinksWithNsArray($term, $namespaceArray));
		}
		$this->putParagraph(WenlinGongju::makeFulltextSearchLink($term));
		/* $namespaceStrForWebSearch: 'Ci' here equiv. to anything but 'En'; 'Zi' here equiv. to anything but 'En',;
				used only for Google Translate */
		$this->putParagraph(WenlinGongju::makeWebSearchButtons($term, $namespaceStrForWebSearch));
		$this->putParagraph(WenlinGongju::makeCreateEntryLinksWithNsArray($term, $namespaceArray));
	} // putZeroEntries

	/** reallyLookupHanziString:
		Given a Chinese vocabulary item as a string of two or more Hanzi, and
		given a particular namespace, get any matching dictionary item(s).

		@param $term the vocabulary item to be looked up.
		@param $namespaceStr the namespace such as 'Ci' or 'Jyut'.
		@param $dbr the database for reading.
		@return the ResultWrapper.
	*/
	private function reallyLookupHanziString($term, $namespaceStr, $dbr)
	{
		if ($namespaceStr == 'Jyut') {
			$query['table'] = array('h' => 'wenlin_jyut_hz_string', 'f' => 'wenlin_jyut_ping');
		}
		else { // Ci
			$query['table'] = array('h' => 'wenlin_ci_hanzi_string', 'f' => 'wenlin_ci_pinyin');
		}
		$query['vars'] = array('serial_number');
		$query['conds'] = array("h.hanzi = '$term'");
		$query['options'] = array('ORDER BY' => 'f.frequency DESC, f.pinyin_collation');
		$query['join_conds'] = array('f' => array('JOIN', 'h.page_id = f.page_id'));
		/* SELECT serial_number FROM `wenlin_ci_hanzi_string` `h` JOIN `wenlin_ci_pinyin` `f` ON ((h.page_id = f.page_id)) WHERE (h.hanzi = '成功') ORDER BY f.frequency DESC, f.pinyin_collation */

		return $this->runQuery($query, $dbr);
	} // reallyLookupHanziString

	/** reallyLookupAlphabeticalString:
		Given an alphabetical vocabulary item (Pinyin/Jyutping/English),
		get any matching dictionary item(s) in the given namespace.

		@param $term the vocabulary item to be looked up.
		@param $namespaceStr the namespace (Ci, Jyut, En, ...).
		@param $dbr the database for reading.
		@return the ResultWrapper.
	*/
	private function reallyLookupAlphabeticalString($term, $namespaceStr, $dbr)
	{
		/* If the term starts with a homophone number, look for an exact match; otherwise simplify the key. */
		$simplifyKey = preg_match('/^[⁰¹²³⁴⁵⁶⁷⁸⁹0-9]/u', $term) ? false : true;
		switch ($namespaceStr) {
		case 'Ci':
			/* We don't need WenlinIndex::escapeString for Ci, since the key isn't compressed and doesn't contain
				any of the seven bytes NUL, \n, \r, \, ', ", or Control-Z. */
			/* Replace zero (used only for tones) with mysql wildcard '_'. This way, "qiguai" matches "qíguài",
				and "women" matches "wǒmen". If using '_' turns out to be slow (does the index still get used?),
				we may consider making a MORE simplified pinyin key, without tones (if there are any zeros
				for tones), and then filtering like the C function CiABCTonesDoMatch. But '_' is much easier!
				It seems the index is used for everything before the wildcard, as long as the wildcard isn't at the
				beginning. See <http://use-the-index-luke.com/sql/where-clause/searching-for-ranges/like-performance-tuning>.
				This should be fine. For mysql LIKE, '_' matches one character, and '%' matches any number of characters. */
			$key = WenlinCollate::makePinyinCollationKey($term, $simplifyKey);
			$like = str_replace('0', '_', $key) . '%';
			$query['table'] = array('wenlin_ci_pinyin');
			break;
		case 'Jyut':
			$key = WenlinCollate::makeJyutpingCollationKey($term, $simplifyKey);
			$like = str_replace('0', '_', $key) . '%';
			$query['table'] = array('wenlin_jyut_ping');
			break;
		case 'En':
			$key = WenlinCollate::makeEnglishCollationKey($term, $simplifyKey);
			/* We do need escapeString here. Without it, we get database error for "dragon":
				Query: SELECT serial_number FROM `wenlin_en_collation` WHERE (english_collation LIKE '-I'3CA%') ORDER BY english_collation
				-- due to lack of backslash for the apostrophe. With escapeString, it appears to work correctly. See comments for WenlinIndex::escapeString
				concerning inconsistency; escapeString is needed for select() but must not be used for insert(), for which MediaWiki uses strencode() to do
				the escaping. */
			$like = WenlinIndex::escapeString($key) . '%'; // for mysql LIKE
			$query['table'] = array('wenlin_en_collation');
			break;
		}
		if ($key == '' || $key == '??') {
			return null;
		}
		/* We could have avoided this awkward $collation code by using the same name
			like 'collation' instead of different names 'pinyin_collation' and 'english_collation'.
			We do use 'pinyin_collation' for both Ci and Jyut.
			Compare similar code in listWordsAlphabetically. */
		$collation = ($namespaceStr == 'En') ? 'english_collation' : 'pinyin_collation';
		$query['vars'] = array('serial_number');
		/* As a special case, enable looking up pinyin including homophone number, even when the
			entry's pinyin might have an asterisk. This is needed, for example, to look up ¹gǔn by inputting
			it, including the homophone number, in the search box. The actual entry has ¹gǔn*
			(including the asterisk) as its pinyin. (The asterisk indicates this is the highest frequency
			vocabulary item in its group of near-homophones, where tone is ignored for determining
			near-homophones.) Collation keys are gun...3..a for ¹gǔn*, and gun...3..b for ¹gǔn.
			Make the query condition like "pinyin_collation = 'gun...3..a' OR 'gun...3..b'", only if the
			first character is a digit ¹ or 1, and the second character is a non-digit (beware ¹⁰yí). */
		if (!$simplifyKey && $namespaceStr == 'Ci' && preg_match('/^[¹1][^⁰¹²³⁴⁵⁶⁷⁸⁹0-9]/u', $term)) {
			$keyA = preg_replace('/([ab])$/u', 'a', $key); // replace final a or b with a
			$keyB = preg_replace('/([ab])$/u', 'b', $key); // replace final a or b with b
			$query['conds'] = array("$collation = '$keyA' OR $collation = '$keyB'");
		}
		else { /* The normal case is with LIKE, even if !$simplifyKey. */
			$query['conds'] = array("$collation LIKE '$like'");
		}
		$query['options'] = array('ORDER BY' => $collation);
		$query['join_conds'] = array();
		/* SELECT serial_number FROM `wenlin_ci_pinyin` WHERE (pinyin_collation LIKE 'keai...34.%') ORDER BY pinyin_collation
			SELECT serial_number FROM `wenlin_jyut_ping` WHERE (pinyin_collation LIKE 'aa.%') ORDER BY pinyin_collation
			SELECT serial_number FROM `wenlin_en_collation` WHERE (english_collation LIKE '...') ORDER BY english_collation
		*/
		return $this->runQuery($query, $dbr);
	} // reallyLookupAlphabeticalString

	/** listWordsAlphabetically:
		List words alphabetically.

		@param $queryOptions the array of options (see runQuery), or null.
	*/
	private function listWordsAlphabetically($queryOptions = array())
	{
		$alpha = $this->webRequest->getText('a'); // where to start in the alphabet
		$namespaceStr = $this->webRequest->getText('ns');
		$this->listNextPrev = $this->webRequest->getText('list'); // 'next', 'prev', or null
		$this->reallyListWordsAlphabetically($alpha, $namespaceStr, $queryOptions);
	} // listWordsAlphabetically

	/** reallyListWordsAlphabetically:
		List words alphabetically.

		@param $alpha where to start in the alphabet.
		@param $namespaceStr the namespace.
		@param $queryOptions the array of options (see runQuery), or null.
	*/
	private function reallyListWordsAlphabetically($alpha, $namespaceStr, $queryOptions)
	{
		/* Simplify the key if we're not starting from a list, to be more inclusive
			regardless of capitalization, homophone number, etc. Don't simplify the key
			for Previous/Next buttons, since simplification would lead to repetition. */
		$simplify = ($this->listNextPrev == null);
		switch ($namespaceStr) {
		case 'Ci':
			$this->setTitle("Mandarin words by Pinyin");
			$query['table'] = array('wenlin_ci_pinyin');
			$key = WenlinCollate::makePinyinCollationKey($alpha, $simplify);
			break;
		case 'En':
			$this->setTitle("English words alphabetically");
			$query['table'] = array('wenlin_en_collation');
			$key = WenlinCollate::makeEnglishCollationKey($alpha, $simplify);
			$key = WenlinIndex::escapeString($key);
			break;
		case 'Jyut':
			$this->setTitle("Cantonese words by Jyutping");
			$query['table'] = array('wenlin_jyut_ping');
			$key = WenlinCollate::makeJyutpingCollationKey($alpha, $simplify);
			break;
		default:
			$this->putLine("Unrecognized namespace $namespaceStr");
			return;
		}
		$ascendingDescending = '';
		switch ($this->listNextPrev) {
		case 'prev':
			$comparison = '<';
			$ascendingDescending = ' DESC';
			break;
		case 'next':
			$comparison = '>';
			break;
		default: // null
			$comparison = '>=';
		}
		/* We could have avoided this awkward $collation code by using the same name
			like 'collation' instead of different names 'pinyin_collation' and 'english_collation'.
			Compare similar code in reallyLookupAlphabeticalString. */
		$collation = ($namespaceStr == 'En') ? 'english_collation' : 'pinyin_collation';
		$query['vars'] = array('serial_number');
		$query['conds'] = array("STRCMP($collation, '$key') $comparison 0");
		$query['options'] = array('ORDER BY' => "$collation$ascendingDescending", 'LIMIT' => '100');
		$query['join_conds'] = array();
		/* SELECT serial_number FROM `wenlin_ci_pinyin` WHERE (STRCMP(pinyin_collation, 'diqiu...42.') >= 0) ORDER BY pinyin_collation LIMIT 100
			SELECT serial_number FROM `wenlin_en_collation` WHERE (STRCMP(english_collation, '...') > 0) ORDER BY english_collation LIMIT 100 */

		$queryOptions['mustList'] = true;
		$queryOptions['nextPrev'] = true;
		if ($alpha === 'a') {
			$queryOptions['noPrev'] = true;
		}
		$dbr = wfGetDB(DB_REPLICA);
		$res = $this->runQuery($query, $dbr);
		$count = ($res == null) ? 0 : $res->numRows();
		/* Problem: a link like "http://localhost/wow/Special:Wenlin/a?a=a*&ns=Ci&list=prev" returns zero results,
			since we're at the start of the alphabet already. In that case, show from the start of the
			alphabet, and omit the "prev" button in the output. */
		if ($count == 0) {
			if ($this->listNextPrev == 'prev') {
				$queryOptions['noPrev'] = true;
				$this->listNextPrev = null;
				$this->reallyListWordsAlphabetically('a', $namespaceStr, $queryOptions); // recursive, only once
				return;
			}
			if ($this->listNextPrev == 'next') {
				$queryOptions['noNext'] = true;
				$this->listNextPrev = 'prev';
				$this->reallyListWordsAlphabetically('zzz', $namespaceStr, $queryOptions); // recursive, only once
				return;
			}
		}
		$dictPref = WenlinDictionaryPreferences::singleton();
		$this->loadEntriesFromQueryResult($res, $namespaceStr, $queryOptions, $dbr, $dictPref);
	} // reallyListWordsAlphabetically

	/** listWordsContaining:
		List polysyllabic words containing a given Hanzi.
	*/
	private function listWordsContaining()
	{
		$zi = $this->webRequest->getText('z');
		$this->setTitle("Words containing $zi");
		$hanziUsv = WenlinUtf::ziNumber($zi);
		if (!WenlinFenlei::zinIsCJK($hanziUsv)) {
			$this->putParagraph("Argument [$zi] not recognized as Hanzi.");
			return;
		}
		$dictPref = WenlinDictionaryPreferences::singleton();
		/* Depending on user rights and preferences, use an array of namespaces
			like ['Jyut', 'Ci'], ['Ci', 'Jyut'], ['Jyut'], or ['Ci']. */
		$namespaceArray = $dictPref->getPreferredChineseNamespaces();
		$dbr = wfGetDB(DB_REPLICA);
		$totalCount = 0;
		foreach ($namespaceArray as $namespaceStr) {
			$res[$namespaceStr] = $this->reallyListWordsContaining($zi, $hanziUsv, $namespaceStr, $dbr);
			$count[$namespaceStr] = ($res == null) ? 0 : $res[$namespaceStr]->numRows();
			$totalCount += $count[$namespaceStr];
		}
		if ($totalCount == 0) {
			$this->summarizeList(0, null);
			return;
		}
		$queryOptions['mustList'] = true;
		foreach ($namespaceArray as $namespaceStr) {
			if ($count[$namespaceStr] > 0) {
				/* TODO: put number of entries BEFORE the list instead of after it; currently loadEntriesFromQueryResult calls summarizeList at the end. */
				$this->loadEntriesFromQueryResult($res[$namespaceStr], $namespaceStr, $queryOptions, $dbr, $dictPref);
			}
		}
	} // listWordsContaining

	/** reallyListWordsContaining:
		List words containing a given Hanzi.

		@param $zi the Hanzi.
		@param $hanziUsv the UCS scalar value.
		@param $namespaceStr the namespace ('Ci', 'Jyut', ...).
		@param $dbr the database for reading.
		@return the ResultWrapper.
	*/
	private function reallyListWordsContaining($zi, $hanziUsv, $namespaceStr, $dbr)
	{
		switch ($namespaceStr) {
		case 'Jyut':
			$query['table'] = array('h' => 'wenlin_jyut_hz_contained', 'f' => 'wenlin_jyut_ping');
			break;
		case 'Ci':
		default:
			$query['table'] = array('h' => 'wenlin_ci_hanzi_contained', 'f' => 'wenlin_ci_pinyin');
			break;
		}
		$query['vars'] = array('serial_number');
		$query['conds'] = array("h.hanzi = '$hanziUsv'");
		/* DISTINCT fixes the problem of getting entries like "hǎohāo(r)* 好好" twice,
			as for <http://localhost/wow/Special:Wenlin/zc?z=好>.
			This is different in our SQL implementation from our CBT implementation. For CBT,
			we don't have anything like syllable_number and only have one index item for 好 in 好好. */
		$query['options'] = array(0 => 'DISTINCT', 'ORDER BY' => 'f.frequency DESC, f.pinyin_collation');
		$query['join_conds'] = array('f' => array('JOIN', 'h.page_id = f.page_id'));
		/* SELECT DISTINCT serial_number FROM `wenlin_ci_hanzi_contained` `h` JOIN `wenlin_ci_pinyin` `f` ON ((h.page_id = f.page_id)) WHERE (h.hanzi = '22909') ORDER BY f.frequency DESC, f.pinyin_collation */
		return $this->runQuery($query, $dbr);
	} // reallyListWordsContaining

	/** listCharactersContaining:
		List Hanzi containing the given component.
	*/
	private function listCharactersContaining()
	{
		$bujian = $this->webRequest->getText('b'); // 部件
		if ($bujian === null || $bujian === '' || $bujian === 0) {
			$this->putParagraph("Argument missing for List Characters Containing.");
			return;
		}
		$this->setTitle("Characters containing $bujian");
		$compUsv = WenlinUtf::ziNumber($bujian);
		if (!WenlinFenlei::zinIsCJK($compUsv)) {
			$this->putParagraph("Argument [$bujian] not recognized as Hanzi.");
			return;
		}
		$query['table'] = array('b' => 'wenlin_zi_bujian', 'f' => 'wenlin_zi_frequency');
		$query['vars'] = array('b.hanzi', 'f.frequency');
		$query['conds'] = array("b.bujian = $compUsv");
		$query['options'] = array('ORDER BY' => 'CASE WHEN f.frequency IS NULL THEN 1 ELSE 0 END, f.frequency, f.hanzi');
		$query['join_conds'] = array('f' => array('LEFT JOIN', 'b.hanzi = f.hanzi'));
		/* SELECT b.hanzi,f.frequency FROM `wenlin_zi_bujian` `b` LEFT JOIN `wenlin_zi_frequency` `f` ON ((b.hanzi = f.hanzi)) WHERE (b.bujian = 25991) ORDER BY CASE WHEN f.frequency IS NULL THEN 1 ELSE 0 END, f.frequency, f.hanzi */
		$dbr = wfGetDB(DB_REPLICA);
		$res = $this->runQuery($query, $dbr);
		$queryOptions['mustList'] = true;
		$dictPref = WenlinDictionaryPreferences::singleton();
		/* TODO: put number of entries BEFORE the list instead of after it; currently loadEntriesFromQueryResult calls summarizeList at the end. */
		$this->loadEntriesFromQueryResult($res, 'Zi', $queryOptions, $dbr, $dictPref);
	} // listCharactersContaining

	/** listCharactersByFrequency:
		List Hanzi ordered by frequency rank.
	*/
	private function listCharactersByFrequency()
	{
		$this->listNextPrev = $this->webRequest->getText('list'); // 'next', 'prev', or null
		$rank = $this->webRequest->getText('rank');
		if ($rank === null || $rank <= 0) {
			$rank = 1;
		}
		if ($this->listNextPrev === 'prev') {
			$rank -= 100;
		}
		if ($rank > 3000) {
			$rank = 3000;
		}
		$endRank = $rank + 100;
		$this->setTitle("Characters by Frequency");

		$query['table'] = array('f' => 'wenlin_zi_frequency');
		$query['vars'] = array('f.frequency', 'f.hanzi');
		$query['conds'] = array("f.frequency >= $rank AND f.frequency < $endRank");
		$query['options'] = array('ORDER BY' => 'f.frequency');
		$query['join_conds'] = array();
		/* SELECT f.frequency,f.hanzi FROM `wenlin_zi_frequency` `f` WHERE (f.frequency >= 123 AND f.frequency < 223) ORDER BY f.frequency */
		$dbr = wfGetDB(DB_REPLICA);
		$res = $this->runQuery($query, $dbr);
		$queryOptions['mustList'] = true;
		$queryOptions['nextPrev'] = true;
		if ($rank == 1) {
			$queryOptions['noPrev'] = true;
		}
		$dictPref = WenlinDictionaryPreferences::singleton();
		$this->loadEntriesFromQueryResult($res, 'Zi', $queryOptions, $dbr, $dictPref);
	} // listCharactersByFrequency

	/** listCharactersByPinyin:
		List Hanzi with the given pronunciation.
	*/
	private function listCharactersByPinyin()
	{
		$py = $this->webRequest->getText('py');
		if ($py === null || $py === '' || $py === 0) {
			$this->putParagraph("Argument missing for List Characters By Pinyin.");
			return;
		}

		$this->setTitle("Characters pronounced $py");
		list($pinyinToneless, $tone) = WenlinPinyin::validateAndSplit($py);
		$query['table'] = array('p' => 'wenlin_zi_pinyin', 'f' => 'wenlin_zi_frequency');
		$query['vars'] = array('p.hanzi', 'f.frequency');
		$query['conds'] = array("p.pinyin = '$pinyinToneless'");
		/* TODO: distinguish neutral from "any" tone. For now, if tone is zero, match all tones ("any"). */
		if ($tone != 0) {
			$query['conds'][] = "p.tone = '$tone'";
		}
		$query['options'] = array('ORDER BY' => 'CASE WHEN f.frequency IS NULL THEN 1 ELSE 0 END, f.frequency, f.hanzi');
		$query['join_conds'] = array('f' => array('LEFT JOIN', 'p.hanzi = f.hanzi'));
		/* p.hanzi,f.frequency FROM `wenlin_zi_pinyin` `p` LEFT JOIN `wenlin_zi_frequency` `f` ON ((p.hanzi = f.hanzi)) WHERE (p.pinyin = 'he') AND (p.tone = '2') ORDER BY CASE WHEN f.frequency IS NULL THEN 1 ELSE 0 END, f.frequency, f.hanzi */
		$dbr = wfGetDB(DB_REPLICA);
		$res = $this->runQuery($query, $dbr);
		$queryOptions['mustList'] = true;
		$dictPref = WenlinDictionaryPreferences::singleton();
		$this->loadEntriesFromQueryResult($res, 'Zi', $queryOptions, $dbr, $dictPref);
	} // listCharactersByPinyin

	/** describePinyinSyllable:
		Describe a pinyin syllable.
		Sample URL: http://localhost/wow/Special:Wenlin/py?py=wa

		Present info like this:

			dàn (Mandarin syllable in Pīnyīn Romanization)
			Hear dàn pronounced by woman   man
			List words / characters pronounced “dàn” (4th tone)
			List words / characters pronounced “dan” (any tone)
			Tones:  dān  dán  dǎn  [dàn]
			Bopomofo: ㄉㄢˋ
			<hr/>
			The sounds were spoken by June Chen (陈佩军) and Prof. Samuel Cheung (張洪年).
			Recorded and edited by Peter Tennenbaum and Tom Bishop.
			Copyright (c) 1996 Wenlin Institute, Inc. SPC, All rights reserved.

		See https://gitlab.com/Wenlin/wanwu/issues/8
		Add monosyllabic and polysyllabic sound recordings to the wiki

		Compare the C function DescribePinyinSyllable in ch_py.c.
	*/	
	private function describePinyinSyllable()
	{
		$py = $this->webRequest->getText('py');
		if ($py === null || $py === '') {
			$py = 'hǎo';
		}
		$this->setTitle("$py (Pīnyīn)");
		$base = WenlinGongju::articlePathBase();
		$this->putLine("$py (Mandarin syllable in Pīnyīn Romanization)");
		list($pinyinToneless, $tone) = WenlinPinyin::validateAndSplit($py);
		if (!$pinyinToneless) {
			$this->putLine("Not recognized as pinyin: $py");
			return;
		}
		$this->putLine(WenlinGongju::makeAudioLink($pinyinToneless . $tone, 'mono'));
		$base = WenlinGongju::articlePathBase();
		/* Note: the desktop Wenlin lists only MATCHING words for the "words" links. It uses the 'tyci' (同音词) command
			for that, which we haven't implemented yet in PHP. TODO: implement tyci. For now, use 'a' instead of 'tyci' here. */
		$u = urlencode($py);
		$linkWordsWithTone = "<a href='$base/Special:Wenlin/a?a=$u&ns=Ci'>words</a>";
		$linkCharsWithTone = "<a href='$base/Special:Wenlin/pyzi?py=$u'>characters</a>";
		$u = urlencode($pinyinToneless);
		$linkWordsWithoutTone = "<a href='$base/Special:Wenlin/a?a=$u&ns=Ci'>words</a>";
		$linkCharsWithoutTone = "<a href='$base/Special:Wenlin/pyzi?py=$u'>characters</a>";
		$this->putLine("List $linkWordsWithTone / $linkCharsWithTone pronounced “${py}” (tone $tone)");
		$this->putLine("List $linkWordsWithoutTone / $linkCharsWithoutTone pronounced “${pinyinToneless}” (any tone)");
		$allTones = 'Tones:';
		for ($i = 1; $i <= 4; $i++) {
			if ($i == $tone) {
				$allTones .= " [$py]"; // no link since we're already on this page
			}
			else {
				$pyOtherTone = WenlinPinyin::addToneToSyllable($pinyinToneless, $i);
				$u = urlencode($pyOtherTone);
				$allTones .= " <a href='$base/Special:Wenlin/py?py=$u'>$pyOtherTone</a>";
			}
		}
		$this->putLine($allTones);
		//// $this->putLine("Bopomofo: [TODO: bopomofo] ㄉㄢˋ");
	} // describePinyinSyllable

	/**************************************/

	/** runQuery:
		Run the given SQL query.

		@param $query an associative array of arguments for Database::select, indexed
				by 'table', 'vars', 'conds', 'options', and 'join_conds'.
		@param $dbr the database for reading.
		@return the ResultWrapper, see http://php.net/manual/en/class.iterator.php;
				or null meaning failure or no results.

		Notes about $conds and $join_conds:
		"$join_conds	Optional associative array of table-specific join conditions.
		In the most common case, this is unnecessary, since the join condition can be in $conds.
		However, it is useful for doing a LEFT JOIN."
			https://doc.wikimedia.org/mediawiki-core/master/php/classDatabase.html
		-- HOWEVER, "The array construction for $conds can only do equality relationships (i.e. WHERE key = 'value')"
			https://www.mediawiki.org/wiki/Manual:Database_access
		-- SO we must avoid the array construction for $conds when using other relationships, such as
			. " WHERE pinyin_collation LIKE '$like'"
			...
			. " WHERE STRCMP(pinyin_collation, '$key') $comparison 0"
	*/
	private function runQuery($query, $dbr)
	{
		/* We always use $dbr->select, never $dbr->query, to minimize risk of SQL injection.
			Therefore $query must be an array. */
		if (!is_array($query)) {
			die("runQuery requires query array");
		}
		// NOTE: can call $sql = $dbr->selectSQLText to see the produced query string for debugging
		if (false) {
			$sql = $dbr->selectSQLText($query['table'], $query['vars'], $query['conds'], __METHOD__,
					$query['options'], $query['join_conds']);
			// if (strstr($sql, "english_collation")) {
			die("runQuery: $sql");
			// }
		}
		/* $dbr->select returns $res = ResultWrapper, or else boolean true/false.
			https://doc.wikimedia.org/mediawiki-core/1.25.6/php/classResultWrapper.html
			We return null instead of true/false. */
		$res = $dbr->select($query['table'], $query['vars'], $query['conds'], __METHOD__,
				$query['options'], $query['join_conds']);
		if ($res === true || $res === false) {
			return null;
		}
		return $res;
	} // runQuery

	/** loadEntriesFromQueryResult:
		Output the dictionary entries indicated by the query results.
		Under some circumstances (not if $queryOptions['mustList']) when there is only one query
		result, redirect to the entry page rather than treat the result as a list (special page).

		@param $res the ResultWrapper. It implements Iterator (a PHP predefined interface),
			see http://php.net/manual/en/class.iterator.php 
		@param $namespaceStr the namespace for the dictionary containing the entries.
		@param $queryOptions the array of options:
			$queryOptions['mustList'] == true if we should show as a list even for single
				result (as for bz, zc); default empty string (except for bz, zc, the user
				may only expect a list when there are homophones/homographs).
				Example: bz 壤 list has only one item: 𤅑.
			$queryOptions['nextPrev'] == true if we should show Next/Previous buttons.
			$queryOptions['noPrev'] == true if we should inhibit showing Previous button, else it's not set.
			$queryOptions['noNext'] == true if we should inhibit showing Next button, else it's not set.
			$queryOptions['bandContext'] == WenlinBandContext object, or null.
		@param $dbr the database for reading.
		@param $dictPref the WenlinDictionaryPreferences.
		@return the number of matching entries appended, or -1 if we redirect to a single entry.

		Use "single line" display for each entry if there are more than one item in list, or if there
		are zero items (since then we want summarizeList), or if $queryOptions['mustList'].
		For some kinds of look-up, $count == 1 means we should show the single entry NOT as a list.
		When looking up what looks like a single vocabulary item, the user in't expecting a list unless
		it's a disambuation-type list for homophones/homographs. But for other kinds of look-up (like bz and zc),
		we should show it as a list of "One item", using 'mustList'.
		In the non-list situation, use redirect to the entry's article, in order to show the Edit tab, etc.
	*/
	private function loadEntriesFromQueryResult($res, $namespaceStr, $queryOptions, $dbr, $dictPref)
	{
		if ($res == null) {
			return 0;
		}
		$bandContext = isset($queryOptions['bandContext']) ? $queryOptions['bandContext'] : new WenlinBandContext();
		$count = $res->numRows();
		$singleLine = ($count != 1)
				|| (isset($queryOptions['mustList']) && $queryOptions['mustList'] === true);
		if ($singleLine) {
			if ($namespaceStr == 'Zi') {
				$this->put("<table>\n");
			}
			else {
				$this->put("<p>\n");
			}
		}
		$reverse = $singleLine && ($this->listNextPrev === 'prev') && ($namespaceStr !== 'Zi');
		$reverseText = '';
		$entryInfo = new WenlinEntryInfo();
		$ziFreq = $firstZiFreq = $lastZiFreq = null;
		$firstHeadword = $lastHeadword = null;
		$entryCountSoFar = 0;
		$excessiveTimeElapsed = false;
		foreach ($res as $row) {
			if ($entryCountSoFar++ % 100 === 99 && microtime(true) > 10 + $_SERVER["REQUEST_TIME_FLOAT"]) {
				$excessiveTimeElapsed = true;
				break;
			}
			$titleStr = ($namespaceStr == 'Zi') ? WenlinUtf::zinToStr($row->hanzi) : $row->serial_number;
			if ($namespaceStr == 'Zi') {
				$ziFreq = isset($row->frequency) ? $row->frequency : 0xffff; // ZIFREQ_NON_URO
				if ($firstZiFreq === null) {
					$firstZiFreq = $ziFreq;
				}
				$lastZiFreq = $ziFreq;
			}
			if (!$singleLine) {
				$this->redirect("$namespaceStr:$titleStr"); // TODO: use $ziFreq here; save in a hash somewhere? And how about $dictPref?
				/* Signal by returning -1 to the caller that we have redirected, might save some pointless processing?
					Currently all callers ignore our return value. */
				return -1;
			}
			/* makeEntrySingleLine fills in $entryInfo->headword, among other things. */
			$entry = $this->makeEntrySingleLine($namespaceStr, $titleStr, $dictPref, $bandContext, $ziFreq, $entryInfo);
			if ($entry === '') {
				; // ignore empty entry, might have been filtered depending on $dictPref.
			}
			elseif ($reverse) {
				$reverseText = $entry . "<br />\n" . $reverseText;
			}
			elseif ($namespaceStr == 'Zi') {
				$this->put("$entry\n");
			}
			else {
				$this->putLine($entry);
			}
			$lastHeadword = $entryInfo->headword;
			if ($firstHeadword == null) {
				$firstHeadword = $lastHeadword;
			}
		}
		if ($reverse) {
			$this->put($reverseText);
		}
		if ($namespaceStr == 'Zi') {
			$this->put("</table>\n");
		}
		else {
			$this->put("</p>\n");
		}
		if ($excessiveTimeElapsed) {
			$this->putParagraph('This list has been cut short due to excessive time elapsed.'
					. " $entryCountSoFar out of $count entries were actually appended.");
			// TODO: add a link for continuing where cut short. Maybe "&skip=1234" added to URL?
		}
		if (isset($queryOptions['nextPrev']) && $queryOptions['nextPrev'] === true && $firstHeadword !== null) {
			$prevNextLinks = $this->makePrevNextLinks($queryOptions, $namespaceStr, $firstHeadword, $lastHeadword, $reverse, $ziFreq, $firstZiFreq, $lastZiFreq);
			$this->putHorizontalRule();
			// TODO: put the links at the top of the list as well as the bottom; might need to save list as string rather than outputting as we go along.
			$this->putLine($prevNextLinks);
		}
		elseif (!$bandContext->ssw) {
			$this->summarizeList($count, $namespaceStr);
		}
		return $count;
	} // loadEntriesFromQueryResult

	/** makePrevNextLinks:
		Make "previous" and "next" links for a list.

		@param $queryOptions the array of options; see loadEntriesFromQueryResult.
		@param $namespaceStr the namespace for the dictionary containing the entries.
		@param $firstHeadword the first headword in the list.
		@param $lastHeadword the last headword in the list.
		@param $reverse true if we're doing "previous" and need to reverse the order of entries, else false.
		@param $ziFreq the frequency rank, for Zidian entries only;
					null means "not applicable" (for Ci/En/Jyut entries).
		@param $firstZiFreq the first frequency rank, for Zidian entries only; or null.
		@param $lastZiFreq the last frequency rank, for Zidian entries only; or null.
		@return the links as HTML.
	*/
	private function makePrevNextLinks($queryOptions, $namespaceStr, $firstHeadword, $lastHeadword, $reverse, $ziFreq, $firstZiFreq, $lastZiFreq)
	{
		if ($reverse) { // swap
			$tmp = $lastHeadword;
			$lastHeadword = $firstHeadword;
			$firstHeadword = $tmp;
		}
		$base = WenlinGongju::articlePathBase();
		$prev = '';
		if (!isset($queryOptions['noPrev'])) {
			if ($ziFreq !== null) {
				$url = "$base/Special:Wenlin/zifreq?rank=$firstZiFreq&list=prev";
			}
			else {
				$url = "$base/Special:Wenlin/a?a=$firstHeadword&ns=$namespaceStr&list=prev";
			}
			$prev = "<a href='$url'>►Previous 100</a>";
		}
		if (isset($queryOptions['noNext'])) {
			return $prev;
		}
		if ($ziFreq !== null) {
			++$lastZiFreq;
			$url = "$base/Special:Wenlin/zifreq?rank=$lastZiFreq&list=next";
		}
		else {
			$u = urlencode($lastHeadword);
			$url = "$base/Special:Wenlin/a?a=$u&ns=$namespaceStr&list=next";
		}
		$next = "<a href='$url'>►Next 100</a>";
		return ($prev === '') ? $next : "$prev &nbsp;&nbsp; $next";
	} // makePrevNextLinks

	/** makeEntrySingleLine:
		Get the specified dictionary entry as one line of text.

		@param $namespaceStr the namespace for the entry, like "Ci", "En", "Zi", or "Jyut".
		@param $titleStr the title string for the entry (without namespace), like "1234567890" or "字".
		@param WenlinDictionaryPreferences $dictPref.
		@param WenlinBandContext $bandContext.
		@param $ziFreq the frequency rank, for Zidian entries only;
					null means "not applicable" (for Ci/En/Jyut entries).
		@param WenlinEntryInfo $entryInfo the entry information to be filled in (like the headword).
		@return the dictionary entry as one line of text, or an empty string if filtered out.
	*/
	private function makeEntrySingleLine($namespaceStr, $titleStr, $dictPref, $bandContext, $ziFreq, $entryInfo)
	{
		$viewable = WenlinXuke::entryIsViewableByThisUser($this->user, $namespaceStr, $titleStr, $msg);

		$nsTitleStr = "$namespaceStr:$titleStr";
		$titleObj = Title::newFromText($nsTitleStr);
		if (!$titleObj) {
			return "(no title for $nsTitleStr)"; // TODO: send message to an optional log, don't return it.
		}
		$text = WenlinGongju::getContentTextFromTitleObject($titleObj);
		if (!$text) {
			return "(no page text for $nsTitleStr)";
		}
		if ($namespaceStr == 'Zi') {
			$entryInfo->headword = $titleStr;
		}
		if ($namespaceStr != 'Zi' && $dictPref->getHideSubentries()) {
			// If the entry contains a 'subof' band, it's a subentry; skip it.
			// Compare EntryIsSubentry in ch_eng.c, which uses FindEntryBand.
			$subof = WenlinBand::getNeededBands($text, array('subof'));
			if (isset($subof['subof'])) {
				return '';
			}
		}
		/* Even if user lacks permission to view the entry, we still call renderEntrySingleLine to get
			the headword, except for Zi, for which we already have the headword in the title. */
		if ($viewable || $namespaceStr != 'Zi') {
			// renderEntrySingleLine gets $entryInfo->headword
			$text = WenlinRender::renderEntrySingleLine($text, $titleObj, $this->wlParser, $dictPref, $bandContext, $entryInfo);
		}
		if (!$viewable) {
			$text = $entryInfo->headword . ' ' . $this->wlParser->parseWikiToHtml($msg);
		}
		if ($namespaceStr == 'Zi') {
			return "<tr><td align='center'>" . self::ziFreqStr($ziFreq) . "</td><td>$text</td></tr>";
		}
		$button = $this->wlParser->parseWikiToHtml("[[$nsTitleStr|►]]");
		return $button . $text;
	} // makeEntrySingleLine

	/** ziFreqStr:
		Convert the given Hanzi frequency rank number to a string.

		@param $ziFreq the frequency as an integer.
		@return the string equivalent.
	*/
	private static function ziFreqStr($ziFreq)
	{
		if ($ziFreq >= 0xff00) { // ZIFREQ_OBSCURE, ZIFREQ_NON_URO
			// return 'xxxx';
			return '...';
		}
		if ($ziFreq >= 0xfb00) { // ZIFREQ_ERJI
			// return '====';
			return '..';
		}
		if ($ziFreq >= 0xf000) { // ZIFREQ_YIJI = 0xfa00; ZIFREQ_IS_PSEUDO >= 0xf000
			// return '----';
			return '.';
		}
		return sprintf("%d", $ziFreq);
	} // ziFreqStr

	/** summarizeList:
		Summarize the list of vocabulary items.

		@param $count the number of items in the list.
		@param $namespaceStr the namespace string, mainly for debugging if count is zero.

		TODO: change this summary to appear at beginning, not end, of all lists that use it.
	*/
	private function summarizeList($count, $namespaceStr)
	{
		$whichNamespace = ($namespaceStr == null) ? 'all namespaces' : WenlinGongju::makeNamespaceLink($namespaceStr);
		if ($count == 0) {
			$this->putParagraph("Zero entries in $whichNamespace."); // namespace helps for debugging
		}
		else {
			$this->putHorizontalRule();
			if ($count == 1) {
				$this->putParagraph("End of list. One item in $whichNamespace.");
			}
			else {
				$this->putParagraph("End of list. Total $count items in $whichNamespace.");
			}
		}
	} // summarizeList

	/** putHorizontalRule:
		Output a horizontal rule.
	*/
	private function putHorizontalRule()
	{
		$this->put("<hr />\n");
	} // putHorizontalRule

	/** putParagraph:
		Output a paragraph.

		@param $text the string.
	*/
	private function putParagraph($text)
	{
		$this->put("<p>$text</p>\n");
	} // putParagraph

	/** putStyledParagraph:
		Output a paragraph in a specified style.

		@param $class the class defining the style.
		@param $text the string.
	*/
	private function putStyledParagraph($class, $text)
	{
		$this->put("<p class='$class'>$text</p>\n");
	} // putStyledParagraph

	/** putLine:
		Output one line of text, with a line break at the end.

		@param $text the string.
	*/
	private function putLine($text)
	{
		$this->put("$text<br />\n");
	} // putLine

	/** put:
		Output the given string.

		@param $text the string.
	*/
	private function put($text)
	{
		if ($this->outputPage !== null) {
			$this->outputPage->addHTML($text);
		}
		else {
			$this->outputString .= $text;
		}
	} // put

	/** setTitle:
		Set the page title to be displayed in the header.

		@param $titleStr the title string.
	*/
	private function setTitle($titleStr)
	{
		if ($this->outputPage !== null) {
			$this->outputPage->setPageTitle($titleStr);
		}
	} // setTitle

	/** redirect:
		Redirect to the article with the given title string.
		The URL bar shows we're on an article page (with the given title string) rather
		than a special page, and there is an Edit tab, etc.
		Discovered this trick by studying SpecialSearch.php.

		@param nsTitleString the namespace and title string, like "Zi:字", "Ci:1234567890", etc.
	*/
	private function redirect($nsTitleString)
	{
		if ($this->outputPage !== null) {
			$titleObj = Title::newFromText($nsTitleString);
			$this->outputPage->redirect($titleObj->getFullURL());
		}
	} // redirect

	/** test:

		https://meta.wikimedia.org/wiki/Search_box
		https://www.mediawiki.org/wiki/Extension:InputBox
		https://www.mediawiki.org/wiki/Extension:MixedNamespaceSearchSuggestions
		https://freephile.org/wiki/Elasticsearch

		search for "SearchInput" , "ShiliangUseSimpleSearch", "makeSearchButton" in ShiliangTemplate.php

		"ShiliangUseSimpleSearch": true -- in skin.json; what if we make if false? Seems to make change fulltext to go...
		Also makes the "Go" button visible in header!!

		id="searchGoButton" is only used by old MonoBook skin?
		fulltext seems to be the key difference? No, change to fulltextoo seems to make no difference
		-- or maybe name="Go"? No, change to Goo seems to make no difference
		Maybe value "Search" versus value "Look up"? Yes! Changing to value="Searchfoo" causes
		both buttons to do lookup.

		Also notice difference between showResults and goResult in SpecialSearch.php.

		It seems our SearchGetNearMatchBefore hook is called only for 'go' searches, not for 'fulltext' searches.
	*/
	private function test()
	{
		$this->setTitle("Look up or Search");
		$base = WenlinGongju::articlePathBase();
		$this->put('<form action="$base/Special:Wenlin">
				<p><input type="text" value="" id="SearchInput" name="search"/><br>
				<input type="submit" value="Look up" name="Goo" id="searchGoButton"/>
				<input type="submit" value="Search" name="fulltextoo"/></p>
				</form>');
	} // test

	/** createNewEntry:
		Create a new entry.

		TODO: go directly to "action=edit" page instead of showing this intermediate page.
		That is, we shouldn't have a 'create' command at all. The page that currently contains
		a 'create' link should instead contain the "action=edit" link directly. That will save the user
		one step. One disadvantage though is that we generate unique ser sooner, maybe no longer
		unique by the time it's used -- but that's unlikely. Also waste some time generating the
		unique ser when it's not needed, but that's a small amount of time.
			-- ALTERNATIVE: keep this function and make it display buttons for multiple namespaces.
		That way, the main entry that points here doesn't need a button for each namespace.

		TODO: this is never actually invoked for namespace Zi, but it should be, just so a template
		is used to start the first line with the character in question, followed by a space, and maybe
		a question mark as placeholder for the top line. (Space at the end of line will be automatically
		deleted by MediaWiki.)
	*/
	private function createNewEntry()
	{
		global $wgScriptPath;
		$term = $this->webRequest->getText('term');
		$namespaceStr = $this->webRequest->getText('ns');
		$this->setTitle("Create new entry for $term in namespace $namespaceStr");
		if ($term === null || $term === '' || $term === 0 || $namespaceStr === null || $namespaceStr === '' || $namespaceStr === 0) {
			$this->putParagraph("Argument missing for Create New Entry.");
			return;
		}
		if (!in_array($namespaceStr, ['En', 'Ci', 'Jyut', 'Zi'])) {
			$this->putParagraph("Unrecognized namespace $namespaceStr for creation of new entry.");
			return;
		}
		if ($namespaceStr !== 'Zi') {
			$ser = self::generateRandomUniqueSerialNumber($namespaceStr);
			if ($ser === null) {
				$this->putParagraph("Unable to generate unique serial number for namespace $namespaceStr.");
				return;
			}
		}
		/* Fill in template for entry, using $term for the headword; compare C functions ConstructCiEntry and ConstructYinghanEntry.

			https://gitlab.com/Wenlin/wanwu/issues/9
			https://www.mediawiki.org/wiki/Manual:Creating_pages_with_preloaded_text

			Assume we have a template for each namespace with a title like "Template:Ci" for namespace "Ci".

			"You can also specify parameters to the preload text with the preloadparams[] url parameter...
			Each parameter specified replaces a $1, $2, ... variable."
			-- This only works with $1, $2, ... in the template, not with {{{1}}}, {{{2}}}, ... Where are $1, $2, ... documented??

			"Note that parameters in the system messages (MediaWiki namespace) are written differently: as $1, $2, etc."
			-- https://meta.wikimedia.org/wiki/Help:Template

			"preloadparams[] (or preloadparams%5B%5D) Replace $1 style parameters in preload text."
			-- https://www.mediawiki.org/wiki/Manual:Parameters_to_index.php#Options_affecting_the_edit_form
		*/
		$url = "$wgScriptPath/index.php?title=$namespaceStr:$ser&action=edit&preload=Template:$namespaceStr";
		if ($namespaceStr === 'En' || $namespaceStr === 'Zi') {
			$url .= "&preloadparams[]=$term";
		}
		else { // $namespaceStr === 'Ci' || $namespaceStr === 'Jyut'
			$termIsChar = WenlinFenlei::zinIsCJK(WenlinUtf::ziNumber($term));
			$char = $termIsChar ? $term : '?';
			$py = $termIsChar ? '?' : $term; // TODO: use first Zidian reading for each Hanzi, instead of '?'
			$url .= "&preloadparams[]=$py"; // $1 in template
			$url .= "&preloadparams[]=$char"; // $2 in template
		}
		if ($namespaceStr !== 'Zi') {
			$url .= "&preloadparams[]=$ser"; // $2 (En) or $3 (Ci/Jyut) in template
		}
		$this->putParagraph("<a rel='nofollow' href='$url'>create this page</a>");
	} // createNewEntry

	/** generateRandomUniqueSerialNumberWithTree:
		Get a new unique serial number key, that is, one that doesn't pre-exist in the given namespace.

		The serial number will be 14 random hex digits plus 2 checksum hex digits.

		This (especially checksum) needs to be consistent with the C function GenerateRandomUniqueSerialNumber in wenlin/c/ch_lb.c.

		@param $namespaceStr the namespace ('Ci', 'Jyut', 'En').
		@return the serial number, or null for failure.
	*/
	public static function generateRandomUniqueSerialNumber($namespaceStr)
	{
		/* How many times should we repeat if random ser pre-exists? 万 wàn = 10,000? */
		for ($try = 0; $try < 10000; $try++) {
			$serBytes = array();
			for ($i = 0; $i < 7; $i++) {
				$serBytes[] = mt_rand() % 0xff;
			}
			$serBytes[7] = self::crc8($serBytes, 7);
			$ser = '';
			for ($i = 0; $i < 8; $i++) {
				$ser .= sprintf("%02X", $serBytes[$i]);
			}
			$titleObj = Title::newFromText("$namespaceStr:$ser");
			if ($titleObj === null || Revision::newFromTitle($titleObj) === null) {
				return $ser; // success
			}
		}
		return null; // failure
	} // generateRandomUniqueSerialNumber

	/** crc8:
		Calculate an 8-bit cyclic redundancy check (CRC).

		@param data the array of bytes from which to calculate the checksum.
		@param dataSize the length of data.
		@return the 8-bit checksum.

		This needs to be consistent with the C function CRC8 in wenlin/c/ch_lb.c.

		Based on code posted by Mark Adler to stackoverflow.com:
		http://stackoverflow.com/questions/15169387/definitive-crc-for-c
		8-bit CRC with polynomial x^8+x^6+x^3+x^2+1, 0x14D.
		Chosen based on Koopman, et al. (0xA6 in his notation = 0x14D >> 1):
		http://www.ece.cmu.edu/~koopman/roses/dsn04/koopman04_crc_poly_embedded.pdf
	*/
	private static function crc8($data, $dataSize)
	{
		static $table;
		static $madeTable = false;
		if (!$madeTable) {
			$table = array();
			$POLY = 0xB2; // 10110010
			for ($i = 0; $i < 256; $i++) {
				$crc = $i;
				for ($j = 0; $j < 8; $j++) {
					$crc = $crc & 1 ? ($crc >> 1) ^ $POLY : $crc >> 1;
				}
				$table[$i] = $crc;
			}
			$madeTable = true;
		}
		$crc = 0xff;
		for ($i = 0; $i < $dataSize; $i++) {
			$crc = $table[$crc ^ $data[$i]];
		}
		return $crc;
	} // crc8

} // class WenlinZhixing
