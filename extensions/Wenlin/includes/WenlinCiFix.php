<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* Work in progress, doesn't run yet... See wenlin/c/ch_cifix.c ... */

class WenlinCiFix
{
	const FANTI_BRA = '[';
	const PINYIN_BRA = '[';
	const PINYIN_KET = ']';
	const MAX_MATCHES = 5;
	const  memAllocErrMsg = "Memory allocation error!";
	const FJ_PREFIX_LEN  =2;

	const JIANT_ZERO = 0;
	const FANTI_ONE = 1;
	const SPY_TONE_MASK = 7;
	public $quietAboutCB_FIX_BAD_JF;

	public function FJPYMatch( $p, $spy, $altZinCountP, $altZinArray, $jorfArray)
	{
		//$p += mb_strlen($p);
		$p = trim($p);
		$jf[jianti] = true;
		$jf[fanti] = false;


	//GetZidianJiantiFanti($jf, p);

		$beforeReadings = (!$jf[jianti] || !$jf[fanti]);
		$altZinCount = 0;
		if($beforeReadings){
			if(! is_null($altZinArray)){

				$altZinArray[0] = iconv("UTF-8", "UTF-32", ($p + FJ_PREFIX_LEN));

			}

		$jorfArray[0] = $jf ? JIANT_ZERO : FANTI_ONE;
		$altZinCount =1;
		}
	$toneMask = 0;

	switch ($spy & SPY_TONE_MASK) {
		case 0:
			# code...
			$spy |= SPY_TONE_MASK;
			break;
		case SPY_TONE_MASK:
			$toneMask = SPY_TONE_MASK;
			break;
		
		}

	$textBeforeAlt = 0;
	$ nReadings = 0;

	while($altZinCount < MAX_MATCHES && ($p = strchr($p, PINYIN_BRA)) != NULL)
		{
			$spy2 = 
		}

	}

	public GetZidianJiantiFanti ($jf, $p)
	{
		$jf = array_fill(0,sizeof($jf) , 0);
		if($p[0] == '(' && $p[1] != 0){
			
			 if(WenlinFenlei::zinIsCJK(WenlinUtf::ziNumber($p)))

		} 
	}


	public MJFP1 {}


}

class SylInfo
{
	public $spy;
	public $altZinArray[MAX_MATCHES];
	public $altZinCount;
	public $status;


}
