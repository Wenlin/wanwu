<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/**
	Update wenlin_en_collation index table, on the assumption that ICU version has changed,
	and that otherwise wenlin_en_collation is already up to date.

	@file
	@ingroup Maintenance
 */

require_once __DIR__ . "/../../../maintenance/Maintenance.php";
require_once __DIR__ . "/WenlinIndex.php"; // WenlinIndex::updateIndexesOnSave
require_once __DIR__ . "/WenlinUtf.php"; // ...
require_once __DIR__ . "/WenlinCharBand.php"; // ...
require_once __DIR__ . "/WenlinCollate.php"; // ...
require_once __DIR__ . "/WenlinBand.php"; // ...
require_once __DIR__ . "/WenlinParser.php"; // ...
require_once __DIR__ . "/WenlinPinyin.php"; // ...
require_once __DIR__ . "/WenlinRender.php"; // ...
require_once __DIR__ . "/WenlinFenlei.php"; // ...

/**
 * Maintenance script that updates wenlin_en_collation for changed ICU version.
 *
 * @ingroup Maintenance
 */
class WenlinReindexEnglish extends Maintenance {
	const EN_COLLATION_CHUNK_SIZE = 500;

	public function __construct() {
		parent::__construct();
		$this->mDescription = "Update wenlin_en_collation for changed ICU version";
	}

	public function getDbType() {
		return Maintenance::DB_ADMIN;
	}

	public function execute() {
		$dbw = wfGetDB( DB_MASTER );
		$res = $dbw->select( 'wenlin_en_collation', 'MAX(page_id) AS count' );
		$s = $dbw->fetchObject( $res );
		$count = $s->count;
		$this->output( "Updating wenlin_en_collation english_collation up to page_id $count...\n" );
		$n = 0;

		while ( $n < $count ) {
			if ( $n ) {
				$this->output( $n . "\n" );
			}
			$end = $n + self::EN_COLLATION_CHUNK_SIZE - 1;

			$query = "SELECT serial_number, page_id" 
				. " FROM wenlin_en_collation"
				. " WHERE page_id BETWEEN $n AND $end";
			/* TODO: we might avoid query() here, use a function less vulnerable to SQL injection;
				however, WenlinReindexEnglish.php is only meant to be called from command line
				as special one-time or rare operation, so SQL injection is unlikely. Still it might be wise
				to have a policy of avoiding query() anyway so code isn't re-used in more vulnerable
				context. */
			$res = $dbw->query($query, __METHOD__);
			foreach ($res as $row) {
				$ser = $row->serial_number;
				$titleStr = "En:$ser";
				$article = WikiPage::newFromID($row->page_id);
				if ( $article === null ) {
					$this->output("\n(No article for $titleStr)\n");
					continue;
				}
				$content = $article->getContent( Revision::RAW );
				if ( $content === null ) {
					$this->output("\n(No content for $titleStr)\n");
					continue;
				}
				$status = Status::newGood();
				WenlinIndex::updateIndexesOnSave($article, $content, $status);
				if (!$status->isGood()) {
					$msg = $status->getHTML();
					$this->output("Indexing error ($titleStr): $msg\n" );
				}
			}
			$n += self::EN_COLLATION_CHUNK_SIZE;
		}
		$this->output( "Done.\n" );
	}
}

$maintClass = "WenlinReindexEnglish";
require_once RUN_MAINTENANCE_IF_MAIN;
