<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** WenlinEntryInfo:
	Store information about a dictionary entry, such as its headword.
	This is meant to be used by callers of WenlinBand.translateToDisplayFormat to extract
	info from an entry, without necessarily displaying it.
*/
class WenlinEntryInfo
{
	public $headword;

	/** __construct:
		Construct a new WenlinEntryInfo.
	*/
	public function __construct()
	{
		$this->headword = '';
	} // __construct

} // WenlinEntryInfo
