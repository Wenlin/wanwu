<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

header('Content-Encoding: UTF-8');

require_once('./WenlinUtf.php'); // WenlinUtf::explode
require_once('./WenlinCharBand.php'); // WenlinCharBand::expandCharBand
require_once('./WenlinCollate.php'); // WenlinCollate::makePinyinCollationKey
require_once('./WenlinIndex.php'); // WenlinIndex::updateIndexesOnSave
require_once('./WenlinBand.php'); // WenlinBand::parseBand
require_once('./WenlinBandContext.php'); // WenlinBandContext::...
require_once('./WenlinBandConverter.php'); // WenlinBandConverter::...
require_once('./WenlinBandCruncher.php'); // WenlinBandCruncher::circledNumber
require_once('./WenlinGongju.php'); // WenlinGongju::...
require_once('./WenlinParser.php'); // WenlinParser::parseWikiToHtml
require_once('./WenlinPinyin.php'); // WenlinPinyin::validateAndSplit
require_once('./WenlinRender.php'); // WenlinRender::renderWL
require_once('./WenlinFenlei.php'); // WenlinFenlei::zinIsCJK
require_once('./WenlinDictionaryPreferences.php'); // WenlinDictionaryPreferences...

error_reporting(E_ALL | E_STRICT);

print (extension_loaded('intl')) ? "✅OK: intl extension is loaded\n" : "❌ERROR: intl extension is NOT loaded\n";
// phpinfo() would tell us the intl and ICU versions when using PHP5, but not when using HHVM.

$wlParser = new WenlinParser(null, null);

$a = [
	'@a@wūshī [巫师]' => 'wūshī [巫师]',
	'@@test' => '',
	'fish@@dog' => 'fish',
	'A@{B}C@{D}E' => 'ACE',
	'@{this}and@{that}' => 'and',
	'item-by-item@@; seriatim' => 'item-by-item',
	'闪烁@{/铄}[閃爍@{/鑠}]' => '闪烁[閃爍]',
	];
foreach ($a as $s => $shouldBe) {
	$u = WenlinBand::abridge($s);
	$ok = ($u == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: abridge($s) = $u\n";
}

$a = [
	'@a@wūshī [巫师]' => '',
	'@@test' => 'test',
	'fish@@dog' => 'fishdog',
	'A@{B}C@{D}E' => 'ABCDE',
	'@{this}and@{that}' => 'thisandthat',
	'item-by-item@@; seriatim' => 'item-by-item; seriatim',
	'闪烁@{/铄}[閃爍@{/鑠}]' => '闪烁/铄[閃爍/鑠]',
	];
foreach ($a as $s => $shouldBe) {
	$u = WenlinBand::unabridge($s);
	$ok = ($u == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: unabridge($s) = $u\n";
}

$s = "跑马溜溜的山上";
$a = WenlinUtf::explode($s);
if (count($a) == 7 && $a[0] == '跑' && $a[1] == '马' && $a[2] == '溜'
		&& $a[3] == '溜' && $a[4] == '的' && $a[5] == '山' && $a[6] == '上') {
	print "✅OK: WenlinUtf::explode $s\n";
}
else {
	print "❌ERROR: WenlinUtf::explode\n";
	printf("Each of the characters in $s should appear separately below surrounded by [brackets].\n");
	foreach ($a as $c) {
		print "[$c]";
	}
	print "\n";
}

$a = [0 => '(0)', 1 => '①', 20 => '⑳', 21 => '㉑', 35 => '㉟', 36 => '㊱', 50 => '㊿', 51 =>'(51)'];
foreach ($a as $n => $shouldBe) {
	$c = WenlinBandCruncher::circledNumber($n);
	$ok = ($c == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: $n → $c\n";
}

$a = ['A' => false, '一' => true, '〇' => true, '鿿' => true, '龥' => true,  '龦' => true, 'ꀀ' => false,
	'•' => false, '𡄔' => true, '㝀' => true, 'ឡ' => false, '쀠' => false, '𠀀' => true, '𬺡' => true, '𬺢' => false,
	'⺀' => true, '⿕' => true, '〡' => true, '〺' => true, '⺀' => true, '' => true, '㇀' => true, '' => false,
	'' => false, '' => false, '󰀀' => false, '􏿽' => true];
foreach ($a as $c => $shouldBe) {
	$z = WenlinUtf::ziNumber($c);
	$u = sprintf("U+%04X", $z);
	$is = WenlinFenlei::zinIsCJK($z);
	$tf = $is ? 'true' : 'false';
	$ok = ($is == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: $c $u isCJK $tf\n";
}

$a = [0x0304 => true, 0x4E00 => false];
foreach ($a as $c => $shouldBe) {
	$z = $c;
	$u = sprintf("U+%04X", $z);
	$is = WenlinFenlei::zinIsDiacritic($z);
	$tf = $is ? 'true' : 'false';
	$ok = ($is == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: $c $u isDiacritic $tf\n";
}

WenlinPinyin::isAllPinyin('qiguai');

$a = ['a' => true, 'ā' => true, 'ǎ' => true, 'à' => true, 'b' => false, 'ān' => true, 'ānx' => false, 'chuáng' => true,  'xǐhuan' => true, 'xǐfuan' => false,
	"kě'ài" => true, 'HǍO' => true, '?' => false, 'ឡ' => false, '쀠' => false, 'hēhēlālā' => true, 'chuángchuángchuángchuángchuáng' => true,
	'⺀' => false, '⿕' => false, 'hū-hū' => true, 'cchuang' => false, "ha===ha" => true, "ha''''ha" => true, "haha==" => true,
	"ha\"ha" => true, "‘ha-ha’" => false, "ha-ha---" => true, "ha?" => false, "ha-='\"‘’ha" => true, 'yī̠xiē' => true, 'cǐdì wú yín sānbạ̌i liǎng' => true,
	'women' => true, 'qiguai' => true];
foreach ($a as $c => $shouldBe) {
	$is = WenlinPinyin::isAllPinyin($c);
	$tf = $is ? 'true' : 'false';
	$ok = ($is == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: $c isAllPinyin $tf\n";
}

$s = "词/辞典学[詞/辭-學]";
$charBand = new WenlinCharBand();
$cbStatus = $charBand->expandCharBand($s, true);
if ($cbStatus == '好' && $charBand->sylCount == 3 && count($charBand->out) == 4
		&& $charBand->out[0] == '词典学'
		&& $charBand->out[1] == '辞典学'
		&& $charBand->out[2] == '詞典學'
		&& $charBand->out[3] == '辭典學') {
	print "✅OK: expandCharBand $s\n";
}
else {
	print "❌ERROR: after expanding $s, cbStatus = $cbStatus, sylCount = $charBand->sylCount\n";
	$varCount = count($charBand->out);
	for ($i = 0; $i < $varCount; $i++) {
		$o = $charBand->out[$i];
		print "charBand->out[$i] = $o\n";
	}
}

$s = "心怦怦跳//心砰砰跳";
$charBand = new WenlinCharBand();
$cbStatus = $charBand->expandCharBand($s, true);
if ($cbStatus == '好' && $charBand->sylCount == 4 && count($charBand->out) == 2
		&& $charBand->out[0] == '心怦怦跳'
		&& $charBand->out[1] == '心砰砰跳') {
	print "✅OK: expandCharBand $s\n";
}
else {
	print "❌ERROR: after expanding $s, cbStatus = $cbStatus, sylCount = $charBand->sylCount\n";
	$varCount = count($charBand->out);
	for ($i = 0; $i < $varCount; $i++) {
		$o = $charBand->out[$i];
		print "charBand->out[$i] = $o\n";
	}
}

$s = "人心惶惶//皇皇";
$charBand = new WenlinCharBand();
$cbStatus = $charBand->expandCharBand($s, true);
if ($cbStatus == '好' && $charBand->sylCount == 4 && count($charBand->out) == 2
		&& $charBand->out[0] == '人心惶惶'
		&& $charBand->out[1] == '人心皇皇') {
	print "✅OK: expandCharBand $s\n";
}
else {
	print "❌ERROR: after expanding $s, cbStatus = $cbStatus, sylCount = $charBand->sylCount\n";
	$varCount = count($charBand->out);
	for ($i = 0; $i < $varCount; $i++) {
		$o = $charBand->out[$i];
		print "charBand->out[$i] = $o\n";
	}
}

$s = "喘吁吁//嘘嘘[---//噓噓]";
$charBand = new WenlinCharBand();
$cbStatus = $charBand->expandCharBand($s, true);
if ($cbStatus == '好' && $charBand->sylCount == 3 && count($charBand->out) == 3
		&& $charBand->out[0] == '喘吁吁'
		&& $charBand->out[1] == '喘嘘嘘'
		&& $charBand->out[2] == '喘噓噓') {
	print "✅OK: expandCharBand $s\n";
}
else {
	print "❌ERROR: after expanding $s, cbStatus = $cbStatus, sylCount = $charBand->sylCount\n";
	$varCount = count($charBand->out);
	for ($i = 0; $i < $varCount; $i++) {
		$o = $charBand->out[$i];
		print "charBand->out[$i] = $o\n";
	}
}

$band = '123df@howdy   行不行?';
list($digits, $name, $comment, $content) = WenlinBand::parseBand($band);
if ($digits == '123' && $name == 'df' && $comment == '@howdy' && $content == '行不行?') {
	print "✅OK: WenlinBand::parseBand\n";
}
else {
	print "❌ERROR: parseBand turned [$band] into: [$digits] [$name] [$comment] [$content]\n";
}

// 19252 is currently the largest homophone number supported
$a = [
	'cílǜ*' => 'cilu.w..24..a',
	'¹címìng' => 'ciming...24..b',
	'²címìng' => 'ciming...24..c',
	'máfan' => 'mafan...20..z',
	'zīchǎn jiējí fǎndòng lùxiàn' => 'zichanjiejifandongluxian...13123444..z',
	'¹⁰¹ḿ' => 'm...2..ycz',
	'⁷⁰⁰yì' => 'yi...4..yzzaa',
	'⁹⁹⁹nǚ' => 'nu.y..3..yzzln',
	'¹⁰⁰¹xíng' => 'xing...2..zaa',
	'¹⁹²⁵²wā' => 'wa...1..zzzzzz',
	"Kě'ài*" => 'keai...34.z.a',
	"women" => 'women...00..z',
	];
foreach ($a as $s => $shouldBe) {
	$key = WenlinCollate::makePinyinCollationKey($s);
	$ok = ($key == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinCollate::makePinyinCollationKey $s → $key\n";
}

/* English collation keys unfortunately depend on the version of ICU used by the "intl" PHP extension,
	which may be different on each server. Ideally we might support English collation without this
	complication, but it's not clear how. At least it would be good if our testing here could take into
	account the variability of ICU versions. Let's try to do so here.
		IMPORTANT: this does not test that our local database matches our ICU version!
		TODO: test that our local database matches our ICU version. */
$eck = WenlinCollate::makeEnglishCollationKey('a');
if ($eck === "\x29\x01\x05\x01\x05\x01\x01\x7a") {
	// like on Tom's MacBook, wiki on localhost, 2017-3-12
	$a = [
		'a' => "\x29\x01\x05\x01\x05\x01\x01\x7a",
		'-a' => "\x29\x01\x05\x01\x05\x01\x05\x0e\x01\x7a",
		'a.' => "\x29\x01\x05\x01\x05\x01\x1c\x08\x01\x7a",
		'am' => "\x29\x41\x01\x06\x01\x06\x01\x01\x7a",
		'a.m.' => "\x29\x41\x01\x06\x01\x06\x01\x1c\x08\x1c\x08\x01\x7a",
		'ax' => "\x29\x57\x01\x06\x01\x06\x01\x01\x7a",
		'¹be' => "\x2b\x31\x01\x06\x01\x06\x01\x01\x62",
		'²be' => "\x2b\x31\x01\x06\x01\x06\x01\x01\x63",
		'cant' => "\x2d\x29\x43\x4f\x01\x08\x01\x08\x01\x01\x7a",
		"can't" => "\x2d\x29\x43\x4f\x01\x08\x01\x08\x01\x1e\x09\x68\x01\x7a",
		'coop' => "\x2d\x45\x45\x47\x01\x08\x01\x08\x01\x01\x7a",
		'co-op' => "\x2d\x45\x45\x47\x01\x08\x01\x08\x01\x1d\x05\x0e\x01\x7a",
		'dictionary' => "\x2f\x39\x2d\x4f\x39\x45\x43\x29\x4b\x59\x01\x0e\x01\x0e\x01\x01\x7a",
		'dragon' => "\x2f\x4b\x29\x35\x45\x43\x01\x0a\x01\x0a\x01\x01\x7a",
		'dry' => "\x2f\x4b\x59\x01\x07\x01\x07\x01\x01\x7a",
		'less' => "\x3f\x31\x4d\x4d\x01\x08\x01\x08\x01\x01\x7a",
		'-less' => "\x3f\x31\x4d\x4d\x01\x08\x01\x08\x01\x05\x0e\x01\x7a",
		'zymurgy' => "\x5b\x59\x41\x51\x4b\x35\x59\x01\x0b\x01\x0b\x01\x01\x7a",
	];
}
elseif ($eck === "\x27\x01\x05\x01\x05\x01\x01\x7a") {
	// like on wenlin.co server, 2017-3-12
	$a = [
		'a' => "\x27\x01\x05\x01\x05\x01\x01\x7a",
		'-a' => "\x27\x01\x05\x01\x05\x01\x06\x01\x7a",
		'a.' => "\x27\x01\x05\x01\x05\x01\x0d\x09\x01\x7a",
		'am' => "\x27\x3f\x01\x06\x01\x06\x01\x01\x7a",
		'a.m.' => "\x27\x3f\x01\x06\x01\x06\x01\x0d\x09\x0d\x09\x01\x7a",
		'ax' => "\x27\x55\x01\x06\x01\x06\x01\x01\x7a",
		'¹be' => "\x29\x2f\x01\x06\x01\x06\x01\x01\x62",
		'²be' => "\x29\x2f\x01\x06\x01\x06\x01\x01\x63",
		'cant' => "\x2b\x27\x41\x4d\x01\x08\x01\x08\x01\x01\x7a",
		"can't" => "\x2b\x27\x41\x4d\x01\x08\x01\x08\x01\x0f\x0a\x93\x01\x7a",
		'coop' => "\x2b\x43\x43\x45\x01\x08\x01\x08\x01\x01\x7a",
		'co-op' => "\x2b\x43\x43\x45\x01\x08\x01\x08\x01\x0e\x06\x01\x7a",
		'dictionary' => "\x2d\x37\x2b\x4d\x37\x43\x41\x27\x49\x57\x01\x0e\x01\x0e\x01\x01\x7a",
		'dragon' => "\x2d\x49\x27\x33\x43\x41\x01\x0a\x01\x0a\x01\x01\x7a",
		'dry' => "\x2d\x49\x57\x01\x07\x01\x07\x01\x01\x7a",
		'less' => "\x3d\x2f\x4b\x4b\x01\x08\x01\x08\x01\x01\x7a",
		'-less' => "\x3d\x2f\x4b\x4b\x01\x08\x01\x08\x01\x06\x01\x7a",
		'zymurgy' => "\x59\x57\x3f\x4f\x49\x33\x57\x01\x0b\x01\x0b\x01\x01\x7a",
	];
}
elseif ($eck === "\x2a\x01\x05\x01\x05\x01\x01\x7a") {
	// Tom's localhost 2019-03-14
	$a = [
		'a' => "\x2a\x01\x05\x01\x05\x01\x01\x7a",
		'-a' => "\x2a\x01\x05\x01\x05\x01\x05\x0e\x01\x7a",
		'a.' => "\x2a\x01\x05\x01\x05\x01\x1c\x08\x01\x7a",
		'am' => "\x2a\x42\x01\x06\x01\x06\x01\x01\x7a",
		'a.m.' => "\x2a\x42\x01\x06\x01\x06\x01\x1c\x08\x1c\x08\x01\x7a",
		'ax' => "\x2a\x58\x01\x06\x01\x06\x01\x01\x7a",
		'¹be' => "\x2c\x32\x01\x06\x01\x06\x01\x01\x62",
		'²be' => "\x2c\x32\x01\x06\x01\x06\x01\x01\x63",
		'cant' => "\x2e\x2a\x44\x50\x01\x08\x01\x08\x01\x01\x7a",
		"can't" => "\x2e\x2a\x44\x50\x01\x08\x01\x08\x01\x1e\x09\x6a\x01\x7a",
		'coop' => "\x2e\x46\x46\x48\x01\x08\x01\x08\x01\x01\x7a",
		'co-op' => "\x2e\x46\x46\x48\x01\x08\x01\x08\x01\x1d\x05\x0e\x01\x7a",
		'dictionary' => "\x30\x3a\x2e\x50\x3a\x46\x44\x2a\x4c\x5a\x01\x0e\x01\x0e\x01\x01\x7a",
		'less' => "\x40\x32\x4e\x4e\x01\x08\x01\x08\x01\x01\x7a",
		'-less' => "\x40\x32\x4e\x4e\x01\x08\x01\x08\x01\x05\x0e\x01\x7a",
		'zymurgy' => "\x5c\x5a\x42\x52\x4c\x36\x5a\x01\x0b\x01\x0b\x01\x01\x7a",
	];
}
else {
	print "WARNING: ICU/intl version evidently doesn't match any that WenlinTest.php knows.\n";
	$a = [
		'a' => "?",
		'-a' => "?",
		'a.' => "?",
		'am' => "?",
		'a.m.' => "?",
		'ax' => "?",
		'¹be' => "?",
		'²be' => "?",
		'cant' => "?",
		"can't" => "?",
		'coop' => "?",
		'co-op' => "?",
		'dictionary' => "?",
		'less' => "?",
		'-less' => "?",
		'zymurgy' => "?",
	];
}

foreach ($a as $s => $shouldBe) {
	$key = WenlinCollate::makeEnglishCollationKey($s);
	$ok = ($key == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinCollate::makeEnglishCollationKey $s → ";
	$len = strlen($key);
	for ($i = 0; $i < $len; $i++) {
		printf("\\x%02x", ord($key[$i]));
	}
	print "\n";
}

$a = [
	'aa1' => 'aa.1.z',
	'aa1*' => 'aa.1.a',
	'naam4 tung4 jyun6/2' => 'naamtungjyun.446.z',
	'3 naam4 tung4 jyun6/2' => 'naamtungjyun.446.d',
	];
foreach ($a as $s => $shouldBe) {
	$key = WenlinCollate::makeJyutpingCollationKey($s);
	$ok = ($key == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinCollate::makeJyutpingCollationKey $s → $key\n";
}

$a = [
	'dạ̌oshuị̌guǎn' => 'dǎoshuǐguǎn',
	'nǚyǔhángyuán' => 'nǚyǔhángyuán',
	'lụ̈̌guǎn*' => 'lǚguǎn*',
	'yī̠diǎnr*' => 'yīdiǎnr*',
	'yị̄yàng' => 'yīyàng',
	'¹yī̠bǎi*' => '¹yībǎi*'
	];
foreach ($a as $s => $shouldBe) {
	$t = WenlinPinyin::removeToneChangeNotation($s);
	$ok = ($t == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinPinyin::removeToneChangeNotation $s → $t\n";
}

$a = [
	'r' => 'r, 0',
	'ň' => 'n, 3',
	'ǎo' => 'ao, 3',
	'dạ̌o' => 'dao, 3',
	'shuị̌' => 'shui, 3',
	'zi' => 'zi, 0',
	'nǚ' => 'nü, 3',
	'yǔ' => 'yu, 3',
	'háng' => 'hang, 2',
	'yī̠' => 'yi, 1',
	// TODO: 'nǎr' => 'nar, 3' ?
	'zip' => ', 0',
	'xxx' => ', 0',
	'123' => ', 0',
	'xüm' => ', 0',
	];
foreach ($a as $s => $shouldBe) {
	list($pinyinToneless, $tone) = WenlinPinyin::validateAndSplit($s);
	$ok = ("$pinyinToneless, $tone" == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinPinyin::validateAndSplit $s → $pinyinToneless, $tone\n";

	if ($pinyinToneless != '' && $s != 'yī̠' && $s != 'dạ̌o' && $s != 'shuị̌') {
		$j = WenlinPinyin::addToneToSyllable($pinyinToneless, $tone);
		$ok = ($j == $s) ? '✅OK': '❌ERROR';
		print "$ok: WenlinPinyin::addToneToSyllable $pinyinToneless, $tone → $j (should be $s)\n";
	}
}

print "\n";

$a = [
	'Nǐhǎo' => 'Ni3hao3',
	'kẹ̌yǐ' => 'ke3yi3',
	'nǚ' => 'nv3',
	'nǚyǔhángyuán' => 'nv3yu3hang2yuan2',
	'lụ̈̌guǎn*' => 'lv3guan3*',
	'cídiǎn zhèngwén' => 'ci2dian3 zheng4wen2',
	'lüèwēi' => 'lve4wei1',
	'kèrén' => 'ke4ren2',
	'ér' => 'er2',
	'érhuà' => 'er2hua4',
	// TODO: 'nǎr' => 'nar3' or 'na3r'?
	];
foreach ($a as $s => $shouldBe) {
	$d = WenlinPinyin::convertTonesToDigits($s);
	$ok = ("$d" == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: WenlinPinyin::convertTonesToDigits $s → $d\n";
}

print "\n";

$text = "py   cídiǎn*\n"
		. "char   词/辞典[詞/辭-]\n"
		. "gr   A\n"
		. "ser   1001930945\n"
		. "ref   6641\n"
		. "ps   n.\n"
		. "1df   dictionary; lexicon\n"
		. "2df   test\n"
		. "mw   běn [本]\n"
		. "freq   2.77 [XHPC:5]\n";
$shouldBe = <<<'EOD'
<span class='wenlin-band-hw'>cídiǎn*</span> <span class='wenlin-band-char'>​[[Zi:词|词]]/​[[Zi:辞|辞]]​[[Zi:典|典]][​[[Zi:詞|詞]]/​[[Zi:辭|辭]]-]</span> <span class='wenlin-band-gr'><a href='/Project:Ci_Grade_A'>{A}</a></span> <span class='wenlin-band-ps'>n.</span> ①<span class='wenlin-band-df'>dictionary; lexicon</span> ②<span class='wenlin-band-df'>test</span> M: <span class='wenlin-band-mw'>běn 本</span>
<br />
<span class='wenlin-band-freq'>2.77 [XHPC:5] average occurrences per million characters of text</span><br />
<a href='/Special:Wenlin/a?a=c%C3%ADdi%C7%8En&ns=Ci'>►list adjacent words alphabetically</a><br />
<a href='/index.php?search=cídiǎn&title=Special:Search&fulltext=1'>►search</a> this wiki for ‘cídiǎn’<br />
<a target="_blank" href="http://www.wenlininstitute.org/cgi-bin/wenti.cgi?wenti=%E8%AF%8D%E5%85%B8">►web links</a> for ‘词典’<br />
search web for ‘词典’:  <a target="_blank" href="https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=%E8%AF%8D%E5%85%B8">►Google</a> <a target="_blank" href="https://translate.google.com/#zh-CN/en/%E8%AF%8D%E5%85%B8">►GT</a> <a target="_blank" href="https://www.baidu.com/s?ie=utf-8&wd=%E8%AF%8D%E5%85%B8">►Baidu</a> <a target="_blank" href="https://www.bing.com/search?q=%E8%AF%8D%E5%85%B8">►Bing</a> <a target="_blank" href="https://us.search.yahoo.com/search?fr=yhs-invalid&p=%E8%AF%8D%E5%85%B8">►Yahoo</a><br />
create new entry for ‘词典’ in namespace <a href='/Special:Wenlin/create?term=词典&ns=Ci'>►Ci</a><br>create new entry for ‘cídiǎn’ in namespace <a href='/Special:Wenlin/create?term=cídiǎn&ns=Ci'>►Ci</a> <a href='/Special:Wenlin/create?term=cídiǎn&ns=En'>►En</a><br>
EOD;

$disp = WenlinBand::translateToDisplayFormat($text, 'Ci', $wlParser);
$ok = ($disp == $shouldBe) ? '✅OK': '❌ERROR';
print "$ok: WenlinBand::translateToDisplayFormat (Ci):\n$disp\n";
if ($disp != $shouldBe) {
	print "\nShould be:\n$shouldBe\n";
}
$info = new CiIndexInfo(123 /* page_id */, '1001930945' /* ser */, $text);
if (!$info->valid) {
	print "❌ERROR: CiIndexInfo invalid ($info->errorMessage)\n";
}
else {
	$queries = $info->index();
	if ($queries == null) {
		print "❌ERROR: WenlinIndex::indexCidian returned null\n";
	}
	else {
		$shouldBe = "delete from wenlin_ci_pinyin where page_id='123';\n"
				. "delete from wenlin_ci_hanzi_string where page_id='123';\n"
				. "delete from wenlin_ci_hanzi_contained where page_id='123';\n"
				. "insert into wenlin_ci_pinyin (page_id, serial_number, pinyin_collation, syllable_count, frequency) values ('123', '1001930945', 'cidian...23..a', '2', '2.77');\n"
				. "insert into wenlin_ci_hanzi_string (page_id, hanzi) values ('123', '词典'), ('123', '辞典'), ('123', '詞典'), ('123', '辭典');\n"
				. "insert into wenlin_ci_hanzi_contained (page_id, hanzi, syllable_number) values ('123', '35789', '1'), ('123', '20856', '2'), ('123', '36766', '1'), ('123', '35422', '1'), ('123', '36781', '1');\n";
		$ok = ($queries == $shouldBe) ? '✅OK': '❌ERROR';
		print "$ok: info->index (Ci) returned:\n\n$queries\n";
	}
}

$text = "hw   dictionary\n"
		. "ipa   ˈdɪkʃəˌnɛri\n"
		. "gr   A\n"
		. "ser   2000365715\n"
		. "ref   ox120a2\n"
		. "ps   n.c.\n"
		. "infl   pl. (-ies) [dictionaries]\n"
		. "df   zì/cídiǎn [字/词典]; císhū [辞书]\n";
$shouldBe = <<<'EOD'
<span class='wenlin-band-hw'>dictionary</span> <span class='wenlin-band-ipa'>[ˈdɪkʃəˌnɛri]</span> <span class='wenlin-band-gr'><a href='/Project:En_Grade_A'>{A}</a></span> <span class='wenlin-band-ps'>n.c.</span> <span class='wenlin-band-infl'>pl. (-ies) [dictionaries]</span> <span class='wenlin-band-df'>zì/cídiǎn 字/词典; císhū 辞书</span><br />
<a href='/Special:Wenlin/a?a=dictionary&ns=En'>►list adjacent words alphabetically</a><br />
<a href='/index.php?search=dictionary&title=Special:Search&fulltext=1'>►search</a> this wiki for ‘dictionary’<br />
<a target="_blank" href="http://www.wenlininstitute.org/cgi-bin/wenti.cgi?wenti=dictionary">►web links</a> for ‘dictionary’<br />
search web for ‘dictionary’:  <a target="_blank" href="https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=dictionary">►Google</a> <a target="_blank" href="https://translate.google.com/#en/zh-CN/dictionary">►GT</a> <a target="_blank" href="https://www.baidu.com/s?ie=utf-8&wd=dictionary">►Baidu</a> <a target="_blank" href="https://www.bing.com/search?q=dictionary">►Bing</a> <a target="_blank" href="https://us.search.yahoo.com/search?fr=yhs-invalid&p=dictionary">►Yahoo</a><br />
create new entry for ‘dictionary’ in namespace <a href='/Special:Wenlin/create?term=dictionary&ns=Ci'>►Ci</a> <a href='/Special:Wenlin/create?term=dictionary&ns=En'>►En</a><br>
EOD;

$disp = WenlinBand::translateToDisplayFormat($text, 'En', $wlParser);
$ok = ($disp == $shouldBe) ? '✅OK': '❌ERROR';
print "$ok: WenlinBand::translateToDisplayFormat (En):\n$disp\n";
if ($disp != $shouldBe) {
	print "\nShould be:\n$shouldBe\n";
}
$info = new YinghanIndexInfo(456 /* page_id */, '2000365715' /* ser */, $text);
if (!$info->valid) {
	print "❌ERROR: YinghanIndexInfo invalid ($info->errorMessage)\n";
}
else {
	$queries = $info->index();
	if ($queries == null) {
		print "❌ERROR: info->index (En) returned null\n";
	}
	else {
		/* Note: some byte values including \x01 that occur in collation keys are supposedly OK for MySQL (which does not provide escapes for them),
			but they don't appear on the Mac Terminal command line and can't be copy-pasted from it.
				TODO: as for the tests for English collation above, we need to adjust the key below to match the particular version of intl (ICU) in use.
				This is right on my Mac currently, but not on wenlin.co... */
		$shouldBe = "delete from wenlin_en_collation where page_id='456';\n"
				. "insert into wenlin_en_collation (page_id, serial_number, english_collation) values ('456', '2000365715', '0:.P:FD*LZz');\n";
		$ok = ($queries == $shouldBe) ? '✅OK': '❌ERROR';
		print "$ok: info->index (En) returned:\n\n$queries\n";
	}
}

$text = "乾 [qián] ☰ ䷀ The Creative  [gān] (S干) dry (乾巴), dried (乾菜, 乾貝, 乾肉)\n"
		. "“The 日 sun rising behind the trees of the  jungle... enlarged by 乙 the meaning of which is uncertain” —Karlgren.\n"
		. "Compare 朝 cháo zhāo. No connection with 乞 qǐ ‘beg’.\n"
		. "As a full form, 乾 is also used for gān ‘dry’ (warmth of the 日 sun). Among simple form characters, 干 is used for gān ‘dry’.\n"
		. "The older version of 乾, without 乙, is phonetic in 幹, which is the full form of 干 gàn ‘do’.\n"
		. "#rG.122.38,142.27 W.117d K.AD299,GSR140c D.1.56.10 M.3233 KX..84.25 B.184.1.05\n"
		. "#c𠦝乞倝\n"
		. "#y gon\\ kihn\n";
$info = new ZiIndexInfo(123 /* page_id */, '乾', $text);
if (!$info->valid) {
	print "❌ERROR: ZiIndexInfo invalid ($info->errorMessage)\n";
}
else {
	$queries = $info->index();
	if ($queries == null) {
		print "❌ERROR: info->index (Zi 1) returned null\n";
	}
	else {
		$shouldBe = "delete from wenlin_zi_pinyin where hanzi='20094';\n"
				. "delete from wenlin_zi_bujian where hanzi='20094';\n"
				. "delete from wenlin_zi_seal where seal='20094';\n"
				. "insert into wenlin_zi_pinyin (hanzi, pinyin, tone, jianti, fanti) values ('20094', 'qian', '2', 'TRUE', 'TRUE'), ('20094', 'gan', '1', 'FALSE', 'TRUE');\n"
				. "insert into wenlin_zi_bujian (hanzi, bujian) values ('20094', '133533'), ('20094', '20062'), ('20094', '20509'), ('20094', '59379');\n";
		$ok = ($queries == $shouldBe) ? '✅OK': '❌ERROR';
		print "$ok: info->index (Zi 1) returned:\n\n$queries\n";
	}
}

$text = "􁈏 􁏂􂍅􁰋􁧕􁂼􁍌􂂓􀵁􁙠􁃰􀀀􁨜􁡪􂘡􁈏􁂼􁜶􀦃􁙠􁈏<hr/>"
		. "日 實也大昜𡳿精𠀚虧𨑢囗一𧰼形凡日𡳿屬皆𨑢日<hr/>"
		. "日 實也大昜之精不虧從囗一象形凡日之屬皆從日<hr/>\n"
		. "#v日1⽇!曰𠮚口召目白自昍晶𣊭𪱈田(􁈏􁈐)\n"
		. "#c􁃰􀀀(439:􁈏)\n"
		. "#rS.4624􁈏302.110 GL..6.4168:6747 FJ..137.020 XK..4646:133.101 DZ..4779〷GY..日◦468.13◦入聲◦質◦日◦人質◦/ȵiet/〷GS..日◦ńi̯ĕt [ȵʲĕt]◦ńźi̯ĕt [ȵʑʲĕt]◦jï◦‘sun; day’; GS404a-d〷BX..日◦njit◦nyit◦rì◦‘sun; day’◦(GS0404a; 1156, 9785.01)\n";
$info = new ZiIndexInfo(123 /* page_id */, '􁈏', $text);
if (!$info->valid) {
	print "❌ERROR: ZiIndexInfo invalid ($info->errorMessage)\n";
}
else {
	$queries = $info->index();
	if ($queries == null) {
		print "❌ERROR: info->index (Zi 2) returned null\n";
	}
	else {
		$shouldBe = "delete from wenlin_zi_pinyin where hanzi='1053199';\n"
				. "delete from wenlin_zi_bujian where hanzi='1053199';\n"
				. "delete from wenlin_zi_seal where seal='1053199';\n"
				. "insert into wenlin_zi_bujian (hanzi, bujian) values ('1053199', '1052912'), ('1053199', '1048576');\n"
				. "insert into wenlin_zi_seal (hanzi, seal, distance) values ('26085', '1053199', '1'), ('12103', '1053199', '2'), ('26352', '1053199', '192'), ('134042', '1053199', '192'), ('21475', '1053199', '192'), ('21484', '1053199', '192'), ('30446', '1053199', '192'), ('30333', '1053199', '192'), ('33258', '1053199', '192'), ('26125', '1053199', '192'), ('26230', '1053199', '192'), ('144045', '1053199', '192'), ('175176', '1053199', '192'), ('30000', '1053199', '192'), ('1053199', '1053199', '192'), ('1053200', '1053199', '192');\n";
		$ok = ($queries == $shouldBe) ? '✅OK': '❌ERROR';
		print "$ok: info->index (Zi 2) returned:\n\n$queries\n";
	}
}

/* Test for polyglot notation like "...21df [en] ... 21df [fr] ..." -- should have structure "①...②...ⓐ...ⓑ...".
	Bug caused it to be "①...②...ⓐ...ⓑ...ⓒ...ⓓ...". See 修行, https://gitlab.com/Wenlin/wanwu/issues/69 */
$text = "py   ā\n"
		. "char   啊\n"
		. "ser   1234567890\n"
		. "ps   n.\n"
		. "1df   [en] test\n"
		. "1df   [fr] test\n"
		. "2en   slang\n"
		. "21df   [en] foo\n"
		. "21df   [fr] foo\n"
		. "22df   [en] bar\n"
		. "22df   [fr] bar\n";
/* TODO: instead of specifying verbatim what the output should be, just use a regex to make sure it
	has the right structure. Verbatim is too brittle. */
$shouldBe = <<<'EODDD'
<span class='wenlin-band-hw'>ā</span> <span class='wenlin-band-char'>​[[Zi:啊|啊]]</span> <span class='wenlin-band-ps'>n.</span> ①<span class='wenlin-band-df'>test</span> ②<span class='wenlin-band-en'>〈slang〉</span> ⓐ<span class='wenlin-band-df'>foo</span> ⓑ<span class='wenlin-band-df'>bar</span><br />
<a href='/Special:Wenlin/a?a=%C4%81&ns=Ci'>►list adjacent words alphabetically</a><br />
<a href='/index.php?search=ā&title=Special:Search&fulltext=1'>►search</a> this wiki for ‘ā’<br />
<a target="_blank" href="http://www.wenlininstitute.org/cgi-bin/wenti.cgi?wenti=%E5%95%8A">►web links</a> for ‘啊’<br />
search web for ‘啊’:  <a target="_blank" href="https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=%E5%95%8A">►Google</a> <a target="_blank" href="https://translate.google.com/#zh-CN/en/%E5%95%8A">►GT</a> <a target="_blank" href="https://www.baidu.com/s?ie=utf-8&wd=%E5%95%8A">►Baidu</a> <a target="_blank" href="https://www.bing.com/search?q=%E5%95%8A">►Bing</a> <a target="_blank" href="https://us.search.yahoo.com/search?fr=yhs-invalid&p=%E5%95%8A">►Yahoo</a><br />
create new entry for ‘啊’ in namespace <a href='/Special:Wenlin/create?term=啊&ns=Ci'>►Ci</a><br>create new entry for ‘ā’ in namespace <a href='/Special:Wenlin/create?term=ā&ns=Ci'>►Ci</a> <a href='/Special:Wenlin/create?term=ā&ns=En'>►En</a><br>
EODDD;

$disp = WenlinBand::translateToDisplayFormat($text, 'Ci', $wlParser);
$disp = rtrim($disp);
$shouldBe = rtrim($shouldBe);
$ok = ($disp == $shouldBe) ? '✅OK': '❌ERROR';
print "$ok: WenlinBand::translateToDisplayFormat (Ci):\n$disp\n";
if ($disp != $shouldBe) {
	print "\nShould be:\n$shouldBe\n";
}

// TODO: enable makeLexemesIntoLinks to work without Mediawiki, with a stub dictionary for
// testing: a few polysyllabic ci including "优等", "意外", and "等等".
// Reference: https://gitlab.com/Wenlin/wanwu/-/issues/81
$a = [
	'yōu [优]; yōuděng [优等]' => 'yōu [[优]]; yōuděng [[优等]]',
	'yìwài de [意外地]' => 'yìwài de [[意外]][[地]]',
	'yī (ge, ²zhī, dd.) [一(个、只、等等)]' => 'yī (ge, ²zhī, dd.) [[一]]([[个]]、[[只]]、[[等等]])',
	];
foreach ($a as $s => $shouldBe) {
	$result = WenlinRender::makeLexemesIntoLinks($s);
	$ok = ($result == $shouldBe) ? '✅OK': '❌ERROR';
	print "$ok: makeLexemesIntoLinks($s) = $result\n";
	if ($result != $shouldBe) {
		print "\nShould be:\n$shouldBe\n";
	}
}
