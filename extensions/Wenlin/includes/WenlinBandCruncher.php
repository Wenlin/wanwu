<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** WenlinBandCruncher:
	Help to implement translation from band notation to display format, especially notations
	related to multiple parts of speech ("ps" bands) and multiple definitions ("df" bands).
	All but the first part of speech are preceded by a diamond, as in:
		ài chuánhuà 爱传话[愛傳話] v.p. blab ◆s.v. blabby
	Definitions (when multiple) are preceded by circled numbers, as in
		àichǒng 爱宠[愛寵] v. ①bestow favor on ②indulge; spoil
	Compare DictOpt in C code.
*/
class WenlinBandCruncher
{
	/* Unicode circled digits start at U+2460 ①:
		① [U+2460] CIRCLED DIGIT ONE .. ⑳ [U+2473] CIRCLED NUMBER TWENTY
		㉑ [U+3251] CIRCLED NUMBER TWENTY ONE .. ㉟ [U+325F] CIRCLED NUMBER THIRTY FIVE
		㊱ [U+32B1] CIRCLED NUMBER THIRTY SIX .. ㊿ [U+32BF] CIRCLED NUMBER FIFTY
		⓪ [U+24EA] CIRCLED NUMBER ZERO
		For HDC we need at least 81; generate CDL on the fly,
		or just parenthesized strings "(100)" */
	const MIN_CIRCLED_NUMBERS_A = 0x2460; // 1..20
	const MIN_CIRCLED_NUMBERS_B = 0x3251; // 21..35
	const MIN_CIRCLED_NUMBERS_C = 0x32b1; // 36..50
	const MIN_CIRCLED_LETTERS_LOWER = 0x24d0; // a..z

	public $multiplePS, $didCircle; // both boolean
	public $partOfSpeechCount;
	public $circNumber, $secondaryCircNumber;

	/** __construct:
		Construct a new WenlinBandCruncher object.
	*/
	public function __construct()
	{
		$this->multiplePS = $this->didCircle = false;
		$this->partOfSpeechCount = $this->circNumber = $this->secondaryCircNumber = 0;
	} // __construct

	/** process:
		While converting an entry from band notation to display notation, do some processing
		on one of the lines of the entry, especially related to band numbering, which sometimes
		causes us to append text such as black diamond or circled numbers to the destination.
		Update the state information in the WenlinBandCruncher.

		@param string digits the band digits.
		@param string name the band name.
		@param string textD the destination text, passed by reference, may be modified.
		@param integer startPos the starting position in textD (currently always 0).
	*/
	public function process($digits, $name, &$textD, $startPos)
	{
		$this->didCircle = false;
		if ($name === 'ps') {
			$this->circNumber = $this->secondaryCircNumber = 0;
			if (strlen($textD) != $startPos) {
				$textD .= ' ';
			} // else no space as this is the 1st thing we had (hanzi+pinyin both hidden)
			if ($this->partOfSpeechCount++ == 0) {
				$this->multiplePS = (strlen($digits) != 0);
			}
			else {
				$textD .= '◆'; // U+25c6 black diamond
			}
		}
		elseif (strlen($digits) != 0 && $name != 'ex' && $name != 'tr'
				&& $name != 'hz' && $name != 'char' && $name != 'mw'
				&& $name != 'seealso') {
			$digitOffset = 0;
			if ($this->multiplePS) {
				$digitOffset = 1;
			}
			if (strlen($digits) > $digitOffset) {
				if ((WenlinUtf::ziNumber($digits[$digitOffset]) - WenlinUtf::ziNumber('0')) == (($this->circNumber + 1) % 10)) {
					if (strlen($textD) != $startPos) {
						$textD .= ' ';
					} // else no space as this is the 1st thing we had (hanzi+pinyin both hidden)
					$textD .= self::circledNumber($this->circNumber + 1);
					if ($this->circNumber < 49) {
						++$this->circNumber;
					}
					$this->secondaryCircNumber = 0;
					$this->didCircle = true;
				}
				elseif (strlen($digits) > $digitOffset + 1) {
					/* Unicode lowercase circled letters start at U+24d0 ⓐ */
					if (strlen($textD) != $startPos) {
						$textD .= ' ';
					} // else no space as this is the 1st thing we had (hanzi+pinyin both hidden)
					$textD .= WenlinUtf::zinToStr(self::MIN_CIRCLED_LETTERS_LOWER + $this->secondaryCircNumber);
					if ($this->secondaryCircNumber < 25) {
						$this->secondaryCircNumber++;
					}
					$this->didCircle = true;
				}
			}
		}
	} // process

	/** circledNumber:
		Get the Unicode character for a circled number.

		@param n the number.
	*/
	public static function circledNumber($n)
	{
		if ($n < 1 || $n > 50) {
			return sprintf("(%d)", $n);
		}
		if ($n < 21) {
			$z = self::MIN_CIRCLED_NUMBERS_A + $n - 1;
		}
		elseif ($n < 36) {
			$z = self::MIN_CIRCLED_NUMBERS_B + $n - 21;
		}
		else {
			$z = self::MIN_CIRCLED_NUMBERS_C + $n - 36;
		}
		return WenlinUtf::zinToStr($z);
	} // circledNumber

} // WenlinBandCruncher
