<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** WenlinBandConverter:
	Store information about a dictionary entry, for the purpose of translating it from
	band notation to presentation (wiki/html) format.
*/
class WenlinBandConverter
{
	public $textB; // string: band notation
	public $textD; // string: destination (output)
	private $namespaceStr; // string: 'Ci', etc.
	private $wlParser; // WenlinParser
	private $dictPref; // WenlinDictionaryPreferences
	private $bandContext; // WenlinBandContext
	private $entryInfo; // WenlinEntryInfo
	private $hw; // string: the headword (py for Ci)
	private $char; // string the headword in Hanzi
	private $startPos; // the starting position, currently always 0
	private $cruncher; // WenlinBandCruncher
	private $autoInsideOut; // boolean
	private $gotHorizontalRule; // boolean
	private $stop; // boolean

	/** __construct:
		Construct a new WenlinBandConverter.
		@param string textB the input band-notation text.
		@param string namespaceStr Ci or En or Jyut (Cantonese).
		@param WenlinParser wlParser, used for parseWikiToHtml.
		@param WenlinDictionaryPreferences dictPref the dictionary display preferences.
		@param WenlinBandContext bandContext the context-specific parameters for translating band notation.
		@param WenlinEntryInfo entryInfo the entry information to be filled in, if not null, for the benefit of the caller.
	*/
	public function __construct($textB, $namespaceStr, WenlinParser $wlParser,
		WenlinDictionaryPreferences $dictPref, WenlinBandContext $bandContext, WenlinEntryInfo $entryInfo = null)
	{
		$this->textB = ltrim($textB); // skip any whitespace, including newline that might have followed WL tag before that was stripped
		$this->textD = ''; // the destination display-format text
		$this->namespaceStr = $namespaceStr;
		$this->wlParser = $wlParser;
		$this->dictPref = $dictPref;
		$this->bandContext = $bandContext;
		$this->entryInfo = $entryInfo;
		$this->hw = '';
		$this->char = '';
		$this->startPos = 0; // if we started with non-empty textD, then startPos would be strlen($textD).
		$this->cruncher = new WenlinBandCruncher();
		$this->autoInsideOut = false;
		$this->gotHorizontalRule = false;
		$this->stop = false;
	} // __construct

	public function loop()
	{
		$lineArray = explode("\n", $this->textB);
		for ($i = 0; $this->stop == false && $i < count($lineArray); $i++) {
			$line = $lineArray[$i];
			$line = rtrim($line);
			if ($i == 0) {
				$line = ltrim($line, '.'); // remove erroneous leading dot in some original En wiki entries.
			}
			$this->translateBand($line);
		}
		if ($this->gotHorizontalRule && !$this->bandContext->singleLine) {
			$this->copyBelowHorizontalRule($lineArray, $i + 1);
		}
	}

	private function translateBand($line)
	{
		list($digits, $name, $comment, $content) = WenlinBand::parseBand($line);
		if ($name === '') {
			return;
		}
		if ($name === 'h') {
			$this->gotHorizontalRule = true;
			return;
		}
		$content = WenlinBand::adjustForPolyglotNotation($content, $this->dictPref);
		if ($content === '') {
			return;
		}
		$content = $this->dictPref->getHideRareInfo() ? WenlinBand::abridge($content) : WenlinBand::unabridge($content);
		if ($content === '') {
			return;
		}
		$this->cruncher->process($digits, $name, $this->textD, $this->startPos); // may modify $this->textD
		/* Style using CSS, with classes like 'wenlin-band-hw-single'. Use the same style for 'py' and 'hw'. 
			We define some styles in Common.css:
				http://localhost/wow/MediaWiki:Common.css
				https://wenlin.co/wow/MediaWiki:Common.css
		*/
		$class = 'wenlin-band-' . (($name == 'py') ? 'hw' : $name);
		if ($this->bandContext->singleLine) {
			$class .= '-single';
		}
		switch ($name) { // the band name
		case 'py':
		case 'hw':
			$this->translateHw($content, $class);
			break;
		case 'char':
			$this->translateChar($content, $class);
			break;
		case 'gr':
			$this->translateGr($content, $class);
			break;
		case 'cons':
			$this->putSpace();
			$this->putWithClass($content, $class);
			break;
		case 'en':
			$this->translateEnEtc("environment", $content, $class);
			break;
		case 'ety':
			$this->translateEnEtc("etymology", $content, $class);
			break;
		case 'note':
			$this->translateEnEtc("note", $content, $class);
			break;
		case 'var':
			$this->translateEnEtc("variant", $content, $class);
			break;
		case 'ex':
			$this->translateEx($content, $class);
			break;
		case 'hz':
			$this->translateHz($content, $class);
			break;
		case 'psx':
			$this->translatePsx($content, $class);
			break;
		case 'da':
			$this->translateDa($content, $class);
			break;
		case 'freq':
			$this->translateFreq($content, $class);
			break;
		case 'hh':
			if (!$this->bandContext->ssw) {
				$this->translateSee($content, $class, $name);
			}
			break;
		case 'see':
		case 'seep':
		case 'seealso':
			$this->translateSee($content, $class, $name);
			break;
		case 'ps':
			$this->translatePs($content, $class);
			break;
		case 'in':
			$this->translateIn($content, $class);
			break;
		case 'mw':
			$this->translateMw($content, $class);
			break;
		case 'ab':
			$this->translateAb($content, $class);
			break;
		case 'tr':
			$this->translateTr($content, $class);
			break;
		case 'df':
			$this->translateDf($content, $class);
			break;
		case 'note':
			$this->translateNote($content, $class);
			break;
		case 'sub':
		case 'subof':
			$this->translateSub($content, $class, $name);
			break;
		case 'infl':
			$this->translateInfl($content, $class);
			break;
		case 'ipa':
			$this->translateIpa($content, $class);
			break;
		case 'auto':
			$this->autoInsideOut = true;
			break;
		case 'rem':
		case 'ref':
		case 'class':
		case 'ser':
			break;
		default:
			$this->putSpaceUnlessDidCircle();
			$this->put("{?$name?} $content");
			break;
		}
	} // translateBand

	private function translateHw($content, $class)
	{
		$this->hw = $content;
		if ($this->entryInfo != null) {
			$this->entryInfo->headword = $this->hw;
			if ($this->bandContext->hwStop && $this->namespaceStr == 'En') {
				$this->stop = true;
			}
		}
		if ($this->namespaceStr == 'Ci') {
			if ($this->dictPref->getHidePinyin()) {
				$content = ' ';
			}
			elseif ($this->dictPref->getHideToneChange()) {
				$content = WenlinPinyin::removeToneChangeNotation($content);
			}
		}
		$this->putWithClass($content, $class);
	} // translateHw

	private function translateChar($content, $class)
	{
		$this->char = $content;
		if ($this->dictPref->getHideHanzi()) {
			return;
		}
		if ($this->dictPref->getPreferSimple() == false) {
			$array = WenlinUtf::explode($this->char);
			$charBand = new WenlinCharBand();
			$array = $charBand->swapCharBandJiantiFanti($array);
			$content = implode('', $array);
		}
		if ($this->bandContext->hwStop) {
			$this->stop = true;
			if ($this->entryInfo == null || $this->entryInfo->headword == '') {
				return;
			}
			$this->entryInfo->headword .= " $this->char";
			return;
		}
		if ($this->dictPref->getHideSFEquiv()) {
			$bra = mb_strpos($content, '[');
			if ($bra > 0) {
				$content = mb_substr($content, 0, $bra);
			}
		}
		$content = WenlinRender::makeHanziIntoLinksOrHide($content, ' ', false);
		$content = $this->wlParser->parseWikiToHtml($content); // after makeHanziIntoLinks
		if ($this->dictPref->getPreferPinyinFirst()) {
			$this->putSpace(); // Pinyin already first
			$this->putWithClass($content, $class); // Hanzi second
		}
		else {
			$py = $this->textD;
			$this->textD = '';
			$this->putWithClass($content, $class); // Hanzi first
			$this->putSpace();
			$this->put($py); // Pinyin second
		}
	} // translateChar

	private function translateGr($content, $class)
	{
		if ($content != '*') {
			$this->putSpace();
			$this->putWithClass(WenlinGongju::makeGradedWordListLink($this->namespaceStr, $content), $class);
		}
	} // translateGr

	private function translatePs($content, $class)
	{
		// (WenlinBandConverter.process already wrote space & maybe diamond)
		// TODO: WENLIN_DESCRIBE_PART_OF_SPEECH
		if ($this->autoInsideOut) {
			$this->put('∾'); // U+223E inverted lazy s
		}
		$this->putWithClass($content, $class);
	}

	private function translatePsx($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		$this->putWithClass("($content)", $class); // notice the parentheses
	} // translatePsx

	private function translateDa($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		$this->putWithClass("($content)", $class); // notice the parentheses
	} // translateDa

	private function translateFreq($content, $class)
	{
		if (!$this->bandContext->singleLine) {
			$this->put("\n<br />\n");
			$content .= ' average occurrences per million characters of text';
			$this->putWithClass($content, $class);
		}
	} // translateFreq

	private function translateEnEtc($type, $content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		$this->putWithClass('〈' . $type . ':〉 ' . $content, $class);
	}

	private function translateEx($content, $class)
	{
		if ($this->dictPref->getHidePinyin() || $this->dictPref->getHideHanzi()) {
			$this->putSpace();
			return;
		}
		$this->put(' ‖ '); // double vertical bar to start example
		if ($this->dictPref->getHideToneChange()) {
			$content = WenlinPinyin::removeToneChangeNotation($content);
		}
		if ($this->dictPref->getReplaceTilde() && $this->hw !== '') {
			/* Delete any leading superscript digits or trailing asterisk.
				This does NOT work correctly:
					$this->hw = trim($this->hw, "*⁰¹²³⁴⁵⁶⁷⁸⁹");
				Although it might seem to work, it treats the string as a string of BYTES,
				not UTF-8 characters, and will trim any and all of those BYTES even when
				they combine to form different characters than the ones intended.
				Unfortunately there's no standard mb_trim yet. Use preg_replace instead. */
			$this->hw = preg_replace('/(^[⁰¹²³⁴⁵⁶⁷⁸⁹]+)|(\*)$/u', '', $this->hw);
			$content = str_replace('∼', $this->hw, $content);
		}
		$this->putSpace();
		$this->putWithClass($content, $class);
	} // translateEx

	private function translateHz($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		// check $this->dictPref->hideHanzi, $this->dictPref->hideToneChange, $this->dictPref->replaceTilde, $this->dictPref->preferSimple
		if ($this->dictPref->getHideToneChange()) {
			$content = WenlinPinyin::removeToneChangeNotation($content);
		}
		if ($this->hw !== '' && $this->dictPref->getReplaceTilde()) {
			if (mb_stripos($this->char, "[") != 0) {
				$pos = mb_stripos($this->char, "[");
				$tmp = mb_substr($this->char, 0, $pos);
				$content = str_replace("∼", $tmp, $content);
			}
			elseif (count($this->char) != 0) {
				$content = str_replace("∼", $this->char, $content);
			}
		}
		if ($this->dictPref->getHideHanzi()) {
			$this->putSpace();
		}
		else {
			$this->putSpace();
			$this->putWithClass($content, $class);
		}
	} // translateHz

	private function translateSee($content, $class, $name)
	{
		$this->putSpaceUnlessDidCircle();
		if ($this->namespaceStr == 'Jyut') {
			// Cantonese "seealso" sometimes precedes gloss. For now, at least, this looks better.
			// It might be better still to move to the end of the entry, but that requires study to
			// make sure "see also" applies to whole entry rather than only a portion...
			$this->put($this->bandContext->singleLine ? '【' : '<p>【');
			// We close the bracket or paragraph below with 】 or 】</p>.
		}
		// TODO: CSS instead of hard-coding bold and italic for "See (also)".
		$this->put('<b><i>See');
		if ($name == 'seealso' || $name == 'hh') {
			$this->put(' also');
		}
		$this->put('</i></b> ');
		$this->chopNumBrackets($content);
		$this->putWithClass($content, $class);
		if ($this->namespaceStr == 'Jyut') {
			$this->put($this->bandContext->singleLine ? '】' : '】</p>');
		}
	} // translateSee

	private function translateIn($content, $class)
	{
		$content = $this->getInBandContent($content);
		$this->chopNumBrackets($content);
		$this->putSpace();
		$this->putWithClass($content, $class);
	} // translateIn

	/** getInBandContent:
		Get the appropriate text for an "in" band, as a link to the entry refered to.

		@param $s the content of the "in" band; for example, "qiútǐ [1010364804]".
		@return: the text to be displayed, as a link to the entry it refers to.
	*/
	private function getInBandContent($s)
	{
		// Get serial number, which should be in brackets at end of string.
		$start = mb_strpos($s, "[");
		$end = mb_strpos($s, "]");
		if ($start === false || $end === false) {
			return $s;
		}
		$range = $end - $start;
		$ser = mb_substr($s, $start + 1, $range - 1);
		if ($ser === false || $ser === '') {
			return $s;
		}
		$py = rtrim(mb_substr($s, 0, $start - 1));
		$text = $py;
		if (!$this->dictPref->getHideHanzi()) {
			$hz = $this->getCharFromCiSer($ser);
			if ($this->dictPref->getHidePinyin()) {
				$text = $hz;
			}
			else if ($this->dictPref->getPreferPinyinFirst()) {
				$text = "$py $hz";
			}
			else {
				$text = "$hz $py";
			}
		}
		$text = "[[$this->namespaceStr:$ser|►$text]]";
		if (defined('MEDIAWIKI')) {
			$text = $this->wlParser->parseWikiToHtml($text);
		}
		return $text;
	} // getInBandContent

	private function translateMw($content, $class)
	{
		// TODO: mw band preferSimple
		if ($this->dictPref->getHideHanzi()) {
			$pos = mb_strpos($content, "[");
			$end = mb_strpos($content, "]");
			$range = $end - $pos;
			$zi = mb_substr($content, $pos, $range);
			$content = str_replace($zi, " ", $content);
		}
		if ($this->dictPref->getHidePinyin()) {
			$arr = preg_split("/\s/", $content);
			foreach ($arr as $value) {
				if (preg_match("/[\x{4e00}-\x{9fa5}]+.*/u", $value, $matches) === 0) {
					$key = array_search($value, $arr);
					if ($key !== null) {
						unset($arr[$key]);
					}
				}
			}
			$content = implode(' ', $arr);
		}
		$content = preg_replace("/[\[\]]/", '', $content); // remove brackets
		if ($this->namespaceStr == 'Jyut' && $this->bandContext->singleLine == false) {
			// Let's be more verbose and use more newlines. This might be good for non-Cantonese entries as well...
			// However, it should be implemented in css not php
			$this->put("<p>Measure word: ");
			$this->putWithClass($content, $class);
			$this->put("</p>");
		}
		else {
			$this->put(' M: ');
			$this->putWithClass($content, $class);
		}
	} // translateMw

	private function translateAb($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		$this->chopNumBrackets($content);
		$this->putWithClass($content, $class);
	} // translateMw

	private function translateTr($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		if ($this->dictPref->getHideGloss()) {
			$this->putSpace();
		}
		else {
			$this->putWithClass($content, $class);
		}
	} // translateTr

	private function translateDf($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		if ($this->dictPref->getHideGloss()) {
			$this->putSpace();
		}
		else {
			if ($this->namespaceStr == 'En') {
				$content = $this->adjustEnDfContent($content);
			}
			if ($content !== '') {
				$this->putWithClass($content, $class);
			}
		}
	} // translateDf

	private function translateNote($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		$this->putWithClass($content, $class);
	} // translateNote

	private function translateSub($content, $class, $name)
	{
		if ($this->bandContext->subentry || $this->bandContext->noSubentry) {
			return; // avoid infinite recursion
		}
		$this->putSpaceUnlessDidCircle();
		if ($name == 'subof') {
			$this->put('(Subentry of:) ');
		}
		$ser = $this->chopNumBrackets($content);
		$content = ' || ' . $content; // two vertical bars to start subentry
		$content = $this->getSubentryText($ser);
		$this->putWithClass($content, $class);
	} // translateSub

	/** getSubentryText:
		Get subentry for 'sub' or 'subof' band from Ying-Han dictionary.

		@param ser the serial number as a string.
		@return the text
	*/
	private function getSubentryText($ser)
	{
		$nsTitleStr = $this->namespaceStr . ':' . $ser;
		$text = WenlinGongju::getContentTextFromTitleString($nsTitleStr);
		if ($text === null) {
			return "(No text for $nsTitleStr)";
		}
		WenlinGongju::stripWLTag($text);

		/* Call translateToDisplayFormat recursively with "subentry" and "singleLine" flags. */
		$bandContext = new WenlinBandContext();
		$bandContext->subentry = true;
		$bandContext->singleLine = true;
		$text = WenlinBand::translateToDisplayFormat($text, $this->namespaceStr, $this->wlParser, $this->dictPref, $bandContext, null /* entryInfo */);
		$button = $this->wlParser->parseWikiToHtml("[[$nsTitleStr|►]]");
		return $button . $text;
	} // getSubentryText

	private function translateInfl($content, $class)
	{
		$this->putSpaceUnlessDidCircle();
		if (preg_match("/of \(/", $content)) {
			$this->chopNumBrackets($content);
		}
		$this->putWithClass($content, $class);
	} // translateInfl

	private function translateIpa($content, $class)
	{
		$this->putSpace();
		if (!$this->dictPref->getHideIpa()) {
			$this->putWithClass("[$content]", $class); // notice brackets
		}
	} // translateIpa

	///////////

	public function afterLoop()
	{
		// TODO: WjInsertNewChangedEntryNotation
		if ($this->bandContext->singleLine) {
			return;
		}
		$simplerHeadword = preg_replace('/(^[⁰¹²³⁴⁵⁶⁷⁸⁹]+)|(\*)$/u', '', $this->hw); // remove homophone number, asterisk
		switch ($this->namespaceStr) {
		case 'Ci':
			$this->char = WenlinCharBand::getCharBandNoBracketsNoVariants($this->char, $this->dictPref);
			$this->put(WenlinBand::makeCiButtons($simplerHeadword, $this->char));
			break;
		case 'Jyut':
			$this->char = WenlinCharBand::getCharBandNoBracketsNoVariants($this->char, $this->dictPref);
			$this->put(WenlinBand::makeJyutButtons($simplerHeadword, $this->char));
			break;
		case 'En':
			$this->char = '';
			$this->put(WenlinBand::makeEnButtons($simplerHeadword));
			break;
		}
		$namespaceArray = $this->dictPref->getPreferredChineseNamespaces();
		if ($this->char !== '') { // before appending 'En' to $namespaceArray
			$this->put(WenlinGongju::makeCreateEntryLinksWithNsArray($this->char, $namespaceArray));
		}
		if ($this->hw !== '') {
			$namespaceArray[] = 'En'; // append
			$this->put(WenlinGongju::makeCreateEntryLinksWithNsArray($simplerHeadword, $namespaceArray));
		}
	} // afterLoop

	/** chopNumBrackets:
		Given the band content containing a headword and its bracketed serial number,
		modify the band content by turning it into a button for opening the entry referred to,
		with the serial number hidden. For "in", "seealso", "hh", "ab" bands.

		@param string content the band content, passed by reference, may get changed.
		@return the serial number (to be used, eventually, for En subentries).
	*/
	private function chopNumBrackets(&$content)
	{
		$ser = '';
		if (preg_match("/^(.+)\s+\[(.+)\]$/", $content, $matches)) {
			$content = trim($matches[1]);
			// echo "match:$content/ ";
			$ser = $matches[2];
			// echo "ser:$ser/ ";
			$content = "[[$this->namespaceStr:$ser|$content]]";
			if (defined('MEDIAWIKI')) {
				$content = $this->wlParser->parseWikiToHtml($content);
			}
		}
		/* Sometimes "ab" bands have no [serial number]. For example, "ESL" has
			"ab	English as a Second Language"
			and there are entries for all those words but not for the complete phrase.
			Treat this as normal, not an error. If the regex doesn't match, leave the content
			unchanged and return an empty string for the serial number. */
		return $ser;
	} // chopNumBrackets

	/** getCharFromCiSer:
		Given the serial number, get the corresponding hanzi (char band).

		https://www.mediawiki.org/wiki/Manual:Database_access

		@param $ser serial number.
		@return the Hanzi, as a string, or '?' for failure.
	*/
	private function getCharFromCiSer($ser)
	{
		$titleStr = "$this->namespaceStr:$ser";
		$text = WenlinGongju::getContentTextFromTitleString($titleStr);
		if ($text == null) {
			return '?';
		}
		WenlinGongju::stripWLTag($text);
		$hz = "";
		$lineArray = explode("\n", $text);
		$lineCount = count($lineArray);
		$lineIndex = 0;
		while ($lineIndex < $lineCount){
			$line = $lineArray[$lineIndex++];
			$line = rtrim($line);
			list($digits, $name, $comment, $content) = WenlinBand::parseBand($line);
			if ($name === 'char'){
				$content = WenlinBand::unabridge($content);
				/* For Jyut, getCharBandNoBracketsNoVariants probably serves no purpose, yet.
					It should be harmless, anyway. */
				$hz = WenlinCharBand::getCharBandNoBracketsNoVariants($content, $this->dictPref);
				break;
			}
		}
		return $hz;
	} // getCharFromCiSer

	/** adjustEnDfContent:
		Given the content of a 'df' band in a namespace 'En' entry, adjust it as
		appropriate for display with the given dictionary preferences.

		@param $content the df band content.
		@return the modified content (as HTML, not wiki text).

		A typical En df band looks like:
			df   wǔyè tiěxiànlián [五叶铁线莲]
		Depending on preferences, we may show only the Pinyin, or only the Hanzi,
		or both, and we may convert the Hanzi to full form. Only simple forms are included
		in the df band, so conversion to full forms isn't trivial, especially if the content doesn't
		match a single existing entry. (Sometimes glosses are phrases not single lexical items.)

		TODO: use $this->dictPref->getPreferSimple() when displaying 'df' bands in 'En' entries.
		For example, the 'En' entry
			woodbine [ˈwʊdˌbaɪn] ∾n. wǔyè tiěxiànlián 五叶铁线莲
		should change to
			woodbine [ˈwʊdˌbaɪn] ∾ n. wǔyè tiěxiànlián 五葉鐵線蓮
		when the option is turned off.
	*/
	private function adjustEnDfContent($content)
	{
		if ($this->dictPref->getHideHanzi()) {
			$arr = preg_split("/\s\[[^[:alnum:]]+\]/", $content);
			$content = implode('', $arr);
		}
		else if ($this->dictPref->getHidePinyin()) {
			$arr = explode(" ", $content);
			foreach ($arr as $value) {
				// TODO: this regex doesn't cover all Hanzi (CJK) characters; shouldn't need regex for that here,
				// just assume everthing in brackets is Hanzi?
				if (preg_match("/[\x{4e00}-\x{9fa5}]+.*/u", $value, $matches) === 0) {
					$temp = "";
					for ($i = 0; $i < strlen($value); $i++) {
						if (preg_match("/[\s(),.;'\"\‘\’\”\“]/", $value[$i]) === 1) {
							$temp .= $value[$i];
						}
						else {
							$temp .= ' ';
						}
					}
					$key = array_search($value, $arr);
					if ($key !== null) {
						$arr[$key] = $temp;
					}
				}
			}
			$content = implode(' ', $arr);
		}
		$content = str_replace('[', '', $content);
		$content = str_replace(']', '', $content);
		$content = WenlinRender::makeLexemesIntoLinks($content);
		$content = $this->wlParser->parseWikiToHtml($content); // after makeLexemesIntoLinks
		return $content;
	} // adjustEnDfContent

	/** copyBelowHorizontalRule:
		Adjust the text that occurs below the "h" band.
		Insert a horizontal rule at the start.
		Lines may start with "note@comment" or "rem@comment", or neither.
		If they start with "note@comment", delete that start but keep the rest of the line.
		If they start with "rem@comment", skip the whole line.
		Otherwise, just keep the line as-is.

		Note: the "h" band serves to support extra text that might not be in band notation.
		Originally it was often unformatted. Later we found it more convenient to make each
		band after "h" a "note" band. We should review how the "h" band is actually used now,
		and possibly revise the notation.
		See 鼠牛虎兔龙蛇马羊猴鸡狗猪 for a sample entry that has an "h" band.

		@param $lineArray the array of lines.
		@param $i the index of the first line to use.
	*/
	private function copyBelowHorizontalRule($lineArray, $i)
	{
		$this->put("\n<hr>");
		$br = '';
		for (++$i; $i < count($lineArray); $i++) {
			$line = $lineArray[$i];
			list($digits, $name, $comment, $content) = WenlinBand::parseBand($line);
			if ($name === 'rem') {
				continue;
			}
			if ($name === 'note') {
				$line = $content;
			}
			$this->put($br . $line);
			$br = "<br>\n";
		}
	} // copyBelowHorizontalRule

	//////////

	private function put($s)
	{
		$this->textD .= $s;
	} // put

	private function putSpace()
	{
		$this->put(' ');
	} // putSpace

	private function putSpaceUnlessDidCircle()
	{
		if (!$this->cruncher->didCircle) {
			$this->putSpace();
		}
	} // putSpaceUnlessDidCircle

	private function putWithClass($content, $class)
	{
		$this->put("<span class='$class'>$content</span>");
	}

} // WenlinBandConverter
