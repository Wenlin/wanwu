<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** 
	A WenlinParser is a container for either a Parser or an OutputPage, both of which are
	defined by Mediawiki. Sometimes we need to parse wiki code into html, such as links we
	construct using "[[...]]". Some functions receive a Parser object, others receive an OutputPage object.
	When we have a Parser we can use $parser->recursiveTagParse.
	When we have an OutputPage we can use $outputPage->parseInlineAsInterface.
	With a WenlinParser, we use whichever we have.
*/
class WenlinParser
{
	private $parser; // a Parser object, or null
	private $outputPage; // an OutputPage object, or null

	/** __construct:
		Construct a new WenlinParser.
		For non-Mediawiki context (as in WenlinTest.php), both $parser and $outputPage may be null.

		@param $parser the Parser, or null.
		@param $outputPage the OutputPage, or null.
	*/
	public function __construct($parser, $outputPage)
	{
		$this->parser = $parser;
		$this->outputPage = $outputPage;
	} // __construct
	
	/** parseWikiToHtml:
		Do either $parser->recursiveTagParse or $outputPage->parseInlineAsInterface, or neither if both null (as in WenlinTest.php).

		@param text the input wiki text.
		@return the html text.

		Note: $parser->recursiveTagParse and $outputPage->parseInlineAsInterface do not produce exactly the same output:
		recursiveTagParse appears to do less; for example, it doesn't appear to process "#" as indentation.
		Formerly renderZidian called recursiveTagParse before replacing "#" lines, and that doesn't work
		when using $outputPage->parseInlineAsInterface. Hopefully, such differences won't matter for the way we're using parseWikiToHtml.
	*/
	public function parseWikiToHtml($text)
	{
		if ($this->parser != null) {
			return $this->parser->recursiveTagParse($text);
		}
		if ($this->outputPage != null) {
			return $this->outputPage->parseInlineAsInterface($text, false /* not necessarily start of line */);
		}
		return $text;
	} // parseWikiToHtml

} // class WenlinParser
