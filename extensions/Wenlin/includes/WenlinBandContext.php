<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** WenlinBandContext:
	Provide storage and access for parameters that affect processing of band-notation entries.
	Compare BandOptionFlags in C code.
*/
class WenlinBandContext
{
	/* The following are all boolean. */
	public $singleLine; /* only show a single line, as in lists */
	public $ssw; /* inhibit "hh" band in zidian window */
	public $ser; /* list words by serial number */
	public $cband; /* char band only -- not really for list */
	public $cbandfj; /* like cband but jianti or fanti only, no brackets */
	public $serbut; /* insert button and serial number at start of line */
	public $hwStop; /* only append headword (for Ci, that means pinyin and hanzi) */
	public $archive; /* show '+' notation for archive bit */
	public $instantLookup; /* called from instant lookup (used by WENLIN_IL_AUTO_LOOKUP_SEE) */
	public $cbandRaw; /* don't remove brackets or slashes from char band; for conversion bar */
	public $subentry; /* when reading yinghan subentry; cf. BAND_OPT_SUBENTRY_FLAG in C */
	public $subOf; /* suppress appending main entry to subentry; cf. BAND_OPT_SUBOF_FLAG in C */
	public $noSubentry; /* suppress appending subentries to main; cf. BAND_OPT_NO_SUBENTRY_FLAG in C */
	/* $noSubentry is currently unused by the wiki. Keep it for future use. In C, BAND_OPT_NO_SUBENTRY_FLAG
		is used only for GetYinghanEntryForDictSearch. Not to be confused with $dictPref->getHideSubentries(). */

	/** __construct:
		Construct a new WenlinBandContext.
	*/
	public function __construct()
	{
		$this->singleLine = false;
		$this->ssw = false;
		$this->ser = false;
		$this->cband = false;
		$this->cbandfj = false;
		$this->serbut = false;
		$this->hwStop = false;
		$this->archive = false;
		$this->instantLookup = false;
		$this->cbandRaw = false;
		$this->subentry = false;
		$this->subOf = false;
		$this->noSubentry = false;
	} // __construct

} // WenlinBandContext
