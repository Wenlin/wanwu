<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* Wenlin::ABC.pm contains a perl version of this code called ABCPinyinKey,
	also included in documentation at <http://wenlin.com/pysort>; excerpts below:

	An important standard-setting book on Pinyin collation is Hànyǔ Pīnyīn Cíhuì
	(汉语拼音词汇, 语文出版社, 1989, ISBN7-80006-335-6), edited under the direction
	of Zhou Youguang 周有光. Hànyǔ Pīnyīn Cíhuì served as the basis for the system
	used in the ABC Chinese-English Dictionary, edited by John DeFrancis. [see Note1]
	The original implementation for ABC was done by Bob Hsu using the SNOBOL language.
*/

class WenlinCollate
{
	private static $abc = 'abcdefghijklmnopqrstuvwxyz';

	/* How would compressed key be handled in SQL queries? Need to escape or bin-hex the special bytes,
		especially nul bytes, etc.? Cf. mysqli_real_escape_string (or deprecated mysql_real_escape_string).
		Maybe better is to use prepare() and bind_param(). Or reconsider whether compression is worthwhile.
		The English collation keys from ICU also use binary (non-ASCII) bytes, so we do need to make them
		OK for SQL anyway (see WenlinIndex::escapeString). However, ICU keys have no nul bytes, while compressed
		pinyin keys might.*/
	private static $keysShouldBeCompressed = false;

	/** makePinyinCollationKey:
		Make a Pinyin collation key for the given pinyin.

		@param $pinyin the Pinyin string.
		@param $simplify true if we should "simplify" the key.
		@return the collation key.

		Called by WenlinIndex::indexCidianWithBands.
	*/
	public static function makePinyinCollationKey($pinyin, $simplify = false)
	{
		$homophoneNumber = self::getHomophoneNumber($pinyin);
		$entryHasAsterisk = false;
		if (preg_match('/\*$/', $pinyin)) { // Ends with asterisk?
			$entryHasAsterisk = true; // cf. ASTERISK_HOMOPHONE in C code
			$pinyin = rtrim($pinyin, '*');
		}
		$key = self::makeUncompressedPinyinCollationKey($pinyin, $homophoneNumber, $entryHasAsterisk);
		if (self::$keysShouldBeCompressed) {
			$key = self::compressPinyinCollationKey($key);
		}
		if ($simplify) {
			/* "simplify" it by removing the trailing part that specifies capitalization, homophone number, ...,
				that is, everything after the FOURTH separator byte ('.'), while keeping the 4th
				separator byte itself. For example, the key for "Kě'ài*" is "keai...34.z.a"; the simplified
				key is "keai...34.". See WenlinTest.php for more examples. */
			$len = 0;
			for ($i = 0; $i < 4; $i++) {
				$len = strpos($key, '.', $len + 1);
			}
			$key = substr($key, 0, $len + 1);
		}
		return $key;
	} // makePinyinCollationKey

	/** makeUncompressedPinyinCollationKey:
		Make a Pinyin collation key, without compression.

		@param $pinyin the Pinyin (without homophone number or asterisk).
		@param $homophoneNumber the homophone number 1, 2, 3, ..., or 0 for none.
		@param boolean $entryHasAsterisk true if entry has asterisk (like “cídiǎn*”), else false;
								   since asterisk entries sort first, the caller may specify entryHasAsterisk
								   to get a key that sorts first among homophones.
		@return the collation key, without compression.

		Called by makePinyinCollationKey.

		Unlike the perl and C versions, here we assume Pinyin uses tone marks, not digits; and we don't
		support 'v' in place of 'ü' (except internally when returned by uniPyChar).
	*/
	private static function makeUncompressedPinyinCollationKey($pinyin, $homophoneNumber, $entryHasAsterisk)
	{
		// Replace anything in parentheses by a hyphen (in case followed by a/e/o).
		$pinyin = preg_replace('/\([^)]*\)/', '-', $pinyin);
		$pinyin = WenlinPinyin::removeToneChangeNotation($pinyin); // includes NFC()

		if ($entryHasAsterisk) {
			if ($homophoneNumber > 1) { // could report an error
				$entryHasAsterisk = false;
			}
		}
		$key = '';
		$umlautBuf = ''; $apostBuf= '';
		$toneBuf = ''; $capBuf = '';
		$prevChar = ''; $vowel = ''; $tone = '';
		$gotVowel = false; $gotTone = false;
		$endOfString = '(end)';

		$homBuf = self::homNumABC($homophoneNumber, $entryHasAsterisk);

		$charArray = WenlinUtf::Explode($pinyin);
		$charArray[] = $endOfString;
		$arrayLength = count($charArray);
		// Use index $i since need ability to look ahead.
		for ($i = 0; $i < $arrayLength; $i++) {
			$c = $charArray[$i];
			$tone = ''; // default, unless this char includes a tone
			$vowel = ''; // default, unless this char includes a vowel
			$usedUniPyChar = false;
			if ($c != $endOfString) {
				$zin = WenlinUtf::ziNumber($c);
				if ($zin >= 0x0080) {
					if ($key != '' && ($tone = WenlinPinyin::pinyinDiacritic($zin)) != '') {
						// tone mark, as a non-spacing diacritic; count it as being on previous character
						$c = ''; // to be ignored
						$gotTone = true;
					}
					else {
						list($ascii, $tone) = WenlinPinyin::uniPyChar($c); // uses 'v' for 'ü'
						if ($ascii != '') {
							$c = $ascii;
							$zin = WenlinUtf::ziNumber($c);
							$usedUniPyChar = true;
						}
					}
				}
				if ($zin >= 0x0041 && $zin <= 0x005A) { // capital letter A-Z
					$capBuf .= self::zyxPosition(strlen($key));
					$c = strtolower($c); // convert to lower case
					$zin += 0x0020; // convert to lower case
				}
				if ($zin >= 0x0061 && $zin <= 0x007A) { // lower-case letter a-z (including those that have just been converted from capital letters)
					// 'v' is returned by uniPyChar
					if ($c == 'v' && $usedUniPyChar) {
						$umlautBuf .= self::zyxPosition(strlen($key));
						$c = 'u';
					}
					if ($c == 'a' || $c == 'e' || $c == 'i' || $c == 'o' || $c == 'u') {
						$vowel = $c;
						$gotVowel = true; // will remain true until we reach a non-vowel
						if ($tone != '') {
							$gotTone = true; // this syllable (vowel string) has a non-neutral tone
						}
						// a/e/o preceded by apostrophe, space, or hyphen?
						if (($c == 'a' || $c == 'e' || $c == 'o') && ($prevChar == '-' || $prevChar == ' ' || $prevChar == "\'")) {
							$apostBuf .= self::zyxPosition(strlen($key));
						}
					}
					$key .= $c;
				}
			}
			if ($vowel == '') {
				// Do this check after each syllable, including end of string.
				// If we've just ended a string of vowels that did not include a tone mark,
				//  then treat it as a neutral tone syllable.
				if ($gotVowel && !$gotTone) {
					$tone = '0';
				}
				$gotVowel = $gotTone = false; // reset in preparation for next syllable
			}
			if ($tone != '') {
				$toneBuf .= $tone; // Represent tones by digits '0' thru '4'
			}
			$prevChar = $c;
		}
		$key .= '.' . $umlautBuf . '.' . $apostBuf . '.' . $toneBuf . '.' . $capBuf . '.' . $homBuf;
		if (strpos($key, '??') !== false) {
			return '??';
		}
		return $key;
	} // makeUncompressedPinyinCollationKey

	/** makeJyutpingCollationKey:
		Make a Jyutping collation key (without compression).

		All lower case.
		Spaces between all syllables (sometimes also comma).
		Ignore changing tones -- treat "6/2" same as "6".

		@param $pin the Jyutping (without homophone number or asterisk).
		@param $simplify true if we should "simplify" the key.
		@return the collation key, without compression; or '??' if bad input.

		Compare ABCJyutpingKey in wenlin/perl/wlpm/Wenlin/ABC.pm.
	*/
	public static function makeJyutpingCollationKey($pin, $simplify = false)
	{
		$pin = preg_replace('/\([^)]*\)/', '', $pin); // Delete anything in parentheses.
		$homophoneNumber = self::getHomophoneNumber($pin);
		$entryHasAsterisk = false;
		if (preg_match('/\*$/', $pin)) { // Ends with asterisk?
			$entryHasAsterisk = true; // cf. ASTERISK_HOMOPHONE in C code
			$pin = rtrim($pin, '*');
		}
		if ($entryHasAsterisk) {
			if ($homophoneNumber > 1) { // could report an error
				$entryHasAsterisk = false;
			}
		}
		$key = '';
		$toneBuf = '';
		$homBuf = self::homNumABC($homophoneNumber, $entryHasAsterisk);
		$endOfString = '(end)';
		$charArray = WenlinUtf::Explode($pin);
		$arrayLength = count($charArray);
		$skipOne = false;
		for ($i = 0; $i < $arrayLength; $i++) {
			if ($skipOne) {
				$skipOne = false;
				continue;
			}
			$c = $charArray[$i];
			$zin = WenlinUtf::ziNumber($c);
			if ($zin >= 0x0061 && $zin <= 0x007A) { // lowercase letter a-z
				$key .= $c;
			}
			elseif ($zin >= 0x0041 && $zin <= 0x005A) { // capital letter A-Z
				$key .= strtolower($c); // convert to lower case
			}
			elseif ($zin >= 0x0031 && $zin <= 0x0036) { // digit 1-6
				$toneBuf .= $c;
			}
			elseif ($c == '/') { # slash -- precedes digit as in 6/2
				$skipOne = true;
			}
			else {
				/* We can't simply ignore all non-ASCII characters (except superscript digits), because, for example,
					'gǒu' would be treated like 'gu' and would matches Jyut entries when it should only match Ci entries.
					This is mainly a problem when the user has entered a string to look up. In that context,
					'gǒu' (Mandarin Pinyin) should not match 'gu' (Cantonese Jyutping). For now at least, simply
					treat the string as invalid Jyutping if it contains any vowels with tone marks. */
				list($ascii, $toneDigit) = WenlinPinyin::uniPyChar($c);
				if ($ascii != '') {
					return '??';
				}
			}
		}
		if ($key == '') {
			return '??';
		}
		$key .= '.' . $toneBuf . '.' . $homBuf;
		if ($simplify) {
			/* "simplify" it by removing the trailing part that specifies capitalization, homophone number, ...,
				that is, everything after the SECOND separator byte ('.'), while keeping the 2nd
				separator byte itself (with exception for final '..', see below). Examples:
					'aa1' => 'aa.1.z' =>'aa.1.'
					'aa1*' => 'aa.1.a' => 'aa.1.'
					'naam4 tung4 jyun6/2' => 'naamtungjyun.446.z' => 'naamtungjyun.446.'
					'3 naam4 tung4 jyun6/2' => 'naamtungjyun.446.d' => 'naamtungjyun.446.'
					'aa' => 'aa.1.z' =>'aa..' -- or 'aa.0.'?
				It's necessary that 'aa' should match 'aa1'. Problem: for (Mandarin) pinyin keys, neutral tone gets
				turned into '0'. But, currently, for Jyutping keys, no '0' is inserted. The mysql wildcard stuff in
				WenlinZhixing::lookupPinyin requires the '0'. For Jyut, 'aa..' won't match 'aa.1.'; 'aa.0.' would be better.
				That would seem to require parsing toneless Jyutping into syllables.
				A shortcut that may work OK at least in the monosyllabic situation is to replace final ".." with "."
				when simplifying. That should also work for toneless polysyllables like "naam tung jyun" or "naamtungjyun".
				It will fail, though, if some syllables have tone digit but others don't, like "naam tung4 jyun6". */
			$key = self::makeJyutpingCollationKey($pin, false);
			$len = 0;
			for ($i = 0; $i < 2; $i++) {
				$len = strpos($key, '.', $len + 1);
			}
			$key = substr($key, 0, $len + 1);
			$key = preg_replace('/(\.\.)$/u', '.', $key); // change final '..' to '.'
		}
		return $key;
	} // makeJyutpingCollationKey

	/** zyxPosition
		Construct a code representing the position of an item affecting a collation key.
		For compactness, we use letters rather than numbers, to indicate the
		position where umlaut, apostrophe, or capitalization occurs. The
		sooner these occur, the later the string should be sorted, so we assign
		letters in reverse alphabetical order. For example:
		"fRog" sorts before "Frog", which sorts before "FROG"; the corresponding
		values for the capitalization key are "y", "z", and "zyxw".

		@param $position the position of the umlaut/apostrophe/etc., relative to the beginning of the string.
		@return the collation code.

		Compare homNumABC.
	*/
	private static function zyxPosition($position)
	{
		// 0 through 24 use single letter 'z', 'y', ... , 'c', 'b'
		if ($position <= 24) {
			return self::$abc[25 - $position];
		}
		// 25 through 700 use three letters "azz", "azy", ... , "aaa"
		$position -= 25;
		if ($position < 26*26) {
			/* Caution: "There is no integer division operator in PHP. 1/2 yields the float 0.5.
				The value can be casted to an integer to round it towards zero..."
				Before fixing with "(int)" cast, we got "Notice: String offset cast occurred..." */
			$a = (int) ($position / 26);
			$b = (int) ($position % 26);
			return 'a' . self::$abc[25 - $a] . self::$abc[25 - $b];
		}
		return '??';
	} // zyxPosition

	/** compressPinyinCollationKey:
		Compress the given Pinyin collation key.

		@param $abc the key to be compressed.
		@return the compressed key.

		Called by makePinyinCollationKey.

		Keys use a-z, 0-4, and . (period/separator).
		Squeeze into 3 bits for period (000), 5 bits per letter,
		and 2 or 3 bits per digit.
		Don't use 00001, 00010, 00011, since conflict with
		using 000 for period.
		00100 (4) through 11111 (31) are available for a-z.
		That leaves two extra; reserve first and last for
		future expansion, one before 'a' and one after 'z'.
			000 = '.'
			00001, 00010, 00011 = illegal
			00100 = reserved
			00101, 00110, ... 11110 = 'a', 'b', ..., 'z'
			11111 = reserved

		Tones are only 0-4. So 2 or 3 bits enough:
			001 = '0'	(3 bits)
			01  = '1'	(2 bits)
			100 = '2'	(3 bits)
			101 = '3'	(3 bits)
			11  = '4'	(2 bits)

		The compressed keys are 53% the size of the uncompressed keys, on average.
		The average uncompressed size is 17 bytes, average compressed size is 9 bytes.
	*/
	private static function compressPinyinCollationKey($abc)
	{
		$key = '';
		$toMask = 0x80;
		$abcLen = strlen($abc);
		$to = 0;
		for ($i = 0; $i < $abcLen; $i++) {
			$c = $abc[$i];
			if ($c == '.') { // emit 3 zero bits 000
				for ($j = 0; $j < 3; $j++) {
					$toMask >>= 1;
					if ($toMask == 0) {
						$toMask = 0x80;
						$key .= $to;
						$to = 0;
					}
				}
			}
			elseif ($c >= '0' && $c <= '4') {
				$tone = ['001', '01', '100', '101', '11'];
				$bits = $tone[intval($c)];
				$bitsLen = strlen($bits);
				for ($j = 0; $j < $bitsLen; $j++) {
					if ($bits[$j] == '1') {
						$to |= $toMask;
					}
					$toMask >>= 1;
					if ($toMask == 0) {
						$toMask = 0x80;
						$key .= $to;
						$to = 0;
					}
				}
			}
			else { // emit 5 bits for a-z
				/* 00101, 00110, ... 11110 = 'a', 'b', ..., 'z' */
				$b = ord($c) - ord('a') + 5; // binary 00101 = decimal 5
				for ($mask = 0x10 /* 1 << 5 */; $mask != 0; $mask >>= 1) {
					if ($b & $mask) {
						$to |= $toMask;
					}
					$toMask >>= 1;
					if ($toMask == 0) {
						$toMask = 0x80;
						$key .= $to;
						$to = 0;
					}
				}
			}
		}
		if ($to != 0) {
			$key .= $to;
		}
		return $key;
	} // compressPinyinCollationKey

	/** makeEnglishCollationKey:
		Make an English collation key for the given English string.

		Get a shift-trimmed sort key for the given source string.

		We want to sort in this order: "am", "a.m.", "ax" (as in New World Dictionary, 2nd college
		edition, for example). Also, "less" before "-less"; "coop" before "co-op"; and "cant" before "can't".
		That is, we ignore punctuation except at the quaternary level, where strings without punctuation
		sort before strings with punctuation.

		In ISO 14651 this is called "forward,position"; in Unicode tr10 it's called "shift-trimmed".
		(See "shift-trimmed" in <http://unicode.org/reports/tr10/>.)

		ICU doesn't support shift-trimmed keys directly. We can use ICU to get a "shifted" key and
		then "trim" it ourselves. (I inquired on the ICU mailing list about this in Oct 2009, but there
		was no reply.)

		@param $s the English string.
		@param $simplify true if we should "simplify" the key.
		@return the collation key.

		See MakeABCEnglishKeyWithICU and ucol_getShiftTrimmedSortKey in ch_icu.c
	*/
	public static function makeEnglishCollationKey($s, $simplify = false)
	{
		$s = preg_replace('/\([^)]*\)/', '', $s); // Convert "ay(e)" to "ay"

		/* Also remove mid-dot U+00B7 and prime U+2032 (for look-up, not so important for indexing. */
		$u00B7 = WenlinUtf::zinToStr(0x00B7);
		$u2032 = WenlinUtf::zinToStr(0x2032);
		$s = preg_replace("/($u00B7|$u2032)/", '', $s);

		$homophoneNumber = self::getHomophoneNumber($s); // remove hom num from s
		$homBuf = self::homNumABC($homophoneNumber, false /* $entryHasAsterisk, unused for English */);
		static $coll = null;
		if (!$coll) {
			$coll = self::newEnglishCollator();
			if (!$coll) {
				return $s;
			}
		}
		static $varTopTrim = null;
		if (!$varTopTrim) {
			$varTopTrim = self::newVarTopTrim($coll); // like "\x1C..\xFF", supposing varTop = 0x1B.
		}
		$key = $coll->getSortKey($s);
		$key = rtrim($key, $varTopTrim) . "\x01" . $homBuf;
		if ($simplify) {
			/* "simplify" it by removing the trailing part that specifies capitalization, homophone number, ...,
				but keeping the separator byte (0x01). */
			$len = strcspn($key, chr(1));
			$key = substr($key, 0, $len + 1);
		}
		return $key;
	} // makeEnglishCollationKey

	/** newEnglishCollator:
		Make an English collator.

		Use ICU, by way of the Collator class: <http://php.net/manual/en/class.collator.php>

		@return the Collator, or null for error.
	*/
	public static function newEnglishCollator()
	{
		if (!class_exists('Collator', false)) {
			error_log("Wenlin cannot make English collator: no class Collator");
			return null;
		}
		$coll = new Collator("en_US");
		$coll->setAttribute(Collator::ALTERNATE_HANDLING, Collator::SHIFTED);
		$coll->setAttribute(Collator::NORMALIZATION_MODE, Collator::ON);
		$coll->setAttribute(Collator::STRENGTH, Collator::QUATERNARY);
		return $coll;
	} // newEnglishCollator

	/** newVarTopTrim:
		Make a string like "\x1C..\xFF", supposing varTop = 0x1B.
		This is for making a shift-trimmed collation key.

		@param Collator $coll the collator.
		@return the string like "\x1C..\xFF".
	*/
	public static function newVarTopTrim(Collator $coll)
	{
		/* Need to know varTop. Collator class doesn't seem to have equivalent of ucol_getVariableTop.
			We can figure it out by comparing 'a' and '-a' keys; first differing byte in 'a' is varTop + 1.
				a  →	 29 01 05 01 05 01 1c
				-a →	 29 01 05 01 05 01 05 0e 1c
			Evidently, varTop = 0x1c for this collator and this ICU version.
			Note that it doesn't work for 'a' and 'a.':
				a →	 29 01 05 01 05 01 1c
				a. →	 29 01 05 01 05 01 1c 08
		*/
		$ka = $coll->getSortKey('a');
		$kb = $coll->getSortKey('-a');
		$len = strlen($ka);
		$varTopPlus = 0;
		for ($i = 0; $i < $len; $i++) {
			if ($ka[$i] != $kb[$i]) {
				$varTopPlus = ord($ka[$i]);
				break;
			}
		}
		if ($varTopPlus == 0) {
			error_log("Wenlin failed to determine variable top for English collation");
		}
		return chr($varTopPlus) . '..\xFF';
	} // newVarTopTrim

	////////////////////////////////////////////

	/** getHomophoneNumber:
		Strip the homophone number (if any), and return it (or 0 if none).

		@param $p the headword, from which the hom num will be stripped and returned.
		@return the homophone number, or 0.

		This is used for both English and Pinyin collation.
	*/
	private static function getHomophoneNumber(&$p)
	{
		$homophoneNumber = 0;
		while (preg_match('/^(⁰|¹|²|³|⁴|⁵|⁶|⁷|⁸|⁹|[0-9])/', $p, $matches)) {
			$m = $matches[1];
			$p = preg_replace("/^$m/", '', $p);
			$n = self::replaceSuperscript($m);
			$homophoneNumber *= 10;
			$homophoneNumber += $n;
		}
		return $homophoneNumber;
	} // getHomophoneNumber

	/** replaceSuperscript:
		@param $digit a superscript or ordinary digit
		@return a number 0 thru 9.

		This doesn't work since PHP doesn't understand UTF-8:
			$m = strtr($m, '⁰¹²³⁴⁵⁶⁷⁸⁹', '0123456789');
	*/
	private static function replaceSuperscript($digit)
	{
		$super = ['⁰' => 0, '¹' => 1, '²' => 2, '³' => 3, '⁴' => 4, '⁵' => 5, '⁶' => 6, '⁷' => 7, '⁸' => 8, '⁹' => 9];
		if (isset($super[$digit])) { // isset is faster than array_key_exists
			return $super[$digit];
		}
		return intval($digit);
	} // replaceSuperscript

	/** homNumABC
		Get a code representing a homophone number or asterisk, to be used in a collation key.

		@param $n the homophone number.
		@param boolean $entryHasAsterisk true if entry has asterisk (like “cídiǎn*”), else false.
		@return the collation code.

		This is used for both English and Pinyin collation.

		Compare zyxPosition.
	*/
	private static function homNumABC($n, $entryHasAsterisk)
	{
		/* 'a' is for asterisk, which can go with either homophone number 1 or no homophone number.
		   Asterisk marks the most frequently used vocabulary item in a group of near-homophones
		   (i.e., items that are homophonous if tones are ignored). */
		if ($entryHasAsterisk) {
			return 'a';	// 'a' is for asterisk!
		}
		/* For homophone number zero (i.e., no homophone number), use 'z'. In the original design,
		   in the published dictionary, in a group of two or more homophones, each entry should
		   have a homophone number. If the user adds an entry to such a group, without giving it
		   a homophone number or asterisk, it should sort last. Hence the 'z'. After Wenlin 4.2.2,
		   however, with the publication of ABC+HDC, we use homophone numbers 1001 and beyond for HDC,
		   and a group of homophones in which only one is an ABC entry shouldn't have a homophone
		   number on the ABC entry. We want the HDC entries to sort below the ABC entries, so the
		   keys for 1001 and beyond must sort after 'z'; see below. */
		if ($n == 0) {
			return 'z';
		}
		/* For 1 thru 23 use single letters 'b' thru 'x'. */
		if ($n <= 23) {
			return self::$abc[$n];
		}
		if ($n < 700) { // 700 = 24 + 26*26
			/* For 24 thru 699 use "yaa", "yab", ... , "yzz".
				Published Wenlin versions through 4.2.2 only supported up to 699 = "yzz". */
			/* Caution: "There is no integer division operator in PHP. 1/2 yields the float 0.5.
				The value can be casted to an integer to round it towards zero..." */
			$a = (int) (($n - 24) / 26);
			$b = (int) (($n - 24) % 26);
			return 'y' . self::$abc[$a] . self::$abc[$b];
		}
		if ($n < 1001) {
			/* For 700 thru 1000 use "yzzaa", "yzzab", ... , "yzzlo".
			   New for Wenlin versions after 4.2.2. There's no particular need for them yet, but don't want
			   to leave a gap for 700 thru 1000. We'll never use "yzzlp" thru "yzzzz", since we switch
			   over to "zaa", ... at 1001. */
			$a = (int) (($n - 700) / 26);
			$b = (int) (($n - 700) % 26);
			return 'yzz' . self::$abc[$a] . self::$abc[$b];
		}
		if ($n < 1677) { // 1677 = 1001 + 26*26
			/* For 1001 thru 1676 use "zaa", "zab", ... , "zzz". New for Wenlin versions after 4.2.2.
			   Important: the choice of 1001 as the starting point for "zaa", ... isn't arbitrary.
			   It enables the use of homophone numbers 1001 and beyond for HDC (Hanyu Da Cidian).
			   Entries with no homophone number and no asterisk get single "z". We want HDC entries,
			   all of which will have homophone number >= 1001, to sort after ABC entries. */
			$a = (int) (($n - 1001) / 26);
			$b = (int) (($n - 1001) % 26);
			return 'z' . self::$abc[$a] . self::$abc[$b];
		}
		if ($n < 19253) { // 2353 = 1677 + 26*26*26
			/* For 1677 thru 19252 use "zzzaaa", "zzzaab", ... , "zzzzzz".
			   New for Wenlin versions after 4.2.2.
			   HDC only has up to 170 homophones, so we only expect to use 1001 thru 1170 for HDC.
			   Here we allow for some extra thousands. If we ever need to go even further, we might
			   go from "zzzzzzaaaaaa" up to "zzzzzzzzzzzz", and so forth. */
			$a = (int) (($n - 1677) / (26*26));
			$b = (int) ((($n - 1677) / 26) % 26);
			$c = (int) (($n - 1677) % 26);
			return 'zzz' . self::$abc[$a] . self::$abc[$b] . self::$abc[$c];
		}
		return '??';
	} // homNumABC

} // class WenlinCollate
