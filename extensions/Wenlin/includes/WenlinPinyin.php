<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

class WenlinPinyin
{
	const SPY_TONE_MASK = 7;
	const TONE_WAS_DIGIT_FLAG = 0x80;

	/* $validPy is derived from WenlinTushuguan/Text/transcriptions.wenlin,
	   all lines matching the regex "/^([a-zü]+)\s.+/"; that is, all the neutral-tone
	   syllables. */
	private static $validPy = [
		'a' => 1,
		'ai' => 1,
		'an' => 1,
		'ang' => 1,
		'ao' => 1,
		'ba' => 1,
		'bai' => 1,
		'ban' => 1,
		'bang' => 1,
		'bao' => 1,
		'bei' => 1,
		'ben' => 1,
		'beng' => 1,
		'bi' => 1,
		'bian' => 1,
		'biang' => 1,
		'biao' => 1,
		'bie' => 1,
		'bin' => 1,
		'bing' => 1,
		'bo' => 1,
		'bu' => 1,
		'ca' => 1,
		'cai' => 1,
		'can' => 1,
		'cang' => 1,
		'cao' => 1,
		'ce' => 1,
		'cei' => 1,
		'cen' => 1,
		'ceng' => 1,
		'cha' => 1,
		'chai' => 1,
		'chan' => 1,
		'chang' => 1,
		'chao' => 1,
		'che' => 1,
		'chen' => 1,
		'cheng' => 1,
		'chi' => 1,
		'chong' => 1,
		'chou' => 1,
		'chu' => 1,
		'chua' => 1,
		'chuai' => 1,
		'chuan' => 1,
		'chuang' => 1,
		'chui' => 1,
		'chun' => 1,
		'chuo' => 1,
		'ci' => 1,
		'cong' => 1,
		'cou' => 1,
		'cu' => 1,
		'cuan' => 1,
		'cui' => 1,
		'cun' => 1,
		'cuo' => 1,
		'da' => 1,
		'dai' => 1,
		'dan' => 1,
		'dang' => 1,
		'dao' => 1,
		'de' => 1,
		'dei' => 1,
		'den' => 1,
		'deng' => 1,
		'di' => 1,
		'dia' => 1,
		'dian' => 1,
		'diao' => 1,
		'die' => 1,
		'din' => 1,
		'ding' => 1,
		'diu' => 1,
		'dong' => 1,
		'dou' => 1,
		'du' => 1,
		'duan' => 1,
		'dui' => 1,
		'dun' => 1,
		'duo' => 1,
		'e' => 1,
		'ei' => 1,
		'en' => 1,
		'eng' => 1,
		'er' => 1,
		'fa' => 1,
		'fan' => 1,
		'fang' => 1,
		'fei' => 1,
		'fen' => 1,
		'feng' => 1,
		'fiao' => 1,
		'fo' => 1,
		'fou' => 1,
		'fu' => 1,
		'ga' => 1,
		'gai' => 1,
		'gan' => 1,
		'gang' => 1,
		'gao' => 1,
		'ge' => 1,
		'gei' => 1,
		'gen' => 1,
		'geng' => 1,
		'gong' => 1,
		'gou' => 1,
		'gu' => 1,
		'gua' => 1,
		'guai' => 1,
		'guan' => 1,
		'guang' => 1,
		'gui' => 1,
		'gun' => 1,
		'guo' => 1,
		'ha' => 1,
		'hai' => 1,
		'han' => 1,
		'hang' => 1,
		'hao' => 1,
		'he' => 1,
		'hei' => 1,
		'hen' => 1,
		'heng' => 1,
		'hm' => 1,
		'hng' => 1,
		'hong' => 1,
		'hou' => 1,
		'hu' => 1,
		'hua' => 1,
		'huai' => 1,
		'huan' => 1,
		'huang' => 1,
		'hui' => 1,
		'hun' => 1,
		'huo' => 1,
		'ji' => 1,
		'jia' => 1,
		'jian' => 1,
		'jiang' => 1,
		'jiao' => 1,
		'jie' => 1,
		'jin' => 1,
		'jing' => 1,
		'jiong' => 1,
		'jiu' => 1,
		'ju' => 1,
		'juan' => 1,
		'jue' => 1,
		'jun' => 1,
		'ka' => 1,
		'kai' => 1,
		'kan' => 1,
		'kang' => 1,
		'kao' => 1,
		'ke' => 1,
		'kei' => 1,
		'ken' => 1,
		'keng' => 1,
		'kong' => 1,
		'kou' => 1,
		'ku' => 1,
		'kua' => 1,
		'kuai' => 1,
		'kuan' => 1,
		'kuang' => 1,
		'kui' => 1,
		'kun' => 1,
		'kuo' => 1,
		'la' => 1,
		'lai' => 1,
		'lan' => 1,
		'lang' => 1,
		'lao' => 1,
		'le' => 1,
		'lei' => 1,
		'len' => 1,
		'leng' => 1,
		'li' => 1,
		'lia' => 1,
		'lian' => 1,
		'liang' => 1,
		'liao' => 1,
		'lie' => 1,
		'lin' => 1,
		'ling' => 1,
		'liu' => 1,
		'lo' => 1,
		'long' => 1,
		'lou' => 1,
		'lu' => 1,
		'luan' => 1,
		'lun' => 1,
		'luo' => 1,
		'lü' => 1,
		'lüe' => 1,
		'm' => 1,
		'ma' => 1,
		'mai' => 1,
		'man' => 1,
		'mang' => 1,
		'mao' => 1,
		'me' => 1,
		'mei' => 1,
		'men' => 1,
		'meng' => 1,
		'mi' => 1,
		'mian' => 1,
		'miao' => 1,
		'mie' => 1,
		'min' => 1,
		'ming' => 1,
		'miu' => 1,
		'mo' => 1,
		'mou' => 1,
		'mu' => 1,
		'n' => 1,
		'na' => 1,
		'nai' => 1,
		'nan' => 1,
		'nang' => 1,
		'nao' => 1,
		'ne' => 1,
		'nei' => 1,
		'nen' => 1,
		'neng' => 1,
		'ng' => 1,
		'ni' => 1,
		'nia' => 1,
		'nian' => 1,
		'niang' => 1,
		'niao' => 1,
		'nie' => 1,
		'nin' => 1,
		'ning' => 1,
		'niu' => 1,
		'nong' => 1,
		'nou' => 1,
		'nu' => 1,
		'nuan' => 1,
		'nun' => 1,
		'nuo' => 1,
		'nü' => 1,
		'nüe' => 1,
		'o' => 1,
		'ou' => 1,
		'pa' => 1,
		'pai' => 1,
		'pan' => 1,
		'pang' => 1,
		'pao' => 1,
		'pei' => 1,
		'pen' => 1,
		'peng' => 1,
		'pi' => 1,
		'pian' => 1,
		'piao' => 1,
		'pie' => 1,
		'pin' => 1,
		'ping' => 1,
		'po' => 1,
		'pou' => 1,
		'pu' => 1,
		'qi' => 1,
		'qia' => 1,
		'qian' => 1,
		'qiang' => 1,
		'qiao' => 1,
		'qie' => 1,
		'qin' => 1,
		'qing' => 1,
		'qiong' => 1,
		'qiu' => 1,
		'qu' => 1,
		'quan' => 1,
		'que' => 1,
		'qun' => 1,
		'r' => 1,
		'ran' => 1,
		'rang' => 1,
		'rao' => 1,
		're' => 1,
		'ren' => 1,
		'reng' => 1,
		'ri' => 1,
		'rong' => 1,
		'rou' => 1,
		'ru' => 1,
		'rua' => 1,
		'ruan' => 1,
		'rui' => 1,
		'run' => 1,
		'ruo' => 1,
		'sa' => 1,
		'sai' => 1,
		'san' => 1,
		'sang' => 1,
		'sao' => 1,
		'se' => 1,
		'sen' => 1,
		'seng' => 1,
		'sha' => 1,
		'shai' => 1,
		'shan' => 1,
		'shang' => 1,
		'shao' => 1,
		'she' => 1,
		'shei' => 1,
		'shen' => 1,
		'sheng' => 1,
		'shi' => 1,
		'shou' => 1,
		'shu' => 1,
		'shua' => 1,
		'shuai' => 1,
		'shuan' => 1,
		'shuang' => 1,
		'shui' => 1,
		'shun' => 1,
		'shuo' => 1,
		'si' => 1,
		'song' => 1,
		'sou' => 1,
		'su' => 1,
		'suan' => 1,
		'sui' => 1,
		'sun' => 1,
		'suo' => 1,
		'ta' => 1,
		'tai' => 1,
		'tan' => 1,
		'tang' => 1,
		'tao' => 1,
		'te' => 1,
		'tei' => 1,
		'teng' => 1,
		'ti' => 1,
		'tian' => 1,
		'tiao' => 1,
		'tie' => 1,
		'ting' => 1,
		'tong' => 1,
		'tou' => 1,
		'tu' => 1,
		'tuan' => 1,
		'tui' => 1,
		'tun' => 1,
		'tuo' => 1,
		'wa' => 1,
		'wai' => 1,
		'wan' => 1,
		'wang' => 1,
		'wei' => 1,
		'wen' => 1,
		'weng' => 1,
		'wo' => 1,
		'wong' => 1,
		'wu' => 1,
		'xi' => 1,
		'xia' => 1,
		'xian' => 1,
		'xiang' => 1,
		'xiao' => 1,
		'xie' => 1,
		'xin' => 1,
		'xing' => 1,
		'xiong' => 1,
		'xiu' => 1,
		'xu' => 1,
		'xuan' => 1,
		'xue' => 1,
		'xun' => 1,
		'ya' => 1,
		'yan' => 1,
		'yang' => 1,
		'yao' => 1,
		'ye' => 1,
		'yi' => 1,
		'yin' => 1,
		'ying' => 1,
		'yo' => 1,
		'yong' => 1,
		'you' => 1,
		'yu' => 1,
		'yuan' => 1,
		'yue' => 1,
		'yun' => 1,
		'za' => 1,
		'zai' => 1,
		'zan' => 1,
		'zang' => 1,
		'zao' => 1,
		'ze' => 1,
		'zei' => 1,
		'zen' => 1,
		'zeng' => 1,
		'zha' => 1,
		'zhai' => 1,
		'zhan' => 1,
		'zhang' => 1,
		'zhao' => 1,
		'zhe' => 1,
		'zhei' => 1,
		'zhen' => 1,
		'zheng' => 1,
		'zhi' => 1,
		'zhong' => 1,
		'zhou' => 1,
		'zhu' => 1,
		'zhua' => 1,
		'zhuai' => 1,
		'zhuan' => 1,
		'zhuang' => 1,
		'zhui' => 1,
		'zhun' => 1,
		'zhuo' => 1,
		'zi' => 1,
		'zong' => 1,
		'zou' => 1,
		'zu' => 1,
		'zuan' => 1,
		'zui' => 1,
		'zun' => 1,
		'zuo' => 1,
	]; // $validPy

	private static $uniPrecomp = [ // for tone marks, use 1-4; ü=v0, Ü=V0
		'À' => 'A4', // U+00c0
		'Á' => 'A2', // U+00c1
		'È' => 'E4', // U+00c8
		'É' => 'E2', // U+00c9
		'Ì' => 'I4', // U+00cc
		'Í' => 'I2', // U+00cd
		'Ò' => 'O4', // U+00d2
		'Ó' => 'O2', // U+00d3
		'Ù' => 'U4', // U+00d9
		'Ú' => 'U2', // U+00da
		'Ü' => 'V0', // U+00dc
		'à' => 'a4', // U+00e0
		'á' => 'a2', // U+00e1
		'è' => 'e4', // U+00e8
		'é' => 'e2', // U+00e9
		'ì' => 'i4', // U+00ec
		'í' => 'i2', // U+00ed
		'ò' => 'o4', // U+00f2
		'ó' => 'o2', // U+00f3
		'ù' => 'u4', // U+00f9
		'ú' => 'u2', // U+00fa
		'ü' => 'v0', // U+00fc
		'Ā' => 'A1', // U+0100
		'ā' => 'a1', // U+0101
		'Ē' => 'E1', // U+0112
		'ē' => 'e1', // U+0113
		'Ě' => 'E3', // U+011a
		'ě' => 'e3', // U+011b
		'Ī' => 'I1', // U+012a
		'ī' => 'i1', // U+012b
		'Ń' => 'N2', // U+0143
		'ń' => 'n2', // U+0144
		'Ň' => 'N3', // U+0147
		'ň' => 'n3', // U+0148
		'Ō' => 'O1', // U+014c
		'ō' => 'o1', // U+014d
		'Ū' => 'U1', // U+016a
		'ū' => 'u1', // U+016b
		'Ǎ' => 'A3', // U+01cd
		'ǎ' => 'a3', // U+01ce
		'Ǐ' => 'I3', // U+01cf
		'ǐ' => 'i3', // U+01d0
		'Ǒ' => 'O3', // U+01d1
		'ǒ' => 'o3', // U+01d2
		'Ǔ' => 'U3', // U+01d3
		'ǔ' => 'u3', // U+01d4
		'Ǖ' => 'V1', // U+01d5
		'ǖ' => 'v1', // U+01d6
		'Ǘ' => 'V2', // U+01d7
		'ǘ' => 'v2', // U+01d8
		'Ǚ' => 'V3', // U+01d9
		'ǚ' => 'v3', // U+01da
		'Ǜ' => 'V4', // U+01db
		'ǜ' => 'v4', // U+01dc
		'Ǹ' => 'N4', // U+01f8
		'ǹ' => 'n4', // U+01f9
		'ḿ' => 'm2', // U+1e3f
	]; // uniPrecomp

	/** validateAndSplit:
		Confirm that the given pinyin syllable is valid, and split it into toneless pinyin and tone.

		@param $py the pinyin syllable.
		@return an array with two elements: the toneless pinyin (or '' if invalid), and the tone number 0-4.
	*/
	public static function validateAndSplit($py)
	{
		$py = self::removeToneChangeNotation($py);
		$pyToneless = '';
		$pyTonelessLower = ''; // could use mb_convert_case instead
		$tone = 0;
		$gotTone = false;
		$a = WenlinUtf::explode($py);
		$len = count($a);
		for ($i = 0; $i < $len; $i++) {
			$c = $a[$i];
			if ($c >= 'a' && $c <= 'z') {
				$pyToneless .= $c;
				$pyTonelessLower .= $c;
				continue;
			}
			if ($c >= 'A' && $c <= 'Z') {
				$pyToneless .= $c;
				$pyTonelessLower .= chr(ord($c) + 0x0020); // or could use strtolower
				continue;
			}
			if ($gotTone) {
				// error_log("validateAndSplit: gotTone ($py)");
				return ['', 0];
			}
			$z = WenlinUtf::ziNumber($c);
			if ($pyToneless != '') {
				$toneDigit = self::pinyinDiacritic($z); // tone mark, as a non-spacing diacritic
				if ($toneDigit != '') {
					$tone = 0 + $toneDigit;
					$gotTone = true;
					continue;
				}
			}
			list($ascii, $toneDigit) = self::uniPyChar($c); // uses 'v' for 'ü'
			if ($ascii == '') {
				// error_log("validateAndSplit: uniPyChar empty ascii ($py)");
				return ['', 0];
			}
			if ($toneDigit != '0') {
				$tone = 0 + $toneDigit;
				$gotTone = true;
			}
			if ($ascii == 'v') {
				$pyToneless .= 'ü';
				$pyTonelessLower .= 'ü';
			}
			elseif ($ascii == 'V') {
				$pyToneless .= 'Ü';
				$pyTonelessLower .= 'ü';
			}
			else {
				$pyToneless .= $ascii;
				if ($ascii >= 'A' && $ascii <= 'Z') {
					$pyTonelessLower .= chr(ord($ascii) + 0x0020); // or could use strtolower
				}
				else {
					$pyTonelessLower .= $ascii;
				}
			}
		}
		if (!isset(self::$validPy[$pyTonelessLower])) {
			// error_log("validateAndSplit: not valid [$pyTonelessLower]");
			return ['', 0];
		}
		return [$pyToneless, $tone];
	} // validateAndSplit

	/** pinyinDiacritic
		If input is one of the 4 Unicode diacritics for a tone mark, then
		return the tone number, as a char '1', '2', '3', '4'. Else return '' (empty string).

		@param $zin the character code point (USV).
		@return '1', '2', '3', '4', or '' (empty string).

		Called from WenlinCollate as well as locally.
	*/
	public static function pinyinDiacritic($zin)
	{
		if ($zin == 0x0304) {
			return '1';	// tone1 -- macron -- U+0304
		}
		if ($zin == 0x0301) {
			return '2';	// tone2 -- acute -- U+0301
		}
		if ($zin == 0x030c) {
			return '3';	// tone3 -- hacek -- U+030c
		}
		if ($zin == 0x0300) {
			return '4';	// tone4 -- grave -- U+0300
		}
		return '';	// not a diacritic
	} // pinyinDiacritic

	/** uniPyChar
		Translate a Unicode pinyin vowel or 'm' or 'n' with tonemark
		into an ASCII letter, plus a number for the tone.

		@param $c the character.
		@return a list of two items:
			(1) ASCII letter, or '' (empty string) if input isn't pinyin letter with tone mark.
			(2) tone: '0' (ü=v0), '1', '2', '3', '4', or '' (not found).
	*/
	public static function uniPyChar($c)
	{
		/* Note: isset is faster than array_key_exists. They're not exactly the same: when the array
			key exists but the value is null, isset returns false and array_key_exists returns true; but
			we don't have null values in $uniPrecomp. */
		if (!isset(self::$uniPrecomp[$c])) {
			return ['', ''];
		}
		$p = self::$uniPrecomp[$c];
		$a = mb_substr($p, 0, 1);
		$b = mb_substr($p, 1, 1);
		return [$a, $b];
	} // uniPyChar

	/** removeToneChangeNotation
		Remove any tone-change diacritics (U+0323 and U+0320 or precomposed versions) from the given string.

		@param $s the string.
		@return the string without tone-change diacritics.

		ICU is needed on the server as PHP "Internationalization extension":
		<http://php.net/manual/en/intro.intl.php>. We want ICU for English collation as well as for Pinyin.
		"php -m | grep intl" should return 'intl', but originally didn't on my Mac or on the wenlin.biz server.
		WestHost recompiled our PHP for wenlin.biz (dedicated server) with intl, now "php -m | grep intl"
		returns 'intl'. WestHost will NOT recompile our PHP for wenlin.co (shared server) with intl. For now,
		the test wiki on wenlin.co has to do without intl; the Mediawiki work-around should be adequate, though
		slow. WenlinTest.php will report errors when run from the command line.

		To install intl on mac El Capitan, "System Integrity Protection" makes it impossible to install pecl using
		older methods. An alternative is to use <http://php-osx.liip.ch/>:
			curl -s http://php-osx.liip.ch/install.sh | bash -s 5.6
		Add to ~/.bash_profile (before /usr/bin):
			export PATH=$PATH:/usr/local/php5/bin
		To install intl on earlier Mac (Mavericks/Yosemite), see versions of WenlinPinyin.php before 2016-3-31.
	*/
	public static function removeToneChangeNotation($s)
	{
		if ($s === null || strlen($s) === 0 ) {
			return "";
		}
		/* Three steps: Convert to NFD, remove U+0323 and U+0320, convert to NFC. */
		/* Convert to NFD. */
		$useIcuDirectly = class_exists('Normalizer', false);
		if ($useIcuDirectly) {
			if (!Normalizer::isNormalized($s, Normalizer::FORM_D)) {
				$s = Normalizer::normalize($s, Normalizer::FORM_D);
			}
		}
		elseif (defined('MEDIAWIKI')) {
			/* Mediawiki itself has a normalization class:
					https://doc.wikimedia.org/mediawiki-core/master/php/classUtfNormal.html
				It uses ICU if present: "If the 'intl' PHP extension is present, ICU library functions are used which
				are *MUCH* faster than doing this work in pure PHP code." */
			$s = UtfNormal\Validator::toNFD($s);
		}
		else {
			///// error_log("Wenlin cannot remove tone change notation: no class Normalizer and no MEDIAWIKI normalizer");
			return $s;
		}
		/* Remove U+0323 and U+0320. */
		$u0320 = WenlinUtf::zinToStr(0x0320);
		$u0323 = WenlinUtf::zinToStr(0x0323);
		$s = preg_replace("/($u0320|$u0323)/", '', $s);

		/* Convert to NFC. */
		if ($useIcuDirectly) {
			if (!Normalizer::isNormalized($s, Normalizer::FORM_C)) {
				$s = Normalizer::normalize($s, Normalizer::FORM_C); // ICU
			}
		}
		else {
			$s = UtfNormal\Validator::toNFC($s); // MEDIAWIKI
		}
		return $s;
	} // removeToneChangeNotation

	/** isAllPinyin:
		Are the characters in the given string Pinyin?

		@param s the string.
		@return true if the characters in s are all Pinyin, else false.

		Note: we skip "harmless pinyin punctuation" between syllables, but NOT
		at the beginning of the string. I don't remember if this is intentional,
		and don't know whether there would be any bad effects if we also skipped
		"harmless" punctuation at the beginning.
	*/
	public static function isAllPinyin($s)
	{
		$s = self::removeToneChangeNotation($s);
		$a = WenlinUtf::explode($s);
		$len = count($a);
		$pyLen = 0;
		$totalPyLen = 0;
		$ofs = 0;
		while (self::pySpyLen($a, $ofs, $pyLen) != 0) {
			$totalPyLen += $pyLen;
			if ($totalPyLen >= $len) {
				break;
			}
			$ofs += $pyLen;
			$puncLen = self::skipHarmlessPinyinPunct($a, $ofs);
			if ($puncLen != 0) {
				$totalPyLen += $puncLen;
				if ($totalPyLen >= $len) {
					break;
				}
				$ofs += $puncLen;
			}
		}
		if ($totalPyLen < $len) {
			return false;
		}
		return true;
	} // isAllPinyin

	/** skipHarmlessPinyinPunct:
		While parsing a pinyin string, skip past "harmless" punctuation marks.
		Skip not only apostrophe, dash, equals, blank, but also double-quotation marks (ai "zi" bing).
		This is in the context of validating the pinyin version of a dictionary headword, where the
		set of allowable characters is very limited.

		@param py the pinyin input, as an array.
		@param ofs the length of py that has already been parsed.
		@return the length of harmless punctuation marks starting at py + ofs.
	*/
	private static function skipHarmlessPinyinPunct($py, $ofs)
	{
		$puncLen = 0;
		$pyLen = count($py);
		while ($ofs + $puncLen < $pyLen) {
			if (self::isHarmlessPinyinPunct($py[$ofs + $puncLen])) {
				++$puncLen;
			}
			else {
				break;
			}
		}
		return $puncLen;
	} // skipHarmlessPinyinPunct

	/** isHarmlessPinyinPunct:
		Is the given character a "harmless" punctuation mark in the context of parsing pinyin?
		Skip not only apostrophe, dash, equals, blank, but also double-quotation marks (ai "zi" bing).
		These marks all can occur in pinyin without anything corresponding to them in the hanzi version (char band).

		@param c the character in question.
		@return true if c is considered a "harmless" punctuation mark, else false.
	*/
	private static function isHarmlessPinyinPunct($c)
	{
		switch ($c) {
		case ' ': // blank; TODO: also consider U+00A0 NO_BREAK_SPACE, see ALLOW_NBSP_IN_PINYIN_BAND
		case '-': // hyphen
		case '=': // equals sign (backward compatibility? not used in 4.0's cidian)
		case '\'': // apostrophe (or single-quotation mark)
		case "\"": // double-quotation mark
		case '‘': // opening single-quotation mark
		case '’': // closing single-quotation mark
		case '“': // opening double-quotation mark
		case '”': // closing double-quotation mark
		case '̠': /* U+0320 line below -- tone change notation (note: dot below is precomposed in NFC pinyin) */
			return true;
		default:
			return false;
		}
	} // isHarmlessPinyinPunct

	/** pySpyLen:
		Convert the given pinyin syllable into a 16-bit "short" form, and get its length.

		@param py the array containing the (possible) pinyin syllable to be converted.
		@param ofs the offset of the pinyin syllable in py.
		@param len to be set to the length (in code points) of the pinyin string.
		@return spy, or 0 if pinyin string isn't valid.

		Called by: isAllPinyin.

		NOTE: the 16-bit "short pinyin" ("SPY") has been used a lot in C code. Whether or not it's very useful
		for PHP or other languages isn't clear. Currently in PHP pySpyLen is only called by isAllPinyin, which uses
		it for parsing but doesn't actually use the integer value except to compare it with zero (meaning invalid).
	*/
	private static function pySpyLen($py, $ofs, &$len)
	{
		$asciiPY = '';
		$tone = 0;
		$toneOffset = 0;
		$charCount = 0;

		self::pyToAscii($py, $ofs, $asciiPY, $tone, $toneOffset);
		$spy = self::asciiPYSpyLength($asciiPY, $charCount);
		if ($spy == 0) {
			return 0;
		}
		$len = $charCount;
		if ($toneOffset >= $charCount) {
			$tone = 7; // no tone
		}
		else if ($tone & self::TONE_WAS_DIGIT_FLAG) {
			++$len;
		}
		return $spy | ($tone & self::SPY_TONE_MASK);
	} // pySpyLen

	/** pyToAscii:
		Convert a single pinyin syllable (as array of characters) from UTF-8 to ASCII.

		@param py the array containing the (possible) pinyin syllable to be converted.
		@param ofs the offset of the (possible) pinyin syllable in py.
		@param asciiPY used for output.
		@param tonep gets filled in with tone, or SPY_TONE_MASK if no tone.
		@param toneOfsp gets filled in with 0 if tone was on first character,
						 1 if on second character, 2 if on third character, etc.;
						 Ascii digit '0' thru '4' at end counts as being on last abc character.
						 (toneOfsp is used only for determining whether tone is in first syllable,
						 which isn't known until after asciiPYSpyLength)
	*/
	private static function pyToAscii($py, $ofs, &$asciiPY, &$tonep, &$toneOfsp)
	{
		$tonep = self::SPY_TONE_MASK;
		$gotTone = false;
		$pyLen = 0;
		$asciiLen = 0;
		$totalLen = count($py);
		while ($ofs + $pyLen < $totalLen) {
			$z = $py[$ofs + $pyLen];
			$zin = WenlinUtf::ziNumber($z);
			$pyLen++;
			$c = 0;
			if ($zin >= 0x0080) {
				/* note: if already have tone, or if asciiLen > 3, we know this
						isn't part of syllable. BUT: what if the last letter was a
						g, n, or r, and this is a vowel! Then the g/n/r might
						wrongly get attached to this syllable when it should be
						in the next syllable. So we have to keep going to collect
						such a vowel into asciiPY. */
				if ($asciiLen != 0 && ($toneDigit = self::pinyinDiacritic($zin)) != '') {
					// this is the tone mark, as a non-spacing diacritic
					// Unicode character in UTF-8 form.
					// count it as being on previous character
					if (!$gotTone) {
						$tonep = 0 + $toneDigit;
						$toneOfsp = ($asciiLen - 1);
					}
					$gotTone = true;
					continue;
				}
				if ($zin == 0x0308) { // U+0308 umlaut
					/* We get U+0308 separately if there's a dot below u as well as two dots
							above; normally the ü is precomposed (with or without tone mark). */
					if ($asciiPY[$asciiLen - 1] == 'u') {
						$asciiPY[$asciiLen - 1] = 'v';
						continue;
					}
					if ($asciiPY[$asciiLen - 1] == 'U') {
						$asciiPY[$asciiLen - 1] = 'V';
						continue;
					}
					break;
				}
				list($c, $toneDigit) = self::uniPyChar($z); // uses 'v' for 'ü'
				if ($c == '') {
					break;
				}
				if ($gotTone && $toneDigit != '0' && ($c == 'n' || $c == 'm')) {
					break; /* Successive syllabic nasals: n2n2, m2m2 ... e.g. HDC: “哽哽 ńń”. */
				}
				if (!$gotTone && $toneDigit != '0') {
					$gotTone = true;
					$tonep = 0 + $toneDigit;
					$toneOfsp = $asciiLen;
				}
			}
			else {
				$c = $z;
				if (!($c >= 'a' && $c <= 'z') && !($c >= 'A' && $c <= 'Z')) { // we already know c >= 0 and c < 0x80
					if (!$gotTone && $c >= '0' && $c <= '4') {
						$tonep = ((0 + $c) & self::SPY_TONE_MASK) | self::TONE_WAS_DIGIT_FLAG;
						$toneOfsp = ($asciiLen-1);
					}
					break;
				}
			}
			if ($c >= 'A' && $c <= 'Z') {
				$c = strtolower($c);
			}
			$asciiPY .= $c;
			if (++$asciiLen >= 7) { // don't stop at 6; may need next vowel
				break;
			}
		}
	} // pyToAscii

	/** asciiPYSpyLength:
		Convert an ASCII pinyin string into a 16-bit integer abbreviated form, without tone, and
		get the significant length of the pinyin string.

		@param py the pinyin string.
		@param len to be set to the length of the pinyin string.
		@return the spy (short pinyin), or 0 if pinyin string isn't valid.
	*/
	private static function asciiPYSpyLength($py, &$len)
	{
		if (strlen($py) == 0) {
			return 0;
		}
		$p0 = $py[0];
		if (!ctype_alpha($p0)) {
			return 0;
		}
		/* 5 bits = first letter */
		$spy = ((ord($p0) & 0x1f) << 10);

		/* next two bits = 00 if (<h); 01 if (=h); 10 if (=i); 11 if (>i) */
		$greater_i = false;

		$len = 1;
		if (strlen($py) > 1) {
			$p1 = $py[1];
			if ($p1 < 'h') {
				$len = 1;
			}
			else if ($p1 == 'h') {
				if ($p0 != 'z' && $p0 != 'c' && $p0 != 's') {
					$len = 1;
					if ($greater_i && $len >= 2) {
						$spy |= (3 << 8);
					}
					return $spy;
				}
				$spy |= (1 << 8);
				$len = 2;
			}
			else if ($p1 == 'i') { // i = second letter
				$spy |= (2 << 8);
				$len = 2;
			}
			else {
				/* want to do --- spy |= (3 << 8) --- but have to wait,
					since possibly $py{1} isn't part of this syllable */
				$greater_i = true;
				$len = 1;
			}
			/* i, u, or v?   i would be third letter;
						u 2nd or 3rd (after h or i);
						v (u umlaut) would be second letter */
			/* Caution: don't give px a numeric value like 0 -- because then 'i' == 0 will be true!
				Strings of letters may be treated as zero when compared with numeric values!! */
			$px = (strlen($py) > $len) ? $py[$len] : '!';
			switch ($px) {
			case 'i':
				$theRemainder = (1 << 5);
				++$len;
				break;
			case 'u':
				$theRemainder = (2 << 5);
				++$len;
				break;
			case 'v':
				$theRemainder = (3 << 5);
				++$len;
				break;
			default:
				$theRemainder = 0;
			}
			/* a, e,  or o? */
			$px = (strlen($py) > $len) ? $py[$len] : '!';
			switch ($px) {
			case 'a':
				$theRemainder |= (1 << 3);
				++$len;
				break;
			case 'e':
				$theRemainder |= (2 << 3);
				++$len;
				break;
			case 'o':
				$theRemainder |= (3 << 3);
				++$len;
				break;
			}
			/* i=1, m=2, n=3, ng=4, o=5, r=6, u=7 */
			$px = (strlen($py) > $len) ? $py[$len] : '!';
			switch ($px) {
			case 'i':
				$theRemainder |= 1;
				++$len;
				break;
			case 'm': // hm only
				/* note -- assuming $greater_i processed LATER */
				if ($spy == ((ord('h') & 0x1f) << 10) && $len == 1) {
					$theRemainder |= 2;
					++$len;
				}
				break;
			case 'n': // final -n or -ng
				$pxx = (strlen($py) > $len + 1) ? $py[$len + 1] : '!';
				if (self::isVowel($pxx)) {
					break;
				}
				if ($pxx == 'g' && !(strlen($py) > $len + 2 && self::isVowel($py[$len + 2]))) {
					$theRemainder |= 4;
					$len += 2;
				}
				else {
					$theRemainder |= 3;
					++$len;
				}
				break;
			case 'g': // abbreviation: allow -g instead of -ng
				// this is also needed for syllable "ng" without vowel
				$pxx = (strlen($py) > $len + 1) ? $py[$len + 1] : '!';
				if (!self::isVowel($pxx)) {
					$theRemainder |= 4;
					++$len;
				}
				break;
			case 'o': // final o, as in -ao, -iao (NOT -uo!)
				$theRemainder |= 5;
				++$len;
				break;
			case 'r': // er only
				/* note -- assuming greater_i processed LATER! */
				$pxx = (strlen($py) > $len + 1) ? $py[$len + 1] : '!';
				if ($len == 1 && $spy == ((ord('e') & 0x1f) << 10) && !self::isVowel($px)) {
					$theRemainder |= 6;
					++$len;
				}
				break;
			case 'u': // -ou only
				$theRemainder |= 7;
				++$len;
				break;
			}
			if ($theRemainder != 0) {
				/* theRemainder so far is seven bits; compress it
					into five bits. */
				$theRemainder = self::spyOrder($theRemainder);
				if ($theRemainder == 0) {
					return 0;
				}
				$spy |= ($theRemainder << 3);
			}
			if ($greater_i && $len >= 2) {
				$spy |= (3 << 8);
			}
		}
		$pyTonelessLower = substr($py, 0, $len);
		if (!isset(self::$validPy[$pyTonelessLower])) {
			return 0;
		}
		return $spy;
	} // asciiPYSpyLength

	/** isVowel:
		Is the given character one of the ASCII pinyin vowels "AaEeIiOoUuVv"?

		@param c the possible vowel.
		@return true if c is an ASCII pinyin vowel, else false.
	*/
	private static function isVowel($c)
	{
		return isset($c) && $c != '' && strpos('AaEeIiOoUuVv', $c) !== false;
	} // isVowel

	/** spyOrder:
		Get the index into orderTable corresponding to the given part of a pinyin syllable.

		@param r the byte encoding part of a pinyin syllable.
		@return the index into orderTable.
	*/
	private static function spyOrder($r)
	{
		static $orderTable = [
			0x08 /* a */,
			0x09 /* ai */,
			0x0b /* an */,
			0x0c /* ang */,
			0x0d /* ao */,
			0x10 /* e */,
			0x11 /* ei */,
			0x13 /* en */,
			0x14 /* eng */,
			0x20 /* i */,
			0x02 /* m */,
			0x03 /* n */,
			0x04 /* ng */,
			0x18 /* o */,
			0x1c /* ong */,
			0x1f /* ou */,
			0x06 /* r */,
			0x40 /* u */,
			0x60 /* v (u umlaut) */,
			0x48 /* ua */,
			0x49 /* uai */,
			0x4b /* uan */,
			0x4c /* uang */,
			0x50 /* ue */,
			0x70 /* ve */,
			0x41 /* ui */,
			0x43 /* un */,
			0x58 /* uo */
		];
		for ($i = 0; $i < count($orderTable); $i++) {
			if ($r == $orderTable[$i]) {
				return $i + 1;
			}
		}
		return 0;
	} // spyOrder

	/** addToneToSyllable:
		Add the given tone to the given (toneless) pinyin syllable.

		@param pinyinToneless the pinyin syllable, as a string, without tone
		@param tone the tone number 1 through 4; or 0 etc. to return $pinyinToneless unchanged.
		@return the pinyin string with tone mark.

		Called by WenlinZhixing::describePinyinSyllable
	*/
	public static function addToneToSyllable($pinyinToneless, $tone)
	{
		if ($tone < 1 || $tone > 4 || !$pinyinToneless) {
			return $pinyinToneless;
		}
		$a = WenlinUtf::explode($pinyinToneless);
		if (!count($a)) {
			return $pinyinToneless;
		}
		$tonePos = self::tonePosition($a);
		$a[$tonePos] = WenlinUtf::zinToStr(self::makePyUni($a[$tonePos], $tone));
		return join('', $a);
	} // addToneToSyllable

	/** tonePosition:
		Figure out which letter the tone should go on.

		@param a the pinyin syllable, as an array of letters; no tone; either v or ü represents umlaut u.
		@return the 0-based index of the letter in a; that is, 0 for first letter, 1 for second, etc.

		Called by: addToneToSyllable

		Compare the C function TonePosition in ch_pin.c; difference: in PHP this function supports uppercase as well as lowercase.
	*/
	private static function tonePosition($a)
	{
		$ofs = count($a);
		while (--$ofs > 0) {
			switch ($a[$ofs]) {
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				switch ($a[$ofs - 1]) { // note: $ofs >= 1
				case 'a':
				case 'A':
				case 'e':
				case 'E':
				case 'o':
				case 'O':
					return $ofs - 1;
				}
				return $ofs;
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'm':
			case 'M':
			case 'ü':
			case 'Ü':
			case 'v':
			case 'V':
				return $ofs;
			}
		}
		return (count($a) == 2 && ($a[0] == 'h' || $a[0] == 'H') && ($a[1] == 'n' || $a[1] == 'N')) ? 1 : 0;
	} // tonePosition

	/** makePyUni:
		Combine a letter and a tone number to make a letter with a tone diacritic.

		@param vowel the letter (a vowel or one of a few cons. such as 'n'), as a string.
		@param tone the tone number (0-4).
		@return the USV (integer) of the combined letter with a tone diacritic.

		Based on the C function MakePyUni in ch_pin.c.
		TODO: make this more efficient and simpler. Maybe use uniPrecomp backwards. Maybe return the string instead of the USV.
	*/
	private static function makePyUni($vowel, $tone)
	{
		if ($tone > 4 || $tone < 0) {
			$tone = 0;
		}
		if ($vowel == 'ü') {
			$vowel = 'v';
		}
		else if ($vowel == 'Ü') {
			$vowel = 'V';
		}
		$vowelString = "AaEeIiOoUuVv";
		$vowelArray = WenlinUtf::explode($vowelString);
		$vaLen = count($vowelArray);
		$v = 0; //size of vowel
		while ($vowelArray[$v] != $vowel) {
			if (++$v == $vaLen) {
				if ($vowel == 'n' || $vowel == 'N') {
					switch ($tone) {
					case 2:
						return 0x0143 + ($vowel == 'n'); // Ń or ń
					case 3:
						return 0x0147 + ($vowel == 'n'); // Ň or ň
					case 4:
						return 0x01f8 + ($vowel == 'n'); // Ǹ or ǹ
					}
				}
				else if ($vowel == 'm' || $vowel == 'M') {
					switch ($tone) {
					case 2:
						return 0x1e3e + ($vowel == 'm'); // Ḿ or ḿ
					}
				}
				return 0; // error
			}
		}
		$uniPyArray = array(
			/* Since high order byte is always 0 or 1, it's more
				economical just to store low bytes and use simple
				rule to determine high byte.
					However, that may not be true or important for modern PHP code! */
			0x41, 0x00, 0xc1, 0xcd, 0xc0, //// A0, A1, A2, A3, A4
			0x61, 0x01, 0xe1, 0xce, 0xe0, //// a0, a1, a2, a3, a4
			0x45, 0x12, 0xc9, 0x1a, 0xc8, //// E....
			0x65, 0x13, 0xe9, 0x1b, 0xe8, //// e
			0x49, 0x2a, 0xcd, 0xcf, 0xcc, //// I
			0x69, 0x2b, 0xed, 0xd0, 0xec, //// i
			0x4f, 0x4c, 0xd3, 0xd1, 0xd2, //// O
			0x6f, 0x4d, 0xf3, 0xd2, 0xf2, //// o
			0x55, 0x6a, 0xda, 0xd3, 0xd9, //// U
			0x75, 0x6b, 0xfa, 0xd4, 0xf9, //// u
			0xdc, 0xd5, 0xd7, 0xd9, 0xdb, //// V (U umlaut)
			0xfc, 0xd6, 0xd8, 0xda, 0xdc //// v (u umlaut)
		);
		$z = $uniPyArray[$v * 5 + $tone];
		if (($tone != 0) && (($tone & 1) || ($v > 9))) {
			$z |= 0x0100;
		}
		return $z;
	} // makePyUni

	/** convertTonesToDigits
		Transform tone marks to digits.
		Also, transform ü to v and Ü TO V.

		For example, convert 'cífǎ guīlǜ' to 'ci2fa3 gui1lv4'; 'kẹ̌yǐ' to 'ke3yi3';
		and 'nǚyǔhángyuán' to 'nv3yu3hang2yuan2'.

		@param $s the input (source) string of pinyin with tone marks.
		@return the converted (destination) string with digits instead of tone marks.

		Based on the C function ToneToDigitInner in ch_py.c.

		Some code is here specifically to get "er4" instead of "e4r".
		Difficulties: there must not be a consonant preceding the "e",
		since "ke2r" can't be "ker2". Also must not be a vowel following
		"r", since "ke4ren2" can't be "ker4en2"!
	*/
	public static function convertTonesToDigits($s)
	{
		$c = ''; // latest character read from input string
		$zin = 0; //USV of  latest character read from input string
		$prevZin = 0;
		$prevPrevZin = 0;
		$vowel = '';
		$prevVowel = '';
		$tone = ''; // tone digit
		$toneGotten = ''; // tone digit that has been gotten from the input string, not yet appended to output string
		$nGotten = ''; // 'n' or 'N' that has been gotten from the input string, not yet appended to output string
		$gGotten = ''; // 'g' or 'G' that has been gotten from the input string, not yet appended to output string
		$rGotten = ''; // 'r' or 'R' that has been gotten from the input string, not yet appended to output string
		$sa = WenlinUtf::explode($s);
		$sourceLength = count($sa);
		$sourcePosition = 0;
		$d = ''; // destination string to be built up and returned
		for (;;) {
			$prevVowel = $vowel;
			$prevPrevZin = $prevZin;
			$prevZin = $zin;
			if ($sourcePosition >= $sourceLength) {
				$zin = 0;
				$c = '';
			}
			else {
				$c = $sa[$sourcePosition++];
				$zin = WenlinUtf::ziNumber($c);
				// Ignore U+0320 or 0x0323 line/dot below vowel (tone-change notation).
				if ($zin == 0x0320 || $zin == 0x0323) {
					continue;
				}
				// Treat precomposed letters with dot below same as if dot wasn't there.
				$zin2 = self::vowelWithDotBelow($zin);
				if ($zin2 != 0) {
					$zin = $zin2;
					$c = WenlinUtf::zinToStr($zin2);
					/* The cidian entry for lụ̈̌guǎn = lv3guan3 (hotel) has l[U+1ee5][U+0308][U+030c]gu[U+01ce]n
						i.e. the lv3 has: (1) u with dot below, (2) combining diaeresis, (3) combining caron.
						VowelWithDotBelow will give us u, but we'll then have the combining diaresis, which UniPyChar will
						NOT pick up and change into v (as it does when the u+diaresis is precombined i.e. when it doesn't
						have dot below). So we'd better look ahead for that special case. */
					if (($c == 'u' || $c == 'U') && $sourcePosition < $sourceLength) {
						if (WenlinUtf::ziNumber($sa[$sourcePosition]) == 0x0308) { /* combining diaresis */
							$zin++; // u to v, U to V
							$c = ($c == 'u') ? 'v' : 'V'; // u to v, U to V
							$sourcePosition++; // discard the combining diaresis
						}
					}
				}
				else { // Also recognize non-precomposed tone marks.
					$tone = self::pinyinDiacritic($zin);
					if ($tone != '') {
						$toneGotten = $tone;
						continue; // as zin is just the mark only
					}
				}
				list($vowel, $tone) = self::uniPyChar($c); // uses 'v' for 'ü'
				if ($tone == '0') {
					$tone = '';
				}
			}
			if ($toneGotten != '') {
				/* We're waiting to output a tone digit. If we get anything other than a vowel or 'n' or 'g' (or 'r'?), output the tone digit
					first. If we get a final vowel, output it plus the tone digit. If we get 'n' or 'g' (after 'n'), (or 'r'?) must wait until see if it's
					followed by a vowel. */
				if ($gGotten != '') {
					if ($vowel != '' || self::isVowel($c)) { // 'g' goes with following syllable
						$d .= $toneGotten . $gGotten;
					}
					else { // 'g' goes with preceding syllable
						$d .= $gGotten . $toneGotten;
					}
					$toneGotten = $gGotten = '';
				}
				else if ($nGotten != '') {
					if ($c == 'g' || $c == 'G') {
						$d .= $nGotten;
						$gGotten = $c;
						$nGotten = ''; // but $toneGotten still nonempty
						continue;
					}
					else if ($vowel != '' || self::isVowel($c)) { // 'n' goes with following syllable
						$d .= $toneGotten . $nGotten;
					}
					else { // 'n' goes with preceding syllable
						$d .= $nGotten . $toneGotten;
					}
					$toneGotten = $nGotten = '';
				}
				else if ($rGotten != '') {
					if ($vowel != '' || self::isVowel($c)) { // 'r' goes with following syllable
						$d .= $toneGotten . $rGotten;
					}
					else { // 'r' goes with preceding syllable
						$d .= $rGotten . $toneGotten;
					}
					$toneGotten = $rGotten = '';
				}
				else if ($c == 'n' || $c == 'N') {
					$nGotten = $c;
					continue;
				}
				else if (($c == 'r' || $c == 'R')
						&& ($prevVowel == 'e' || $prevVowel == 'E')
						&& !(($prevPrevZin >= 0x0061 /* 'a' */ && $prevPrevZin <= 0x007A /* 'z' */)
							|| ($prevPrevZin >= 0x0041 /* 'A' */ && $prevPrevZin <= 0x005A /* 'Z' */))) {
					/* Only treat R different from other consonants if R is preceded by E and E is NOT preceded by A-Z */
					$rGotten = $c;
					continue;
				}
				else if (self::isVowel($c) && $c != $prevVowel) {
					/* Output vowel before tone, unless it duplicates the toned vowel we just had - that happens only in
						non-pinyin French words etc. and we want to keep the number as close as possible to the letter it came from. */
					$d .= $c . $toneGotten;
					$toneGotten = '';
					continue;
				}
				else { // In all other cases, output the tone before the letter
					$d .= $toneGotten;
					$toneGotten = '';
				}
			}
			if ($c == '') {
				break;
			}
			if ($vowel == '') {
				$d .= $c;
			}
			else {
				$d .= $vowel;
				if ($tone != '') { // handle "lve4"
					$toneGotten = $tone;
				}
			}
		}
		return $d;
	} // convertTonesToDigits

	/** vowelWithDotBelow:
		Get the code point that results from removing a dot below the given code point,
		if the given code point is for a vowel with dot below that we recognize for this purpose.

		@param zin the character code point that might be a vowel with dot below.
		@return the character code point for the vowel without dot below; or 0.
	*/
	private static function vowelWithDotBelow($zin)
	{
		switch ($zin) {
		case 0x1ea1: // a with dot below
			return 0x0061 /* 'a' */;
		case 0x1eb9:
			return 0x0065 /* 'e' */;
		case 0x1ecb:
			return 0x0069 /* 'i' */;
		case 0x1ecd:
			return 0x006F /* 'o' */;
		case 0x1ee5:
			return 0x0075 /* 'u' */;
		case 0x1ea0:
			return 0x0041 /* 'A' */;
		case 0x1eb8:
			return 0x0045 /* 'E' */;
		case 0x1eca:
			return 0x0049 /* 'I' */;
		case 0x1ecc:
			return 0x004F /* 'O' */;
		case 0x1ee4:
			return 0x0055 /* 'U' */;
		default:
			return 0;
		}
	} // vowelWithDotBelow

	/** applyToneChangeNotation:
		Apply tone-change notation to the given pinyin string.

		@param py the pinyin string that may contain tone-change notation, like "kẹ̌yǐ".
		@return the converted pinyin string, like "kéyǐ".

		Caution: changes in output are all lowercase.

		Based on perl code in wenlin/perl/abc_pronounce.pl.
	*/
	public static function applyToneChangeNotation($py)
	{
		$py = preg_replace('/ạ̌/i', 'á', $py);
		$py = preg_replace('/ẹ̌/i', 'é', $py);
		$py = preg_replace('/ị̌/i', 'í', $py);
		$py = preg_replace('/ọ̌/i', 'ó', $py);
		$py = preg_replace('/ụ̌/i', 'ú', $py);
		$py = preg_replace('/ụ̈̌/i', 'ǘ', $py);
		$py = preg_replace('/ī̠/i', 'ì', $py);
		$py = preg_replace('/ị̄/i', 'í', $py);
		$py = preg_replace('/ụ̀/i', 'ú', $py);
		return $py;
	} // applyToneChangeNotation

	/** changeBPbipi:
		If char band contains A-Z, and pinyin contains corresponding capital letter, substitute equivalent pinyin syllable ; only 10 such entries (in gr A-E):
		A-gǔ A股, BB-jī BB机, B-gǔ B股, BP-jī BP机, kǎlā-OK 卡拉OK, OK OK, PC-jī PC机, T-xù shān T恤衫, X-guāng X光, X-jí piàn X级片.
		Only these 8 letters are used (in gr A-E): A, B, C, K, O, P, T, X.
		Per Wiedenhof p. 370: A ēi, B bī, C xī, K kèi, O ōu, P pī, T tī, X àikesī.
		Full alphabet per Wiedenhof is "ēi bī xī dī yī áifu jī èichi āi/ài zhèi kèi áilu áimu ēn ōu pī keyōu ár áisi tī yōu wéi dàboyōu àikesī wài sáide/[ziː]".

		@param py the pinyin string that may contain capital, like "BB-jī".
		@return the converted pinyin string, like "bībījī".

		Based on perl code in wenlin/perl/abc_pronounce.pl.
	*/
	public static function changeBPbipi($py)
	{
		$py = preg_replace('/^A-/', 'ēi', $py); // A-gǔ = A股 = ēigǔ
		$py = preg_replace('/^BB-/', 'bībī', $py); // BB-jī = BB机 = bībījī
		$py = preg_replace('/^B-/', 'bī', $py); // B-gǔ = B股 = bīgǔ
		$py = preg_replace('/^BP-/', 'bīpī', $py); // BP-jī = BP机 = bīpījī
		$py = preg_replace('/-OK$/', 'ōukèi', $py); // kǎlā-OK = 卡拉OK = kǎlā'ōukèi
		$py = preg_replace('/^OK/', 'ōukèi', $py); // OK = OK = ōukèi
		$py = preg_replace('/^PC-/', 'pīxī', $py); // PC-jī = PC机 = pīxījī
		$py = preg_replace('/^T-/', 'tī', $py); // T-xù shān = T恤衫 = tīxùshān
		$py = preg_replace('/^X-/', 'àikesī', $py); // X-guāng = X光 = àikesīguāng; X-jí piàn = X级片 = àikesījípiàn
		return $py;
	} // changeBPbipi

} // class WenlinPinyin
