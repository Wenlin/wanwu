<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

	/* https://ma.ttias.be/how-to-clear-php-opcache/
		https://wenlin.biz/wanwu/extensions/Wenlin/includes/WenlinResetOpcache.php 

		If this produces no visible output, look in ./error_log, which may contain:
		"[10-Jan-2017 11:14:35 America/Denver] PHP Fatal error:  Call to undefined function opcache_reset() in
		/home/wenlinco/public_html/wanwu/extensions/Wenlin/includes/WenlinResetOpcache.php on line ...".
	*/
	if (opcache_reset()) {
		print "The opcode cache was reset.\n";
	}
	else {
		print "The opcode cache is disabled.\n";
	}
?>
