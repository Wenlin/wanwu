<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* WenlinGongju: general-purpose tools for use by other modules in the Wenlin MediaWiki extension.
	¹gōngjù* 工具 {B} n. ①tool; instrument; implement
	| Yǔyán shì yī̠ zhǒng jiāoliú gōngjù. 语言是一种交流工具。 Language is a tool for communication.
	②〈lg.〉 means; instrumental M:²jiàn 件 */

use MediaWiki\MediaWikiServices;
use MediaWiki\Revision\SlotRecord;

class WenlinGongju
{
	/** articlePathBase:
		Get the prefix for relative URLs in our wiki, e.g., "/wow".

		$wgArticlePath is defined in LocalSettings.php, with something like:
			$wgArticlePath = "/wow/$1";
		We don't want to hard-code "wow" everywhere, so we encapsulate it by defining it
		only in LocalSettings. Everywhere else we call this function.

		TODO: make a similar function for getting $wgScriptPath without the leading slash, e.g., "wanwu".
	*/
	public static function articlePathBase()
	{
		global $wgArticlePath;
		static $base = null;
		if ($base === null) {
			$base = str_replace('/$1', '', $wgArticlePath);
		}
		return $base;
	} // articlePathBase

	/** scriptPathNoSlash:
		Get the "script path" for file system paths in our wiki, e.g., "wanwu", without any slash.

		$wgScriptPath is defined in LocalSettings.php, with something like:
			$wgScriptPath = "/wanwu";
		We don't want to hard-code "wanwu" everywhere, so we encapsulate it by defining it
		only in LocalSettings. Everywhere else we (should) call this function.
	*/
	public static function scriptPathNoSlash()
	{
		global $wgScriptPath;
		static $scriptPath = null;
		if ($scriptPath === null) {
			$scriptPath = str_replace('/', '', $wgScriptPath);
		}
		return $scriptPath;
	} // scriptPathNoSlash

	/** getContentTextFromTitleString:
		Given a title string including namespace, like "Zi:好", return the content text
		of the page that has that title.

		@param $titleStr the title string including namespace, like "Zi:好".
		@return the page content text, or null for failure.
	*/
	public static function getContentTextFromTitleString($titleStr)
	{
		$titleObj = Title::newFromText($titleStr);
		return self::getContentTextFromTitleObject($titleObj);
	} // getContentTextFromTitleString

	/** getContentTextFromTitleObject:
		Given a Title object, return the content text of the page that has that title.

		@param Title $titleObj the Title object.
		@return the page content text, or null for failure.
	*/
	public static function getContentTextFromTitleObject($titleObj)
	{
		if (!$titleObj) {
			return null;
		}
		$rev = MediaWikiServices::getInstance()->getRevisionLookup()->getRevisionByTitle($titleObj);
		if (!$rev) {
			MWDebug::log("WenlinGongju::getContentTextFromTitleObject getRevisionByTitle failed\n");
			return null;
		}
		$content = $rev->getContent(SlotRecord::MAIN);
		if (!($content instanceof TextContent)) {
			MWDebug::log("WenlinGongju::getContentTextFromTitleObject content instanceof TextContent failed\n");
			return null;
		}
		return $content->getText();
	} // getContentTextFromTitleObject

	/** makeAdjAlphaLinksWithNsArray:
		Make one or more "list adjacent entries alphabetically" links for the given term,
		in each of the namespaces in the given array.

		@param $term the term.
		@param $namespaceArray the namespaceArray (e.g., array('Ci', 'Jyut', 'En')).
		@return the text link(s), as HTML.
	*/
	public static function makeAdjAlphaLinksWithNsArray($term, $namespaceArray)
	{
		if (count($namespaceArray) == 1) {
			return self::makeAdjacentAlphaLink($term, $namespaceArray[0]);
		}
		$links = '';
		$base = self::articlePathBase();
		$termEnc = urlencode($term);
		foreach ($namespaceArray as $namespaceStr) {
			$links .= "<a href='$base/Special:Wenlin/a?a=$termEnc&ns=$namespaceStr'>"
			. "►list adjacent words alphabetically in namespace $namespaceStr</a><br>";
		}
		return $links;
	} // makeAdjAlphaLinksWithNsArray

	/** makeAdjacentAlphaLink:
		Make a "list adjacent entries alphabetically" link for the given term.

		@param $term the term.
		@param $namespaceStr the namespace (e.g., En, Ci or Jyut).
		@return the text link, as HTML.
	*/
	public static function makeAdjacentAlphaLink($term, $namespaceStr)
	{
		$base = self::articlePathBase();
		$termEnc = urlencode($term);
		return "<a href='$base/Special:Wenlin/a?a=$termEnc&ns=$namespaceStr'>"
			. "►list adjacent words alphabetically</a>";
	} // makeAdjacentAlphaLink

	/** makeFulltextSearchLink:
		Make a fulltext search link for the given term.

		@param $term the search string.
		@return the text link, as HTML.
	*/
	public static function makeFulltextSearchLink($term)
	{
		global $wgScriptPath;
		$url = "$wgScriptPath/index.php?search=$term&title=Special:Search&fulltext=1";
		return "<a href='$url'>►search</a> this wiki for ‘" . $term . "’";
	} // makeFulltextSearchLink

	/** makeWebSearchButtons:
		Append web-search buttons to an entry.

		@param hw the headword (or char band for Ci).
		@param namespace the namespace (Zi, Ci, Jyut, ...).
		@return the text with links.
	*/
	public static function makeWebSearchButtons($hw, $namespaceStr)
	{
		$u = urlencode($hw);
		$web = "<a target=\"_blank\" href=\"http://www.wenlininstitute.org/cgi-bin/wenti.cgi?wenti=$u\">►web links</a> for ‘"
			. $hw . "’<br />\n";
		$pre = "search web for ‘" . $hw . "’: ";
		$google = " <a target=\"_blank\" href=\"https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=$u\">►Google</a>";
		/* Google Translate doesn't currently distinguish Cantonese from Mandarin. */
		$tran = ($namespaceStr == 'En') ? '#en/zh-CN' : '#zh-CN/en'; // the latter is for Zi, Ci, or Jyut.
		$GT = " <a target=\"_blank\" href=\"https://translate.google.com/$tran/$u\">►GT</a>";
		$baidu = " <a target=\"_blank\" href=\"https://www.baidu.com/s?ie=utf-8&wd=$u\">►Baidu</a>";
		$bing = " <a target=\"_blank\" href=\"https://www.bing.com/search?q=$u\">►Bing</a>";
		$yahoo = " <a target=\"_blank\" href=\"https://us.search.yahoo.com/search?fr=yhs-invalid&p=$u\">►Yahoo</a><br />\n";
		return $web . $pre . $google . $GT . $baidu . $bing . $yahoo;
	} // makeWebSearchButtons

	/** makeCreateEntryLinksWithNsArray:
		Make one or more "create new entry" links for the given term,
		in each of the namespaces in the given array.

		@param $term the term.
		@param $namespaceArray the namespaceArray (e.g., array('Ci', 'Jyut', 'En')).
		@return the text link(s), as HTML.

		Example:
			create new entry for ‘中文电脑’ in namespace: ►Jyut ►Ci

		The links are to special pages "Special:Wenlin/create...".
		The code invoked by those links is createNewEntry in WenlinZhixing.php.
	*/
	public static function makeCreateEntryLinksWithNsArray($term, $namespaceArray)
	{
		$base = self::articlePathBase();
		$links = "create new entry for ‘";
		$links .= $term;
		$links .= "’ in namespace";
		foreach ($namespaceArray as $namespaceStr) {
			$links .= " <a href='$base/Special:Wenlin/create?term=$term&ns=$namespaceStr'>►$namespaceStr</a>";
		}
		$links .= '<br>';
		return $links;
	} // makeCreateEntryLinksWithNsArray

	/** makeNamespaceLink:
		Make a link to a page describing the given namespace.

		@param $namespaceStr the namespace string.
		@return the link, as HTML.
	*/
	public static function makeNamespaceLink($namespaceStr)
	{
		$base = self::articlePathBase();
		$dictName = self::getDictNameFromNamespace($namespaceStr);
		return "<a href='$base/Project:$namespaceStr'>$dictName</a> (namespace $namespaceStr)";
	} // makeNamespaceLink

	/** getDictNameFromNamespace:
		Get the dictionary name for the given namespace.

		@param $namespaceStr the namespace string.
		@return the dictionary name.

		TODO: include Chinese names as well? If so, jianti/fanti/pinyin?
	*/
	public static function getDictNameFromNamespace($namespaceStr)
	{
		switch ($namespaceStr) {
		case 'Ci':
			return 'The ABC Chinese-English Comprehensive Dictionary'; // 汉英大词典
		case 'En':
			return 'The ABC English-Chinese Dictionary'; // 英汉词典
		case 'Jyut':
			return 'The ABC Cantonese-English Dictionary'; // 粤英詞典
		case 'Zi':
			return 'The Wenlin Zidian'; // 文林字典
		}
		return $namespaceStr;
	} // getDictNameFromNamespace

	/** stripWLTag:
		Strip the WL tags surrounding the text.
		Also strip any whitespace following the opening WL tag, to allow it to be followed by a newline for example.

		@param $text the page content, to be modified.
	*/
	public static function stripWLTag(&$text)
	{
		$text = preg_replace('/^\<WL\>\s*/', '', $text);
		$text = preg_replace('/\<\/WL\>$/', '', $text);
	} // stripWLTag

	/** getCurrentUser:
		Get the User object describing the current user.

		@param msg input value maybe '?'; passed by reference; to be set by this function
			to a wikitext message to the user in case we return false.
		@return the User object, or null for failure.
	*/
	public static function getCurrentUser(&$msg)
	{
		// global $wgUser;
		if (!class_exists('RequestContext', false)) {
			$msg = 'WenlinGongju::getCurrentUser: missing RequestContext';
			return null;
		}
		$user = RequestContext::getMain()->getUser(); // Is this the best way to get the current user? Maybe better than $wgUser.
		if (false) { // To debug, make true and uncomment "global $wgUser;" above.
			/* "$wgUser is initialized towards the end of setup. Some hooks are called earlier than that
				and should avoid using it. When in doubt use $wgUser->isSafeToLoad()"
				https://www.mediawiki.org/wiki/Manual:$wgUser
			*/
			if ($wgUser->isSafeToLoad()) {
				if ($user !== $wgUser) {
					$msg = 'WenlinGongju::getCurrentUser: user is not wgUser'; // never happens
					return null;
				}
			}
			else {
				$msg = 'WenlinGongju::getCurrentUser: not wgUser isSafeToLoad'; // never happens
				return null;
			}
		}
		return $user;
	} // getCurrentUser

	/** makeGradedWordListLink:
		Make a link to a page listing vocabulary in the given grade and the given namespace.

		@param $namespaceStr the namespace string.
		@param $gradeLetter the grade letter.
		@return the link, as HTML.
	*/
	public static function makeGradedWordListLink($namespaceStr, $gradeLetter)
	{
		$base = self::articlePathBase();
		if ($gradeLetter == 'A' || $gradeLetter == 'B' || $gradeLetter == 'C' || $gradeLetter == 'D') {
			$gradeStr = $namespaceStr . '_Grade_' . $gradeLetter;
		}
		else {
			$gradeStr = 'ECCE_Graded_Word_Lists#EF';
		}
		return "<a href='$base/Project:$gradeStr'>{".$gradeLetter."}</a>";
	} // makeGradedWordListLink

	/** makeAudioLink:
		Make a link to zero or more sound files for the pronunciation of the given term.

		@param $term the term, like 'hao3' for mono or 'kẹ̌yǐ' for poly.
		@param $monopoly 'mono' for pinyin monosyllables, or 'poly' for pinyin polysyllables; the
				code here assumes this string matches the folder name like "wanwu/wenlinAudio/poly".
		@return the link, as HTML.

		Called by:
			WenlinZhixing::describePinyinSyllable ('mono')
			WenlinBand::makeCiButtons ('poly')
	*/
	public static function makeAudioLink($term, $monopoly)
	{
		if ($monopoly == 'mono') {
			/* Monosyllable like 'hao3' -- already has tone digit instead of tone mark. */
			$py = str_replace('ü', 'v', $term);
		}
		else if ($monopoly == 'poly') {
			$py = $term;
			$py = preg_replace('/\([^)]*\)/', '', $py); // remove parenthetical (r), (zi), (r/zi), etc.
			$py = preg_replace('/\s+/', '', $py); // remove whitespace
			$py = WenlinPinyin::applyToneChangeNotation($py); // kẹ̌yǐ becomes kéyǐ
			$py = WenlinPinyin::changeBPbipi($py); // BP-jī (BP机) becomes bīpījī, etc.
			$py = WenlinPinyin::convertTonesToDigits($py); // kéyǐ becomes ke2yi3
		}
		else { // In the future we may have other values such as for English pronunciations.
			return '';
		}
		$fname = strtoupper($py);
		$firstLetter = mb_substr($fname, 0, 1);
		$extensions = array('mp3', 'WAV'); // if mp3 not found, look for WAV in same location
		$scriptPath = self::scriptPathNoSlash();
		$monopolyPath = "$scriptPath/wenlinAudio/$monopoly";
		$audioLink = '';
		/* $_SERVER['DOCUMENT_ROOT'] may or may not end with slash, it depends on the server.
			For me currently it does NOT end with slash on wenlin.co, but DOES end with slash on localhost!
			To be safe, trim any trailing slash, and then insert one manually where needed. */
		$docRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/'); // does NOT end with slash on wenlin.co, but DOES end with slash on localhost!
		$globStr = "$docRoot/$monopolyPath/" . '*';
		/* Loop through folders named 'W', 'M' for women's/men's voices. This code allows for future addition of
			more "genders", e.g.: unknown; neutral; synthesized. Any folder besides 'W' or 'M' here will be detected
			at runtime and treated as an additional gender. */
		foreach (glob($globStr) as $genderPath) {
			$genderPathBase = basename($genderPath);
			$gz = ($genderPathBase == 'W') ? '女' : (($genderPathBase == 'M') ? '男' : $genderPathBase);
			/* Loop through folders named 'ZHN' for Zhang Hongnian, 'WSM' for Wang Shuangmei, et al.
				Support any number of voices for each gender, by detecting the folders at runtime. */
			foreach (glob("$genderPath/" . '*', GLOB_ONLYDIR) as $voicePath) {
				$voice = basename($voicePath);
				foreach ($extensions as $ext) {
					$pathname = "$monopolyPath/$genderPathBase/$voice/$firstLetter/$fname.$ext";
					if (file_exists("$docRoot/$pathname")) {
						$audioLink .= "<button onclick=\"new Audio('/$pathname').play();\">${gz} ${voice} 🔈</button>\n";
						break;
					}
				}
			}
		}
		if ($audioLink != '') {
			$base = self::articlePathBase();
			$audioLink .= "<a style='font-size: small;' href='$base/Project:Audio_Credits'>►audio credits</a>";
		}
		return $audioLink;
	} // makeAudioLink

} // class WenlinGongju
