<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/*	Our custom database tables are similar to the indexes marked by prefix bytes
		in .wenlintree files (used by the Wenlin desktop app, in C programming language).
		Those prefix bytes are:

	Cidian:

		GSI_SER_PREFIX: key = serial number; item = db file position (as with all Cidian indexes).
		≈ SQL table not needed: serial number (wiki entry title) already gives access to the entry.
			For the other Cidian keys, there will be an sql column for serial number instead of item = db file position.

		GSI_ABC_PREFIX: key = pinyin (alphabetical order), adjusted for collation. Used both for looking up
			a single word (or group of homophones) according to pinyin, and for listing words alphabetically.
		≈ SQL table with 2 columns: pinyin-collation-key, serial-number. Or, better, combine with CIFREQ_PREFIX in single table,
			since there is only one py per ser and only one freq per ser.
			Instead of having ser in all tables, it might be more efficient to have an integer id ( = $article->getID() ? )
			 in all the tables, and only have ser in wenlin_ci_pinyin.
		→wenlin_ci_pinyin: 5 columns: page_id, serial_number, pinyin_collation, syllable_count, frequency
			(page_id = WikiPage page_id; serial_number is unique, and normally pinyin_collation is also unique,
			with homograph numbers, but allow for temporary non-uniqueness of pinyin_collation)

		CI_PREFIX: key = Chinese characters in ci (unicode order), plus freq (twisted).
			Used only to find the set of entries with a given full-length string of Hanzi.
			Usually there is only one matching entry but sometimes there are multiple for Hanzi homographs,
			such as 啊 ¹ā, á, ǎ, à; 重重 ¹chóngchóng, zhòngzhòng;多少 duōshao, duōshǎo. The key includes
			freq for ordering Hanzi homographs. A single db entry can have multiple index entries for
			jianti/fanti/yiti, such as ³ái 𫘤/呆[騃/獃].
		≈ SQL table with 3 columns: hanzi-string, ciFreq, serial-number.
			Or, possibly don't need ciFreq in this table, could sort query result by ciFreq from another table (cf. CIFREQ_PREFIX)?
			Also, simple ciFreq will suffice, don't need "twisted" to reverse order since sql can sort ascending or descending.
			What about "pseudo" ciFreq (see TWISTED_CIFREQ_IS_REAL)? Shouldn't be needed to ensure stable ordering of hanzi
			homographs; for that, use serial number (already in the table) instead.
		→wenlin_ci_hanzi_string: 3 columns: id, page_id, hanzi (id is unique auto-increment column; page_id matches wenlin_ci_pinyin)

		ZC_PREFIX: key = one hanzi, then ci frequency (twisted), then pinyin-collation-key.
			There is a separate entry in the index for each character in the ci. There are multiple keys
			for each ci, each character forming its own key. Used for "list words containing zi". The
			order for that list depends mainly on ciFreq. The key also includes pinyin-collation-key because,
			for example: 重复 and 尊重 both occur in "list words containing 重", and have the same ciFreq (27).
			Their order is determined by the pinyin: 重复 chóngfù precedes 尊重 zūnzhòng.
		≈ SQL table with 4 columns: hanzi, ciFreq, pinyin-collation-key, serial-number.
			Or, possibly don't need ciFreq or pinyin-collation-key in this table, could sort query result by getting those
			from other tables (cf. CIFREQ_PREFIX and GSI_ABC_PREFIX)? But is that more expensive at runtime?
		→wenlin_ci_hanzi_contained: 4 columns: id, page_id, hanzi, syllable_number (many-to-many, no unique column)
				syllable_number = 1 for 文 in 文林; syllable_number = 2 for 林 in 文林;
				syllable_number = 7 for 化 in 资产阶级自由化.
			To get "List words starting with 文", use the subset of rows for which syllable_number = 1.
			To get "List words ending in 化", use syllable_number == syllable_count.
			Note: just as ZC_PREFIX is only used for containment in polysyllables, wenlin_ci_hanzi_contained should
			not include monosyllabic entries. The relation that the character 的 is contained in the word(s) 的,
			is in wenlin_ci_hanzi_string, no need to repeat it in wenlin_ci_hanzi_contained. Arguably we don't need
			to include syllable_number = 1 either, since wenlin_ci_hanzi_string can be used quickly to find all
			the words starting with a character; but it will be more convenient to implement "words containing"
			if we do include syllable_number = 1 in wenlin_ci_hanzi_contained. Note, however, that there could be
			scenarios where it would make sense to include the word(s) 的 in a list of words containing 的.
			In Wenlin zidian entry windows, we show the monosyllabic cidian entries near the top, and then we
			have a button for showing "words containing", meaning polysyllables, since the user would already
			see the monosyllables before seeing the button. In a different interface, it might be more convenient
			if wenlin_ci_hanzi_contained did include monosyllables. We could add them later.

		ZC1_PREFIX: key = first character, then pinyin-collation-key.
			Alphabetical, but divided according to first character, just like nearly all dictionaries that
			use pinyin ordering -- used for "list words starting with zi", UU_ZC_STARTING.
		≈ SQL table with 3 columns: hanzi, pinyin-collation-key, serial-number.
			Or, possibly don't need pinyin-collation-key in this table, could sort query result by getting pinyin-collation-key
			from another table (cf. GSI_ABC_PREFIX)?
		→wenlin_ci_hanzi_contained (see above)

		CIFREQ_PREFIX: key = frequency count (average # of occurrances per million words).
			Used for simple list of all high-frequency words in freq order (see CIFREQ_CUTOFF).
		≈ SQL table with 2 columns: ciFreq, serial-number.
			If we use this table to sort query results from other tables, we don't want CIFREQ_CUTOFF
			(maybe don't want CIFREQ_CUTOFF anyway). Combine with table for GSI_ABC_PREFIX.
		→wenlin_ci_pinyin: (see under GSI_ABC_PREFIX)

	Ying-Han:

		GSI_SER_PREFIX: key = serial number; item = db file position (as with all Ying-Han indexes).
		≈ SQL table not needed: serial number (wiki entry title) already gives access to the entry.

		GSI_ABC_PREFIX: key = alphabetical collation key made with ICU, assuming WENLIN_ENGLISH_USE_ICU_COLLATION.
		≈ SQL table with 2 columns: english-collation-key, serial-number. (Keep Ying-Han distinct table from Cidian.)
			ICU needed on server as PHP "Internationalization extension": <http://php.net/manual/en/intro.intl.php>.
			"php -m | grep intl" should return 'intl', but unfortunately doesn't on my Mac or on wenlin.biz server.
			<http://darraghenright.tumblr.com/post/22027208929/installing-intl-package-on-osx-lion>
		→wenlin_en_collation: page_id, serial_number, english_collation (normally one-to-one, ser is unique, and normally
			english_collation is also unique, with homograph numbers, but allow for temporary non-uniqueness of english_collation)
			Not sure whether id is helpful, since there's no other yinghan table to join with. But if id is
			same as mediawiki page_id, that may be convenient info to have in the table, and also if we add
			more yinghan tables, id will help to join them. And, using id is consistent with cidian.

	Zidian:

		ZI_PREFIX: key = Hanzi (as UTF-8). Item = file position. This is the only Zidian key for which
			the item is the file position. In all the other Zidian keys, the item is a Hanzi (sometimes with
			some extra info squeezed into the left-over bits).
		≈ SQL table not needed: Hanzi (wiki entry title) already gives access to the entry.

		BZ_PREFIX: key = component Hanzi (2 or 4 bytes, UTF-16) plus ziFreq (2 bytes). Item is also Hanzi.
			For example, the key might be 木 and ziFreq, and the item might be 林, reflecting the fact that
			木 is a component (bùjiàn 部件) in 林. The B in BZ stands for bùjiàn, and the Z stands for zì 字.
			In this example, the ziFreq part of the key indicates the frequency rank of 林 (not 木), so that
			the list of the characters containing 木 is ordered by the frequency ranks of those characters.
		≈ SQL table with 3 columns: bujian, ziFreq, hanzi.
			Or, possibly don't need ziFreq in this table, could sort query result by getting ziFreq from another
			table (cf. non-existent ZIFREQ_PREFIX)?
		→wenlin_zi_bujian: hanzi, bujian

		ZIPY_PREFIX: key = short pinyin (2 bytes) plus ziFreq (2 bytes). Item is Hanzi with 2 extra bits for
			WLJiantiFanti squeezed in. Used for phonetic conversion (Edit menu Convert command) as well as for
			List characters by pinyin.
		≈ SQL table with 4 columns: pinyin, ziFreq, hanzi, WLJiantiFanti.
			Or, possibly don't need ziFreq in this table, could sort query result by getting ziFreq from another
			table (cf. non-existent ZIFREQ_PREFIX)? Not so with WLJiantiFanti, which may be specific to reading.
			Compare:
				ZIPY_PREFIX; py = 0x1c19 (gān), zifreq = 0x3e4, item = 干[U+5E72] (11 JIAN_AND_FAN)
				ZIPY_PREFIX; py = 0x1c1c (gàn), zifreq = 0x3e4, item = 干[U+5E72] (01 JIAN_ONLY)
			Split pinyin into two columns: pinyin (toneless), tone (number); that way this table can support ZIPY0_PREFIX as well.
		→wenlin_zi_pinyin: 5 columns: hanzi, pinyin, tone, jianti, fanti (many-to-many, no unique column)
				干, gan, 1, TRUE (jianti), TRUE (fanti)
				干, gan, 4, TRUE (jianti), FALSE (fanti)
	 			乾, gan, 1, FALSE (jianti), TRUE (fanti)
				乾, qian, 2, TRUE (jianti), TRUE (fanti)

		ZIPY0_PREFIX: key = same as ZIPY_PREFIX (except for the prefix itself) and the short pinyin does not
			indicate the tone (spy == spy | SPY_TONE_MASK).
		≈ SQL table with 4 columns: pinyin-toneless, ziFreq, hanzi, WLJiantiFanti. (Or, ... see ZIPY_PREFIX.)
			No, instead combine with wenlin_zi_pinyin, see above.

		ZIMAP_PREFIX: key = Hanzi (2 or 4 bytes UTF-16) followed by "distance" (1 byte). Maps ordinary Hanzi
			to their corresponding Seal characters. The "distance" indicates how closely related the ordinary
			and Seal characters are. The key contains the ordinary Hanzi, and the item contains the Seal character.
		≈ SQL table with 3 columns: hanzi, seal, distance.
		→wenlin_zi_seal: hanzi, seal, distance

		There is no ZIFREQ_PREFIX; instead, the files ziFreq.wenlinbin (binary) and mostused.wenlin (text) are used.
		≈ SQL table with 2 columns: hanzi, ziFreq. It might be generated from ziFreq.wenlinbin.
		→wenlin_zi_frequency: hanzi, frequency

	Summary of SQL tables:
		→wenlin_ci_pinyin: 5 columns: page_id, serial_number, pinyin_collation, syllable_count, frequency
			(page_id = WikiPage page_id; ; serial_number is unique, and normally pinyin_collation is also unique,
			with homograph numbers, but allow for temporary non-uniqueness of pinyin_collation)
		→wenlin_ci_hanzi_string: 3 columns: id, page_id, hanzi (page_id matches wenlin_ci_pinyin)
		→wenlin_ci_hanzi_contained: 4 columns: id, page_id, hanzi, syllable_number (page_id matches wenlin_ci_pinyin)
		→wenlin_en_collation: 3 columns: page_id, serial_number, english_collation (normally one-to-one, serial_number
			is unique, and normally english_collation is also unique, with homograph numbers, but allow for temporary
			non-uniqueness of english_collation)
		→wenlin_zi_bujian: 3 columns: id, hanzi, bujian (many-to-many)
		→wenlin_zi_pinyin: 6 columns: id, hanzi, pinyin, tone, jianti, fanti (many-to-many)
		→wenlin_zi_seal: 4 columns: id, hanzi, seal, distance (many-to-many)
		→wenlin_zi_frequency: 2 columns: hanzi, frequency (hanzi is unique)
*/

class WenlinIndex
{
	const MAX_SER_LENGTH = 20; /* max size of serial number -- MAX_SER_LENGTH = 20 in C code; cf. sql table definitions.*/
	const MAX_COLLATION_LEN = 300; /* max size of collation key -- MAX_ABC_EXP_LEN = 300 in C code; cf. sql table definitions.*/

	/** entryIsValidForSaving:
		Is the new/revised entry valid for saving to the dictionary?

		@param WikiPage $article the article.
		@param Content $content New entry content.
		@param Status $status warning/error list, boolean "OK" status.
		@return true if the entry is valid, else false.

		Called by WenlinHooks::onEditFilterMergedContent.
		@see https://www.mediawiki.org/wiki/Manual:Hooks/EditFilterMergedContent

		Validate the entry and prevent saving if it's invalid. Compare the C functions ZiGatherIndexInfo et al.,
		which do both validation and gathering for indexing.
	*/
	public static function entryIsValidForSaving(WikiPage $article, Content $content, Status $status)
	{
		return self::validateOrIndex($article, $content, $status, 'validate');
	} // entryIsValidForSaving

	/** updateIndexesOnSave:
		Update our custom indexes based on the content of the new/revised entry that has just been saved.

		@param WikiPage $article the article.
		@param Content $content New content, as a Content object.
		@param Status $status warning/error list, boolean "OK" status.
		@return null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki.

		Called by WenlinHooks::onPageContentSaveComplete, and from WenlinReindex.php.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/PageContentSaveComplete
		@see https://www.mediawiki.org/wiki/Category:PageContentSaveComplete_extensions

		There are two ways to go about updating the indexes.
			(1) (like current C code): gather info from both old and new entries, using the old info
				to determine what needs to be deleted/updated from existing indexes;
			(2) simply delete everything in existing entries that matches the page_id/hanzi, and regenerate
				from scratch everything for the new entry.
			As far as efficiency for the SQL queries is concerned, (1) is more efficient, since with (2)
			we'll often be deleting a row and then inserting a row identical to the one we deleted.
			On the other hand, (2) is simpler, since there's no need to gather info about the old entry,
			and who knows, that gathering (1) might be slower than (2). If we do (1), probably we can
			gather the old info based on the "revision" -- old revisions are kept around rather than
			deleted entirely. Still, (2) seems easier to implement. One possible problem with (2) is that
			AUTO_INCREMENT columns will keep getting incremented unnecessarily, and could overflow
			even though there isn't a large number of rows, just a large number of edits that might not
			even change the other columns. When an AUTO_INCREMENT column reaches the upper limit
			of the data type, "the next attempt to generate a sequence number fails." Still, 4 billion is a lot
			of edits. We could renumber the AUTO_INCREMENT columns when we get to a billion.
	*/
	public static function updateIndexesOnSave(WikiPage $article, Content $content, Status $status)
	{
		return self::validateOrIndex($article, $content, $status, 'index');
	} // updateIndexesOnSave

	/** updateIndexesOnDelete:
		Update our custom indexes based on the content of the entry that has just been deleted.

		@param WikiPage $article the article.
		@param Content $content New content, as a Content object.
		@param $id: id of the article that was deleted (note that $article->getID() == 0 after deletion).
		@return null.

		Called by WenlinHooks::onArticleDeleteComplete.
		@see https://www.mediawiki.org/wiki/Manual:Hooks/ArticleDeleteComplete
		@see https://www.mediawiki.org/wiki/Category:ArticleDeleteComplete_extensions
	*/
	public static function updateIndexesOnDelete(WikiPage $article, Content $content, $id)
	{
		$status = Status::newGood();
		self::validateOrIndex($article, $content, $status, 'delete', $id);
		return null;
	} // updateIndexesOnDelete

	/** updateIndexesOnUndelete:
		Update our custom indexes based on the content of the entry that has just been undeleted (restored).

		@param $titleObj: Title corresponding to the article restored.
		@param $create: Whether or not the restoration caused the page to be created (i.e. it didn't exist before).
		@param $comment: Comment explaining the undeletion.
		@param $oldPageId: ID of page previously deleted (from archive table) (since gerrit:133631) (Wenlin doesn't actually use this).
		@return null.

		Called by WenlinHooks::onArticleDeleteComplete.
		@see https://www.mediawiki.org/wiki/Manual:Hooks/ArticleUndelete
		@see https://www.mediawiki.org/wiki/Category:ArticleUndelete_extensions
	*/
	public static function updateIndexesOnUndelete(Title $titleObj, $create, $comment, $oldPageId)
	{
		$article = new WikiPage($titleObj);
		$rev = Revision::newFromTitle($titleObj);
		if (!$rev) {
			return null;
		}
		$content = $rev->getContent();
		$status = Status::newGood();
		/* Although we receive the old page id as $oldPageId, it seems that we should ignore
			that. A new page id is created and we can get it as $titleObj->getArticleID(),
			or validateOrIndex can get it as $article->getID(). */
		// $id = $titleObj->getArticleID();
		self::validateOrIndex($article, $content, $status, 'undelete');
		return null;
	} // updateIndexesOnUndelete

	/** validateOrIndex:
		Gather info from the new/revised entry that has just been saved or (un)deleted,
		and validate it, index it, or un-index it, depending on $doWhat.

		@param WikiPage $article the article.
		@param Content $content New content, as a Content object.
		@param Status $status warning/error list, boolean "OK" status.
		@param string $doWhat 'validate', 'index', 'delete', or 'undelete'.
		@param $id: id of the article that was deleted (if $doWhat == 'delete').
		@return for 'validate', true if the entry is valid for saving, else false;
				 for 'index', null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki;
				 for 'delete' or 'undelete', null;
	*/
	private static function validateOrIndex(WikiPage $article, Content $content, Status $status, $doWhat, $id = 0)
	{
		$titleObj = $article->getTitle();
		$titleStr = $titleObj->getText(); // serial number (for cidian/ying-han), or Hanzi (for zidian)
		$namespaceNumber = $titleObj->getNamespace();
		$text = ContentHandler::getContentText($content);
		WenlinGongju::stripWLTag($text);

		/* page_id: Uniquely identifying primary key. This value is preserved across edits and renames.
			This field can be accessed by WikiPage::getId(), Title::getArticleID(), etc.
			<https://www.mediawiki.org/wiki/Manual:Page_table>
			Let's try using it for our custom tables as well -- shorter, faster than using serial_number (title) in all tables.
			For 'delete', $article->getID() would return zero, and we must use the $id argument instead. */
		$page_id = ($id != 0) ? $id : $article->getID();
		$info = null;
		if ($namespaceNumber == NS_WL_ZI) { // 'Zi'
			$info = new ZiIndexInfo($page_id, $titleStr, $text, $status);
		}
		elseif ($namespaceNumber == NS_WL_CI) { // 'Ci'
			$info = new CiIndexInfo($page_id, $titleStr, $text, $status);
		}
		elseif ($namespaceNumber == NS_WL_EN) { // 'En'
			$info = new YingHanIndexInfo($page_id, $titleStr, $text, $status);
		}
		elseif ($namespaceNumber == NS_WL_JYUT) { // 'Jyut'
			$info = new JyutIndexInfo($page_id, $titleStr, $text, $status);
		}
		else { // if ($namespaceNumber == NS_WL_CDL) {
			return false;
		}
		if (!$info->valid) {
			$status->fatal($info->errorMessage);
		}
		if ($doWhat == 'validate') {
			return $info->valid; // true or false
		}
		if (!$info->valid) {
			return null;
		}
		if ($doWhat == 'index') {
			return $info->index(); // a string, or null!
		}
		if ($doWhat == 'delete') {
			$info->justDelete = true;
			// WenlinDebug::log('validateOrIndex justDelete');
			/* "WenlinDebug log: Deleting all rows with page_id = 0" -- zero! That was using $article->getID(),
				which is zero. Need to use $id from doDeleteArticleReal instead.
				The hook is called by doDeleteArticleReal in WikiPage.php, thus:
				Hooks::run( 'ArticleDeleteComplete', array( &$this, &$user, $reason, $id, $content, $logEntry ) );
			*/
			# debug_print_backtrace();
			$info->index();
			return null;
		}
		if ($doWhat == 'undelete') {
			WenlinDebug::log('validateOrIndex undelete');
			$info->index();
			return null;
		}
		return null;
	} // validateOrIndex

	/** escapeString:
		Change seven problematic non-ASCII bytes into escape codes with backslash.
		Characters encoded are NUL (ASCII 0), \n, \r, \, ', ", and Control-Z.

		See http://dev.mysql.com/doc/refman/5.6/en/string-literals.html.

		Compare mysql_real_escape_string (deprecated), mysqli_real_escape_string (problematic when no sql connection is open?).

		"Prepared statements" might be better but it's not clear how to use them with Mediawiki
		(though Database.php does have "fillPrepared" and "execute").

		Note: some byte values including \x01 that occur in collation keys are supposedly OK for MySQL
		(which does not provide escapes for them), but they don't appear on the Mac Terminal command
		line and can't be copy-pasted from it.

		Note: this function should be called in preparation for calling select(), but not in preparation for calling insert().
		At least, that is what we find for English collation keys as used in WenlinZhixing.php (select()) and WenlinIndex.php (insert()).
		The reason appears to be this difference between insert() and select() as implemented in MediaWiki 1.28 (and, let's hope,
		consistently in other MediaWiki versions!):
		Given $a = array('foo' => 'bar'), insert($table, $a, __METHOD__) calls makeList(), which calls addQuotes, which calls strencode() on 'bar'.
		Given $conds = array("foo LIKE 'bar'"), select($table, $vars, $conds, ...) simply appends "(foo LIKE 'bar')" to the query, without calling strencode().
		Details:
			When we save an entry, given
				$a = array('page_id' => $this->page_id, 'serial_number' => $this->bands['ser'], 'english_collation' => $this->collationKey)
			insert() calls makeList(), which calls addQuotes, which calls strencode(), which calls mysql_real_escape_string():
				$sql .= '(' . $this->makeList( $a ) . ')';
			Then makeList does this, given default $mode = self::LIST_COMMA:
				$list .= $mode == self::LIST_NAMES ? $value : $this->addQuotes( $value );
			But when we look up an entry, strencode() isn't called at all. selectSQLText does this with our $conds = array("$collation LIKE '$like'"):
				$conds = $this->makeList( $conds, self::LIST_AND );
			Then makeList does this:
				if ( ( $mode == self::LIST_AND || $mode == self::LIST_OR ) && is_numeric( $field ) ) {
					$list .= "($value)";
			--- where $field = 0 and $value = "$collation LIKE '$like'".
		@param $s the string.
		@return the escaped string.
	*/
	public static function escapeString($s)
	{
		$t = '';
		$len = strlen($s);
		for ($i = 0; $i < $len; $i++) {
			switch ($s[$i]) {
			case "\x00": // nul
				$t .= '\0';
				break;
			case "\n": // linefeed
				$t .= '\n';
				break;
			case "\r": // return
				$t .= '\r';
				break;
			case "\\": // backslash
				$t .= '\\\\';
				break;
			case "\x27": // apostrophe
				$t .= "\x5c\x27";
				break;
			case "\x22": // double-quote
				$t .= "\\\"";
				break;
			case "\x1a": // Ctrl+Z
				$t .= '\Z';
				break;
			default:
				$t .= $s[$i];
			}
		}
		return $t;
	} // escapeString

} // class WenlinIndex

////////////////////////////////////

/** The WenlinIndexInfo object is for assembling and indexing information for a dictionary entry.
	Since it's abstract, it's not used directly. It's inherited by CiAbstractIndexInfo, YingHanIndexInfo, ZiIndexInfo.
*/
abstract class WenlinIndexInfo
{
	public $valid;
	public $justDelete;
	public $errorMessage;

	protected $page_id;
	protected $titleStr;
	protected $bands; // the array of bands needed for indexing (keys may be 'py', 'char', 'ser', 'freq'; values are band contents).
	protected $collationKey;

	public function __construct($page_id, $titleStr, $text) // WenlinIndexInfo
	{
		$this->page_id = $page_id;
		$this->titleStr = $titleStr;
		$this->valid = false;
		$this->justDelete = false;
		$this->errorMessage = null;
		$this->gather($text);
	} // __construct

	/** gather:
		Get the info needed for indexing.

		@param $text the entry.
	*/
	abstract protected function gather($text);

	/** index:
		Update our custom indexes based on the content of the new/revised entry that has just been saved,
		or deleted (if justDelete == true);

		@return null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki.
	*/
	abstract public function index();

} // class WenlinIndexInfo

////////////////////////////////////

/** The CiAbstractIndexInfo class can be extended to either CiIndexInfo (Mandarin) or JyutIndexInfo (Cantonese).
*/
abstract class CiAbstractIndexInfo extends WenlinIndexInfo
{
	/* There are distinct table names for Mandarin and Cantonese. Within those tables, however, the column
		names are identical, including "...pinyin..." and "...hanzi...", to simplify the code and preserve compatibility
		with the tables that already exist for Mandarin, even though "...jyutping..." and "...honzi..." would be more
		suitable for Cantonese. For new code, "pin" can cover both "pinyin" and "jyutping", while "hz" can cover
		both "hanzi" and "honzi". */
	protected $tableNamePin;
	protected $tableNameHzString;
	protected $tableNameHzContained;

	protected $charBand; // WenlinCharBand

	/* We don't have a gather() function for CiAbstractIndexInfo; instead we have distinct gather() functions
		for CiIndexInfo and JyutIndexInfo. */

	/** index:
		Update our custom indexes based on the content of the new/revised entry that has just been saved,
		or deleted (if justDelete == true);

		@return null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki.

		Tables:
		→wenlin_ci_pinyin: 5 columns: page_id, serial_number, pinyin_collation, syllable_count, frequency
			(page_id = WikiPage page_id; ; serial_number is unique, and normally pinyin_collation is also unique,
			with homograph numbers, but allow for temporary non-uniqueness of pinyin_collation)
		→wenlin_ci_hanzi_string: 3 columns: id (auto-inc), page_id, hanzi (page_id matches wenlin_ci_pinyin)
		→wenlin_ci_hanzi_contained: 4 columns: id (auto-inc), page_id, hanzi, syllable_number (page_id matches wenlin_ci_pinyin)

		Here's how we'll query the tables for "List words containing character":
			SELECT f.serial_number
			FROM wenlin_ci_hanzi_contained h JOIN wenlin_ci_pinyin f
			ON h.page_id = f.page_id
			WHERE h.hanzi = 'Z'
			ORDER BY f.frequency DESC, f.pinyin_collation;
	*/
	public function index() // CiAbstractIndexInfo
	{
		$wdb = new WenlinDB;

		/* Delete from all three tables all rows that have this page_id. */
		$a = array('page_id' => $this->page_id);
		$wdb->delete($this->tableNamePin, $a, __METHOD__);
		$wdb->delete($this->tableNameHzString, $a, __METHOD__);
		$wdb->delete($this->tableNameHzContained, $a, __METHOD__);
		/* Note: due to incomplete implementation of delete/undelete hooks, it happened that we ended up
		   with some index entries that should have been deleted but weren't, and then when the pages were
		   undeleted, we ended up with duplicate index entries, such as:
				mysql> select * from wenlin_ci_pinyin where serial_number='1010422519';
				|  434363 | 1010422519    | qu...4..a        |              1 |    2919.7 |
				|  434367 | 1010422519    | qu...4..a        |              1 |    2919.7 |
		   It would be possible, and might be a good idea, to avoid duplicates for serial_number in
		   wenlin_ci_pinyin by deleting according to titleStr here as well as page_id, even though
		   it would normally be redundant. However, that still wouldn't fix duplicates for wenlin_ci_hanzi_string
		   or wenlin_ci_hanzi_contained, and for those we can't assume uniqueness. */
		if ($this->justDelete) {
			return null;
		}
		/* Insert one row in the wenlin_ci_pinyin or wenlin_jyut_ping table. */
		$this->indexCiPinyinTable($wdb);

		/* Insert one or more rows in the wenlin_ci_hanzi_string or wenlin_jyut_hz_string table. */
		$this->indexCiHanziStringTable($wdb);

		/* No wenlin_ci_hanzi_contained entries are needed for monosyllables. */
		if ($this->charBand->sylCount > 1) {
			/* Insert one or more rows in the wenlin_ci_hanzi_contained or wenlin_jyut_hz_contained table. */
			$this->indexCiHanziContainedTable($wdb);
		}
		return $wdb->queries();
	} // index

	/** indexCiPinyinTable:
		Insert one row in the wenlin_ci_pinyin or wenlin_jyut_ping table.

		@param $wdb the database for writing.
	*/
	private function indexCiPinyinTable($wdb)
	{
		$frequency = floatval($this->bands['freq']); // changes "2.77 [XHPC:5]" to 2.77 (float, not string).
		$a = array(
			'page_id' => $this->page_id,
			'serial_number' => $this->bands['ser'],
			'pinyin_collation' => $this->collationKey,
			'syllable_count' => $this->charBand->sylCount,
			'frequency' => $frequency
		);
		$wdb->insert($this->tableNamePin, $a, __METHOD__);
	} // indexCiPinyinTable

	/** indexCiHanziStringTable:
		Insert one or more rows in the wenlin_ci_hanzi_string or wenlin_jyut_hz_string table.

		@param $wdb the database for writing.
	*/
	private function indexCiHanziStringTable($wdb)
	{
		$data = []; // array for possibly multiple rows
		$varCount = count($this->charBand->out);
		for ($i = 0; $i < $varCount; $i++) {
			$hanziString = $this->charBand->out[$i];
			$data[] = array(
				'page_id' => $this->page_id,
				'hanzi' => $hanziString
			);
		}
		$wdb->insert($this->tableNameHzString, $data, __METHOD__);
	} // indexCiHanziStringTable

	/** indexCiHanziContainedTable:
		Insert one or more rows in the wenlin_ci_hanzi_contained or wenlin_jyut_hz_contained table.

		@param $wdb the database for writing.
	*/
	private function indexCiHanziContainedTable($wdb)
	{
		/* The number of rows is zero if sylCount < 2, else varCount*sylCount,
			minus duplicates. Duplicates are common, as in "词/辞典[詞/辭-]",
			where (id, '典', 2) should be inserted only once, though it comes up in
			all varCount == 4 versions of $hanziString ('词典', '辞典', '詞典', '辭典').
			To avoid duplicate rows, keep track of the ones we add with the 'already' array.
			Duplicates may have different varCount index, so initialize the array before the
			loop on varCount. */
		$already = [];
		$data = []; // array for possibly multiple rows
		$varCount = count($this->charBand->out);
		for ($i = 0; $i < $varCount; $i++) {
			$hanziArray = WenlinUtf::explode($this->charBand->out[$i]);
			for ($j = 0; $j < $this->charBand->sylCount; $j++) {
				$hanziContained = WenlinUtf::ziNumber($hanziArray[$j]);
				$syllableNumber = $j + 1;
				if (!in_array("$hanziContained:$syllableNumber", $already)) {
					$data[] = array(
						'page_id' => $this->page_id,
						'hanzi' => $hanziContained,
						'syllable_number' => $syllableNumber
					);
					$already[] = "$hanziContained:$syllableNumber";
				}
			}
		}
		$wdb->insert($this->tableNameHzContained, $data, __METHOD__);
	} // indexCiHanziContainedTable

} // class CiAbstractIndexInfo

////////////////////////////////////

/** The CiIndexInfo object is used for assembling and indexing information for a cidian entry.
*/
class CiIndexInfo extends CiAbstractIndexInfo
{
	public function __construct($page_id, $titleStr, $text) // CiIndexInfo
	{
		/* TODO: 'wenlin_ci_hanzi_string' and 'wenlin_ci_pinyin' are separately hard-coded in
			listMonosyllabicCidianEntriesForZidianEntry in WenlinRender.php, shouldn't be there... */
		$this->tableNamePin = 'wenlin_ci_pinyin';
		$this->tableNameHzString = 'wenlin_ci_hanzi_string';
		$this->tableNameHzContained = 'wenlin_ci_hanzi_contained';

		parent::__construct($page_id, $titleStr, $text);
	} // __construct

	/** gather:
		Get the cidian info needed for indexing.

		@param $text the cidian entry.
	*/
	protected function gather($text) // CiIndexInfo
	{
		$contextMsg = "Gathering information for Cidian entry";
		$bandsNeeded = ['py', 'char', 'ser', 'freq'];
		$this->bands = WenlinBand::getNeededBands($text, $bandsNeeded);
		foreach ($bandsNeeded as $b) {
			if (!isset($this->bands[$b])) {
				if ($b == 'freq') {
					$this->bands['freq'] = '0'; // freq is optional; the rest are required.
				}
				else {
					$this->errorMessage = "$contextMsg: missing $b band";
					return;
				}
			}
		}
		$ser = $this->bands['ser'];
		if (strlen($ser) > WenlinIndex::MAX_SER_LENGTH) {
			$maxLen = WenlinIndex::MAX_SER_LENGTH;
			$this->errorMessage = "$contextMsg: ser [$ser] exceeds maximum length [$maxLen]";
			return;
		}
		if ($ser != $this->titleStr) {
			$this->errorMessage = "$contextMsg: ser [$ser] does not match title [$this->titleStr]";
			return;
		}
		$this->collationKey = WenlinCollate::makePinyinCollationKey($this->bands['py']); // CiIndexInfo
		if (strlen($this->collationKey) > WenlinIndex::MAX_COLLATION_LEN) {
			$maxLen = WenlinIndex::MAX_COLLATION_LEN;
			$this->errorMessage = "$contextMsg: collation key exceeds maximum length [$maxLen]";
			return;
		}
		$char = $this->bands['char'];
		$char = WenlinBand::unabridge($char);
		$this->charBand = new WenlinCharBand();
		$cbStatus = $this->charBand->expandCharBand($char, true);
		if ($cbStatus != WenlinCharBand::CB_NO_ERR) {
			$this->errorMessage = "$contextMsg: char band error ($ser, $char): $cbStatus"; // '{I} Simple/full form characters do not match'
			return;
		}

		/* TODO: important! We should check zidian entries to make sure jianti/fanti really correspond.
			The C code does that, but PHP code doesn't yet.
			We should also check zidian for pinyin readings, etc. Good: mǎ 马[馬]; bad: hū 马[虎]. */

		$this->valid = true;
	} // gather

} // class CiIndexInfo

////////////////////////////////////

/** The JyutIndexInfo object is used for assembling and indexing information for a Jyut (Cantonese) entry.
*/
class JyutIndexInfo extends CiAbstractIndexInfo
{
	public function __construct($page_id, $titleStr, $text) // JyutIndexInfo
	{
		$this->tableNamePin = 'wenlin_jyut_ping';
		$this->tableNameHzString = 'wenlin_jyut_hz_string';
		$this->tableNameHzContained = 'wenlin_jyut_hz_contained';

		parent::__construct($page_id, $titleStr, $text);
	} // __construct

	/** gather:
		Get the Jyut entry info needed for indexing.

		@param $text the Jyut entry.
	*/
	protected function gather($text) // JyutIndexInfo
	{
		$contextMsg = "Gathering information for Jyut entry";
		$bandsNeeded = ['hw', 'char', 'ser', 'freq']; // 'hw' for Jyut, cf. 'py' for Ci
		$this->bands = WenlinBand::getNeededBands($text, $bandsNeeded);
		foreach ($bandsNeeded as $b) {
			if (!isset($this->bands[$b])) {
				if ($b == 'freq') {
					$this->bands['freq'] = '0'; // freq is optional; the rest are required.
				}
				else {
					$this->errorMessage = "$contextMsg: missing $b band";
					return;
				}
			}
		}
		$ser = $this->bands['ser'];
		if (strlen($ser) > WenlinIndex::MAX_SER_LENGTH) {
			$maxLen = WenlinIndex::MAX_SER_LENGTH;
			$this->errorMessage = "$contextMsg: ser [$ser] exceeds maximum length [$maxLen]";
			return;
		}
		if ($ser != $this->titleStr) {
			$this->errorMessage = "$contextMsg: ser [$ser] does not match title [$this->titleStr]";
			return;
		}
		$this->collationKey = WenlinCollate::makeJyutpingCollationKey($this->bands['hw']);
		if (strlen($this->collationKey) > WenlinIndex::MAX_COLLATION_LEN) {
			$maxLen = WenlinIndex::MAX_COLLATION_LEN;
			$this->errorMessage = "$contextMsg: collation key exceeds maximum length [$maxLen]";
			return;
		}
		$char = $this->bands['char'];
		$char = WenlinBand::unabridge($char);
		$this->charBand = new WenlinCharBand();

		/* TODO: adjust expandCharBand to handle Jyut, in which we don't require jianti: "馬" not "馬[马]".
			Actually, expandCharBand works OK with Jyut if there are no brackets, as is in fact the case.
			Eventually we should enforce that there are no brackets for Jyut, assuming we'll continue
			using fantizi only. The bigger difference between Ci and Jyut will be in the "CiFix" code,
			which will automatically add jianti/fanti/brackets in some cases for Ci, but not for Jyut. */
		$cbStatus = $this->charBand->expandCharBand($char, true);
		if ($cbStatus != WenlinCharBand::CB_NO_ERR) {
			$this->errorMessage = "$contextMsg: char band error ($ser, $char): $cbStatus";
			return;
		}

		/* TODO: add more consistency checking for JyutIndexInfo, compare CiIndexInfo::gather. */

		$this->valid = true;
	} // gather

} // class JyutIndexInfo

////////////////////////////////////

/** The YingHanIndexInfo object is used for assembling and indexing information for a yinghan entry.
*/
class YingHanIndexInfo extends WenlinIndexInfo
{
	const MAX_WORD = 60; /* Compare MAX_WORD in C code. */

	/** gather:
		Get the yinghan info needed for indexing.

		@param $text the yinghan entry.
	*/
	protected function gather($text) // YingHanIndexInfo
	{
		$contextMsg = "Gathering information for Ying-Han entry";
		$bandsNeeded = ['hw', 'ser'];
		$this->bands = WenlinBand::getNeededBands($text, $bandsNeeded);
		foreach ($bandsNeeded as $b) {
			if (!isset($this->bands[$b])) {
				$this->errorMessage = "$contextMsg: missing $b band [$text]";
				return;
			}
		}
		$hw = $this->bands['hw'];
		if (strlen($hw) > self::MAX_WORD) {
			$maxLen = self::MAX_WORD;
			$this->errorMessage = "$contextMsg: headword [$hw] exceeds maximum length [$maxLen]";
			return;
		}
		$ser = $this->bands['ser'];
		if (strlen($ser) > WenlinIndex::MAX_SER_LENGTH) {
			$maxLen = WenlinIndex::MAX_SER_LENGTH;
			$this->errorMessage = "$contextMsg: ser [$ser] exceeds maximum length [$maxLen]";
			return;
		}
		if ($ser != $this->titleStr) {
			$this->errorMessage = "$contextMsg: ser [$ser] does not match title [$this->titleStr]";
			return;
		}
		$this->collationKey = WenlinCollate::makeEnglishCollationKey($hw);
		if (strlen($this->collationKey) > WenlinIndex::MAX_COLLATION_LEN) {
			$maxLen = WenlinIndex::MAX_COLLATION_LEN;
			$this->errorMessage = "$contextMsg: collation key exceeds maximum length [$maxLen]";
			return;
		}
		$this->valid = true;
	} // gather

	/** index:
		Update our custom indexes based on the content of the new/revised entry that has just been saved,
		or deleted (if justDelete == true);

		@return null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki.

		Table (only one):
		→wenlin_en_collation: id, ser, english_collation (normally one-to-one, ser is unique, and normally english_collation
			is also unique, with homograph numbers, but allow for temporary non-uniqueness of english_collation)
	*/
	public function index() // YingHanIndexInfo
	{
		$table = 'wenlin_en_collation';
		$wdb = new WenlinDB;

		/* Delete all rows that have this page_id. */
		$wdb->delete($table, array('page_id' => $this->page_id), __METHOD__);
		if ($this->justDelete) {
			return null;
		}

		/* Escape certain binary bytes in the collation key as appropriate for MySQL.
			Evidently there was a bug here in the user of escapeString.
			After running WenlinReindexEnglish.php, the actual key for "dragon" (ser 2000403477), as shown by phpmyadmin,
			was 2d495c27334341015c6e015c6e01017a, including "5c" (backslash) before "27" (apostrophe). The actual key should
			NOT include the backslash character here. The backslash is only supposed to make mysql process the "insert" query
			correctly. Does $wdb->insert() add another level of escaping?
			wanwu/includes/libs/rdbms/database/DatabaseMysqlBase.php includes
				function strencode( $s ) {
					return $this->mysqlRealEscapeString( $s );
				}
			which evidently is called by addQuotes in Database.php, called by makeList, called by selectSQLText.
			So maybe we shouldn't call escapeString here at all.
		*/
		### $this->collationKey = WenlinIndex::escapeString($this->collationKey);

		/* Insert the new row. */
		$a = array(
			'page_id' => $this->page_id,
			'serial_number' => $this->bands['ser'],
			'english_collation' => $this->collationKey,
		);
		$wdb->insert($table, $a, __METHOD__);
		return $wdb->queries();
	} // index

} // class YingHanIndexInfo

////////////////////////////

/** The ZiIndexInfo object is used for assembling and indexing information for a zidian entry.
*/
class ZiIndexInfo extends WenlinIndexInfo
{
	private $headChar; // the head Hanzi (as a string)
	private $headUsv; // the head Hanzi (as a USV, integer)
	private $components; // an array of Hanzi (USV)
	private $readings; // an array of ZiReading
	private $shuowenMaps; // an array of ShuowenMap

	/* $page_id is unused for zidian, but here to match the WenlinIndexInfo constructor */
	public function __construct($page_id, $titleStr, $text) // ZiIndexInfo
	{
		$this->headChar = '';
		$this->headUsv = 0;
		$this->components = []; // array of USV (integers)
		$this->readings = [];
		$this->shuowenMaps = [];
		parent::__construct($page_id, $titleStr, $text);
	} // __construct

	/** gather:
		Get the zidian info needed for indexing.

		@param $text the zidian entry.

		Use top line for: head Hanzi; pinyin and jianti, fanti for each syllable.
		Use #c line for components.
		Use #v line for Shuowen maps.
	*/
	protected function gather($text) // ZiIndexInfo
	{
		/* Avoid strtok, use explode instead; strtok is dangerous if it's also called by subroutines */
		$lineArray = explode("\n", $text);
		$line = $lineArray[0];
		$this->gatherTopLine(rtrim($line));
		if ($this->errorMessage != null) {
			return;
		}
		if ($this->headChar != $this->titleStr) {
			$this->errorMessage = "indexing Zidian entry: head Hanzi [$this->headChar] does not match title [$this->titleStr]";
			return;
		}
		for ($i = 1; $i < count($lineArray); $i++) {
			$line = $lineArray[$i];
			if (mb_substr($line, 0, 1) == '#') {
				$c = mb_substr($line, 1, 1);
				if ($c == 'c') { // #c
					$this->gatherComponents(rtrim($line));
					if ($this->errorMessage != null) {
						return;
					}
				}
				elseif ($c == 'v') { // #v
					$this->gatherShuowen(rtrim($line));
					if ($this->errorMessage != null) {
						return;
					}
				}
			}
		}
		$this->valid = true;
	} // gather

	/** gatherTopLine:
		Get some of the zidian info needed for indexing, specifically the head Hanzi and the array of readings.

		@param $line the zidian entry top line.

		Use top line for: head Hanzi; pinyin and jianti, fanti for each syllable.

		See GetZiSpyAndValidate in ch_lb.c.
	*/
	private function gatherTopLine($line) // ZiIndexInfo
	{
		$contextMsg = "gathering Zidian entry info";
		$a = WenlinUtf::explode($line);
		$len = count($a);
		if ($len == 0) {
			return;
		}
		$this->headChar = $a[0];
		$this->headUsv = WenlinUtf::ziNumber($this->headChar);
		$i = 1;
		while ($i < $len && ($a[$i] == ' ' || $a[$i] == "\t")) {
			++$i;
		}
		if ($i >= $len) {
			return;
		}
		/* If notation (F*) or (S*) occurs before any readings, that means
			all readings are jianti-only or fanti-only.
			Otherwise, determine jianti/fanti for each reading individually. */
		$allJianti = true;
		$allFanti = true;
		if ($a[$i] == '(' && $i + 2 < $len && $a[$i + 3] == ')') {
			if ($a[$i + 1] == 'F') {
				$allFanti = false; // we found (F*), so for all readings the headword is jianti only
			}
			elseif ($a[$i + 1] == 'S') {
				$allJianti = false;  // we found (S*), so for all readings the headword is fanti only
			}
		}
		while ($i < $len) {
			if ($a[$i] == '[') {
				$i++;
				$py = '';
				while ($i < $len && $a[$i] != ']') {
					$py .= $a[$i++];
				}
				if ($a[$i] != ']') {
					$this->errorMessage = "$contextMsg: missing right bracket";
					return;
				}
				$i++; // skip past ']'
				$thisJianti = true;
				$thisFanti = true;
				if (!$allFanti || !$allJianti) {
					$thisJianti = $allJianti;
					$thisFanti = $allFanti;
				}
				else {
					while ($i < $len && ($a[$i] == ' ' || $a[$i] == "\t")) {
						++$i;
					}
					if ($i < $len && $a[$i] == '(' && $i + 3 < $len && $a[$i + 3] == ')') {
						if ($a[$i + 1] == 'F') {
							$thisFanti = false; // we found (F*), so for this reading the headword is jianti only
						}
						elseif ($a[$i + 1] == 'S') {
							$thisJianti = false;  // we found (S*), so for this reading the headword is fanti only
						}
					}
					$i += 2;
				}
				$reading = new ZiReading($py, $thisJianti, $thisFanti);
				if (!$reading->valid) {
					$this->errorMessage = "indexing Zidian entry [$this->headChar]: $reading->msg";
					return;
				}
				if (in_array($reading, $this->readings)) {
					$this->errorMessage = "indexing Zidian entry [$this->headChar]: duplicate pinyin";
					return;
				}
				$this->readings[] = $reading;
			}
			$i++;
		}
	} // gatherTopLine

	/** gatherComponents:
		Get some of the zidian info needed for indexing, specifically the array of components.

		@param $line the #c line from the zidian entry.

		See GetZiComponents in ch_lb.c.
	*/
	private function gatherComponents($line)
	{
		$a = WenlinUtf::explode($line);
		$len = count($a);
		$i = 2; // skip past '#' and 'c'
		while ($i < $len) {
			$comp = $a[$i++];
			if ($comp != $this->headChar) {
				$z = WenlinUtf::ziNumber($comp);
				if (!in_array($z, $this->components) && WenlinFenlei::zinIsCJK($z)) {
					$this->components[] = $z;
				}
			}
		}
	} // gatherComponents

	/** gatherShuowen:
		Get some of the zidian info needed for indexing, specifically the array shuowenMaps.

		@param $line the #v line from the zidian entry.

		See GetZiShuowenMaps in ch_lb.c.
	*/
	private function gatherShuowen($line)
	{
		$a = WenlinUtf::explode($line);
		$len = count($a);
		$i = 2; // skip past '#' and 'v'
		$dist = 0;
		while ($i < $len) {
			$c = $a[$i++];
			if ($c == '?') {
				$dist = ShuowenMap::SHUOWEN_QUESTIONABLE_MAP_DISTANCE;
			}
			elseif ($c == '!') {
				$dist = ShuowenMap::SHUOWEN_IMPROPER_MAP_DISTANCE;
			}
			elseif ($c == '#') {
				$dist = ShuowenMap::SHUOWEN_PUA_MAP_DISTANCE;
			}
			else {
				$z = WenlinUtf::ziNumber($c);
				if (WenlinFenlei::zinIsCJK($z)) {
					if ($dist < ShuowenMap::SHUOWEN_PROPER_MAP_DISTANCE_MAX) {
						++$dist;
					}
					$map = new ShuowenMap($z, $dist);
					if (!in_array($map, $this->shuowenMaps)) {
						$this->shuowenMaps[] = $map;
					}
				}
			}
		}
	} // gatherShuowen

	/** index:
		Update our custom indexes based on the content of the new/revised entry that has just been saved,
		or deleted (if justDelete == true);

		@return null for failure, or the array of query strings if not mediawiki, or 'mw' if mediawiki.

		Tables to be updated:
		→wenlin_zi_pinyin: 5 columns: hanzi, pinyin, tone, jianti, fanti
		→wenlin_zi_bujian: 2 columns: hanzi, bujian (many-to-many, no unique column)
		→wenlin_zi_seal: 3 columns: hanzi, seal, distance (many-to-many, no unique column)

		Note: we don't update wenlin_zi_frequency here. We assume it is created independently; it doesn't
		depend on the Zidian entry contents, instead it's equivalent to ziFreq.wenlinbin, and might never
		change (just as ziFreq.wenlinbin hasn't changed, except in name, since Wenlin 1.0).
	*/
	public function index() // ZiIndexInfo
	{
		$wdb = new WenlinDB;

		/* Delete from all three tables all rows for this character.
			Important: for wenlin_zi_seal, $this->headUsv is the Shuowen character,
			so the rows to be deleted are those in which 'seal' has this value, NOT
			those in which 'hanzi' has this value. */
		$a = array('hanzi' => $this->headUsv);
		$wdb->delete('wenlin_zi_pinyin', $a, __METHOD__);
		$wdb->delete('wenlin_zi_bujian', $a, __METHOD__);
		$wdb->delete('wenlin_zi_seal', array('seal' => $this->headUsv), __METHOD__);
		if ($this->justDelete) {
			return null;
		}

		/* Insert new rows into all three tables. */
		$this->indexZiPinyinTable($wdb);
		$this->indexZiBujianTable($wdb);
		$this->indexZiSealTable($wdb);

		return $wdb->queries();
	} // index

	/** indexZiPinyinTable:
		Insert rows in the wenlin_zi_pinyin table.

		@param $wdb the database for writing.

		→wenlin_zi_pinyin: 6 columns: id (auto-inc), hanzi, pinyin, tone, jianti, fanti
				id, 干, gan, 1, TRUE (jianti), TRUE (fanti)
				id, 干, gan, 4, TRUE (jianti), FALSE (fanti)
	 			id, 乾, gan, 1, FALSE (jianti), TRUE (fanti)
				id, 乾, qian, 2, TRUE (jianti), TRUE (fanti)
	*/
	private function indexZiPinyinTable($wdb)
	{
		$table = 'wenlin_zi_pinyin';
		$data = []; // array for possibly multiple rows
		foreach ($this->readings as $r) {
			$data[] = array(
				'hanzi' => $this->headUsv,
				'pinyin' => $r->pinyinToneless,
				'tone' => $r->tone,
				'jianti' => $r->jianti ? 'TRUE' : 'FALSE',
				'fanti' => $r->fanti ? 'TRUE' : 'FALSE'
			);
		}
		$wdb->insert($table, $data, __METHOD__);
	} // indexZiPinyinTable

	/** indexZiBujianTable:
		Insert rows in the wenlin_zi_bujian table.

		@param $wdb the database for writing.

		→wenlin_zi_bujian: 3 columns: id (auto-inc), hanzi, bujian
	*/
	private function indexZiBujianTable($wdb)
	{
		$table = 'wenlin_zi_bujian';
		$data = []; // array for possibly multiple rows
		foreach ($this->components as $compUsv) {
			$data[] = array(
				'hanzi' => $this->headUsv,
				'bujian' => $compUsv
			);
		}
		$wdb->insert($table, $data, __METHOD__);
	} // indexZiBujianTable

	/** indexZiSealTable:
		Insert rows in the wenlin_zi_seal table.

		@param $wdb the database for writing.

		→wenlin_zi_seal: 4 columns: id (auto-inc), hanzi, seal, distance (many-to-many)
	*/
	private function indexZiSealTable($wdb)
	{
		$table = 'wenlin_zi_seal';
		$data = []; // array for possibly multiple rows
		foreach ($this->shuowenMaps as $map) {
			$data[] = array(
				'hanzi' => $map->songUsv,
				'seal' => $this->headUsv,
				'distance' => $map->distance
			);
		}
		$wdb->insert($table, $data, __METHOD__);
	} // indexZiSealTable

} // ZiIndexInfo

////////////////////////////////////

class ZiReading
{
	public $pinyinToneless;
	public $tone; // a number 0-4
	public $jianti; // boolean
	public $fanti; // boolean
	public $valid; // boolean: pinyin is valid and (jianti || fanti)
	public $msg;

	public function __construct($py, $jianti, $fanti)
	{
		$this->valid = false;
		$this->msg = '';
		if (!$jianti && !$fanti) {
			$this->msg = "jianti and fanti cannot both be false";
			return;
		}
		list($this->pinyinToneless, $this->tone) = WenlinPinyin::validateAndSplit($py);
		if ($this->pinyinToneless == '') {
			$this->msg = "pinyin [$py] is invalid";
			return;
		}
		$this->jianti = $jianti;
		$this->fanti = $fanti;
		$this->valid = true;
	} // __construct

} // ZiReading

////////////////////////////////////

class ShuowenMap
{
	const SHUOWEN_PROPER_MAP_DISTANCE_MAX   = 0x7f; /* X: 00..7f for proper mappings: (best, 2nd best, ...)  */
	const SHUOWEN_QUESTIONABLE_MAP_DISTANCE = 0xa0; /* Y: for questionable/dubious mappings (following "?") */
	const SHUOWEN_IMPROPER_MAP_DISTANCE     = 0xc0; /* Z: for improper/confusable mappings (following "!") */
	const SHUOWEN_PUA_MAP_DISTANCE          = 0xe0; /* P: for PUA/comp mappings (following "#") */

	/* The Shuowen mappings are based on #v lines in the zidian entries for the *Shuowen* characters,
		not in the corresponding non-Seal Hanzi. */
	public $songUsv; // USV of Sòng (not seal!) Hanzi
	public $distance;

	public function __construct($songUsv, $distance)
	{
		$this->songUsv = $songUsv;
		$this->distance = $distance;
	} // __construct

} // ShuowenMap

////////////////////////////////////

class WenlinDB
{
	private $mw; // mediawiki database for writing, or null
	private $queries; // alternative to mediawiki, store queries in this string

	public function __construct()
	{
		if (defined('MEDIAWIKI') ) {
			$this->mw = wfGetDB(DB_MASTER);
			$this->queries = 'mw';
		}
		else {
			$this->mw = null;
			$this->queries = '';
		}
	} // __construct

	/** queries:
		Get the non-mediawiki queries for recent insert/delete.

		@return the queries, as an array of strings.
	*/
	public function queries()
	{
		return $this->queries;
	} // queries

	/** delete:
		Delete rows from a table.
		This is a wrapper for mediawiki delete() in db/Database.php, or if $wdb is null as when MEDIAWIKI is not defined,
		then we save the query as a string, $this->queries, accessible with our queries() method.

		@param $table the table name.
		@param string|array $a Array of conditions.
		@param string $fname Calling function name (use __METHOD__) for logs/profiling.
	*/
	public function delete($table, $a, $fname = __METHOD__)
	{
		if ($this->mw != null) {
			$this->mw->delete($table, $a, $fname);
			return;
		}
		$sql = "delete from $table where ";
		$first = true;
		foreach ($a as $field => $value) {
			if ($first) {
				$first = false;
			}
			else {
				$sql .= ' and ';
			}
			$sql .= "$field='$value'";
		}
		$this->queries .= $sql . ";\n";
	} // delete

	/** insert:
		Insert zero or more rows in a table.
		This is a wrapper for mediawiki insert() in db/Database.php, or if MEDIAWIKI is not defined,
		then we save the query as a string, $this->queries, accessible with our queries() method.

		@param $table the table name.
		@param $a either:
			- A single associative array. The array keys are the field names, and
				the values are the values to insert. The values are treated as data
				and will be quoted appropriately. If NULL is inserted, this will be
				converted to a database NULL.
			- An array with numeric keys, holding a list of associative arrays.
				This causes a multi-row INSERT on DBMSs that support it. The keys in
				each subarray must be identical to each other, and in the same order.
		@param string $fname Calling function name (use __METHOD__) for logs/profiling.

		Public, for calling from ZiIndexInfo.
	*/
	public function insert($table, $a, $fname = __METHOD__)
	{
		if (count($a) == 0) {
			return;
		}
		$multi = isset($a[0]) && is_array($a[0]);
		if ($this->mw != null) {
			$this->mw->insert($table, $a, $fname);
			$rowsExpected = $multi ? count($a) : 1;
			if ($this->mw->affectedRows() != $rowsExpected) {
				error_log("Wenlin insert, affected rows != rowsExpected"); // raise an exception?
			}
			return;
		}
		$keys = $multi ? array_keys($a[0]) : array_keys($a);
		$sql = 'insert into ' . "$table (" . implode(', ', $keys) . ') values ';
		if ($multi) {
			$first = true;
			foreach ($a as $row) {
				if ($first) {
					$first = false;
				}
				else {
					$sql .= ', ';
				}
				$sql .= '(' . self::makeSqlRowValues($row) . ')';
			}
		}
		else {
			$sql .= '(' . self::makeSqlRowValues($a) . ')';
		}
		$this->queries .= $sql . ";\n";
	} // insert

	/** makeSqlRowValues:
		Make a list of values, quoted, with commas in between, for an SQL "insert" command,
		using the given associative array.

		@param $a an array whose keys are the field names, and whose values are the values to insert.

		Public for calling from ZiIndexInfo.
	*/
	private static function makeSqlRowValues($a)
	{
		$first = true;
		$list = '';
		foreach ($a as $field => $value) {
			if ($first) {
				$first = false;
			}
			else {
				$list .= ', ';
			}
			/* Put quotes around all values. They are superfluous but harmless (on MySQL) for some types like integers.
				Mediawiki Database.php always puts quotes. */
			$list .= "'$value'";
		}
		return $list;
	} // makeSqlRowValues

} // class WenlinDB
