<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* To enable logging to FirePHP in Firefox browser, put this in private_inc/wow_db_settings.php:
		define("ENABLE_WENLIN_DEBUG", true);
	or
		define("ENABLE_WENLIN_DEBUG", false);
	It requires installation of FirePHP on the server. To begin with, it's true only for localhost (MacBook),
	and it's false for wenlin.biz and wenlin.co.

	To view the log in Firefox, choose Tools : Web Developer : Firebug : Open Firebug.
	Then, in the Firebug browser window, click on the Console tab.
*/
if (ENABLE_WENLIN_DEBUG) {
	require_once $_SERVER['DOCUMENT_ROOT'] . 'FirePHPCore/FirePHP.class.php'; 
}

class WenlinDebug
{
	private static $firephp;

	/** log:
		Write a message to the log.

		@param string the message text.
	*/
	public static function log($string)
	{
		// debug_print_backtrace(); die("ouch in log WenlinDebug.php");

		if (!ENABLE_WENLIN_DEBUG) {
			return;
		}
		if (!self::initialize()) {
			return;
		}
		static::$firephp->log($string, 'WenlinDebug log');
		// error_log($string);
	} // log

	/** initialize:
		Initialize debugging. 

		@return true for success, false for failure.
	*/
	public static function initialize()
	{
		if (!ENABLE_WENLIN_DEBUG) {
			return false;
		}
		static $initialized = false;
		static $succeeded = false;
		if ($initialized == false) {
			$initialized = true;
			#Start buffering the output. Not required if output_buffering is set on in php.ini file
			ob_start();
			static::$firephp = FirePHP::getInstance(true);
			$succeeded = true;
		}
		return $succeeded;
	} // initialize

} // class WenlinDebug
