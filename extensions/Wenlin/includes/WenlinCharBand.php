<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** 
	Wenlin routines for processing the "char" band of a cidian entry;
	for counting the number of syllables, swapping simple/complex forms, etc.

	Make sure don't assume both jianti and [fanti] include slashes!
	Could be just one or the other.
*/
class WenlinCharBand
{
	const JIANTI_ZERO = 0;
	const FANTI_ONE = 1;

	const SLASH_NONE = 0;
	const SLASH_SINGLE = 1;
	const SLASH_DOUBLE = 2;

	const CB_NO_ERR = "好";
	const CB_BAD_BRACKET = "{A} Invalid bracket notation";
	const CB_BAD_SLASH = "{B} Invalid slash notation";
	const CB_TOO_LONG = "{C} Too long";
	const CB_TOO_SHORT = "{D} Too short";
	const CB_TOO_MANY_VARS = "{E} Too many variants (slashes)";
	const CB_MEMORY_ERROR = "{F} Memory allocation error";
	const CB_FIX_BAD_ZI = "{G} Missing zidian entry for character";
	const CB_FIX_BAD_PY = "{H} Characters don't match pinyin";
	const CB_BAD_JIAN_FAN = "{I} Simple/full form characters do not match";
	const CB_FIX_BAD_JF = "{J} Simple/full form characters need fixing to match";
	const CB_FIX_AMBIG = "{K} Simple/full form characters need clarification";
	const CB_MISSING_CHAR = "{L} Missing char band";
	const CB_FIX_BOTH_JF = "{M} Head character can be both simple and full form";
	const CB_FIX_CHANGED = "{N} Changes were made";
	const CB_FIX_FANTI_AMBIG = "{O} Full form character is ambiguous";
	const CB_FIX_NOT_BOTH = "{P} Ambiguous";

	const CB_ILLEGAL_OFFSET = 0xffff; /* An integer, not like other CB_* */

	/* We need to enforce limits like MAX_ZI_PER_CI, MAX_CI_VARIANTS, as in C code.
		At first this might seem unnecessary for PHP code, but if an entry breaking the limits were created
		with the web browser interface, and then got synced from web into .wenlindb file, an error would occur
		in the C code. */
	const MAX_ZI_PER_CI = 20; /* Max allowed number of syllables (or 儿) in a word. */
	const MAX_CI_VARIANTS = 20; /* Max allowed length of out array. */
	const MAX_SLASHES_PER_SYL = 20;

	/* out is for output to be filled in, an array of strings of Hanzi (each string has length sylCount).
		For example, if in = "词/辞典学[詞/辭-學]", then expandCharBand will set:
			out[0] = "词典学"; out[1] = "辞典学"; out[2] = "詞典學"; out[3] = "辭典學".
		Compare ciVarArray in CiIndexInfo.
		Maybe we should use the name ciVarArray here as well; it's "output" for expandCharBand,
		but indexing routines, for example, use it as input. */
	public $out;

	/* sylCount is the number of syllables in the word, if we count "-r" suffix as a syllable;
		or it is the number of Hanzi in a single way of writing the word.
		For example, for "cídiǎnxué 词/辞典学[詞/辭-學]", sylCount = 3.
		Compare CiIndexInfo sylCount. */
	public $sylCount;

	public $cbStatus;

	/* inArray is the input array of Hanzi, exploded from the char band content string
		(which is like "词/辞典学[詞/辭-學]"). It may include slashes and fanti in brackets
		(possibly with hyphens), but no abridgement notation. */
	private $inArray;
	private $inCount; // count(inArray) -- not same as sylCount if there are brackets or slashes
	private $ciVarIndex; // how many variants output so far
	private $slashType; // SLASH_NONE, SLASH_SINGLE, or SLASH_DOUBLE
	private $bra, $ket; /* offset positions of bra (openBracket) and ket (closeBracket), in *in */
	private $jiantiNumSlashesForSyl, $fantiNumSlashesForSyl, $slashForSyl; // arrays
	private $dsCount; // array of two DoubleSlashCount elements: double slash info for jianti, and for fanti

	/** TODO: function comment for __construct */
	public function __construct()
	{
		$this->ciVarIndex = 0;
		$this->slashType = self::SLASH_NONE;
		$this->bra = 0;
		$this->ket = 0;
		$this->cbStatus = self::CB_NO_ERR;
		$this->inArray = null;
		$this->jiantiNumSlashesForSyl = null;
		$this->fantiNumSlashesForSyl = null;
		$this->slashForSyl = null;
		$this->dsCount = null;
	} // __construct

	/** expandCharBand:
		Given in = char band content string, may include slashes and fanti in brackets
		(possibly with hyphens), but no abridgement notation.
		Expand into out[] = multiple strings without slashes or brackets.

		Example: in = "gre/ayness[GRE/AY----]"
			--> out = {"greyness", "grayness", "GREYness", "GRAYness"}

		@param string $s the char band content string, may include slashes and fanti in brackets
				(possibly with hyphens), but no abridgement notation.
		@param boolean $reallyExpand true if we should expand into multiple strings (out[]), 
										or false if we only want syntax check and sylCount.
		@return CB_NO_ERR if successful, or CB_BAD_BRACKET, etc., for error.

		On successful return, $charBand->out, $charBand->sylCount are set.
		$charBand->out filled in with an array of multiple strings without slashes or brackets.
	*/
	public function expandCharBand($s, $reallyExpand)
	{
		if (strlen($s) == 0) {
			return self::CB_TOO_SHORT;
		}
		$this->reallyExpand = $reallyExpand;
		$this->inArray = WenlinUtf::explode($s);
		$this->inCount = count($this->inArray);
		$this->cbStatus = $this->findBrackets(); // get bra, ket
		if ($this->cbStatus != self::CB_NO_ERR) {
			return $this->cbStatus;
		}
		$this->cbStatus = $this->findSlashes();
		if ($this->cbStatus != self::CB_NO_ERR) {
			return $this->cbStatus;
		}
		switch ($this->slashType) {
		case self::SLASH_SINGLE:
			$this->cbStatus = $this->ecbSingleSlashes();
			break;
		case self::SLASH_DOUBLE:
			$this->cbStatus = $this->ecbDoubleSlashes();
			break;
		//case self::SLASH_NONE:
		default:
			$this->cbStatus = $this->ecbNoSlashes();
			break;
		}
		// free the private arrays we're finished with
		$this->inArray = null;
		$this->jiantiNumSlashesForSyl = null;
		$this->fantiNumSlashesForSyl = null;
		$this->slashForSyl = null;
		$this->dsCount = null;

		if ($this->cbStatus == self::CB_NO_ERR) {
			if (count($this->out) > self::MAX_CI_VARIANTS) {
				$this->cbStatus = self::CB_TOO_MANY_VARS;
			}
			elseif ($this->sylCount > self::MAX_ZI_PER_CI) {
				$this->cbStatus = self::CB_TOO_LONG;
			}
		}
		return $this->cbStatus;
	} // expandCharBand

	/** findBrackets:
		Set $this->bra and $this->ket to the offsets of '[' and ']', respectively,
		or leave them with their default values of 0 if there are no brackets.

		If have left bracket, then must have right bracket.
		Only one of each, or none.

		@return CB_NO_ERR for success, or CB_BAD_BRACKET for bad brackets.
	*/
	private function findBrackets()
	{
		for ($ofs = 0; $ofs < $this->inCount; $ofs++) {
			if ($this->inArray[$ofs] == '[') {
				if ($ofs == 0 || $this->bra != 0) {
					return self::CB_BAD_BRACKET;
				}
				$this->bra = $ofs;
			}
			elseif ($this->inArray[$ofs] == ']') {
				if ($this->ket != 0 || $this->bra == 0 || $this->bra == $ofs - 1 || $ofs + 1 != $this->inCount) {
					return self::CB_BAD_BRACKET;
				}
				$this->ket = $ofs;
			}
		}
		if ($this->bra != 0 && $this->ket == 0) {
			return self::CB_BAD_BRACKET;
		}
		return self::CB_NO_ERR;
	} // findBrackets

	/** findSlashes:
		Find any slashes in the char band and set $this->slashType accordingly.
		$this->slashType is already SLASH_NONE by default; this function may
		change it to SLASH_SINGLE or SLASH_DOUBLE.

		Check for valid notation: may have either single-slashes or double-slashes, but not combination.

		findBrackets must be called before findSlashes.

		@return CB_NO_ERR if OK, or CB_BAD_SLASH for invalid slash notation.
	*/
	private function findSlashes()
	{
		for ($ofs = 0; $ofs < $this->inCount; $ofs++) {
			if ($this->inArray[$ofs] == '/') {
				if ($ofs == 0) {
					return self::CB_BAD_SLASH;
				}
				if ($this->inArray[$ofs + 1] == '/') { // double slash
					if ($this->slashType == self::SLASH_SINGLE || $this->inArray[$ofs + 2] == '/' /* triple */) {
						return self::CB_BAD_SLASH;
					}
					$this->slashType = self::SLASH_DOUBLE;
					++$ofs;
				}
				else {
					if ($this->slashType == self::SLASH_DOUBLE) {
						return self::CB_BAD_SLASH;
					}
					$this->slashType = self::SLASH_SINGLE;
				}
				// don't allow slash at the very end, or adjacent to bracket
				if ($this->bra != 0) {
					if ($ofs + 1 == $this->bra
						|| $ofs - 1 == $this->bra
						|| $ofs + 1 == $this->ket) {
						return self::CB_BAD_SLASH;
					}
				}
				elseif ($ofs + 1 == $this->inCount) {
					return self::CB_BAD_SLASH;
				}
			}
		}
		return self::CB_NO_ERR;
	} // findSlashes

	/** ecbNoSlashes:
		Expand char band, given that it contains no slashes.

		@return the CharBandStatus.
	*/
	private function ecbNoSlashes()
	{
		if ($this->bra == 0) {
			// No slashes, no brackets. Easy!
			$this->sylCount = $this->inCount;
			if ($this->reallyExpand) {
				$this->out[] = implode($this->inArray);
			}
			return self::CB_NO_ERR;
		}
		// compare number of zi inside and outside brackets -- must be same!
		// xyz[XYZ] ... number of jiantizi = $this->bra; number of fantizi = (ket - bra - 1)
		$this->sylCount = $this->bra;
		if ($this->ket - $this->bra - 1 != $this->sylCount) {
			return self::CB_BAD_BRACKET;
		}
		if ($this->reallyExpand) {
			$len = $this->bra;
			$outArrayJianti = array_slice($this->inArray, 0, $len);
			$outArrayFanti = array_slice($this->inArray, $len + 1, $len);
			for ($i = 0; $i < $len; $i++) { // replace fanti hyphens with jianti
				if ($outArrayFanti[$i] == '-') {
					$outArrayFanti[$i] = $outArrayJianti[$i];
				}
			}
			$this->out[0] = implode($outArrayJianti);
			$this->out[1] = implode($outArrayFanti);
		}
		return self::CB_NO_ERR;
	} // ecbNoSlashes

	/** ecbSingleSlashes:
		Expand char band, given that it contains single-slash notation (not double-slash).

		@return the CharBandStatus.
	*/
	private function ecbSingleSlashes()
	{
		$this->cbStatus = $this->ssCountSyls(); // count number of syllables (set $this->sylCount)
		if ($this->cbStatus != self::CB_NO_ERR) {
			return $this->cbStatus;
		}
		$this->jiantiNumSlashesForSyl = array_fill(0, $this->sylCount, 0);
		if ($this->bra != 0) {
			$this->fantiNumSlashesForSyl = array_fill(0, $this->sylCount, 0);
		}
		$this->slashForSyl = array_fill(0, $this->sylCount, 0);
		$this->cbStatus = $this->ecbSS();
		return $this->cbStatus;
	} // ecbSingleSlashes

	/** ssCountSyls:
		Count the number of syllables -- set $this->sylCount.

		@return the CharBandStatus (CB_NO_ERR or error code).
	*/
	private function ssCountSyls()
	{
		$ofs = 0;
		while ($ofs < $this->inCount && $this->inArray[$ofs] != '[') {
			$this->sylCount++;
			$ofs++;
			while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
				$ofs += 2;
			}
		}
		// The rest is only validity checking
		if ($this->bra != 0) {
			$fantiSylCount = 0;
			$ofs = $this->bra + 1;
			while ($ofs < $this->ket) {
				$fantiSylCount++;
				$ofs++;
				while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
					$ofs += 2;
				}
			}
			if ($fantiSylCount != $this->sylCount) {
				return self::CB_BAD_JIAN_FAN;
			}
		}
		return self::CB_NO_ERR;
	} // ssCountSyls

	/** ecbSS:
		Expand char band, given that it contains single-slash notation (not double-slash).
		Example: in = "gre/ayness[GRE/AY----]"
			--> out = {"greyness", "grayness", "GREYness", "GRAYness"}

		$this->sylCount has already been set.
		$this->jiantiNumSlashesForSyl and $this->fantiNumSlashesForSyl have been allocated
				-- and initalized with all zeros -- but not filled in.
		Warning --- don't assume both jianti and [fanti] include slashes! Could
		be just one or the other!

		@return the CharBandStatus.
	*/
	private function ecbSS()
	{
		// fill in $this->jiantiNumSlashesForSyl and $this->fantiNumSlashesForSyl
		$this->cbStatus = $this->ssCountJiantiAndFantiVariants();
		if ($this->cbStatus != self::CB_NO_ERR) {
			return $this->cbStatus;
		}
		/* Only call ssReplaceHyphensWithHanzi if reallyExpand.
			There seems to be no reason to call it otherwise, and this way we solve
			the bug where substituteHyphensIfNeeded returned CB_FIX_CHANGED even when
			the result is the same as the original char band that was passed to CreateCharBandInfo.
			I made the same change here as in the corresponding C code.
			See <https://gitlab.com/Wenlin/wenlin/issues/10>.
			TODO: it would probably be even better if we never modified inArray,
			except for functions whose purpose is to modify inArray.
			We should be able to replace the hyphens only in outArrayFanti, as is already
			done by ecbNoSlashes. Compare ecbDoubleSlashes, which has similar problem. */
		/* Replace hyphens in brackets with hanzi -- modify $this->inArray. */
		if ($this->bra != 0) {
			$this->ssReplaceHyphensWithHanzi(false /* don't swap */);
		}
		// From now on do jianti and fanti separately.
		if ($this->reallyExpand) {
			$this->ssInToOut(0, $this->jiantiNumSlashesForSyl); // jianti
			if ($this->bra != 0) {
				$this->ssInToOut($this->bra + 1, $this->fantiNumSlashesForSyl); // fanti
			}
		}
		return self::CB_NO_ERR;
	} // ecbSS

	/** ssCountJiantiAndFantiVariants:
		Fill in $this->jiantiNumSlashesForSyl and $this->fantiNumSlashesForSyl.

		Note: the slash count for each syllable is zero-based, but the variant
		totals are one-based -- if there are no slashes, that counts as one "variant".

		@return the CharBandStatus.
	*/
	private function ssCountJiantiAndFantiVariants()
	{
		$ofs = 0;
		for ($syl = 0; $syl < $this->sylCount; $syl++) {
			++$ofs;
			while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
				$ofs += 2;
				$this->jiantiNumSlashesForSyl[$syl]++;
				if ($this->jiantiNumSlashesForSyl[$syl] > self::MAX_SLASHES_PER_SYL) {
					return self::CB_TOO_MANY_VARS;
				}
			}
		}
		if ($this->bra != 0) {
			$ofs = $this->bra + 1;
			for ($syl = 0; $syl < $this->sylCount; $syl++) {
				++$ofs;
				while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
					$ofs += 2;
					$this->fantiNumSlashesForSyl[$syl]++;
					if ($this->fantiNumSlashesForSyl[$syl] > self::MAX_SLASHES_PER_SYL) {
						return self::CB_TOO_MANY_VARS;
					}
				}
			}
		}
		return self::CB_NO_ERR;
	} // ssCountJiantiAndFantiVariants

	/** ssReplaceHyphensWithHanzi:
		Single-slash. Replace hyphens in brackets with hanzi -- modify $this->in.
		Assume FindBrackets, FindSlashes already called.
		Most often, there will be the same pattern of slashes inside brackets as
		before the brackets. However, there are exceptions.
			fish[F/G---]  more slashes in fanti
			f/gish[F---]  more slashes in jianti
		Therefore must go syllable-by-syllable. If hyphen is in a syllable with
		variants, then (examples are monosyllabic words):
			a/b[-/B]  ... a/b[a/B]
			a/b[A/-]  ... a/b[A/b]
			a[-/B]    ... a[a/B]
			a[A/-]    ... a[A/a]
			a/b[-]    ... a/b[a]
			a/b[-/-]  ... a/b[a/b]
		Rule: if hyphen is in nth position within syllable, replace it with the nth
		jianti for the same syllable, unless there is no nth jianti for that
		syllable, in which case replace it with the last jianti for that syl.
		Compare ValidateSyllableWithMultipleVariants, which needs to be consistent.

		@param Boolean $swap true if we should swap jianti with fanti, else false.
	*/
	private function ssReplaceHyphensWithHanzi($swap)
	{
		$jianOfs = 0;
		$fanOfs = $this->bra + 1;
		do { // for each syllable
			for (;;) { // for each fanti variant in the syllable
				if ($this->inArray[$fanOfs] == '-') { // if it is a hyphen
					$this->inArray[$fanOfs] = $this->inArray[$jianOfs]; // replace it with hanzi
					if ($swap) {
						$this->inArray[$jianOfs] = '-';
					}
				}
				/* There may or may not be more jianti variants.
					If not, don't point to next jianti syllable until no
					more fanti variants; keep pointing to last jianti variant. */
				if ($this->inArray[$jianOfs + 1] == '/') {
					$jianOfs += 2; // point to next jianti variant
				}
				++$fanOfs; // next fanti variant or next syllable
				if ($this->inArray[$fanOfs] == '/') {
					++$fanOfs; // there is another fanti variant
				}
				else {
					break; // now $fanOfs points to next syllable
				}
			}
			++$jianOfs; // make $jianOfs point to next syllable
			while ($this->inArray[$jianOfs] == '/') {
				$jianOfs += 2; // skip any remaining jianti variants
			}
		} while ($fanOfs < $this->ket);
	} // ssReplaceHyphensWithHanzi

	/** ssInToOut:
		Single-slash. Copy from in to out, adjust $this->slashForSyl[].
		We can forget about brackets now.
		The total number of ci variants is the product (multiplicative) of the numbers of
		variants for each syllable.
		-- like enumerating all the numbers with a given number of digits, but
		the "base" is different for each place --
			"0/1 0 0/1/2" --> {"000", "001", "002", "100", "101", "102"}
			"a/b a a/b/c" --> {"aaa", "aab", "aac", "baa", "bab", "bac"}

		@param $ofs the offset of the first character (0 for jianti, $this->bra + 1 for fanti).
		@param $numSlashesForSyl the array indicating how many slashes for each syllable.
	*/
	private function ssInToOut($ofs, $numSlashesForSyl)
	{
		$this->slashForSyl = array_fill(0, $this->sylCount, 0);
		for (;;) {
			$this->ssIO($ofs, $numSlashesForSyl);
			for ($syl = $this->sylCount - 1; $this->slashForSyl[$syl] == $numSlashesForSyl[$syl]; $syl--) {
				if ($syl == 0) {
					return;
				}
				$this->slashForSyl[$syl] = 0;
			}
			$this->slashForSyl[$syl]++;
		}
	} // ssInToOut

	/** ssIO:
		Single-slash. Copy from $this->inArray to $this->out, choosing variants according to the
		current settings in $this->slashForSyl[].

		@param $ofs the offset of the first character (0 for jianti, $this->bra + 1 for fanti).
		@param $numSlashesForSyl the array indicating how many slashes for each syllable.
	*/
	private function ssIO($ofs, $numSlashesForSyl)
	{
		$this->out[$this->ciVarIndex] = '';
		for ($syl = 0; $syl < $this->sylCount; $syl++) {
			$this->out[$this->ciVarIndex] .= $this->inArray[$ofs + $this->slashForSyl[$syl] * 2];
			$ofs += 1 + $numSlashesForSyl[$syl] * 2;
		}
		$this->incrementVarIndexIfUnique();
	} // ssIO

	/** ecbDoubleSlashes:
		Expand char band, given that it contains double-slash notation (not single-slash).
		WARNING: jianti and [fanti] might not BOTH include slashes!

		@return the CharBandStatus.
	*/
	private function ecbDoubleSlashes()
	{
		// count number of syllables (set $this->sylCount)
		$endOfs = ($this->bra > 0) ? $this->bra : $this->inCount;
		$dsc = $this->dsCount[self::JIANTI_ZERO] = new DoubleSlashCount();
		$this->cbStatus = $dsc->dsCountSyls($this->inArray, 0, $endOfs);
		if ($this->cbStatus != self::CB_NO_ERR) {
			return $this->cbStatus;
		}
		$this->sylCount = $dsc->sylCount;
		if ($this->bra) { // fill in $this->dsCount[self::FANTI_ONE]
			$dsc = $this->dsCount[self::FANTI_ONE] = new DoubleSlashCount();
			$this->cbStatus = $dsc->dsCountSyls($this->inArray, $this->bra + 1, $this->ket);
			if ($this->cbStatus != self::CB_NO_ERR) {
				return $this->cbStatus;
			}
			if ($dsc->sylCount != $this->sylCount) {
				return self::CB_BAD_JIAN_FAN;
			}
		}
		if ($this->reallyExpand) {
			$this->dsInToOut($this->dsCount[self::JIANTI_ZERO]);
			if ($this->bra) {
				/* TODO: avoid modifying inArray here with dsReplaceHyphensWithHanzi;
					see comment about same problem in ecbSS. */
				$this->dsReplaceHyphensWithHanzi(false /* swap */);
				$this->dsInToOut($this->dsCount[self::FANTI_ONE]);
			}
		}
		return self::CB_NO_ERR;
	} // ecbDoubleSlashes

	/** dsInToOut:
		Double-slash. Copy in to out. (Compare ssIO for single-slash.)

		@param DoubleSlashCount $dsc .
	*/
	private function dsInToOut(DoubleSlashCount $dsc)
	{
		/* There are three parts to a double-slash string: the "prefix", the "middle", and the "suffix".
			The prefix and suffix are identical for all variants. The middle is the part that varies. */
		$prefixOfs = $dsc->startOfs;
		$middleOfs = $dsc->startOfs + $dsc->prefixSylCount;
		$suffixOfs = $dsc->endOfs - $dsc->suffixSylCount;
		for ($var = 0; $var <= $dsc->doubleSlashCount; $var++) {
			$out = '';
			for ($i = 0; $i < $dsc->prefixSylCount; $i++) {
				$out .= $this->inArray[$prefixOfs + $i];
			}
			for ($i = 0; $i < $dsc->middleSylCount; $i++) {
				$out .= $this->inArray[$middleOfs++];
			}
			$middleOfs += 2; // skip past double-slash for next time through loop
			for ($i = 0; $i < $dsc->suffixSylCount; $i++) {
				$out .= $this->inArray[$suffixOfs + $i];
			}
			$this->out[$this->ciVarIndex] = $out;
			$this->incrementVarIndexIfUnique();
		}
	} // dsInToOut

	/** dsReplaceHyphensWithHanzi:
		Double-slash. Replace hyphens in brackets with hanzi -- modify $this->inArray.
		Most often, there will be the same pattern of slashes inside brackets as
			before the brackets. However, there are exceptions.
				fish[FI--//SH]	more slashes in fanti  = {"fish", "FIsh", "FISH"}
				tree//oo[T---]	more slashes in jianti = {"tree", "troo", "Tree"}

		Note: "fish[--SH//--]" = {"fish", "fiSH", "fish"} = {"fish", "fiSH"} = "fish[--SH]" ?
			The meanings of "fish[--SH//--]" and "fish[--SH]" are different: the former
		implies that both "fiSH" and "fish" can occur among fantizi, the latter implies
		that only "fiSH" can occur among fantizi. Still, as with "Tai" in "Taiwan", it
		seems reasonable to use the simpler notation.

		Given a hyphen among the fantizi, which jiantizi do we replace it with?
		suppose the structure is like this:
			"alpha//beta//gamma//...//omega[ALPHA//BETA//GAMMA//...//OMEGA]"
		If the number of double-slashes is same for jianti and fanti, and if the lengths
		of alpha & ALPHA are the same, and likewise for beta & BETA etc., then the
		replacement rule is simple: go to the corresponding offset within the
		corresponding string.
			Complications could occur if jiantiDoubleSlashCount != fantiDoubleSlashCount,
		or if the string lengths are different.
			The solution for jiantiDoubleSlashCount != fantiDoubleSlashCount seems clear,
		like for single-slash: if a hyphen occurs in GAMMA, then replace it with the
		corresponding jiantizi in gamma -- unless there is no gamma, in which case use
		beta (or alpha if no beta). "fish[FISH//--]" = {"fish", "FISH", "FIsh"}.
			For different string lengths, the problem seems trickier.
				"abc//def[ABC//E-]" = {"abc", "def", "ABC", "AEf"}
				"alpha//omega[ALPHA//OMEGA]"
			The hyphen in the second syllable in OMEGA corresponds to the third syllable
		in omega. Must go by which syllable in the whole word.

		@param Boolean $swap true to swap jianti with fanti, else false.

		Called by ecbDoubleSlashes and swapCBJF.
		The caller has already created new $this->dsCount[self::FANTI_ONE].
	*/
	private function dsReplaceHyphensWithHanzi($swap)
	{
		$fanDSC = $this->dsCount[self::FANTI_ONE];
		$syl = 0;
		$var = 0;
		for ($fanOfs = $this->bra + 1; $fanOfs < $this->ket;) {
			if ($this->inArray[$fanOfs] == '-') {
				$jianOfs = $this->computeDSJiantiOffset($syl, $var);
				$this->inArray[$fanOfs] = $this->inArray[$jianOfs];
				if ($swap) {
					$this->inArray[$jianOfs] = '-';
				}
			}
			++$fanOfs;
			if ($this->inArray[$fanOfs] == '/') {
				$fanOfs += 2;
				++$var;
				$syl = $fanDSC->prefixSylCount;
			}
			elseif (++$syl == $fanDSC->prefixSylCount + $fanDSC->middleSylCount) {
				$var = 0; // we're now in the suffix
			}
		}
	} // dsReplaceHyphensWithHanzi

	/** computeDSJiantiOffset:
		Double-slash. Compute the jianti offset for the given syllable and variant.

		@param $syl the index of the syllable.
		@param $var the index of the variant within the syllable.
		@return the offset of the ith jiantizi for the jth syllable, where i = $var, j = syl; or,
				if there is no ith jiantizi for the jth syllable, return the offset of the last
				jiantizi for the jth syllable.
	*/
	private function computeDSJiantiOffset($syl, $var)
	{
		do {
			$ofs = $this->dsJFOffset(self::JIANTI_ZERO, $syl, $var);
			if ($ofs != self::CB_ILLEGAL_OFFSET) {
				return $ofs;
			}
		} while ($var-- != 0);
		return 0; // should never happen if notation is valid (dsc->count == $this->count)
	} // computeDSJiantiOffset
	
	/** incrementVarIndexIfUnique:
		If the latest variant char band is unique, then increment ciVarIndex.
	*/
	private function incrementVarIndexIfUnique()
	{
		for ($i = 0; $i < $this->ciVarIndex; $i++) {
			if ($this->out[$i] == $this->out[$this->ciVarIndex]) {
				return; // not unique
			}
		}
		$this->ciVarIndex++; // unique
	} // IncrementVarIndexIfUnique

	/** charBandHasBrackets:
		Does the char band have brackets?

		@return true if the char band has brackets, else false.
	*/
	public function charBandHasBrackets()
	{
		return $this->bra != 0;
	} // charBandHasBrackets

	/** charBandSyllableCount:
		Get the number of syllables represented by the char band.

		@return the number of syllables.
	*/
	public function charBandSyllableCount()
	{
		return $this->sylCount;
	} // charBandSyllableCount

	/** substituteHyphensIfNeeded:
		If fantizi = jiantizi for any syl, replace it with a hyphen rather than
		duplicating the hanzi.
		If any hyphens need to be put in, do so and return self::CB_FIX_CHANGED.

		@return CB_FIX_CHANGED or CB_NO_ERR.

		Called by FixCiCharBand.
	*/
	public function substituteHyphensIfNeeded()
	{
		$this->cbStatus = self::CB_NO_ERR;
		if ($this->ket == 0) { /* Don't need hyphens if no brackets. */
			return $this->cbStatus;
		}
		for ($syl = 0; $syl < $this->sylCount; $syl++) {
			$jVarCount = $this->charBandNumberOfVariantsInSyllable(self::JIANTI_ZERO, $syl);
			$fVarCount = $this->charBandNumberOfVariantsInSyllable(self::FANTI_ONE, $syl);
			if ($jVarCount > 1) {
				if ($fVarCount == 1) {
					// Change 大伙/夥当家[-伙當-] to 大伙/夥当家[--當-]; jVarCount = 2, fVarCount = 1.
					$fOfs = CharBandJiantiOrFantiOffset(self::FANTI_ONE, $syl, 0);
					for ($var = 0; $var < jVarCount; $var++) {
						$jOfs = CharBandJiantiOrFantiOffset(self::JIANTI_ZERO, $syl, $var);
						$zin = $this->inArray[$fOfs];
						if ($zin == $this->inArray[$jOfs]
							&& $zin != ','
							&& $zin != '('
							&& $zin != ')') {
							$this->inArray[fOfs] = '-';
							$this->cbStatus = CB_FIX_CHANGED;
						}
					}
				}
				elseif ($jVarCount == $fVarCount) {
					// Change "𫘤/呆板[騃/呆-]" to "𫘤/呆板[騃/--]".
					for ($var = 0; $var < $fVarCount; $var++) {
						$jOfs = CharBandJiantiOrFantiOffset(self::JIANTI_ZERO, $syl, $var);
						$fOfs = CharBandJiantiOrFantiOffset(self::FANTI_ONE, $syl, $var);
						$zin = $this->inArray[fOfs];
						if ($zin == $this->inArray[jOfs]
							&& $zin != ','
							&& $zin != '('
							&& $zin != ')') {
							$this->inArray[fOfs] = '-';
							$this->cbStatus = CB_FIX_CHANGED;
						}
					}
				}
				// else continue -- could do more here (jVarCount > 1, fVarCount > 1, jVarCount != fVarCount)
			}
			else { // jVarCount == 1
				$jOfs = $this->charBandJiantiOrFantiOffset(self::JIANTI_ZERO, $syl, 0);
				for ($var = 0; $var < $fVarCount; $var++) {
					$fOfs = CharBandJiantiOrFantiOffset(self::FANTI_ONE, $syl, $var);
					$zin = $this->inArray[fOfs];
					if ($zin == $this->inArray[jOfs]
						&& $zin != ','
						&& $zin != '('
						&& $zin != ')') {
						$this->inArray[fOfs] = '-';
						$this->cbStatus = CB_FIX_CHANGED;
						break; // from inner loop only
					}
				}
			}
		}
		return $this->cbStatus;
	} // substituteHyphensIfNeeded

	/** charBandNumberOfVariantsInSyllable:
		Get the number of character variants for the specified syllable.
		For example, "x/y/z" has 3 variants; "x" has one variant.

		@param $jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl the index of the syllable.
		@return the number of variants in the syllable, including the first or "standard" form
				(never return 0).
	*/
	public function charBandNumberOfVariantsInSyllable($jorf, $syl)
	{
		switch ($this->slashType) {
		case self::SLASH_NONE:
			return 1;
		case self::SLASH_SINGLE:
			return $this->ssVarsInSyl($jorf, $syl);
		// case self::SLASH_DOUBLE:
		default:
			break;
		}
		return $this->dsVarsInSyl($jorf, $syl);
	} // charBandNumberOfVariantsInSyllable

	/** ssVarsInSyl:
		Get the number of character variants for the specified syllable, given that single-slash
		("SS") notation is used.

		@param jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl the index of the syllable.
		@return the number of variants in the syllable, including the first or "standard" form
				(never return 0).
	*/
	private function ssVarsInSyl($jorf, $syl)
	{
		$ofs = ($jorf == self::JIANTI_ZERO) ? 0 : $this->bra + 1;
		while ($syl-- > 0) {
			++$ofs; // next variant or next syllable
			while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
				$ofs += 2;
			}
		}
		$varCount = 1;
		++$ofs;
		while ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
			++$varCount;
			$ofs += 2;
		}
		return $varCount;
	} // ssVarsInSyl

	/** dsVarsInSyl:
		Get the number of character variants for the specified syllable, given that double-slash
		("DS") notation is used.

		@param $jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl the index of the syllable.
		@return the number of variants in the syllable, including the first or "standard" form
				(never return 0).
	*/
	private function dsVarsInSyl($jorf, $syl)
	{
		$varCount = 1;
		while ($this->dsJFOffset($jorf, $syl, $varCount) != self::CB_ILLEGAL_OFFSET) {
			++$varCount;
		}
		return $varCount;
	} // dsVarsInSyl

	/** charBandJiantiOrFantiOffset:
		Get the offset for a jianti or fanti character specified by the syllable index
		and variant index within that syllable.

		@param $jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl the index of the syllable.
		@param $var the index of the variant.
		@return the offset of the Hanzi for the syllable.
	*/
	public function charBandJiantiOrFantiOffset($jorf, $syl, $var)
	{
		switch ($this->slashType) {
		case self::SLASH_DOUBLE:
			return $this->dsJFOffset($jorf, $syl, $var);
		// case self::SLASH_NONE:
		// case self::SLASH_SINGLE:
		default:
			return $this->ssJFOffset($jorf, $syl, $var);
		}
	} // charBandJiantiOrFantiOffset

	/** ssJFOffset:
		Get the offset for a jianti or fanti character specified by the syllable index
		and variant index within that syllable, given that single-slash ("SS") notation
		is used (or no slashes).

		@param $jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl0 the index of the syllable.
		@param $var0 the index of the variant.
		@return the offset of the Hanzi for the syllable.
	*/
	private function ssJFOffset($jorf, $syl0, $var0)
	{
		$ofs = ($jorf == self::JIANTI_ZERO) ? 0 : $this->bra + 1;
		$var = 0;
		for ($syl = 0; $syl != syl0 || $var != $var0;) {
			++$ofs; // next variant or next syllable
			if ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
				++$ofs; // there is another variant
				++$var;
			}
			else {
				$var = 0;
				if (++$syl > $syl0) {
					return self::CB_ILLEGAL_OFFSET;
				}
			}
		}
		return $ofs;
	} // ssJFOffset

	/** dsJFOffset:
		Get the offset for a jianti or fanti character specified by the syllable index
		and variant index within that syllable, given that double-slash ("DS") notation
		is used.

		@param $jorf self::JIANTI_ZERO if we're interested in the jianti form, else self::FANTI_ONE.
		@param $syl0 the index of the syllable.
		@param $var0 the index of the variant.
		@return the offset of the Hanzi for the syllable.
	*/
	private function dsJFOffset($jorf, $syl0, $var0)
	{
		$dsc = $this->dsCount[$jorf];
		$ofs = $dsc->startOfs;
		$var = 0;
		for ($syl = 0; $syl != $syl0 || $var != $var0;) {
			++$ofs;
			if ($ofs < $this->inCount && $this->inArray[$ofs] == '/') {
				$ofs += 2; // skip past double-slash
				++$var;
				$syl = $dsc->prefixSylCount;
			}
			else {
				if (++$syl == $this->sylCount) {
					return self::CB_ILLEGAL_OFFSET;
				}
				if ($syl == $dsc->prefixSylCount + $dsc->middleSylCount) {
					$var = 0; // we're now in the suffix
				}
			}
		}
		return $ofs;
	} // dsJFOffset

	/** TODO: function comment for getCharBandNoBracketsNoVariants
	*/
	public static function getCharBandNoBracketsNoVariants($s, $dictPref)
	{
		if ($s == null) {
			return $s;
		}
		$to = '';
		$array = WenlinUtf::explode($s);
		$count = count($array);
		$preferSimple = $dictPref->getPreferSimple();
		for ($i = 0; $i < $count; $i++) {
			if ($array[$i] == '/') {
				if ($array[$i+1] == '/') {
					return self::dsGetCharBandNoBracketsNoVariants($array, $dictPref);
				}
			}
		}
		$fanOfs = 0;
		$ofs = 0;
		$len = 0;
		$p = 0;
		if ($preferSimple == false) {
			while ($array[$p] !== '[' && $p + 1< $count) {
				$p += 1;
			}
			if ($p + 1 !== $count) {
				$ofs = $p + 1;
				$fanOfs = $ofs;
			}
		}
		do {
			$to .= $array[$ofs++];
			if ($ofs < $count) {
				while ($ofs < $count && $array[$ofs] == '/') {
					$ofs += 2; 
				}
			}
		} while ( $ofs < $count && $array[$ofs] !== '[' && $array[$ofs] !== ']');

		if ($preferSimple == false && $fanOfs != 0) {
			$ofs = 0;
			$toArray = WenlinUtf::explode($to);
			$toCount = count($toArray);
			for ($i = 0; $i < $toCount; $i++) {
				if ($toArray[$i] == '-') {
					$toArray[$i] = $array[$ofs];
				}
				++$ofs;
				while ($array[$ofs] == '/' && $ofs+2 < $count) {
					$ofs += 2;
				}
			}
			$to = '';
			for ($i = 0; $i < $toCount; $i++) {
				$to .= $toArray[$i];
			}
		}
		return $to;
	} // getCharBandNoBracketsNoVariants

	/** TODO: function comment for dsGetCharBandNoBracketsNoVariants
	*/
	private static function dsGetCharBandNoBracketsNoVariants($array, $dictPref)
	{
		$bra = 0;
		$count = count($array);
		$preferSimple = $dictPref->getPreferSimple();
		while ($bra < $count - 1 && $array[$bra] != '[') {
			$bra++;
		}
		$fanti = ($preferSimple == false && $bra != $count - 1);
		if ($fanti) {
			$array = self::swapCharBandJiantiFanti($array);
			$bra = 0;
			while ($bra < $count - 1 && $array[$bra] != '[') {
				$bra++;
			}
		}
		$endOfs = ($bra == $count - 1) ? $count : $bra;
		return self::dsGetCBNoVar($array, $endOfs);
	} // dsGetCharBandNoBracketsNoVariants

	/** TODO: function comment for dsGetCBNoVar
	*/
	private static function dsGetCBNoVar($array, $endOfs) 
	{
		$dsc = new DoubleSlashCount();
		$cbStatus = $dsc -> dsCountSyls($array, 0, $endOfs);
		$ofs = 0;
		$to = '';
		for ($i = $dsc->prefixSylCount + $dsc->middleSylCount; $i > 0; $i--) {
			$to .= $array[$ofs++];
		}
		$ofs = $endOfs - $dsc->suffixSylCount;
		for ($i = $dsc->suffixSylCount;$i > 0; $i--) {
			$to .= $array[$ofs++]; 
		}
		return $to;
	} // dsGetCBNoVar

	/** TODO: function comment for swapCharBandJiantiFanti
	*/
	public static function swapCharBandJiantiFanti($array)
	{
		$charBand = new WenlinCharBand();
		$charBand->inArray = $array;
		$charBand->inCount = count($array);

		$charBand = self::swapCBJF($charBand);
		return $charBand->inArray;
	} // swapCharBandJiantiFanti

	/** TODO: function comment for swapCBJF
	*/
	private static function swapCBJF($charBand)
	{
		if ($charBand->inCount <= 2) {
			return $charBand;
		}
		$charBand->cbStatus = $charBand->findBrackets();
		if ($charBand->bra == 0) {
			return $charBand;
		}
		$bra = $charBand->bra;
		while ($bra < $charBand->inCount - 1) {
			$bra++;
			if ($charBand->inArray[$bra] == '-') {
				break;
			}
		}
		if ($bra != $charBand->inCount - 1) {
			$flag = 0;
			for ($i = 0; $i < $charBand->inCount; $i++) {
				if ($charBand->inArray[$i] == '/') {
					if ($charBand->inArray[$i+1] == '/') {
						$flag = 1;
					}
				}
			}
			if ($flag == 0) {
				$charBand->ssReplaceHyphensWithHanzi(true);
			}
			else {
				$dsc = $charBand->dsCount[self::JIANTI_ZERO] = new DoubleSlashCount();
				$charBand->cbStatus = $dsc->dsCountSyls($charBand->inArray, 0, $charBand->bra);
				$dsc = $charBand->dsCount[self::FANTI_ONE] = new DoubleSlashCount();
				$charBand->cbStatus = $dsc->dsCountSyls($charBand->inArray, $charBand->bra + 1, $charBand->ket);
				$charBand->dsReplaceHyphensWithHanzi(true);
			}
		}
		$charBand = self::simplySwap($charBand);
		return $charBand;
	} // swapCBJF

	/** TODO: function comment for simplySwap
	*/
	private static function simplySwap($charBand)
	{
		if ($charBand->inCount <= 2) {
			return $charBand;
		}
		$copy = $charBand->inArray;
		$p = 0;
		for ($ofs = $charBand->bra + 1; $ofs < $charBand->ket; $ofs++)  {
			$charBand->inArray[$p++] = $copy[$ofs];
		}
		$needBrackets = FALSE;
		for ($ofs = 0; $ofs < $charBand->bra; $ofs++) {
			$z = $copy[$ofs];
			if ($z !== '-' && $z !== '/') {
				$needBrackets = TRUE;
				break;
			}
		}
		if ($needBrackets) {
			$charBand->inArray[$p++] = '[';
			for ($ofs = 0; $ofs < $charBand->bra; $ofs++) {
				$charBand->inArray[$p++] = $copy[$ofs];
			}
			$charBand->inArray[$p++] = ']';
		}
		return $charBand;
	} // simplySwap

} // class WenlinCharBand

class DoubleSlashCount
{
	public $doubleSlashCount;
	public $sylCount, $alphaSylCount, $omegaSylCount, $middleSylCount;
	public $prefixSylCount, $suffixSylCount;
	public $startOfs, $endOfs;

	/** TODO: function comment for __construct
	*/
	public function __construct()
	{
		$this->doubleSlashCount = 0;
		$this->sylCount = 0;
		$this->alphaSylCount = 0;
		$this->omegaSylCount = 0;
		$this->middleSylCount = 0;
		$this->prefixSylCount = 0;
		$this->suffixSylCount = 0;
		$this->startOfs = 0;
		$this->endOfs = 0;
	} // __construct

	/** dsCountSyls:
		Double-slash. Count number of syllables -- fill in dsc.

		@param $inArray the array of characters.
		@param $startOfs the offset of the first character (0 for jianti, or bra + 1 for fanti)
		@param $endOfs the offset after the last character (bra or end of string for jianti, or ket for fanti).
		@return the CharBandStatus.

		Examples of double-slash:
		"ab//cd" --> {"ab", "cd"}
		"abc//def" --> {"abc", "def"}
		"abc//de" --> {"abc", "ade"}
		"ab//cde" --> {"abe", "cde"}
		"ab//cd//ef" --> {"ab", "cd", "ef"}
		"abc//de//fg" --> {"abc", "ade", "afg"}
		"ab//cd//efg" --> {"abg", "cdg", "efg"}
		"abcd//efgh" --> {"abcd", "efgh"}
		Analyze as alpha//beta//gamma//delta//...//omega
		where alpha etc. are strings. All strings must be the same length,
		except that the first or the last (but not both!) may be longer, in which
		case the start of the first string, or the end of the last string, is
		to be shared by all the variants.

		Sept 8, 2001: Some new ABC2 entries violate the old convention.
		"xin pengpeng tiao" is "abc//def" --> {"abcf", "adef"} !
		"nian2qing1qing1r" same.

		The only hints are that there are four syllables in the pinyin,
		and that the middle syllable is doubled. We can't rely on the
		pinyin. The doubling is a dubious basis for generalizing a rule,
		but we could recognize it.

		"ABB//CCD" --> {"ABBD", "ACCD"}

		this overrides the rule

		"abc//def" --> {"abc", "def"}

		on the basis that b=c and d=e.

		"nian2hu1hu1(r de0)" would be another problem if we didn't ignore "(r de0)"...

		For only two or three entries, doesn't seem worth the trouble. Better just
		re-write as "abbc//aBBc".
	*/
	function dsCountSyls($inArray, $startOfs, $endOfs)
	{
		$this->startOfs = $startOfs;
		$this->endOfs = $endOfs;

		/* Set alphaSylCount = number of syllables before first slash,
				omegaSylCount = number of syllables after last slash. */
		$ofs = $this->startOfs;
		do {
			$this->alphaSylCount++;
			if (++$ofs == $this->endOfs) {
				// $this->doubleSlashCount = $this->prefixSylCount = $this->suffixSylCount = 0;
				$this->sylCount = $this->middleSylCount = $this->alphaSylCount;
				return WenlinCharBand::CB_NO_ERR;
			}
		} while ($inArray[$ofs] != '/');
		$ofs = $this->endOfs - 1;
		do {
			$this->omegaSylCount++;
		} while ($ofs > 0 && $inArray[--$ofs] != '/');
		$omegaSlashOfs = $ofs - 1;
		/* The total number of syllables in the word is equal to the longer of alpha and omega. */
		/* If there are any strings surrounded on both sides by double-slashes, then
			their length must be middleSylCount, equal to the shorter of alpha and omega. */
		if ($this->alphaSylCount > $this->omegaSylCount) {
			$this->prefixSylCount = $this->alphaSylCount - $this->omegaSylCount;
			$this->suffixSylCount = 0;
			$this->sylCount = $this->alphaSylCount;
			$this->middleSylCount = $this->omegaSylCount;
		}
		else {
			$this->prefixSylCount = 0;
			$this->suffixSylCount = $this->omegaSylCount - $this->alphaSylCount;
			$this->sylCount = $this->omegaSylCount;
			$this->middleSylCount = $this->alphaSylCount;
		}
		/* Count double-slashes */
		$ofs = $this->startOfs + $this->alphaSylCount + 2; // skip past alpha and first double-slash
		$this->doubleSlashCount = 1;
		$pieceLen = 0;
		while ($ofs <= $omegaSlashOfs) {
			if ($inArray[$ofs] == '/') {
				if ($pieceLen != $this->middleSylCount) {
					return WenlinCharBand::CB_BAD_SLASH;
				}
				$ofs += 2;
				$this->doubleSlashCount++;
				$pieceLen = 0;
			}
			else {
				++$ofs;
				++$pieceLen;
			}
		}
		return WenlinCharBand::CB_NO_ERR;
	} // dsCountSyls

} // class DoubleSlashCount
