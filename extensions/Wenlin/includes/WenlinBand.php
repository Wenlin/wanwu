<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/** WenlinBand:
	Process dictionary entries that use band notation.
*/
class WenlinBand
{
	/** translateToDisplayFormat:
		Translate from band notation to display format (for both the Cidian and Ying-Han).

		@param string textB the input band-notation text.
		@param string namespaceStr Ci or En or Jyut (Cantonese).
		@param WenlinParser wlParser, used for parseWikiToHtml.
		@param WenlinDictionaryPreferences dictPref the dictionary display preferences.
		@param WenlinBandContext bandContext the context-specific parameters for translating band notation.
		@param WenlinEntryInfo entryInfo the entry information to be filled in, if not null, for the benefit of the caller.
		@return the display-format text as HTML (wiki markup already processed by parseWikiToHtml as needed).
	*/
	public static function translateToDisplayFormat($textB, $namespaceStr, WenlinParser $wlParser,
		WenlinDictionaryPreferences $dictPref = null, WenlinBandContext $bandContext = null, WenlinEntryInfo $entryInfo = null)
	{
		if ($dictPref == null) {
			$dictPref = WenlinDictionaryPreferences::singleton();
		}
		if ($bandContext == null) {
			$bandContext = new WenlinBandContext();
		}
		$bandTranslator = new WenlinBandConverter($textB, $namespaceStr, $wlParser, $dictPref, $bandContext, $entryInfo);
		$bandTranslator->loop();
		$bandTranslator->afterLoop();
		return $bandTranslator->textD;
	} // translateToDisplayFormat

	/** adjustForPolyglotNotation:
		Adjust the content of a band taking polyglot notation into account.

		"Polyglot notation" means: the content starts with something like "[en]"
		or "[fr]", that is, a two-letter language code in brackets.

		If there is no polyglot notation, return the content unchanged.
		If there is polyglot notation and the language code matches the user's
		preferred language, return the content that follows the notation.
		If there is polyglot notation and the language code doesn't match
		the user's preferred language, return an empty string.

		"The user's preferred language" is tricky. We base it on the generic
		MediaWiki "language" preference, but if it's not in our list of supported
		languages (namely, 'en' and 'fr', currently), we use 'en'.

		TODO: make more flexible rather than hard-code ['en', 'fr'] here.
		Ideally, before displaying, scan all the available versions of the band
		in question, and if the most preferred language is there, use it, else
		fall back to one that is present. For example,
			1df   [en] dog
			1df   [fr] chien
			1df   [es] perro
		All have same band number/name "1df", so make a list ['en', 'fr', 'es']
		and then fall back on 'en' if preferred language isn't in the list.

		@param $content the content string.
		@param $dictPref the WenlinDictionaryPreferences.
		@return the modified content.
	*/
	public static function adjustForPolyglotNotation($content, $dictPref)
	{
		if (mb_substr($content, 0, 1) !== '[') {
			return $content; // no polyglot notation
		}
		$lang = $dictPref->getPolyglotLanguage();
		if ($lang !== "en" && $lang !== "fr") {
			$lang = "en";
		}
		$end = mb_strpos($content, "]");
		$bandLang = mb_substr($content, 1, $end - 1);
		if ($bandLang === $lang) {
			return trim(mb_substr($content, $end + 1));
		}
		return '';
	} // adjustForPolyglotNotation

	/** makeCiButtons:
		Make various buttons for adding to the end of a Cidian entry.

		@param term the Pinyin headword.
		@param char the char band, without variants.
		@return the buttons.

		Compare C functions CidianAddButtons, AppendPyAdjacentButton
		"list adjacent words by pinyin"
	*/
	public static function makeCiButtons($term, $char)
	{
		$namespaceStr = 'Ci';
		$monopoly = 'poly';
		$py = $term;
		if (mb_strlen($char) == 1) {
			$monopoly = 'mono';
			list($pinyinToneless, $tone) = WenlinPinyin::validateAndSplit($py);
			$py = "$pinyinToneless$tone"; // compare how WenlinZhixing::describePinyinSyllable calls makeAudioLink
		}
		$audioLink = WenlinGongju::makeAudioLink($py, $monopoly);
		if ($audioLink != '') {
			$audioLink .= "<br />\n";
		}
		$u = urlencode($term);
		return "<br />\n"
			. $audioLink
			. WenlinGongju::makeAdjacentAlphaLink($term, $namespaceStr) . "<br />\n"
			. WenlinGongju::makeFulltextSearchLink($term) . "<br />\n"
			. WenlinGongju::makeWebSearchButtons($char, $namespaceStr);
	} // makeCiButtons

	/** makeAudioLink:
		Make zero or more buttons for audio.

		@param term the term to be pronounced, such as a Pinyin headword.
		@param string namespaceStr Ci or En or Jyut (Cantonese).
		@return the buttons, as a string.
	*/
	/** makeJyutButtons:
		Make various buttons for adding to the end of a Jyut entry.

		@param term the Jyutping headword.
		@param char the char band, without variants.
		@return the buttons.
	*/
	public static function makeJyutButtons($term, $char)
	{
		$u = urlencode($term);
		$namespaceStr = 'Jyut';
		return "<br />\n"
			. WenlinGongju::makeAdjacentAlphaLink($term, $namespaceStr) . "<br />\n"
			. WenlinGongju::makeFulltextSearchLink($term) . "<br />\n"
			. WenlinGongju::makeWebSearchButtons($char, $namespaceStr);
	} // makeJyutButtons

	/** makeEnButtons:
		Make various buttons for adding to the end of a Yinghan entry.

		@param term the English headword.
		@return the buttons.
	*/
	public static function makeEnButtons($term)
	{
		$u = urlencode($term);
		$namespaceStr = 'En';
		return "<br />\n"
			. WenlinGongju::makeAdjacentAlphaLink($term, $namespaceStr) . "<br />\n"
			. WenlinGongju::makeFulltextSearchLink($term) . "<br />\n"
			. WenlinGongju::makeWebSearchButtons($term, $namespaceStr);
	} // makeEnButtons

	/** getNeededBands:
		Get the values for a specified set of band names.

		@param string $text the cidian or yinghan entry in band notation.
		@param array $bandsNeeded the array of band names needed for indexing.
		@return the array of bands and values needed (keys are from $bandsNeeded; values are band contents).

		Note: if a band name occurs twice in the entry, the values for all but the last get overwritten.
		This function is mainly intended for bands that only occur once.
	*/
	public static function getNeededBands($text, array $bandsNeeded)
	{
		$bands = [];

		/* Avoid strtok, use explode instead; strtok is dangerous if it's also called by subroutines */
		$lineArray = explode("\n", $text);
		for ($i = 0; $i < count($lineArray); $i++) {
			$line = $lineArray[$i];
			$line = rtrim($line);
			if ($i == 0) {
				$line = ltrim($line, '.'); // remove erroneous leading dot in some original En wiki entries.
			}
			list($digits, $name, $comment, $content) = self::parseBand($line);
			if (in_array($name, $bandsNeeded)) {
				$bands[$name] = $content; // could do self::unabridge here??
			}
			if ($name == 'h') {
				break;
			}
		}
		return $bands;
	} // getNeededBands

	/** parseBand:
		Parse the band into digits, name, comment, and content; return them as an array.

		@param string band the input text.
		@return array (digits, name, comment, content).

		http://stackoverflow.com/questions/5692568/php-function-return-array
	*/
	public static function parseBand($band)
	{
		// Not sure whether /u matters when the regex itself contains only ascii, but better safe than sorry.
		if (preg_match("/^(\d*)(\w+)(\S*)\s+(.+)$/u", $band, $matches)) {
			return array($matches[1], $matches[2], $matches[3], $matches[4]);
		}
		if ($band === 'h' || $band === 'auto') {
			return array('', $band, '', ''); // special allowance for "h" and "auto" bands; content not needed.
		}
		return array('', '', '', ''); // empty strings: band is invalid
	} // parseBand

	/** abridge:
		Interpret abridgment notation, and return an "abridged" version of the given string.

		Remove @a@ but keep what follows it.
		Remove @@ and anything following it.
		Remove @{...} including all between the curly braces.
		The caller should check whether we return '' (empty string).

		@param s the string to be modified.
		@return the string, possibly modified, containing no abridgment notation.
	*/
	public static function abridge($s)
	{
		if (strncmp($s, "@a@", 3) == 0) {
			return mb_substr($s, 3);
		}
		$i = mb_strpos($s, '@@');
		if ($i !== false) {
			return mb_substr($s, 0, $i);
		}
		while (($i = mb_strpos($s, '@{')) !== false && ($j = mb_strpos($s, '}', $i + 2)) !== false) {
			$s = mb_substr($s, 0, $i) . mb_substr($s, $j + 1);
		}
		return $s;
	} // abridge

	/** unabridge:
		Interpret abridgment notation, and return an "unabridged" version of the given string.

		Remove @@ (but not anything following it).
		Remove @{...} but keep what is between the curly braces.
		Remove entire content if it starts with @a@.
		The caller should check whether we return '' (empty string).

		@param s the string to be modified.
		@return the string, possibly modified, containing no abridgment notation.
	*/
	public static function unabridge($s)
	{
		if (strncmp($s, "@a@", 3) == 0) {
			return '';
		}
		/* NOTE: @@ syntax is invalid if more than one @@ occurs (non-first remain);
			See CheckAbridgeNotation in C code. But we don't check that here. */
		$s = str_replace('@@', '', $s);
		while (($i = mb_strpos($s, '@{')) !== false && ($j = mb_strpos($s, '}', $i + 2)) !== false) {
			$s = mb_substr($s, 0, $i) . mb_substr($s, $i + 2, $j - $i - 2) . mb_substr($s, $j + 1);
		}
		return $s;
	} // unabridge

	/** getHeadwordFromContentText:
		Get the headword from the given entry content text.

		@param $contentText the entry content text.
		@param $namespaceStr the namespace string.
		@param $outputPage the OutputPage.
		@return the headword.
	*/
	public static function getHeadwordFromContentText($contentText, $namespaceStr, $outputPage)
	{
		$entryInfo = new WenlinEntryInfo();
		$bandContext = new WenlinBandContext();
		$bandContext->hwStop = true;
		$wlParser = new WenlinParser(null, $outputPage);
		$dictPref = WenlinDictionaryPreferences::singleton();
		WenlinBand::translateToDisplayFormat($contentText, $namespaceStr, $wlParser, $dictPref, $bandContext, $entryInfo);
		return $entryInfo->headword;
	} // getHeadwordFromContentText

} // WenlinBand
