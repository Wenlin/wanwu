<?php
/* Copyright (c) 2020 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* WenlinRender:
	Render dictionary entries. That is, convert them from the format in which they are stored in
	the database (and displayed while editing), into the format in which they should be displayed to
	the user (when not editing).
*/
class WenlinRender
{
	/** renderWL:
		Convert the input text to HTML output.
		Entries are surrounded like <WL>...</WL>. Convert the text between the tags to HTML.
		This is for showing the regular page for a single entry.
		We're called as a hook set by onParserFirstCallInit, so the protocol for the parameters
		needs to be exactly like this.

		@param text the input text (not including  <WL>...</WL> tags).
		@param args the array of tag arguments (unused), which are entered like HTML tag attributes;
					this is an associative array indexed by attribute name. Supposing the WL tag had
					attributes like <WL beast="dragon">, we could access them thru args.
		@param parser the Parser, used for recursiveTagParse.
		@param frame the PPFrame, used for getTitle and recursiveTagParse.
		@return the converted text.

		Note: page title display is suppressed for namespaces Zi, Ci, Jyut, and En using MediaWiki:Common.css
		to make the style (class="firstHeading") invisible, because "Ci:1004949003" displayed in bold at the top
		of the page is unattractive; the entry headword is more suitable. We compensate here by showing
		the entry headword somewhat enlarged.
			(Look into hooking onBeforePageDisplay as alternative to MediaWiki:Common.css?)

		Call stack:
		#0  WenlinRender::renderWL()
		#1  call_user_func_array() called at [/Users/tbishop/Sites/wanwu/includes/parser/Parser.php:4212]
		#2  Parser->extensionSubstitution() called at [/Users/tbishop/Sites/wanwu/includes/parser/Preprocessor_DOM.php:1252]
		#3  PPFrame_DOM->expand() called at [/Users/tbishop/Sites/wanwu/includes/parser/Parser.php:3322]
		#4  Parser->replaceVariables() called at [/Users/tbishop/Sites/wanwu/includes/parser/Parser.php:1231]
		#5  Parser->internalParse() called at [/Users/tbishop/Sites/wanwu/includes/parser/Parser.php:434]
		#6  Parser->parse() called at [/Users/tbishop/Sites/wanwu/includes/content/WikitextContent.php:333]
		#7  WikitextContent->fillParserOutput() called at [/Users/tbishop/Sites/wanwu/includes/content/AbstractContent.php:497]
		#8  AbstractContent->getParserOutput() called at [/Users/tbishop/Sites/wanwu/includes/poolcounter/PoolWorkArticleView.php:140]
		#9  PoolWorkArticleView->doWork() called at [/Users/tbishop/Sites/wanwu/includes/poolcounter/PoolCounterWork.php:123]
		#10 PoolCounterWork->execute() called at [/Users/tbishop/Sites/wanwu/includes/page/Article.php:674]
		#11 Article->view() called at [/Users/tbishop/Sites/wanwu/includes/actions/ViewAction.php:44]
		#12 ViewAction->show() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:395]
		#13 MediaWiki->performAction() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:273]
		#14 MediaWiki->performRequest() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:566]
		#15 MediaWiki->main() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:414]
		#16 MediaWiki->run() called at [/Users/tbishop/Sites/wanwu/index.php:41]
	*/
	public static function renderWL($text, array $args, Parser $parser, PPFrame $frame)
	{
		// WenlinDebug::log("renderWL HOWDY HOWDY HOWDY\n");
		// debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS); die("\nrenderWL ouch\n");
		$wlParser = new WenlinParser($parser, null);
		$titleObj = $frame->getTitle();
		$namespaceNumber = $titleObj->getNamespace();
		$dictPref = WenlinDictionaryPreferences::singleton();
		if ($namespaceNumber == NS_WL_CI // Cidian
		 || $namespaceNumber == NS_WL_EN // Ying-Han
		 || $namespaceNumber == NS_WL_JYUT) { // Jyut (jyut6 jyu5 粵語 Cantonese)
			$namespaceStr = $titleObj->getNsText();
			$titleStr = $titleObj->getText(); // NOT getFullText (which includes namespace)
			if (!WenlinXuke::entryIsViewableByCurrentUser($namespaceStr, $titleStr, $msg)) { // may change $msg
				// TODO: even if user lacks permission, we may display some part of the entry, such as the headword...
				$text = $wlParser->parseWikiToHtml($msg);
			}
			else {
				$text = WenlinBand::translateToDisplayFormat($text, $namespaceStr, $wlParser, $dictPref);
			}
		}
		elseif ($namespaceNumber == NS_WL_ZI) {
			$text = self::renderZidian($text, $wlParser, $dictPref, false /* not singleLine */);
		}
		elseif ($namespaceNumber == NS_WL_CDL) {
			// For NS_WL_CDL, we might render the CDL as an image, with stroking box controls, etc.; see wenlin/c/ch_skbox.c.
			// $text = self::renderCdl($text, ...);
		}
		self::appendGenericWikiPageInfo($text, $titleObj, $wlParser);
		return $text;
	} // renderWL

	/** renderEntrySingleLine:
		Convert the input text to HTML output.
		Entries are surrounded like <WL>...</WL>. Remove the tags and convert the text between them to HTML.
		This function is for lists of entries, so we produce the concise one-line version of the entry.
		We're called by the execute() method of a SpecialPage, using our own protocol. 

		@param $text the input text including the surrounding <WL>...</WL> tags.
		@param Title $titleObj the entry title object (not the same as $specialPage->getPageTitle()!).
		@param WenlinParser $wlParser.
		@param WenlinDictionaryPreferences $dictPref.
		@param WenlinBandContext $bandContext.
		@param WenlinEntryInfo $entryInfo the entry information to be filled in (like the headword).
		@param $renderSub if true, it's to render subentry.
		@return the converted text.
	*/
	public static function renderEntrySingleLine($text, Title $titleObj, WenlinParser $wlParser,
			WenlinDictionaryPreferences $dictPref, WenlinBandContext $bandContext, WenlinEntryInfo $entryInfo = null)
	{
		WenlinGongju::stripWLTag($text);
		$namespaceNumber = $titleObj->getNamespace();
		if ($namespaceNumber == NS_WL_CI || $namespaceNumber == NS_WL_EN || $namespaceNumber == NS_WL_JYUT) {
			if ($bandContext === null) {
				$bandContext = new WenlinBandContext();
			}
			$bandContext->singleLine = true;
			$text = WenlinBand::translateToDisplayFormat($text, $titleObj->getNsText(), $wlParser, $dictPref, $bandContext, $entryInfo);
		}
		elseif ($namespaceNumber == NS_WL_ZI) {
			$text = self::renderZidian($text, $wlParser, $dictPref, true /* $singleLine */);
		}
		else {
			$namespaceStr = $titleObj->getNsText();
			$text .= "\n<p>Unrecognized namespace: $namespaceStr</p>";
		}
		return $text;
	} // renderEntrySingleLine

	///////////////////////////////////////////////////
	// APPEND WIKI PAGE INFO FOR ANY NAMESPACE
	///////////////////////////////////////////////////

	/** appendGenericWikiPageInfo:
		Append some generic Wiki page information to HTML output.

		@param $text the input text, to which we append more text; passed by reference.
		@param Title $titleObj the Title object.
		@param WenlinParser wlParser, used for parseWikiToHtml.
	*/
	private static function appendGenericWikiPageInfo(&$text, Title $titleObj, WenlinParser $wlParser)
	{
		$text .= "\n<hr />\n";
		$namespaceStr = $titleObj->getNsText();
		$link = WenlinGongju::makeNamespaceLink($namespaceStr);
		$text .= "$link<br />\n";
		$url = $titleObj->getFullURL();
		$text .= "URL: $url<br />\n";
		$text .= $wlParser->parseWikiToHtml("[$url?action=purge Refresh this page]<br />\n");

		// TESTING -- <http://www.w3schools.com/html/html5_audio.asp>
		// <https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_HTML5_audio_and_video>
		// <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio>
		// $text .= "<p><audio controls>\n<source src='https://wenlin.biz/luyin/recordings/chenghua/KE2YI3.WAV'></audio></p>";
	} // appendGenericWikiPageInfo

	///////////////////////////////////////////
	// ZIDIAN -- Namespace NS_WL_ZI
	///////////////////////////////////////////

	/** renderZidian:
		Convert the input Zidian (character dictionary) text to HTML output. For NS_WL_ZI.

		@param text the input text.
		@param WenlinParser wlParser, used for parseWikiToHtml.
		@param WenlinDictionaryPreferences $dictPref .
		@param Boolean singleLine.
		@return the converted text.

		Try to imitate "DoUseZi" in wenlin/c/ch_zi.c almost exactly,
		except appropriate changes for wiki context. Use an equivalent of the ZiEntryStuff struct.
	*/
	private static function renderZidian($textZ, WenlinParser $wlParser, WenlinDictionaryPreferences $dictPref, $singleLine = false)
	{
		/* Process the entry line-by-line. */
		$textZ = ltrim($textZ); // skip any whitespace, including newline that might have followed WL tag before that was stripped
		$lineArray = explode("\n", $textZ);
		$topLine = $lineArray[0];
		$topLine = $dictPref->getHideRareInfo() ? WenlinBand::abridge($topLine) : WenlinBand::unabridge($topLine);
		$zi = mb_substr($topLine, 0, 1); // The first character in the line is the head Hanzi.
		$zin = WenlinUtf::ziNumber($zi);
		$usvLower = dechex($zin); // dechex returns lowercase, which we want for png
		$usvUpper = strtoupper($usvLower);
		$isShuowen = WenlinFenlei::zinIsShuowen($zin);

		$msg = '?';
		$user = WenlinGongju::getCurrentUser($msg);
		if ($user === null || !WenlinXuke::entryIsViewableByThisUser($user, 'Zi', $zi, $msg)) { // may change $msg
			$msg = $wlParser->parseWikiToHtml($msg);
			if ($isShuowen) { // don't load the Shuowen font if user doesn't have viewing right
				$zi = "(Shuowen seal character U+$usvUpper)";
			}
			return "$zi [$msg]\n";
		}
		if ($dictPref->getHideToneChange()) {
			$topLine = WenlinPinyin::removeToneChangeNotation($topLine);
		}
		if ($dictPref->getHidePinyin()) {
			$topLine = self::hidePinyinInZiTopLine($topLine);
		} 
		if ($dictPref->getHideSFEquiv()) {
			$topLine = self::hideSFEquivInZiTopLine($topLine);
		}
		if ($dictPref->getHideGloss()) {
			$topLine = self::hideGlossInZiTopLine($topLine);
		}
		$hideHanzi = $dictPref->getHideHanzi(); // boolean
		if ($singleLine) {
			$topLine = self::makeHanziIntoLinksOrHide($topLine, ' ', $hideHanzi); // ' ' so we get wiki link even for head character
			$topLine = $wlParser->parseWikiToHtml($topLine); // after makeHanziIntoLinksOrHide
			return self::addPinyinLinksToZidianTopLine($topLine);
		}

		// Not single line.
		$topLineRemainder = mb_substr($topLine, 1);
		$topLine = "<span style='font-size:150%; font-weight: bold'>$zi</span>$topLineRemainder<br />";	

		/* It would be possible to be more efficient since WenlinXuke::entryIsViewableByThisUser has already called
			$user->isAllowed('WenlinRightToViewAllZi'). However, that value hasn't been stored. We could
			pass by reference another parameter to entryIsViewableByThisUser, to get the value... Or make
			entryIsViewableByThisUser return a numeric value rather than boolean... */
		$userHasShuowenRight = $isShuowen || $user->isAllowed('WenlinRightToViewAllZi');
		if (!$userHasShuowenRight) {
			$topLine = WenlinFenlei::hideShuowenSeal($topLine);
		}
		$lineCount = count($lineArray);
		$lineIndex = 1;
		$textD = ''; // destination text
		$insideExplanationDetails = false;
		while ($lineIndex < $lineCount) {
			$line = $lineArray[$lineIndex++];
			$line = rtrim($line);
			if (!$userHasShuowenRight) {
				$line = WenlinFenlei::hideShuowenSeal($line);
			}
			$c = mb_substr($line, 0, 1); // the first character in the line
			if ($c == '#') {
				$c = mb_substr($line, 1, 1); // the character following '#'
				$s = mb_substr($line, 2); // the remainder of the line
				if ($insideExplanationDetails && $c != 'i') {
					$textD .= "\n|}\n";
					$insideExplanationDetails = false;
				}
				if ($c == 'r') {
					$textD .= self::ziTranslateReferences($s);
				}
				elseif ($c == 'c') {
					$textD .= "[[Help:Components|►components:]] $s<br />";
				}
				elseif ($c == 'y') {
					$textD .= "[[Help:Cantonese|►Cantonese:]] $s<br />";
				}
				elseif ($c == 'i') { // image
					$src = $usvLower;
					if (preg_match('/^#ix\d+:(.+)$/u', $line, $matches)) {
						// extended image notation like "#ix3:src
						$src = $matches[1];
						$src0 = mb_substr($src, 0, 1); // the first character in src
						$srcZin = WenlinUtf::ziNumber($src0);
						if ($srcZin > 0x7F) { // if first character in name isn't ASCII, convert it to USV lowercase hex
							$src = strtolower(dechex($srcZin)) . mb_substr($src, 1); // e.g., 里fan becomes 91ccfan
						}
					}
					$textD .= "[[File:$src.png|x100px]]<br />";
				}
				elseif ($c == 's') { // read the simple form entry and insert its explanation here
					$textD .= self::getSimpleFormExplanationFromZidian($topLineRemainder);
				}
			}
			elseif ($line == '[Details]') {
				/*
					{| role="presentation" class="mw-collapsible mw-collapsed"
					| <i>Details</i>
					|-
					| Negar la sucesión temporal,<br>&nbsp;&nbsp;&nbsp;&nbsp;negar el yo ....
					|}
				*/
				$textD .= "{| role=\"presentation\" class=\"mw-collapsible mw-collapsed\n| <i>Details</i>\n|-\n| ";
				$insideExplanationDetails = true;
			}
			elseif ($line != '') {
				if ($c == "\t" || $c == " ") { // line starts with tab or space(s); treat same, indent
					$line = "&nbsp;&nbsp;&nbsp;&nbsp;" . ltrim($line);
				}
				if ($insideExplanationDetails) {
					$textD .= "$line<br />";
				}
				else {
					$textD .= "$line<br />\n";
				}
			}
		}
		if ($userHasShuowenRight && !$isShuowen) {
			$sw = self::appendShuowenSealCharacters(array($zin), false);
			if ($sw !== '') {
				$textD .= "Shuōwén: $sw<br />";
			}
		}
		$textD = self::makeHanziIntoLinksOrHide($textD, $zi, $hideHanzi);
		$textD = $wlParser->parseWikiToHtml($textD); // after makeHanziIntoLinksOrHide and replacing #r, etc.

		$buttons = self::makeBzAndZcButtons($zi);
		$webLinks = WenlinGongju::makeWebSearchButtons($zi, 'Zi'); 
		$usvText = "Unicode: U+" . $usvUpper . "<br />\n"; // TODO: link to "list characters by Unicode"

		$topLine = self::makeHanziIntoLinksOrHide($topLine, $zi, $hideHanzi);
		$topLine = $wlParser->parseWikiToHtml($topLine);
		$topLine = self::addPinyinLinksToZidianTopLine($topLine);
		if ($isShuowen) {
			$monoCiEntries = '';
			$strokeButton = '';
		}
		else {
			$monoCiEntries = WenlinZhixing::listMonosyllabicEntriesForZiEntry($zi, $wlParser, $dictPref);
			$strokeButton = self::makeStrokeButtonForZidianEntry($zi, $usvUpper);
		}
		return $topLine . $monoCiEntries . $textD . $strokeButton . $buttons . $usvText . $webLinks;
	} // renderZidian

	/** addPinyinLinksToZidianTopLine:
		Add pinyin links to the zidian top line.
		For example, given this input:
			巴 [bā] 尾巴 wěiba tail ...  [bà] 嘴巴子...
		make both [bā] and [bà] into links that will invoke WenlinZhixing::describePinyinSyllable
		with URLs like http://localhost/wow/Special:Wenlin/py?py=bā

		@param $topLine the input text.
		@return the converted text.
	*/
	private static function addPinyinLinksToZidianTopLine($topLine)
	{
		# die($topLine);
		$base = WenlinGongju::articlePathBase();
		$newTop = '';
		for ($i = 0; $i < strlen($topLine); $i++){
			$newTop .= mb_substr($topLine, $i, 1);
			if (mb_substr($topLine, $i, 1) == ']') {
				$startOfPinyin = mb_strrpos($newTop, '[');
				$endOfPinyin = mb_strrpos($newTop, ']');
				$pinyin = mb_substr($newTop, $startOfPinyin + 1, $endOfPinyin - $startOfPinyin - 1);
				$newTop = mb_substr($newTop, 0, $startOfPinyin);
				$u = urlencode($pinyin);
				$link = "[<a href='$base/Special:Wenlin/py?py=$u'>$pinyin</a>]";
				$newTop .= $link;
			}
		}
		return $newTop;
	} // addPinyinLinksToZidianTopLine

	/** makeBzAndZcButtons:
		Make bz and zc links for a zidian entry.

		@param zi the head character (Hanzi) of the Zidian entry.
		@return the text of the links.

		Called by renderZidian.
	*/
	private static function makeBzAndZcButtons($zi)
	{
		$u = urlencode($zi);
		$base = WenlinGongju::articlePathBase();
		return "<a href='$base/Special:Wenlin/bz?b=$u'>"
				. "►list characters containing $zi as a component</a><br />\n"
				. "<a href='$base/Special:Wenlin/zc?z=$u'>"
				. "►list words containing $zi (most common words first)</a><br />\n";
	} // makeBzAndZcButtons

	/** makeHanziIntoLinksOrHide:
		Find all Hanzi in the given string, then either make them into links or hide them.
		Change each Han character in $text (except for the head character $zi) to a wiki-link [[Zi:...]].
		OR, if $hideHanzi, then remove ALL Hanzi from the text.

		@param text the input text.
		@param zi the head character (Hanzi) of the entry, not to be made into a link; or ' ' if not applicable.
		@param hideHanzi true if all Hanzi should be hidden, else false.
		@return the converted text, as wiki text.

		Called by WenlinBand::translateToDisplayFormat for cidian char band, as well as locally.
	*/
	public static function makeHanziIntoLinksOrHide($text, $zi, $hideHanzi)
	{
		/* First match anything outside the range U+0000..2E7F, in order to avoid wasting time
			on the most frequent non-Hanzi. (Cf. MIN_RAD_ZIN = 0x2e80 in WenlinFenlei.php.)
			Then use WenlinFenlei::zinIsCJK for exact determination of what's a "Hanzi".
			The category includes some Wenlin PUA characters, so we can't rely completely
			on anything standard like [\\p{Han}]. */
		return preg_replace_callback(
			'/[^\\x{0000}-\\x{2E7F}]/u',
			function ($matches) use ($zi, $hideHanzi) { // Note "use ($zi, $hideHanzi)", necessary.
				$m = $matches[0];
				if ($m == $zi) { // don't add link to the head character
					return $hideHanzi ? '' : $m;
				}
				if (!WenlinFenlei::zinIsCJK(WenlinUtf::ziNumber($m))) {
					return $m; // don't add link to non-Hanzi
				}
				if ($hideHanzi) {
					return '';
				}
				/* Insert U+200B (zero-width space) to avoid "[[[" in char band
					like "马[馬]" → "[[马]][[[馬]]]", which would baffle the wiki parser. */
				return WenlinUtf::zinToStr(0x200B) . "[[Zi:$m|$m]]";
			}, $text);
	} // makeHanziIntoLinksOrHide

	/* getSimpleFormExplanationFromZidian:
		Get the "explanation" part of the simple form zidian entry corresponding to
		the full form entry we're displaying.

		For example, if we're displaying the zidian entry for the full form 馬, then we
		want to get the explanation from the simple form 马. Then we'll return:
		"(Explanation from the entry for the simple form 马:) Picture of a horse. Compare 鸟(鳥) niǎo ‘bird’."

		@param topLineRemainder the full form top line, minus the leading head character; e.g., "(S马) [mǎ] horse ...".
		@return the explanation, or '' if no simple-form entry or simple-form entry lacks explanation.

		Called by renderZidian.
	*/
	private static function getSimpleFormExplanationFromZidian($topLineRemainder)
	{
		/* $topLineRemainder should include a string like "(S马)". Normally it's at the beginning, but
			don't assume that. Find the first occurrence of "(S.)" where "." stands for a single character.
			Note that /u after the regex means UTF-8 (<http://php.net/manual/en/reference.pcre.pattern.modifiers.php>). */
		if (!preg_match("/\(S(.)\)/u", $topLineRemainder, $matches)) {
			return '';
		}
		$simpleForm = $matches[1]; // e.g., 马
		$jiantiTitleString = "Zi:" . $simpleForm;
		$entry = WenlinGongju::getContentTextFromTitleString($jiantiTitleString);
		if ($entry == null) {
			return '';
		}
		WenlinGongju::stripWLTag($entry); // bug fix: the closing WL tag was accidentally included
		$entryArray = explode("\n", $entry);
		$entryLength = count($entryArray);
		// The "explanation" part of a zidian entry is all the lines (except the top line) that don't start with '#'.
		$explanation = '';
		for ($i = 1; $i < $entryLength; $i++) {
			if (mb_substr($entryArray[$i], 0, 1) != '#') {
				if ($explanation == '') { // Beginning of explanation.
					$explanation = "(Explanation from the entry for the simple form $simpleForm:) ";
				}
				else { // Indent subsequent lines (paragraphs).
					$explanation .= "&nbsp;&nbsp;&nbsp;";
				}
				$explanation .= "$entryArray[$i]<br />\n";
			}
		}
		return $explanation;
	} // getSimpleFormExplanationFromZidian

	/** addStrokeButtonForZidianEntry:
		Add a "stroke" button for a Zidian entry, along with CDL in hidden span elements.

		@param zi the head character (Hanzi) of the entry.
		@param usv the uppercase hex USV corresponding to zi, like '4E00'.
		@return the HTML for the button.

		Fetch CDL from the wiki and put it in invisible span elements.
		Include the descriptions of all components, recursively, without doing
		any coordinate arithmetic. The JavaScript code will scale the stroke coordinates.
		Disguise XML by replacing < and > with &lt; and &gt;, because XML self-closing
		tag syntax doesn't work in HTML though it might work in XHTML. Otherwise the
		browser may ignore slash before greater-than "<stroke ... />", and then convert it into:
			<cdl char="十" uni="5341">
				<stroke type="h" points="0,58 128,58">
					<stroke type="s" points="64,0 64,128">
					</stroke>
				</stroke>
			</cdl>
		(but without added whitespace/indentation). "stroke" elements treated as nested, which fails.
		The JavaScript code will do the reverse conversion, replacing &lt; and &gt; with < and >.
	*/
	private static function makeStrokeButtonForZidianEntry($zi, $usv)
	{
		$cdlArray = array();
		self::getCdlForComponentsRecursively($usv, 0 /* variant */, $cdlArray);
		$text = '';
		foreach ($cdlArray as $uv => $cdl) {
			$cdl = str_replace('<', '&lt;', $cdl); // replace '<' with '&lt;'
			$cdl = str_replace('>', '&gt;', $cdl); // replace '>' with '&gt;'
			$text .= "<span id='strokingBoxCdl-$uv' hidden='hidden'>$cdl</span>\n";
		}
		if ($text == '') {
			return ''; // don't add button
		}
		/* The id="strokingStuff" needs to match strokebutton.js, which inserts a link to the stroking box,
			along with the stroke count and the "fanned character" (static stroke diagram) svg. We could just
			leave the element empty and let it be filled in by strokebutton.js. However, there would be a delay after
			most of the page gets displayed, before the "stroke" line gets displayed, while the javascript is
			loading and running (probably svg generation is a slow part). If we display "►stroke $zi (... strokes)"
			here at least temporarily as placeholder, the delay may be less visually jarring. Also, the javascript makes
			use of the width in pixels of that piece of text (including nbsp!), to determine how many pixels are available
			for the stroke diagram on the remainder of the line. Include $zi as the title attribute to make it
			convenient for the js to get it. */
		$text .= "<a href='#' id='strokingStuff' title='$zi'>►stroke $zi (<span id='strokeCount'>...</span> strokes)&nbsp;&nbsp;&nbsp;</a><br />\n";
		return $text;
	} // makeStrokeButtonForZidianEntry

	/** getCdlForComponentsRecursively:
		Get CDL for the given character and its components, and for all their components, recursively.

		@param $usv the uppercase hex USV of the character, like '4E00'.
		@param $variant the variant number for the character.
		@param $cdlArray an array we add to, whose keys are $usv-$variant and values are $cdl.
	*/
	private static function getCdlForComponentsRecursively($usv, $variant, &$cdlArray)
	{
		$uv = "$usv-$variant";
		$cdl = WenlinGongju::getContentTextFromTitleString("Cdl:$uv");
		if (!$cdl) {
			return;
		}
		$cdlArray[$uv] = $cdl;
		/* For each comp element in $cdl, call getCdlForComponentsRecursively recursively. */
		// temporary hack -- should parse XML with parser
		foreach (explode("\n", $cdl) as $line) {
			if (preg_match("/\\<comp.+\\s+uni='([\\S]+)'/u", $line, $matches)) {
				$usv = $matches[1];
				$variant = 0;
				if (preg_match("/\\s+variant='([\\S]+)'/u", $line, $matches)) {
					$variant = $matches[1];
				}
				if (!isset($cdlArray["$usv-$variant"])) { // avoid duplication
					self::getCdlForComponentsRecursively($usv, $variant, $cdlArray);
				}
			}
		}
	} // getCdlForComponentsRecursively

	/** hidePinyinInZiTopLine:
		Remove pinyin from the input string, which is a Zidian top line.

		@param $s the input string, Zidian top line.
		@return the modified string.
	*/
	protected static function hidePinyinInZiTopLine($s)
	{
		$pyStart = mb_strpos($s, "[");
		$pyEnd = mb_strpos($s, "]");
		$pinyin = mb_substr($s, $pyStart + 1, $pyEnd - $pyStart - 1 );

		// remove pinyin within brackets
		$pyS = mb_strpos($s, "[");
		$subS = $s;
		while ($pyS !== false) {
			$pyE = mb_strpos($subS, "]");
			$py = mb_substr($subS, $pyS, $pyE - $pyS + 1);
			$s = str_replace($py, "", $s);
			$subS = mb_substr($subS, $pyE + 1);
			$pyS = mb_strpos($subS, "[");
		}

		// remove other pinyin
		$range = $pyEnd - $pyStart - 1;
		$s = self::removeOtherPinyin($s, $range, $pinyin);

		return $s;
	} // hidePinyinInZiTopLine

	/** removeOtherPinyin:
		Remove pinyin based on range and given pinyin.

		@param $s the input string.
		@param $range
		@param $pinyin
		@return the modified string.
	*/
	protected static function removeOtherPinyin($s, $range, $pinyin)
	{
		$pyStart = 0; // check every index range
		$pyEnd = strlen($s);
		while ($pyStart + $range < $pyEnd) {
			$temp = mb_substr($s, $pyStart, $range);
			if ($temp === $pinyin) {
				$replace = "";
				for ($i = 0; $i < $range; $i++) {
					$replace .= '.';
				}
				$s = str_replace($temp, $replace, $s);
			}
			$pyStart++;
		}
		return $s;
	} // removeOtherPinyin

	/** hideGlossInZiTopLine:
		Hide the gloss part of the input string, which is Zidian top line.

		@param $s the input string
		@return the modified input string.
	*/
	protected static function hideGlossInZiTopLine($s)
	{
		$zi = mb_substr($s, 0, 1); // head character
		$s = mb_substr($s, 1); // ignore the head character
		$arr = preg_split("/[\s(),.;']{1}/", $s);
		// differentiate pinyin and english words from toplineremainder
		foreach ($arr as $value) {
			if (mb_substr($value, 0, 1) === 'F' ) { // escape full form (fanti)
				$key = array_search($value, $arr);
				$val = '(';
				$val .= $value;
				$val .= ')';
				$arr[$key] = $val;
			}
			else if (mb_substr($value, 0, 1) === 'S' ) { // escape simple form (jianti)
				$key = array_search($value, $arr);
				$val = '(';
				$val .= $value;
				$val .= ')';
				$arr[$key] = $val;
			}
			else if (preg_match("/[a-zA-Z0-9]+/", $value, $matches)) { // delete english words or numeric explanation
				if (WenlinPinyin::isAllPinyin($value)) { // delete pinyin
					$key = array_search($value, $arr);
					if ($key !== null) {
						unset($arr[$key]);
					}
				}
				else { // delete english words
					$len = strlen($value);
					$len_test = strlen("$matches[0]");
					if ($len === $len_test ) {
						$key = array_search($value, $arr);
						if ($key !== null) {
							unset($arr[$key]);
						}
					}
				}
			}
		}
		$s = implode(' ', $arr);
		$textD = "";
		$textD .= $zi;
		$textD .= $s;
		return $textD;
	} // hideGlossInZiTopLine

	/** hideSFEquivInZiTopLine:
		Hide any simple/full equivalents in the given Zidian top line.

		For example, given "干 [gān] stem ... (F乾) dry; (F幹) ... [gàn] (F幹) do",
		return 干 [gān] stem ... dry; ... [gàn] do".

		@param $s the input string, Zidian top line
		@return the modified input string.
	*/
	protected static function hideSFEquivInZiTopLine($s)
	{
		/* The pattern to match is "(sz)" where s = S or F, and z = any Hanzi.
			For simplicity and efficiency, treat any non-ASCII character as a Hanzi;
			that will exclude most likely false matches such as "(Si)" for "silicon". */
		return preg_replace('/(\([SF][^\x00-\xFF]\))/u', '', $s);
	} // hideSFEquivInZiTopLine

	/** ziTranslateReferences:
		Expand the abbreviations in the given #r line, and make them into links to sections
		in Help:References, as the desktop Wenlin does. For example,
			#rG.357.13 W.94a H.28 ...
		gets expanded to:
			→references: →《宋本廣韻》:357.13; →Wieger:94a; →Henshall:28; ...
		See https://gitlab.com/Wenlin/wanwu/issues/6
		and ZiTranslateReferences in wenlin/c/ch_zi.c.

		@param $s the input string, Zidian #r line.
		@return the modified references line, including a link.
	*/
	private static function ziTranslateReferences($s)
	{
		$outputText = '';
		// Expand and link to particular sections.
		// We have some flexibility about how to map abbreviations to section names...
		// First find first 〷, if any:
		$extendedArray = null;
		$i = mb_strpos($s, '〷');
		if ($i > 0) {
			$s = mb_substr($s, 0, $i - 1);
			$extendedArray = explode('〷', mb_substr($s, $i + 1));
		}
		// die($s);
		$baseArray = explode(' ', $s);
		// die($baseArray[0]);
		foreach ($baseArray as $ref) {
			$a = self::parseZiRef($ref); // cf. ZtrFindAbbrNameAdLen
			if ($outputText != '') {
				$outputText .= '; ';
			}
			$outputText .= $a['name'] . ':' . $a['body'];
		}
		if ($extendedArray) {
			$name = null;
			$prevName = null;
			foreach ($extendedArray as $ref) {
				if ($ref === '') {
					continue; // explode('〷', ...) evidently can produce some empty strings, just skip them
				}
				$a = self::parseZiRef($ref); // cf. ZtrFindAbbrNameAdLen
				if ($prevName == null || $a['name'] !== $prevName) {
					/* only write first of successive dupe names */
					$outputText .= "\n<" . 'hr' . "/>\n"; // horizontal rule
					$outputText .= $a['name'] . ':';
					$prevName = $name;
				}
				/* TODO: makeHanziIntoLinksOrHide seems to mess up the links here if $name contains Hanzi,
					so the links display as "[[Help:References...]]". Either prevent makeHanziIntoLinksOrHide from
					operating on the references line at all, or make makeHanziIntoLinksOrHide skip any text that's
					already inside double brackets? */
				$name = $a['name'];
				$outputText .= $a['body'];
			}
			$outputText .= "\n<" . 'hr' . "/>\n"; // horizontal rule
		}
		return "[[Help:References|►references:]] $outputText<br />";
	} // ziTranslateReferences

	/** parseZiRef:
		Parse a Zidian reference into a name and a body.

		@param $ref the input string, part of Zidian #r line, like 'G.357.13' or 'GY..天◦133.14◦下平◦先◦天◦他前◦/thin/'.
		@return an array $a indexed by 'name' and 'body', where
				$a['name'] looks like '《宋本廣韻》' and is also a hyperlink to a section of Help:References, and
				$a['body'] is like '357.13' or '天◦133.14◦下平◦先◦天◦他前◦/thin/'.
	*/
	private static function parseZiRef($ref)
	{
		$i = mb_strpos($ref, '.'); // first period
		if ($i > 0) {
			$abbrev = mb_substr($ref, 0, $i);
			$a['body'] = ltrim(mb_substr($ref, $i + 1), '.');
			$a['name'] = self::convertZiRefName($abbrev);
			// die("$abbrev: " . "name: " . $a['name'] . "body: " . $a['body']);
		}
		else {
			// Note: this happened for Shuowen entries, with $ref === ''; now caller skips those.
			// die("parseZiRef: $ref");
			$a['body'] = $ref;
			$a['name'] = '';
		}
		return $a;
	} // parseZiRef

	/** convertZiRefName:
		Convert a Zidian reference abbreviation to an expanded name that is a hyperlink,
		or, if the abbreviation is unrecognized, return it unexpanded.

		@param $abbrev the abbreviation, like 'K'.
		@return an expanded name like 'Karlgren', made into a hyperlink to section of Help:References.
	*/
	private static function convertZiRefName($abbrev)
	{
		$name = '';
		$section = '';
		switch ($abbrev) {
		// Length 1
		case 'A':
			$name = "DeFrancis(2003)";
			break;
		case 'B':
			$name = "Bǐshùn Guīfàn";
			break;
		case 'C':
			$name = "Hànyǔ Dà Cídiǎn";
			break;
		case 'D':
			$name = "Hànyǔ Dà Zìdiǎn";
			break;
		case 'E':
			$name = "EDOC";
			break;
		case 'G':
			$name = "Guǎngyùn";
			break;
		case 'H':
			$name = "Henshall";
			break;
		case 'K':
			$name = "Karlgren";
			break;
		case 'L':
			$name = "Lindqvist";
			break;
		case 'M':
			$name = "Mathews";
			break;
		case 'S':
			$name = "《說文解字·注》";
			break;
		case 'W':
			$name = "Wieger";
			break;
		// Length 2
		case 'BX':
			$name = "Baxter(1992)";
			break;
		case 'CY':
			$name = "Chao&Yang(1947)";
			break;
		case 'DK':
			$name = "《大漢和辞典》"; // Morohashi
			break;
		case 'DZ':
			$name = "《說文大字典》";
			break;
		case 'FJ':
			$name = "《說文解字·附檢字》";
			break;
		case 'GL':
			$name = "《說文解字·詁林》";
			break;
		case 'GS':
			$name = "Karlgren(1957)"; /* cp. "K" above */
			$section = 'K'; // This is currently the only item where $section is different like this
			break;
		case 'GY':
			$name = "《宋本廣韻》";
			break;
		case 'KX':
			$name = "Kāngxī Zìdiǎn";
			break;
		case 'SC':
			$name = "SuperCJK14";
			break;
		case 'TH':
			$name = "《說文解字‧藤花榭本》";
			break;
		case 'XK':
			$name = "《說文解字·繫傳》";
			break;
		// Length 3
		case 'LLY':
			$name = "Lǐ(1992)"; // 李乐毅 Lǐ Lèyì 1992 (twice in Zidian)
			break;
		case 'LCI':
			$name = "Creel(1939:V.1)";
			break;
		case 'WHY':
			$name = "Wáng(1993)";
			break;
		case 'XHC':
			$name = "《现代汉语词典》";
			break;
		case 'XHG':
			$name = "李行健(1998)"; /* 《现代汉语规范字典》 Xiàndài Hànyǔ Guīfàn Zìdiǎn (PRC version) */
			break;
		case 'XYY':
			$name = "李行健(2003)"; /* 《形音義規範字典》 Xíng-yīn-yì Guīfàn Zìdiǎn (ROC version) */
			break;
		// Length 4
		case 'LLYJ':
			$name = "Lǐ(1996)";
			break;
		case 'LCII':
			$name = "Creel(1939:V.2)";
			break;
		// Length 5
		case 'LCIII':
			$name = "Creel(1939:V.3)";
			break;
		}
		if ($name == '') {
			return $abbrev; // not found, no hyperlink
		}
		if ($section == '') {
			$section = $abbrev;
		}
		return "[[Help:References#$section|$name]]";
	} // convertZiRefName

	/** appendShuowenSealCharacters:
		Get any Seal script character(s) that correspond to any of
		the characters in modernZin[] according to the variant mapping #v fields
		of seal script Zidian entries. Typically modernCount is 1, 2, or 3, and if > 1,
		the relationship is between jiantizi and fantizi or vice-versa.

		Complication: normally modernZin[0] is the head character, but if the
		head character is a jiantizi with one or more corresponding fantizi
		(i.e. isFanti == false), then the fantizi are all listed first, and
		modernZin[modernCount-1] is the (jiantizi) head character.

		@param $modernZin the array of modern characters.
		@param $isFanti true or false.
		@return the string containing Shuowen characters and related info.
	*/
	public static function appendShuowenSealCharacters($modernZin, $isFanti)
	{
		if (WenlinFenlei::zinIsShuowen($modernZin[0])) {
			return '';
		}
		$shuowenArray = array();
		$modernCount = count($modernZin);
		$dbr = wfGetDB(DB_REPLICA);
		if (!$isFanti && $modernCount > 1) {
			$shuowenArray = self::getShuowenArray($dbr, $modernZin[$modernCount - 1], $shuowenArray);
			--$modernCount; // the last shall be first -- caution: either remove last item from $modernZin or avoid "foreach ($modernZin ...)" below.
		}
		for ($i = 0; $i < $modernCount; $i++) {
			$shuowenArray = self::getShuowenArray($dbr, $modernZin[$i], $shuowenArray);
		}
		$text = '';
		foreach ($shuowenArray as $seal) {
			$text .= WenlinUtf::zinToStr($seal);
			$song = self::getBestSongForSeal($dbr, $seal);
			if ($song) {
				$text .= '(' . WenlinUtf::zinToStr($song) . ')';
			}
			$text .= ' ';
		}
		return $text;
	} // appendShuowenSealCharacters

	/** getShuowenArray:
		Get the array of Shuowen characters.

		@param $dbr the database for reading.
		@param $zinModern the USV of the modern character.
		@param $shuowenArray the array of Shuowen characters to be appended to.
		@return the possibly enlarged array of Shuowen characters.

		CALLED BY: appendShuowenSealCharacters.
	*/
	public static function getShuowenArray($dbr, $zinModern, $shuowenArray)
	{
		$query['table'] = array('wenlin_zi_seal');
		$query['vars'] = array('seal', 'distance');
		$query['conds'] = array("hanzi = '$zinModern'");
		$query['options'] = array('ORDER BY' => 'distance');
		$query['join_conds'] = array();
		$res = $dbr->select($query['table'], $query['vars'], $query['conds'], __METHOD__, $query['options'], $query['join_conds']);
		foreach ($res as $row) {
			if ($row->distance >= ShuowenMap::SHUOWEN_IMPROPER_MAP_DISTANCE) {
				break;
			}
			if (!in_array($row->seal, $shuowenArray)) {
				$shuowenArray[] = $row->seal;
			}
		}
		return $shuowenArray;
	} // getShuowenArray

	/** getBestSongForSeal:
		Get the best-matching Song character for the given Seal character.

		@param $dbr the database for reading.
		@param $zinSeal the USV of the Seal character.
		@return the USV of the Song character, or 0 for none.
	*/
	public static function getBestSongForSeal($dbr, $zinSeal)
	{
		$query['table'] = array('wenlin_zi_seal');
		$query['vars'] = array('hanzi');
		$query['conds'] = array("seal = '$zinSeal'");
		$query['options'] = array('ORDER BY' => 'distance', 'LIMIT' => '1');
		$query['join_conds'] = array();
		$res = $dbr->select($query['table'], $query['vars'], $query['conds'], __METHOD__, $query['options'], $query['join_conds']);
		foreach ($res as $row) {
			return $row->hanzi;
		}
		return 0;
	} // getBestSongForSeal

	/** makeLexemesIntoLinks:
		Parse the content into existing lexemes in the dictionary-database and make them into links.

		@param $text the content for parsing.
		@return lexemes with double brackets.
	*/
	public static function makeLexemesIntoLinks($text)
	{
		// TODO: restore; makeLexemesIntoLinks is temporarily disabled (returns string unchanged)
		// Reference: https://gitlab.com/Wenlin/wanwu/-/issues/81
		if (true || !defined('MEDIAWIKI') ) {
			return $text; // for WenlinTest.php, MEDIAWIKI and wfGetDB are undefined, so skip this function
		}
		/* preprocess */
		$text_split = preg_split('/[\\x{003B}]/u', $text);		
		/* get database in Ci namespace for query */
		$dbr = wfGetDB(DB_REPLICA);
		/* store the final result in a string */
		$result = '';

		/* loop over text_split */
		for ($i = 0; $i < count($text_split); $i++) {
			$chunk = $text_split[$i];
			$word_array = WenlinUtf::explode($chunk);
			$words = '';
			$length = 0;
			foreach ($word_array as $zi) {
				if (WenlinFenlei::zinIsCJK(WenlinUtf::ziNumber($zi))) {
					$words .= $zi;
					$length++;
				} else {
					$result .= $zi;
				}
			}
			$hanzi = WenlinUtf::explode($words);

			$juncArray = array();
			for ($j = 0; $j < $length; $j++) {
				array_push($juncArray, 0);
			}
			/* Forward */
			$offset = 0;
			while (1) {
				$lexemeLength = $length - $offset;
				// WenlinCharBand.MAX_ZI_PER_CI = 20;
				if ($lexemeLength > 20) {
					$lexemeLength = 20;
				}
				while ($lexemeLength > 1) {
					$term = mb_substr($words, $offset, $lexemeLength);
					// Ci
					$query['table'] = array('h' => 'wenlin_ci_hanzi_string', 'f' => 'wenlin_ci_pinyin');
					$query['vars'] = array('serial_number');
					$query['conds'] = array("h.hanzi = '$term'");
					$query['options'] = array('ORDER BY' => 'f.frequency DESC, f.pinyin_collation');
					$query['join_conds'] = array('f' => array('JOIN', 'h.page_id = f.page_id'));

					$res = $dbr->select($query['table'], $query['vars'], $query['conds'], __METHOD__,
					$query['options'], $query['join_conds']);

					if ($res === true || $res === false || $res->numRows() == 0) {
						$lexemeLength--;
					}
					else {
						break;
					}
				}
				$offset += $lexemeLength;	
				if ($offset == $length) {
					break;
				}
				$juncArray[$offset - 1] |= 1;	
			}
			// mark the end
			$juncArray[$length - 1] = 1;
			// add brackets at marking point
			$writeIdx = 0;
			for ($j = 0; $j < $length; $j++) {
				if ($juncArray[$j] == 1) {
					$result .= WenlinUtf::zinToStr(0x200B) . "[[";
					while ($writeIdx <= $j) {
						$result .= $hanzi[$writeIdx];
						$writeIdx++;
					}
					$result .= "]]";
				}
			}
			if ($i != count($text_split) - 1) {
				$result .= ";";
			}
		}
		return $result;
	} // makeLexemesIntoLinks

} // class WenlinRender
