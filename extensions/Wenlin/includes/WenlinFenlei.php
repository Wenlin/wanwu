<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/


/* fēnlèi 分类[-類] {D} v.o. classify; sort ◆n. classification; type; taxonomy */

class WenlinFenlei
{
	const CJK_ZERO = 0x3007; /* 〇 */
	const MIN_CJK_ORIG = 0x4e00; /* 一 */
	const MAX_CJK_BMP_BIG = 0x9fff;
	const MIN_CJK_EXTA = 0x3400;
	const MAX_CJK_EXTA = 0x4db5;
	const MIN_SIP_ZIN = 0x20000; /* == start of ZinInCJKUI_XB_3_1(zin) */
	const MAX_SIP_ZIN = 0x2ffff; /* last in plane 2, undef between CJK Exts, and undef after last CJK Ext */
	const MIN_CJK_EXTB = 0x20000; /* == MIN_SIP_ZIN */
	const MAX_CJK_EXTB = 0x2a6d6;
	const MIN_CJK_EXTC = 0x2a700;
	const MAX_CJK_EXTC = 0x2b734;
	const MIN_CJK_EXTD = 0x2b740;
	const MAX_CJK_EXTD = 0x2b81d;
	const MIN_CJK_EXTE = 0x2B820;
	const MAX_CJK_EXTE = 0x2CEA1;
	const MIN_PUA_BMP = 0xe000;
	const MAX_PUA_BMP = 0xf8ff;
	const MIN_PUA_SUP_A = 0xf0000; /* == MIN_PUA_VARSEL */
	const MAX_PUA_SUP_A = 0xffffd; /* == MAX_PUA_VARSEL; NOTE: Excludes 2 ZinIsNoncharacter at end of block! */
	const MIN_PUA_VARSEL = 0xf0000;
	const MAX_PUA_VARSEL = 0xffffd;
	const MIN_PUA_SUP_B = 0x100000; /* == MIN_SHUOWEN */
	const MAX_PUA_SUP_B = 0x10fffd;/* NOTE: Excludes 2 ZinIsNoncharacter at end of block! */
	const MIN_RAD_ZIN = 0x2e80; /* start of ZinIsCJKRadSup */
	const MAX_RAD_ZIN = 0x2fdf; /* past end of ZinIsKangxiRad */
	const MIN_CJK_RAD_SUP = 0x2e80; /* == MIN_RAD_ZIN */
	const MAX_CJK_RAD_SUP = 0x2ef3; /* end of CJK Radicals Supplement (note the gap at = 0x2e9a) */
	const MIN_KXRAD_ZIN = 0x2f00;
	const MAX_KXRAD_ZIN = 0x2fd5; /* end of block is undef to MAX_RAD_ZIN */
	const MIN_HANGZHOU_ONES = 0x3021;
	const MAX_HANGZHOU_ONES = 0x3029;
	const MIN_HANGZHOU_TENS = 0x3038;
	const MAX_HANGZHOU_TENS = 0x303A;
	const CJK_ITERATION = 0x3005; // IDEOGRAPHIC ITERATION MARK
	const CJK_ITERATION_VERTICAL = 0x303B; // VERTICAL IDEOGRAPHIC ITERATION MARK
	const CJK_RADICAL_REPEAT = 0x2E80; // ZinIsKXCJKRad; not really a radical ...
	const BUTTON_ZINUM = 0xefff; 
	const MIN_CJK_STROKES = 0x31c0; /* start of CJK Strokes block */
	const MAX_CJK_STROKES = 0x31ef; /* end of CJK Strokes block */
	const MIN_SHUOWEN = 0x100000; /* start of Shuowen Seal characters in PUA */
	const MAX_SHUOWEN = 0x102bed; /* end of Shuowen Seal characters in PUA */
	const MIN_SHUOWEN_MAIN = 0x100000; /* end of "Main" Shuowen Seal characters in PUA */
	const MAX_SHUOWEN_MAIN = 0x1029d1; /* end of "Main" Shuowen Seal characters in PUA */

	/** zinIsCJK:
		Is the given character (code point) what Wenlin treats as CJK?

		Our CJK category is large and complicated. Generally it includes Hanzi,
		including radicals and Wenlin PUA Hanzi and Shuowen characters, and
		also excludes many characters, such as LATIN1 and compatibility characters.

		This function is currently used for many purposes, such as deciding whether the code
		point can have a Zidian entry, or whether it should be assumed Chinese (rather,
		than, say, English) for instant look-up or interpreting parameters.

		I have trouble remembering, for example, whether it includes BOPOMOFO characters (it doesn't),
		or Hexagrams (it doesn't).
	
		To avoid breaking existing code, we should avoid changing the category defined
		by this function without really good reason (such as new CJK extension Z).
		Good alternatives might exist, such as defining other, similar functions,
		with more limited usage and more clearly defined meaning, like ZinCanUseSongStyle
		and ZinIsNonHanziButOkForCidian.

		@param zin the character code point.
		@return Boolean true if zin is in the category (character class), else false.
	*/
	public static function zinIsCJK($zin)
	{
		if (!is_int($zin)) {
			/* Should all the functions in this class make this check? Should they just convert silently and treat this
				usage as acceptable, or should they treat it as an error? It would be convenient for the callers not to
				need to call WenlinUtf::ziNumber themselves... */
			error_log("Warning: parameter for zinIsCJK is not an integer ($zin); converting with ziNumber");
			$zin = WenlinUtf::ziNumber($zin);
		}
		if ($zin < self::MIN_RAD_ZIN) {
			return false; // most common case, handle it quickly.
		}
		return self::zinIsUnihanNonCompat($zin) /* excludes all zinIsCJKCompat */
			|| self::zinIsPUACJK($zin) /* includes zinIsCdpOperator; see definition and NOTE 6 above */
			|| self::zinIsNotReallyCJKBMPCompat($zin) /* excluded from zinIsCJKCompat */
			|| self::zinIsKXCJKRad($zin) /* loose: whole Rad blocks */
			|| self::zinIsCJKStroke($zin) /* (U+31c0..U+31ef) */
			/* below 3 are subset of zinIsCJKSymbol : */
			|| ($zin == self::CJK_ZERO) /* IDEOGRAPHIC NUMBER ZERO */
			|| self::zinIsSuzhouNumeral($zin) /* "HANGZHOU" NUMERALS 1..9,10,20,30 */
			|| self::zinIsCjkIteration($zin) /* 3 CJK iteration marks */
			;
	} // zinIsCJK

	private static function zinIsUnihanNonCompat($zin)
	{
		return (self::zinIsCJKBMP($zin) || self::zinIsCJKSIP($zin));
	} // zinIsUnihanNonCompat

	private static function zinIsCJKBMP($zin)
	{
		return (($zin >= self::MIN_CJK_ORIG && $zin <= self::MAX_CJK_BMP_BIG)
				|| ($zin >= self::MIN_CJK_EXTA && $zin <= self::MAX_CJK_EXTA));
	} // zinIsCJKBMP

	private static function zinIsCJKSIP($zin)
	{
		return (($zin >= self::MIN_SIP_ZIN) && ($zin <= self::MAX_CJK_EXTE));
	} // zinIsCJKSIP

	private static function zinIsPUACJK($zin)
	{
		return self::zinIsPUA($zin)
			&& !self::zinIsTriangleButton($zin)
			&& !self::zinIsPuaVarSel($zin)
			&& ($zin != 0xE091) // DOTTED BOX
			&& ($zin != 0xECCC); // NESTED TRIANGLE EXPERIMENT
	} // zinIsPUACJK

	private static function zinIsPUA($zin)
	{
		if ($zin < self::MIN_PUA_BMP) {
			return false; // most common case, do this 1st
		}
		if ($zin <= self::MAX_PUA_BMP) {
			return true;
		}
		if (self::zinIsPUASups($zin)) {
			return true;
		}
		return false;
	} // zinIsPUA

	private static function ZinIsPUASups($zin)
	{
		return self::zinIsPUASupA($zin) || self::zinIsPUASupB($zin);
	} // ZinIsPUASups

	private static function zinIsPUASupA($zin)
	{
		return $zin >= self::MIN_PUA_SUP_A && $zin <= self::MAX_PUA_SUP_A;
	} // zinIsPUASupA

	private static function zinIsPUASupB($zin)
	{
		return $zin >= self::MIN_PUA_SUP_B && $zin <= self::MAX_PUA_SUP_B;
	} // zinIsPUASupB

	private static function zinIsTriangleButton($zin)
	{
		return $zin == self::BUTTON_ZINUM;
	} // zinIsTriangleButton

	private static function zinIsPuaVarSel($zin)
	{
		return $zin >= self::MIN_PUA_VARSEL && $zin <= self::MAX_PUA_VARSEL;
	} // zinIsPuaVarSel

	private static function zinIsNotReallyCJKBMPCompat($zin)
	{
		if ($zin >= 0xfa0e && $zin <= 0xfa29) {
			if ($zin == 0xfa0e || //  1:12
				$zin == 0xfa0f || //  2:12
				$zin == 0xfa11 || //  3:12
				$zin == 0xfa13 || //  4:12
				$zin == 0xfa14 || //  5:12
				$zin == 0xfa1f || //  6:12
				$zin == 0xfa21 || //  7:12
				$zin == 0xfa23 || //  8:12
				$zin == 0xfa24 || //  9:12
				$zin == 0xfa27 || // 10:12
				$zin == 0xfa28 || // 11:12
				$zin == 0xfa29 // 12:12
				) {
				return true;
			}
		}
		return false;
	} // zinIsNotReallyCJKBMPCompat

	private static function zinIsKXCJKRad($zin)
	{
		return $zin >= self::MIN_RAD_ZIN && $zin <= self::MAX_RAD_ZIN;
	} // zinIsKXCJKRad

	private static function zinIsCJKStroke($zin)
	{
		return $zin >= self::MIN_CJK_STROKES && $zin <= self::MAX_CJK_STROKES;
	} // zinIsCJKStroke

	private static function zinIsSuzhouNumeral($zin)
	{
		return self::zinIsHangzhouOnes($zin) || self::zinIsHangzhouTens($zin);
	} // zinIsSuzhouNumeral

	private static function zinIsHangzhouOnes($zin)
	{
		return $zin >= self::MIN_HANGZHOU_ONES && $zin <= self::MAX_HANGZHOU_ONES;
	} // zinIsHangzhouOnes

	private static function zinIsHangzhouTens($zin)
	{
		return $zin >= self::MIN_HANGZHOU_TENS && $zin <= self::MAX_HANGZHOU_TENS;
	} // zinIsHangzhouTens

	private static function zinIsCjkIteration($zin)
	{
		return $zin == self::CJK_ITERATION || $zin == self::CJK_ITERATION_VERTICAL || $zin == self::CJK_RADICAL_REPEAT;
	} // zinIsCjkIteration

	public static function zinIsShuowen($zin)
	{
		return $zin >= self::MIN_SHUOWEN && $zin <= self::MAX_SHUOWEN;
	} // zinIsShuowen

	public static function zinIsShuowenMain($zin)
	{
		return $zin >= self::MIN_SHUOWEN_MAIN && $zin <= self::MAX_SHUOWEN_MAIN;
	} // zinIsShuowenMain

	/** hideShuowenSeal:
		Hide (strip) any Seal characters (and associated notation) from the given string.
		Intended primarily for any non-Seal Zidian entry line (top or otherwise).
		Step A: strip all Seal characters from non-Seal entries
		Step B: strip empty parens left by #A from non-top-lines # assumes certain usage of Seal in non-Seal
		Step C: strip empty variant notation left by #A from top-lines

		@param $s the input string, Zidian line.
		@return the modified input string.
	*/
	public static function hideShuowenSeal($s)
	{
		/* the char class is the same as WenlinFenlei::zinIsShuowen;
		   see zidian_unseal.pl (in SVN 'abc/zidian/unseal') */
		$s = preg_replace('/[\x{100000}-\x{102bed}]+/u', '', $s);  /* Step A */ /* Cf. self::MIN_SHUOWEN_MAIN, self::MAX_SHUOWEN_MAIN */
		$s = preg_replace('/\(\)/u', '', $s);  /* Step B */
		return preg_replace('/ *\([=\?!]\) */', ' ', $s);  /* Step C */
	} // hideShuowenSeal

	/** zinIsDiacritic:
		Is the given code point a combining diacritic for a tone mark, or one of certain other
		combining diacritics?

		@param zin the character code point.
		@return WL_YES if zin is a combining diacritic for a tone mark, or one of certain other
				combining diacritics; else WL_NO.

		Based on the C function ZinIsDiacritic in ch_zin.c.
		Make sure this function stays consistent with DiacriticIsTop and
		ConvertCombiningToSpacingDiacritic.

		This function zinIsDiacritic is currently unused except in a test by WenlinTest.php;
		added for potential future use in WenlinPinyin.php.
	*/
	public static function zinIsDiacritic($zin)
	{
		if ($zin < 0x0300 || $zin > 0x036F) {
			return false; // do this first, for speed.
		}
		/* In fact, everything from U+0300 thru U+036F is combining, but shouldn't treat them
		   as such here unless we also handle them properly with DiacriticIsTop and
		   ConvertCombiningToSpacingDiacritic. */
		// return WL_YES;

		switch ($zin) {
		case 0x0304: // tone1 -- macron -- U+0304
		case 0x0301: // tone2 -- acute -- U+0301
		case 0x030c: // tone3 -- caron/hacek -- U+030c
		case 0x0306: // tone3 -- breve -- U+0306 (common mistake for tone3)
		case 0x0300: // tone4 -- grave -- U+0300
		case 0x0302: // U+0302 combining circumflex accent
		case 0x0303: // U+0303 COMBINING TILDE; for EDOC
		case 0x0307: // U+0307 combining dot above; for EDOC
		case 0x0308: // U+0308 combining diaeresis (umlaut)
		case 0x030a: // U+030a combining ring above; for GSR
		case 0x0311: /* U+0311 combining inverted breve (above); in GSR (Karlgren 1957),
			occurs over "d" and "t", in IPA == U+0221,U+0236 D and T WITH CURL, respectively;
			positioning over "d" might need vertical adjustment, but "t" seems OK */
		case 0x031e: // U+031e combining down tack below (IPA: indicates vowel lowering); for GSR
		case 0x0320: // U+0320 combining minus below; ZinIsSandhiToneChangeNotation
		case 0x0323: // U+0323 combining dot below; ZinIsSandhiToneChangeNotation
		case 0x0324: // U+0324 COMBINING DIAERESIS BELOW; for EDOC
		case 0x0325: // U+0325 COMBINING RING BELOW (voiceless)
		case 0x032f: // U+032f combining inverted breve below; for GSR
		case 0x0331: // U+0331 combining macron below
			return true;
		}
		return false;
	} // zinIsDiacritic

} // class WenlinFenlei
