<?php
/* Copyright (c) 2018 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

class WenlinUtf
{
	/** initialize:
		Initialize PHP to handle UTF-8 properly. 

		@return true for success, false for failure.
	*/
	public static function initialize()
	{
		static $initialized = false;
		static $succeeded = false;
		if ($initialized == false) {
			$initialized = true;
			if (extension_loaded('mbstring') === false) {
				error_log("Wenlin cannot initialize UTF-8: the mbstring extension is not loaded");
				return false;
			}
			mb_language('Neutral');
			mb_internal_encoding('UTF-8');
			mb_detect_order(array('UTF-8'));
			$succeeded = true;
		}
		return $succeeded;
	} // initialize

	/** ziNumber:
		Convert one UTF-8 character to its USV (UCS Scalar Value).

		@param string the input text.
		@return the USV.

		Compare ord() and mb_ord().

		TODO: compare UtfNormal\Utils::utf8ToCodepoint which is included in Mediawiki source. Is it faster?
	*/
	public static function ziNumber($string)
	{
		if (!self::initialize()) {
			return 0;
		}
		if ($string === '' || $string === null) { // caution: string could be '0', which is valid but evaluates to zero! So don't want if (!$string).
			// This happened for listCharactersContaining missing param.
			// As of 2018-08-03 this still happens once in a while, caller unknown since log doesn't include backtrace.
			// error_log("WenlinUtf::ziNumber empty string");
			// debug_print_backtrace();
			return 0;
		}
		$firstChar = mb_substr($string, 0, 1);
		/* Warning: unpack(): Type N: not enough input, need 4, have 0 in .../WenlinUtf.php on line 52 */
		$result = unpack('N', mb_convert_encoding($firstChar, 'UCS-4BE', 'UTF-8'));
		if (is_array($result) === false) {
			// error_log("WenlinUtf::ziNumber unpack failed"); // never happens
			// debug_print_backtrace();
			return 0;
		}
		return $result[1];
	} // ziNumber

	/** zinToStr:
		Convert one USV (UCS Scalar Value) character to its UTF-8 string equivalent.

		@param string the USV as an integer or a string.
		@return the UTF-8 string.

		TODO: compare UtfNormal\Utils::codepointToUtf8 which is included in Mediawiki source. Is it faster?
	*/
	public static function zinToStr($string)
	{
		if (!self::initialize()) {
			return false;
		}
		$c = '&#' . intval($string) . ';';
		return mb_convert_encoding($c, 'UTF-8', 'HTML-ENTITIES');
	} // zinToStr

	/** explode:
		Convert the UTF-8 string into an array of UTF-8 characters.

		@param string the input text.
		@return the array.
	*/
	public static function explode($string)
	{
		if (!self::initialize()) {
			return false;
		}
		$a = [];
		while ($string != '') {
			$a[] = mb_substr($string, 0, 1);
			$string = mb_substr($string, 1);
		}
		return $a;
	} // explode

	/** advance:
		Get the first UTF-8 character in a string, and trim that character from the string.

		@param string the input text, from which the first character will be trimmed.
		@return the first character.
	*/
	public static function advance(&$string)
	{
		if (!self::initialize()) {
			return false;
		}
		$firstChar = mb_substr($string, 0, 1);
		$string = mb_substr($string, 1);
		return $firstChar;
	} // advance

} // class WenlinUtf
