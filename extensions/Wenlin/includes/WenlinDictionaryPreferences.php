<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

use MediaWiki\MediaWikiServices;

/** WenlinDictionaryPreferences:
	Implement access for a particular user's dictionary-related preferences.
	Compare DictOpt in C code.
*/
class WenlinDictionaryPreferences
{
	/* Implement this class as a singleton, that is, with a single instance, since
		for a given http request there is only a single user, and we don't want to
		waste time repeatedly accessing the database for the same user's preferences. */
	protected static $instance = null;

	/* Boolean data values for dictionary-related preferences.
		Their values for the current user are retrieved by loadFromDatabase(),
		and their default values are set in the DefaultUserOptions section of extension.json.
		Initializing them here would normally make no difference; however, for example
		when running WenlinTest.php at the command line, loadFromDatabase might not
		do anything, so we'd better initialize them here anyway.  */
	protected $preferSimple = true; // same as CfgOptionSimpleForm in C
	protected $preferPinyinFirst = true; // related to CfgOptionPinyinAlwaysFirstInCidian in C
	protected $hideSubentries = false; // same as CfgOptionDictHideSubentries in C
	protected $hideGloss = false; // same as CfgOptionDictHideGloss in C
	protected $hideHanzi = false; // same as CfgOptionDictHideHanzi in C
	protected $hideIpa = false; // same as CfgOptionDictHideIPA in C
	protected $hidePinyin = false; // same as CfgOptionDictHidePinyin in C
	protected $hideSFEquiv = false; // same as CfgOptionDictHideSFEquiv in C
	protected $hideToneChange = false; // same as CfgOptionDictHideToneChange in C
	protected $hideRareinfo = false; // same as CfgOptionDictHideRareInfo in C
	protected $replaceTilde = false; // same as CfgOptionDictReplaceTilde in C
	protected $polyglotLanguage = 'en'; // similar to GetDictionaryLanguageCode in C
	protected $preferredChineseNamespaces = ['Ci']; // array: Jyut, Ci, or both, in which order

	/* preferSimpleHeadword is NOT a user preference. Maybe it should
		be moved to WenlinBandContext... In the PHP code it currently has no
		effect. In the C code diop->preferSimpleHeadword has a purpose involving monosyllabic
		Cidian entries displayed in a Zidian entry. Even though the user may have the preferSimple
		option turned on, when the Zidian entry is for a fantizi, then the monosyllabic Cidian entries
		should be shown with fantizi first; and vice-versa; it looks strange otherwise. Nevertheless we
		haven't implemented it yet in PHP. Compare:
		DESKTOP (C):
			馬(S马) [mǎ] horse; 馬上 mǎshàng right away; (radical 187, 馬部)
			▷¹mǎ 馬[马] {A} n. ①horse ②horse chess piece ③Surname
		WIKI (PHP):
			馬(S马) [mǎ] horse; 馬上 mǎshàng right away; (radical 187, 馬部)
			▷¹mǎ 马[馬] {A} n. ①horse ②horse chess piece ③Surname
		TODO: finish implementing $preferSimpleHeadword for the wiki.
	*/
	protected $preferSimpleHeadword = false;

	/** singleton:
		Get the one and only instance of WenlinDictionaryPreferences, and load its values
		from the database if not already loaded.

		@return the WenlinDictionaryPreferences instance.
	*/
	public static function singleton()
	{
		if (self::$instance == null) {
			self::$instance = new self();
			self::$instance->loadFromDatabase();
		}
		return self::$instance;
	} // singleton

	/* loadFromDatabase:
		Load all preference values from the database.

		Called only by singleton().
	*/
	private function loadFromDatabase()
	{
		if (!class_exists('RequestContext', false)) {
			return;
		}
		$user = RequestContext::getMain()->getUser();
		$opt = MediaWikiServices::getInstance()->getUserOptionsLookup();
		/* The names of Wenlin-specific options like 'WenlinHideHanzi' must match those
			in WenlinHooks::onGetPreferences and extension.json */
		$this->hideHanzi = (bool) $opt->getOption($user, 'WenlinHideHanzi');
		$this->hideRareinfo = (bool) $opt->getOption($user, 'WenlinHideRareinfo');
		$this->hidePinyin = (bool) $opt->getOption($user, 'WenlinHidePinyin');
		$this->hideIpa = (bool) $opt->getOption($user, 'WenlinHideIPA');
		$this->hideGloss = (bool) $opt->getOption($user, 'WenlinHideGloss');
		$this->hideSubentries = (bool) $opt->getOption($user, 'WenlinHideSubentries');
		$this->hideSFEquiv = (bool) $opt->getOption($user, 'WenlinHideSFEquiv');
		$this->preferSimple = (bool) $opt->getOption($user, 'WenlinPreferSimple');
		$this->preferPinyinFirst = (bool) $opt->getOption($user, 'WenlinPreferPinyinFirst');
		$this->hideToneChange = (bool) $opt->getOption($user, 'WenlinHideToneChange');
		$this->replaceTilde = (bool) $opt->getOption($user, 'WenlinReplaceTilde');
		$this->polyglotLanguage = $opt->getOption($user, 'language'); // not Wenlin-specific
		$this->preferredChineseNamespaces = $user->isAllowed('WenlinRightToViewAllJyut')
			? explode('-', $opt->getOption($user, 'WenlinJyutAndOrCi')) : array('Ci');
	} // loadFromDatabase

	public function getHideHanzi()
	{
		return $this->hideHanzi;
	} // getHideHanzi

	public function getHideRareInfo()
	{
		return $this->hideRareinfo;
	} // getHideRareInfo

	public function getHidePinyin()
	{
		return $this->hidePinyin;
	} // getHidePinyin

	public function getHideIpa()
	{
		return $this->hideIpa;
	} // getHideIpa

	public function getHideGloss()
	{
		return $this->hideGloss;
	} // getHideGloss

	public function getPolyglotLanguage()
	{
		return $this->polyglotLanguage;
	} // getPolyglotLanguage

	/** getHideSubentries:
		Get the current user preference for "Hide English subentries separated in alphabetical listing".
		When the option is on, a subentry like woodworm will not be listed alphabetically (between
		woodworker and woody), it will only be listed as part of the entry for wood.

		Reference for the desktop version:
		http://guide.wenlininstitute.org/wenlin4.3/The_Options_Menu#Show.2FHide_Dictionary_Items...

		@return boolean.
	*/
	public function getHideSubentries()
	{
		return $this->hideSubentries;
	} // getHideSubentries

	public function getHideSFEquiv()
	{
		return $this->hideSFEquiv;
	} // getHideSFEquiv

	public function getPreferSimple()
	{
		return $this->preferSimple;
	} // getPreferSimple

	public function getPreferPinyinFirst()
	{
		return $this->preferPinyinFirst;
	} // getPreferPinyinFirst

	public function getPreferSimpleHeadword()
	{
		return $this->preferSimpleHeadword;
	} // getPreferSimpleHeadword

	public function getHideToneChange()
	{
		return $this->hideToneChange;
	} // getHideToneChange

	public function getReplaceTilde()
	{
		return $this->replaceTilde;
	} // getReplaceTilde

	/** getPreferredChineseNamespaces:
		Get an array of namespaces indicating the rights and preferences of the user.

		We make some assumptions, e.g. about WenlinRightToViewAllJyut, which may not be
		justified and may need to change sooner or later... See loadFromDatabase() where
		preferredChineseNamespaces is set, and WenlinHooks::onGetPreferences().

		@return an array like ['Jyut', 'Ci'], ['Ci', 'Jyut'], ['Jyut'], or ['Ci'].
	*/
	public function getPreferredChineseNamespaces()
	{
		return $this->preferredChineseNamespaces;
	} // getPreferredChineseNamespaces

} // WenlinDictionaryPreferences
