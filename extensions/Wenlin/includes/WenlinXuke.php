<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* WenlinXuke: class for determining permissions for dictionary access.
		xụkě 许可[許-] {D} n./v. permit; allow
	Our implementation currently depends on the fact that our dictionary entries
	are surrounded by "WL" tags, so WenlinRender.php can choose not to display the full
	text of an entry if the user lacks permission. Also, lists, and anything displayed through
	Special:Wenlin URLs (SpecialWenlin.php), are constructed by our code rather than simply
	derived from pages, and that code can filter the results.
*/

class WenlinXuke
{
	/** entryIsViewableByCurrentUser:
		Is the dictionary entry for the given title viewable by the current user?
		That is, does the current user have permission to view it?
		If not, set an explanatory message for the user.

		@param namespace the namespace ('Ci', etc.).
		@param titleStr the page title string, such as the serial number for Ci/En/Jyut, or the Hanzi for Zi.
		@param msg input value maybe '?'; passed by reference; to be set by this function
			to a wikitext message to the user in case we return false.
		@return true if the user has permission, else false.

		Called by renderWL, ...
	*/
	public static function entryIsViewableByCurrentUser($namespaceStr, $titleStr, &$msg)
	{
		$user = WenlinGongju::getCurrentUser($msg);
		if ($user === null) {
			return false;
		}
		return self::entryIsViewableByThisUser($user, $namespaceStr, $titleStr, $msg);
	} // entryIsViewableByCurrentUser

	/** entryIsViewableByThisUser:
		Is the dictionary entry for the given title viewable by the given user?
		That is, does the given user have permission to view it?
		If not, set an explanatory message for the user.

		@param user the user.
		@param namespace the namespace ('Ci', etc.).
		@param titleStr the page title string, such as the serial number for Ci/En/Jyut, or the Hanzi for Zi.
		@param msg input value maybe '?'; passed by reference; to be set by this function
			to a wikitext message to the user in case we return false.
		@return true if the user has permission, else false.

		Called by entryIsViewableByCurrentUser, onMediaWikiPerformAction, onUserCan, ...

		Called from the hook onMediaWikiPerformAction to prevent user from seeing entry by
		clicking "Edit" or "View Source" tab if they lack permission to view the page.
	*/
	public static function entryIsViewableByThisUser($user, $namespaceStr, $titleStr, &$msg)
	{
		$wenlinRightViewAllThisNamespace = self::rightToViewAllThisNamespace($namespaceStr);
		if ($wenlinRightViewAllThisNamespace == '') {
			// TODO: may want to return false here for some namespaces not covered by rightToViewAllThisNamespace
			return true;
		}
		if ($user->isAllowed($wenlinRightViewAllThisNamespace)) {
			return true;
		}
		/* TODO: there seems to be a failure here occasionally, especially with newly added Cantonese entries.
			Even though I'm logged in, I see "To view this entry, please log in". Reloading the page with "purge"
			makes it load correctly, without needing to log in again. So isLoggedIn [isRegistered] is returning false when it should
			return true. Confirmed that $user == $wgUser and isSafeToLoad in entryIsViewableByCurrentUser.
			However, it seems that the PHP file changes are not always taking effect when uploaded to the wenlin.co
			server, even if I restart Apache. Debugging PHP is horrible!!! Tested on Firefox, Chrome, and Safari.
				It seems as though the problem can be prevented by setting
					$wgParserCacheType = CACHE_NONE;
			in LocalSettings.php, but it's not a reasonable permanent solution to keep it there.
			Good news, however: after removing that from LocalSettings.php, the problem did not come
			back, so maybe it succeeding in fixing a messed-up cache. */

		/* TODO: implement with a database table. Two columns: title and level.
			Title includes namespace, like "Zi:好" or "Ci:1234567890".
			Level is 0 if viewable by anonymous, 1 if viewable only by logged-in users.
			Function getLevelForTitle returns -1 if title not found in table.
			$level = self:getLevelForTitle("$namespaceStr:$titleStr");
			if ($level === 0 || ($level === 1 && $user->isRegistered())) {
				return true;
			}
		*/
		if ($user->isRegistered()) {
			// Users who are logged in but don't have WenlinRightToViewAllCi/... are
			// allowed to view a limited set, larger than anon.
			if (self::entryIsViewableByAnyLoggedInUser($namespaceStr, $titleStr)) {
				return true;
			}
			$msg = self::messagePleaseLogInOrSubscribe('subscribe');
			return false;
		}
		// It should be impossible to reach here if user is logged in.
		if (self::entryIsViewableByAnonymousUser($namespaceStr, $titleStr)) {
			return true;
		}
		$msg = self::messagePleaseLogInOrSubscribe('login');
		return false;
	} // entryIsViewableByThisUser

	/** messagePleaseLogInOrSubscribe:
		Get the string like 'To view this entry, please ...'.

		@param $action 'login' or 'subscribe'.
		@return the string like 'To view this entry, please subscribe/login ...'.

		Called by entryIsViewableByThisUser and onMediaWikiPerformAction.
	*/
	public static function messagePleaseLogInOrSubscribe($action)
	{
		switch ($action) {
		case 'login':
			return 'To view this entry, please [[Special:UserLogin|log in]]';
		case 'subscribe':
			// TODO: change to a more specific URL than simply wenlinshangdian.com.
			return 'To view this entry, please [https://wenlinshangdian.com subscribe] for full access';
		}
		return "messagePleaseLogInOrSubscribe: unimplemented action ($action)";
	} // messagePleaseLogInOrSubscribe

	/** rightToViewAllThisNamespace:
		Get the string like 'WenlinRightToViewAllCi' corresponding to the given namespace like 'Ci',
		as for use with $wgGroupPermissions.

		@param $namespaceStr the namespace for the entries whose access is in question.
		@return the string describing the right, or '' (empty string) for namespaces outside our special set.

		Called by entryIsViewableByThisUser.
	*/
	public static function rightToViewAllThisNamespace($namespaceStr)
	{
		if ($namespaceStr === 'Ci' || $namespaceStr === 'Ci_talk') {
			return 'WenlinRightToViewAllCi';
		}
		if ($namespaceStr === 'Zi' || $namespaceStr === 'Zi_talk') {
			return 'WenlinRightToViewAllZi';
		}
		if ($namespaceStr === 'En' || $namespaceStr === 'En_talk') {
			return 'WenlinRightToViewAllEn';
		}
		if ($namespaceStr === 'Jyut' || $namespaceStr === 'Jyut_talk') {
			return 'WenlinRightToViewAllJyut';
		}
		// TODO: 'Cdl'? Cf. onUserCan hook.
		return '';
	} // rightToViewAllThisNamespace

	/** entryIsViewableByAnyLoggedInUser:
		Is the entry for the given namespace and title viewable by any logged in user, without
		needing any particular rights?
		That is, do all users who are logged in have permission to view it?

		@param $namespaceStr the namespace (string) for the entries whose access is in question.
		@param $titleStr the page title string, such as the serial number for Ci/En/Jyut, or the Hanzi for Zi.
		@return true if the user has permission, else false.

		Called by entryIsViewableByThisUser.
	*/
	private static function entryIsViewableByAnyLoggedInUser($namespaceStr, $titleStr)
	{
		// Logged-in users can see everything anon users can see, and more.
		// Note: since we check this first, there is no need for the ...ExtraViewableIfLoggedIn
		// functions to make the same check.
		if (self::entryIsViewableByAnonymousUser($namespaceStr, $titleStr)) {
			return true;
		}
		// TODO: implement this in a more object-oriented way, maybe with a class for each namespace.
		if ($namespaceStr === 'Ci' || $namespaceStr === 'Ci_talk') {
			return self::ciEntryIsInExtraSetViewableIfLoggedIn($titleStr);
		}
		if ($namespaceStr === 'Zi' || $namespaceStr === 'Zi_talk') {
			return self::ziEntryIsInExtraSetViewableIfLoggedIn($titleStr);
		}
		if ($namespaceStr === 'En' || $namespaceStr === 'En_talk') {
			return self::enEntryIsInExtraSetViewableIfLoggedIn($titleStr);
		}
		if ($namespaceStr === 'Jyut' || $namespaceStr === 'Jyut_talk') {
			return self::jyutEntryIsInExtraSetViewableIfLoggedIn($titleStr);
		}
		return true; // TODO: entryIsViewableByAnyLoggedInUser true or false for other namespaces?
	} // entryIsViewableByAnyLoggedInUser

	/** entryIsViewableByAnonymousUser:
		Is the entry for the given namespace and title viewable by anonymous users?
		That is, do users who aren't logged in have permission to view it?

		@param $namespaceStr the namespace for the entries whose access is in question.
		@param $titleStr the page title string, such as the serial number for Ci/En/Jyut, or the Hanzi for Zi.
		@return true if the user has permission, else false.

		Called by entryIsViewableByThisUser.
	*/
	private static function entryIsViewableByAnonymousUser($namespaceStr, $titleStr)
	{
		if ($namespaceStr === 'Ci' || $namespaceStr === 'Ci_talk') {
			return self::ciEntryIsViewableByAnonymousUser($titleStr);
		}
		if ($namespaceStr === 'Zi' || $namespaceStr === 'Zi_talk') {
			return self::ziEntryIsViewableByAnonymousUser($titleStr);
		}
		if ($namespaceStr === 'En' || $namespaceStr === 'En_talk') {
			return self::enEntryIsViewableByAnonymousUser($titleStr);
		}
		if ($namespaceStr === 'Jyut' || $namespaceStr === 'Jyut_talk') {
			return self::jyutEntryIsViewableByAnonymousUser($titleStr);
		}
		if ($namespaceStr === 0 || $namespaceStr === null || $namespaceStr === '') {
			/* For Main namespace, we get $namespaceStr === 0 here -- not null or ''. */
			// debug_print_backtrace(); die("ooo!");
			return true;
		}
		// die("entryIsViewableByAnonymousUser false: $namespaceStr");
		return false; // TODO: entryIsViewableByAnonymousUser true or false for other namespaces?
	} // entryIsViewableByAnonymousUser

	/************* Namespace Zi (ZIDIAN 字典) **************/

	/** ziEntryIsInExtraSetViewableIfLoggedIn:
		Is the specified entry in a special set that are viewable if logged in, but not
		viewable if anonymous?

		@param zi the head character (Hanzi) of the Zidian entry.
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnyLoggedInUser.
	*/
	private static function ziEntryIsInExtraSetViewableIfLoggedIn($zi)
	{
		// TODO: ziEntryIsInExtraSetViewableIfLoggedIn: We might do something based on frequency...
		// Temporarily just add add this arbitrary small set, for testing.
		$loggedInZi = array("摇", "摆", "舞");
		return in_array($zi, $loggedInZi);
	} // ziEntryIsInExtraSetViewableIfLoggedIn

	/** ziEntryIsViewableByAnonymousUser:
		Is the zidian entry for the given title viewable by anonymous users?
		That is, do users who aren't logged in have permission to view it?

		@param $zi the page title string, which, for the Zi namespace, is the Hanzi.
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnonymousUser.
	*/
	private static function ziEntryIsViewableByAnonymousUser($zi)
	{
		/* This array is copied from wenlin/perl/demo_dict.pl. It contains 371 characters that have entries
			in Wenlin Mianfeiban 4.3, the published free version for desktop (Mac/Windows).
			It's not strictly based on frequency rank. */
		$mianfeiZi = array("一", "七", "三", "上", "下", "不", "且", "世", "业", "东", "个", "中", "为",
		"主", "么", "义", "之", "九", "也", "习", "了", "事", "二", "于", "五", "些", "产", "人", "什",
		"今", "从", "他", "代", "以", "们", "会", "但", "何", "作", "你", "來", "係", "個", "們", "候",
		"像", "儿", "元", "兄", "先", "免", "兒", "兔", "八", "六", "关", "兴", "农", "出", "分", "切",
		"別", "别", "到", "刻", "前", "办", "加", "动", "劳", "動", "勞", "化", "十", "单", "历", "原",
		"去", "又", "友", "发", "只", "可", "史", "同", "后", "告", "呢", "和", "啊", "問", "單", "四",
		"因", "国", "國", "在", "地", "基", "声", "外", "多", "大", "天", "太", "夫", "头", "她", "好",
		"如", "妈", "姆", "始", "媽", "子", "字", "学", "孩", "學", "守", "完", "定", "家", "容", "对",
		"将", "將", "對", "小", "就", "展", "工", "己", "已", "常", "干", "年", "并", "幹", "庆", "应",
		"开", "弟", "张", "張", "当", "录", "待", "後", "得", "從", "心", "必", "快", "怎", "思", "情",
		"想", "慌", "慶", "應", "成", "我", "所", "招", "捡", "掉", "提", "撞", "撿", "故", "整", "文",
		"方", "於", "旁", "日", "时", "易", "是", "時", "最", "會", "月", "有", "朋", "木", "末", "本",
		"机", "来", "東", "林", "果", "树", "株", "样", "棵", "樣", "樹", "機", "正", "歷", "死", "比",
		"民", "汉", "汤", "汽", "沒", "没", "法", "活", "济", "消", "漢", "濟", "為", "然", "版", "牛",
		"牠", "特", "狗", "猪", "猴", "现", "現", "生", "產", "用", "田", "由", "电", "界", "當", "瘦",
		"發", "的", "目", "直", "看", "眼", "着", "睛", "知", "研", "确", "確", "社", "祇", "种", "科",
		"究", "突", "立", "竟", "笑", "第", "等", "简", "簡", "类", "系", "級", "絕", "經", "级", "经",
		"绝", "羊", "美", "義", "習", "老", "而", "聲", "能", "自", "興", "著", "虎", "蛇", "行", "裏",
		"裡", "西", "要", "見", "覺", "见", "觉", "解", "許", "訴", "該", "說", "説", "講", "識", "讲",
		"许", "识", "诉", "该", "说", "象", "豬", "費", "费", "走", "起", "跑", "路", "車", "較", "轉",
		"车", "转", "较", "辦", "農", "过", "运", "还", "这", "远", "這", "運", "過", "道", "遠", "還",
		"那", "邮", "郵", "都", "里", "重", "錄", "開", "間", "關", "问", "间", "阳", "阶", "院", "陽",
		"階", "隻", "雞", "電", "面", "音", "頭", "題", "類", "题", "飛", "飞", "餓", "饿", "馬", "马",
		"高", "鸡", "麼", "鼠", "龍", "龙"); // $mianfeiZi 
		return in_array($zi, $mianfeiZi);
	} // ziEntryIsViewableByAnonymousUser

	/************* Namespace Ci (CIDIAN 词典) **************/

	/** ciEntryIsInExtraSetViewableIfLoggedIn:
		Is the specified entry in a special set that are viewable if logged in, but not
		viewable if anonymous?

		@param $titleStr the page title string (serial number).
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnyLoggedInUser.
	*/
	private static function ciEntryIsInExtraSetViewableIfLoggedIn($titleStr)
	{
		// TODO: ciEntryIsInExtraSetViewableIfLoggedIn: We might do something based on frequency, or on the contents of the entry itself (like "gr" band)
		// Temporarily just add add this arbitrary small set, for testing.
		$loggedInCi = array(
			'1015472533', // yáobǎiwǔ 摇摆舞
			'1005365715', // húdié 蝴蝶
			'1009995331', // qǐchuáng 起床
		);
		return in_array($titleStr, $loggedInCi);
	} // ciEntryIsInExtraSetViewableIfLoggedIn

	/** ciEntryIsViewableByAnonymousUser:
		Is the cidian entry for the given title viewable by anonymous users?
		That is, do users who aren't logged in have permission to view it?

		@param $titleStr the page title string.
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnonymousUser.
	*/
	private static function ciEntryIsViewableByAnonymousUser($titleStr)
	{
		/* This array of 312 entries (as of 2017-3-23) is based on the output of wenlin/perl/demo_dict.pl.
			egrep '^ser' demo_ce.u8 | sort | uniq > tmp.u8 */
		$mianfeiCi = array(
			'1000000063',
			'1000000160',
			'1000000354',
			'1000000451',
			'1000000548',
			'1000121604',
			'1000258956',
			'1000538122',
			'1000680421',
			'1000680518',
			'1000737360',
			'1000859871',
			'1001621321',
			'1001643728',
			'1001832393',
			'1001884579',
			'1001930945',
			'1002001367',
			'1002099434',
			'1002100404',
			'1002174221',
			'1002174997',
			'1002224176',
			'1002303716',
			'1002339897',
			'1002363856',
			'1002363953',
			'1002508289',
			'1002508386',
			'1002508483',
			'1002508580',
			'1002514788',
			'1002523906',
			'1002562609',
			'1002563773',
			'1002563870',
			'1002564258',
			'1002648648',
			'1002716645',
			'1002716742',
			'1002805788',
			'1002898229',
			'1002898326',
			'1003141408',
			'1003170508',
			'1003171284',
			'1003201063',
			'1003318821',
			'1003488571',
			'1003524655',
			'1003944665',
			'1003944762',
			'1003946217',
			'1004084927',
			'1004091717',
			'1004104133',
			'1004105976',
			'1004264765',
			'1004314041',
			'1004318212',
			'1004472248',
			'1004611637',
			'1004612122',
			'1004612607',
			'1004633753',
			'1004663629',
			'1004663726',
			'1004703884',
			'1004704369',
			'1004763927',
			'1004778865',
			'1004860539',
			'1004863352',
			'1004863643',
			'1004906808',
			'1004923395',
			'1005119626',
			'1005142421',
			'1005142518',
			'1005168029',
			'1005277542',
			'1005307515',
			'1005378519',
			'1005507044',
			'1005613744',
			'1005664572',
			'1005685136',
			'1005700462',
			'1005701044',
			'1005701723',
			'1006020950',
			'1006102527',
			'1006295751',
			'1006317285',
			'1006470254',
			'1006570455',
			'1006571134',
			'1006597615',
			'1006745152',
			'1006747868',
			'1006921304',
			'1006937212',
			'1006937988',
			'1006970677',
			'1007054970',
			'1007070490',
			'1007085040',
			'1007090569',
			'1007177287',
			'1007318616',
			'1007324921',
			'1007406692',
			'1007406789',
			'1007421145',
			'1007485068',
			'1007550737',
			'1007550834',
			'1007683821',
			'1007692745',
			'1007769763',
			'1007783052',
			'1007897221',
			'1007914584',
			'1008009838',
			'1008185505',
			'1008204905',
			'1008214217',
			'1008273096',
			'1008450024',
			'1008519573',
			'1008798351',
			'1008808439',
			'1008894963',
			'1008980808',
			'1009022712',
			'1009037553',
			'1009037650',
			'1009038620',
			'1009133389',
			'1009164138',
			'1009204296',
			'1009355422',
			'1009386850',
			'1009386947',
			'1009476187',
			'1009758263',
			'1009872044',
			'1009989899',
			'1010071573',
			'1010422519',
			'1010585188',
			'1010629614',
			'1010668705',
			'1010673264',
			'1010673943',
			'1010801595',
			'1010862899',
			'1010947192',
			'1011102295',
			'1011103071',
			'1011103168',
			'1011126642',
			'1011286692',
			'1011300757',
			'1011354689',
			'1011381752',
			'1011447227',
			'1011485057',
			'1011569835',
			'1011571290',
			'1011650636',
			'1011664798',
			'1011673431',
			'1011737354',
			'1011737451',
			'1011877228',
			'1011996344',
			'1012000030',
			'1012000418',
			'1012231860',
			'1012327114',
			'1012327211',
			'1012416548',
			'1012584261',
			'1012618308',
			'1012625583',
			'1012658660',
			'1012668748',
			'1012858092',
			'1012858286',
			'1012917456',
			'1013036087',
			'1013208553',
			'1013227177',
			'1013227371',
			'1013287802',
			'1013507992',
			'1013575310',
			'1013663871',
			'1013689382',
			'1013706648',
			'1013724787',
			'1013749910',
			'1013762520',
			'1013774742',
			'1014015205',
			'1014039164',
			'1014054684',
			'1014154400',
			'1014229187',
			'1014273031',
			'1014287872',
			'1014288066',
			'1014398161',
			'1014840772',
			'1014840869',
			'1015051068',
			'1015051650',
			'1015088801',
			'1015098889',
			'1015108298',
			'1015256029',
			'1015357297',
			'1015360886',
			'1015573898',
			'1015638403',
			'1015699222',
			'1015742484',
			'1015767025',
			'1015869069',
			'1015873434',
			'1016012241',
			'1016063942',
			'1016163367',
			'1016177141',
			'1016219045',
			'1016244362',
			'1016288206',
			'1016288497',
			'1016289079',
			'1016312262',
			'1016459217',
			'1016482691',
			'1016515380',
			'1016604523',
			'1016727131',
			'1016727228',
			'1016891449',
			'1017036367',
			'1017169548',
			'1017169645',
			'1017238321',
			'1017239097',
			'1017248021',
			'1017251610',
			'1017251707',
			'1017257042',
			'1017259079',
			'1017367137',
			'1017439305',
			'1017441148',
			'1017448229',
			'1017449490',
			'1017480918',
			'1017597706',
			'1017682484',
			'1017683163',
			'1017683357',
			'1017712069',
			'1017764255',
			'1017779387',
			'1017825462',
			'1017834677',
			'1017834774',
			'1017845056',
			'1018035855',
			'1018109381',
			'1018144107',
			'1018147405',
			'1018167678',
			'1018334809',
			'1018395046',
			'1018536666',
			'1018538606',
			'1018578182',
			'1018581092',
			'1018583032',
			'1018602723',
			'1018611744',
			'1018615333',
			'1018616982',
			'1018622414',
			'1018661214',
			'1018682554',
			'1018683330',
			'1018722615',
			'1018733091',
			'1018748611',
			'1018782173',
			'1018916421',
			'1018941253',
			'1018947170',
			'1018948916',
			'1019013130',
			'1019060563',
			'1019288901',
			'1019298407',
			'1019299668',
			'1019311114',
			'1019771282',
			'1019773028',
			'1019949180',
			'1019950247',
		); // $mianfeiCi 
		return in_array($titleStr, $mianfeiCi);
	} // ciEntryIsViewableByAnonymousUser

	/************* Namespace En (YING-HAN 英汉) **************/

	/** enEntryIsInExtraSetViewableIfLoggedIn:
		Is the specified entry in a special set that are viewable if logged in, but not
		viewable if anonymous?

		@param $titleStr the page title string (serial number).
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnyLoggedInUser.
	*/
	private static function enEntryIsInExtraSetViewableIfLoggedIn($titleStr)
	{
		// TODO: enEntryIsInExtraSetViewableIfLoggedIn: We might do something based on frequency, or on the contents of the entry itself (like "gr" band)
		// Temporarily just add add this arbitrary small set, for testing.
		$loggedInEn = array(
			'2001724357', // yolk
			'2001726174', // zeal
			'2001726332', // zebra
		);
		return in_array($titleStr, $loggedInEn);
	} // enEntryIsInExtraSetViewableIfLoggedIn

	/** enEntryIsViewableByAnonymousUser:
		Is the English entry for the given title viewable by anonymous users?
		That is, do users who aren't logged in have permission to view it?

		@param $titleStr the page title string.
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnonymousUser.
	*/
	private static function enEntryIsViewableByAnonymousUser($titleStr)
	{
		/* This array of 687 serial numbers (as of 2017-3-23) is based on the output of wenlin/perl/demo_dict.pl.
				egrep '^ser' demo_ec_sep.u8 | sort | uniq > tmp.u8
			NOTE: There was a bug where --meta-- in ec.u8 as merged by abc_ec_sub_sep.pl caused demo_dict.pl to
			output no more than one subentry for any main entry, and this array was only 287 entries instead of
			687 as it should be to match Wenlin Mianfeiban 4.3.2. Fixed 2017-3-23.
			TODO: This is too big an array to clutter a source file like this! Either move it to its own source
			file, or store it in a custom db table instead? Cf. enEntryIsInExtraSetViewableIfLoggedIn. */
		$mianfeiEn = array(
			'2000000103',
			'2000002710',
			'2000009583',
			'2000009662',
			'2000019774',
			'2000019932',
			'2000020406',
			'2000020485',
			'2000029017',
			'2000031071',
			'2000032098',
			'2000037944',
			'2000041262',
			'2000049004',
			'2000064567',
			'2000071519',
			'2000101381',
			'2000101460',
			'2000103909',
			'2000106753',
			'2000114021',
			'2000169795',
			'2000171138',
			'2000171217',
			'2000171691',
			'2000171770',
			'2000210717',
			'2000210796',
			'2000210875',
			'2000210954',
			'2000246425',
			'2000246504',
			'2000246583',
			'2000246662',
			'2000246741',
			'2000246820',
			'2000246978',
			'2000247215',
			'2000247294',
			'2000247373',
			'2000247452',
			'2000285609',
			'2000285767',
			'2000285846',
			'2000285925',
			'2000297696',
			'2000297775',
			'2000298012',
			'2000345807',
			'2000391706',
			'2000391785',
			'2000393286',
			'2000393365',
			'2000400238',
			'2000400317',
			'2000400396',
			'2000400475',
			'2000400554',
			'2000403477',
			'2000403556',
			'2000403635',
			'2000419830',
			'2000420304',
			'2000424412',
			'2000427888',
			'2000446137',
			'2000461147',
			'2000461226',
			'2000461305',
			'2000461858',
			'2000462174',
			'2000488481',
			'2000513445',
			'2000531220',
			'2000539436',
			'2000540937',
			'2000541016',
			'2000544018',
			'2000544097',
			'2000550022',
			'2000573722',
			'2000573801',
			'2000573880',
			'2000573959',
			'2000574038',
			'2000577672',
			'2000577751',
			'2000577830',
			'2000577909',
			'2000584150',
			'2000584229',
			'2000584308',
			'2000584387',
			'2000584545',
			'2000584624',
			'2000584703',
			'2000584782',
			'2000584861',
			'2000585098',
			'2000585177',
			'2000585493',
			'2000585572',
			'2000585809',
			'2000586046',
			'2000586125',
			'2000586836',
			'2000587152',
			'2000587310',
			'2000587468',
			'2000587547',
			'2000587626',
			'2000591497',
			'2000591576',
			'2000591971',
			'2000592050',
			'2000593077',
			'2000593156',
			'2000593630',
			'2000594025',
			'2000594104',
			'2000594736',
			'2000594815',
			'2000594973',
			'2000633604',
			'2000633683',
			'2000635105',
			'2000645691',
			'2000668364',
			'2000674131',
			'2000674210',
			'2000674289',
			'2000686455',
			'2000699174',
			'2000699253',
			'2000699332',
			'2000733144',
			'2000771854',
			'2000771933',
			'2000804560',
			'2000804639',
			'2000804718',
			'2000820123',
			'2000820202',
			'2000820281',
			'2000844929',
			'2000845008',
			'2000845087',
			'2000845166',
			'2000863731',
			'2000902757',
			'2000902836',
			'2000902915',
			'2000913817',
			'2000916108',
			'2000916187',
			'2000931987',
			'2000932066',
			'2000932145',
			'2000932224',
			'2000932303',
			'2000943916',
			'2000945338',
			'2000949841',
			'2000949999',
			'2000951658',
			'2000954028',
			'2000954107',
			'2000954186',
			'2000966905',
			'2000966984',
			'2000967063',
			'2000967142',
			'2000973778',
			'2000973857',
			'2000974173',
			'2000974252',
			'2000975516',
			'2000980651',
			'2000985470',
			'2000986260',
			'2000986339',
			'2000986497',
			'2000986576',
			'2000991474',
			'2000991553',
			'2001053252',
			'2001053331',
			'2001053410',
			'2001066208',
			'2001066287',
			'2001066366',
			'2001139520',
			'2001139678',
			'2001139757',
			'2001140389',
			'2001140468',
			'2001147894',
			'2001149000',
			'2001163615',
			'2001163694',
			'2001234636',
			'2001241746',
			'2001241825',
			'2001321062',
			'2001321141',
			'2001343103',
			'2001343182',
			'2001343735',
			'2001343814',
			'2001343893',
			'2001350292',
			'2001350371',
			'2001357955',
			'2001358034',
			'2001366171',
			'2001366250',
			'2001366329',
			'2001390187',
			'2001390266',
			'2001409226',
			'2001413887',
			'2001413966',
			'2001414045',
			'2001422103',
			'2001422182',
			'2001427791',
			'2001427870',
			'2001427949',
			'2001428028',
			'2001465869',
			'2001465948',
			'2001466027',
			'2001466106',
			'2001466185',
			'2001466264',
			'2001492966',
			'2001493045',
			'2001520300',
			'2001520379',
			'2001520458',
			'2001520537',
			'2001520616',
			'2001520695',
			'2001535863',
			'2001542420',
			'2001543052',
			'2001543131',
			'2001543210',
			'2001543447',
			'2001544395',
			'2001544948',
			'2001547239',
			'2001548424',
			'2001549293',
			'2001550399',
			'2001550478',
			'2001550557',
			'2001550636',
			'2001550715',
			'2001552137',
			'2001552216',
			'2001552295',
			'2001555850',
			'2001556403',
			'2001556482',
			'2001556561',
			'2001561775',
			'2001561854',
			'2001563118',
			'2001564619',
			'2001600327',
			'2001600406',
			'2001600485',
			'2001600564',
			'2001600643',
			'2001600722',
			'2001600801',
			'2001610597',
			'2001635324',
			'2001635403',
			'2001635482',
			'2001635561',
			'2001635640',
			'2001654521',
			'2001654600',
			'2001662895',
			'2001662974',
			'2001686990',
			'2001687464',
			'2001687543',
			'2001690387',
			'2001690466',
			'2001692836',
			'2001692994',
			'2001694021',
			'2001694100',
			'2001697023',
			'2001698524',
			'2001700815',
			'2001700894',
			'2001700973',
			'2001701052',
			'2001706503',
			'2001723488',
			'2001723567',
			'2001723646',
			'2001724515',
			'2001757695',
			'2001772389',
			'2001772468',
			'2001772547',
			'2001772626',
			'2001772705',
			'2001786135',
			'2001786214',
			'2001786293',
			'2001786372',
			'2001790006',
			'2001790085',
			'2001790164',
			'2001790243',
			'2001790322',
			'2001790401',
			'2001790559',
			'2001790638',
			'2001790717',
			'2001790796',
			'2001790875',
			'2001790954',
			'2001797037',
			'2001804305',
			'2001804384',
			'2001804463',
			'2001804542',
			'2001804621',
			'2001816550',
			'2001817893',
			'2001823344',
			'2001823423',
			'2001823502',
			'2001844832',
			'2001845306',
			'2001845385',
			'2001845464',
			'2001845622',
			'2001845701',
			'2001845780',
			'2001857788',
			'2001876274',
			'2001910165',
			'2001910244',
			'2001910323',
			'2001910402',
			'2001910481',
			'2001913483',
			'2001913562',
			'2001919329',
			'2001926676',
			'2001926755',
			'2001929599',
			'2001929678',
			'2001929757',
			'2001929836',
			'2001929915',
			'2001929994',
			'2001930073',
			'2001930152',
			'2001930784',
			'2001941133',
			'2001941212',
			'2001941291',
			'2001941370',
			'2001961989',
			'2001962068',
			'2001962147',
			'2001962226',
			'2001962463',
			'2001962542',
			'2001963253',
			'2001973681',
			'2001973760',
			'2001973839',
			'2001973918',
			'2001973997',
			'2001974076',
			'2001974155',
			'2001974234',
			'2001974313',
			'2001974392',
			'2001974471',
			'2001974550',
			'2001974629',
			'2001974708',
			'2001974787',
			'2001974866',
			'2001974945',
			'2001975024',
			'2001975103',
			'2001975182',
			'2001975261',
			'2001976209',
			'2001976288',
			'2001976367',
			'2001976446',
			'2001976525',
			'2001976604',
			'2001978737',
			'2001978816',
			'2001978895',
			'2001978974',
			'2001979053',
			'2001993115',
			'2001993194',
			'2001993273',
			'2001993352',
			'2001996828',
			'2001996907',
			'2001996986',
			'2002002516',
			'2002002595',
			'2002004096',
			'2002004175',
			'2002004254',
			'2002004333',
			'2002004412',
			'2002009468',
			'2002009547',
			'2002009626',
			'2002009705',
			'2002009784',
			'2002009863',
			'2002048257',
			'2002048336',
			'2002048415',
			'2002048494',
			'2002048573',
			'2002048652',
			'2002048731',
			'2002048810',
			'2002048889',
			'2002048968',
			'2002064373',
			'2002064452',
			'2002064531',
			'2002064610',
			'2002064689',
			'2002064768',
			'2002064847',
			'2002064926',
			'2002065005',
			'2002065084',
			'2002069745',
			'2002069824',
			'2002078830',
			'2002078909',
			'2002078988',
			'2002079067',
			'2002079146',
			'2002079225',
			'2002079304',
			'2002079383',
			'2002079462',
			'2002079541',
			'2002079620',
			'2002079699',
			'2002079778',
			'2002079857',
			'2002079936',
			'2002087678',
			'2002087757',
			'2002109166',
			'2002110351',
			'2002110430',
			'2002110509',
			'2002110588',
			'2002110667',
			'2002110746',
			'2002116039',
			'2002120779',
			'2002120858',
			'2002120937',
			'2002121016',
			'2002121095',
			'2002123070',
			'2002123149',
			'2002123228',
			'2002123307',
			'2002124255',
			'2002124334',
			'2002124413',
			'2002127178',
			'2002127257',
			'2002127336',
			'2002127415',
			'2002127494',
			'2002127573',
			'2002132234',
			'2002132313',
			'2002133577',
			'2002133656',
			'2002133735',
			'2002133814',
			'2002134525',
			'2002134604',
			'2002134683',
			'2002134999',
			'2002135078',
			'2002139423',
			'2002139502',
			'2002139581',
			'2002139660',
			'2002139739',
			'2002139818',
			'2002139897',
			'2002139976',
			'2002140055',
			'2002140134',
			'2002140213',
			'2002140292',
			'2002140371',
			'2002141240',
			'2002141319',
			'2002141398',
			'2002141477',
			'2002141556',
			'2002166520',
			'2002166599',
			'2002166678',
			'2002166757',
			'2002203334',
			'2002203413',
			'2002203492',
			'2002203571',
			'2002203650',
			'2002203729',
			'2002203808',
			'2002203887',
			'2002203966',
			'2002204045',
			'2002204124',
			'2002204203',
			'2002204282',
			'2002204361',
			'2002204440',
			'2002204519',
			'2002204598',
			'2002204677',
			'2002204756',
			'2002204835',
			'2002204914',
			'2002206257',
			'2002215816',
			'2002251603',
			'2002251682',
			'2002251761',
			'2002251840',
			'2002251919',
			'2002259424',
			'2002259503',
			'2002259582',
			'2002259661',
			'2002259740',
			'2002259819',
			'2002259898',
			'2002259977',
			'2002260056',
			'2002260135',
			'2002261399',
			'2002261478',
			'2002261557',
			'2002261636',
			'2002261715',
			'2002261794',
			'2002294026',
			'2002294105',
			'2002294184',
			'2002294263',
			'2002294342',
			'2002294421',
			'2002294500',
			'2002294579',
			'2002294658',
			'2002299161',
			'2002299240',
			'2002318674',
			'2002327364',
			'2002327443',
			'2002327522',
			'2002327601',
			'2002335975',
			'2002336054',
			'2002336133',
			'2002336212',
			'2002336291',
			'2002336370',
			'2002336449',
			'2002336528',
			'2002336607',
			'2002336686',
			'2002336765',
			'2002336844',
			'2002336923',
			'2002337002',
			'2002337081',
			'2002337160',
			'2002337239',
			'2002337318',
			'2002337397',
			'2002337476',
			'2002337555',
			'2002345376',
			'2002345455',
			'2002345534',
			'2002345613',
			'2002346087',
			'2002346166',
			'2002346245',
			'2002346324',
			'2002346403',
			'2002346798',
			'2002346877',
			'2002346956',
			'2002347035',
			'2002347114',
			'2002347193',
			'2002347272',
			'2002347746',
			'2002347825',
			'2002347904',
			'2002347983',
			'2002348773',
			'2002348852',
			'2002348931',
			'2002349010',
			'2002349089',
			'2002349168',
			'2002349247',
			'2002349326',
			'2002349405',
			'2002349484',
			'2002349563',
			'2002349642',
			'2002349721',
			'2002349800',
			'2002350906',
			'2002350985',
			'2002351617',
			'2002351696',
			'2002354066',
			'2002354145',
			'2002361255',
			'2002361334',
			'2002361808',
			'2002361887',
			'2002361966',
			'2002362914',
			'2002362993',
			'2002390011',
			'2002390090',
			'2002390169',
			'2002390248',
			'2002390327',
			'2002390406',
			'2002395541',
			'2002410235',
			'2002410314',
			'2002410551',
			'2002410630',
			'2002410709',
			'2002410788',
			'2002410867',
			'2002410946',
			'2002411025',
			'2002412368',
			'2002412447',
			'2002412526',
			'2002412842',
			'2002413237',
			'2002413711',
			'2002413790',
			'2002413869',
			'2002421216',
			'2002433461',
			'2002434172',
			'2002442862',
			'2002481730',
			'2004137965',
		); // $mianfeiEn
		return in_array($titleStr, $mianfeiEn);
	} // enEntryIsViewableByAnonymousUser

	/************* Namespace Jyut (Cantonese 粤语) **************/

	/** jyutEntryIsInExtraSetViewableIfLoggedIn:
		Is the specified entry in a special set that are viewable if logged in, but not
		viewable if anonymous?

		@param $titleStr the page title string (serial number).
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnyLoggedInUser.
	*/
	private static function jyutEntryIsInExtraSetViewableIfLoggedIn($titleStr)
	{
		// TODO: jyutEntryIsInExtraSetViewableIfLoggedIn: We might do something based on frequency, or on the contents of the entry itself (like "gr" band)
		// Temporarily just add add this arbitrary small set, for testing.
		$loggedInJyut = array(
			'00006', '00007', '00008',
		);
		return in_array($titleStr, $loggedInJyut);
	} // jyutEntryIsInExtraSetViewableIfLoggedIn

	/** jyutEntryIsViewableByAnonymousUser:
		Is the Cantonese entry for the given title viewable by anonymous users?
		That is, do users who aren't logged in have permission to view it?

		@param $titleStr the page title string (serial number).
		@return true if the user has permission, else false.

		Called by entryIsViewableByAnonymousUser.
	*/
	private static function jyutEntryIsViewableByAnonymousUser($titleStr)
	{
		/* This array isn't based on much of anything -- just the first five serial numbers. */
		$mianfeiJyut = array(
			'00001', '00002', '00003', '00004', '00005',
		); // $mianfeiJyut
		return in_array($titleStr, $mianfeiJyut);
	} // jyutEntryIsViewableByAnonymousUser

} // class WenlinXuke
