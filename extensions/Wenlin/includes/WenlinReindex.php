<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/**
 * Rebuild Wenlin extension custom indexes. Based on maintenance/rebuildall.php and $refreshLinks.php.

	NOTE: if running on the command line, look in current directory for error_log!

	<https://www.mediawiki.org/wiki/Manual:Writing_maintenance_scripts>
 *
 * @file
 * @ingroup Maintenance
 */

require_once __DIR__ . "/../../../maintenance/Maintenance.php";
require_once __DIR__ . "/WenlinIndex.php"; // WenlinIndex::updateIndexesOnSave
require_once __DIR__ . "/WenlinUtf.php"; // ...
require_once __DIR__ . "/WenlinCharBand.php"; // ...
require_once __DIR__ . "/WenlinCollate.php"; // ...
require_once __DIR__ . "/WenlinBand.php"; // ...
require_once __DIR__ . "/WenlinParser.php"; // ...
require_once __DIR__ . "/WenlinPinyin.php"; // ...
require_once __DIR__ . "/WenlinRender.php"; // ...
require_once __DIR__ . "/WenlinFenlei.php"; // ...

ini_set('display_errors', 1);

/**
 * Maintenance script that rebuilds Wenlin extension custom indexes.
 *
 * @ingroup Maintenance
 */
class WenlinReindex extends Maintenance
{
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Rebuild Wenlin extension custom indexes";
		$this->addOption( 'e', 'Last page id to refresh', false, true );
		$this->addArg( 'start', 'Page_id to start from, default 1', false );
	}

	public function getDbType() {
		return Maintenance::DB_ADMIN;
	}

	public function execute() {
		$this->output( "\n\n** Rebuilding Wenlin tables -- this can take a long time. "
			. "It should be safe to abort via ctrl+C if you get bored.\n" );

		$reportingInterval = 100;
		$dbr = wfGetDB( DB_REPLICA );

		$startTime = microtime(true);
		$nonemptyEntriesCounted = 0;

		$start = (int)$this->getArg( 0 ) ?: null;
		$end = (int)$this->getOption( 'e' ) ?: null;
		if ( $start === null ) {
			$start = 1;
		}
		if ( !$end ) {
			$maxPage = $dbr->selectField( 'page', 'max(page_id)', false );
			$maxRD = $dbr->selectField( 'redirect', 'max(rd_from)', false );
			$end = max( $maxPage, $maxRD );
		}
		$this->output( "Refreshing tables.\n" );
		$this->output( "Starting from page_id $start of $end.\n" );

		for ($id = $start; $id <= $end; $id++) {
			if (($id % $reportingInterval) == 0) {
				$timeElapsedSecs = microtime(true) - $startTime;
				$this->output("$id; $timeElapsedSecs seconds elapsed; $nonemptyEntriesCounted non-empty entries so far.\n");
			}
			if (self::fixTables($id) === true) {
				++$nonemptyEntriesCounted;
			}
		}
		$this->output( "Done. $timeElapsedSecs seconds elapsed. $nonemptyEntriesCounted non-empty entries found for page_id $start..$end.\n" );
	} // execute

	/** fixTables:
	 * @param int $id The page_id
		@return true if we find a non-empty page with this page_id, else false.
	 */
	private function fixTables($id) {
		$article = WikiPage::newFromID( $id );
		if ( $article === null ) {
			return false;
		}
		/* TODO: save some time here by skipping if not in one of the namespaces
			handled by WenlinIndex::updateIndexesOnSave; should also add a command-line
			option for limiting to a particular namespace... */
		if (false) {
			$titleObj = $article->getTitle();
			$namespaceNumber = $titleObj->getNamespace();
			if ($namespaceNumber != NS_WL_JYUT) { // 'Jyut'
				return false;
			}
		}
		$content = $article->getContent( Revision::RAW );
		if ( $content === null ) {
			return false;
		}
		$status = Status::newGood();
		WenlinIndex::updateIndexesOnSave($article, $content, $status);
		if (!$status->isGood()) {
			$msg = $status->getHTML();
			$this->output("Indexing error ($id): $msg\n" );
		}
		return true;
	} // fixTables

} // class WenlinReindex

$maintClass = "WenlinReindex";
require_once RUN_MAINTENANCE_IF_MAIN;
