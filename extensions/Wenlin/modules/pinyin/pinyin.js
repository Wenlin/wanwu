/* Copyright (c) 2019 Wenlin Institute, Inc. SPC.
	This JavaScript file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop, Kerry Ngan, and other members of the Wenlin development team.
*/

'use strict';

/* This way of wrapping the file in an IIFE with parameters $ and mw, and calling it with
 		arguments jQuery and mediaWiki, is compatible with MediaWiki 1.32.0. It is in imitation of
		extensions/CategoryTree/modules/ext.categoryTree.js which is part of the default
		MediaWiki 1.32.0 installation.
	*/
(function($, mw) {

	const pyVerbose = false; // should be false unless debugging; turns on extra console messages if true

	/* When a MediaWiki page is being edited, the text is in a "textarea" element with id="wpTextbox1". */
	const pyTextSelector = '#wpTextbox1';

	var pyConversionOn = true; // when true, typing 1-4 adds tone marks; clicking button toggles it

	/* When a MediaWiki page is being edited, buttons are provided in a toolbar, including "B" for bold,
		"I" for italic, buttons for inserting IPA symbols, etc. We add our own custom button.
		Adding our button immediately on document ready could fail if the toolbar isn't ready.
		Instead, wait for 'wikiEditor-toolbar-doneInitialSections'.

		See https://www.mediawiki.org/wiki/Extension:WikiEditor/Toolbar_customization
	*/
	$(function () {
		$(pyTextSelector).on('wikiEditor-toolbar-doneInitialSections', addPinyinButton);
	});

	/** addPinyinButton:
		Add a button to the MediaWiki toolbar, for toggling enablement of automatic conversion
		of digits to pinyin tone marks, similar to "Typing 1-4 Adds Tone Marks" in the Options menu
		in the desktop Wenlin app. By default, the conversion is enabled, except for Jyut (Cantonese) namespace.
		Besides adding the button, we set up the needed keyboard event handler here.

		@return {void}
	*/
	function addPinyinButton() {
		var textArea = $(pyTextSelector);
		if (!textArea.length) {
			if (pyVerbose) {
				console.log("Text area does not exist.");
			}
			return;
		}
		if (typeof textArea.wikiEditor == 'undefined') {
			if (pyVerbose) {
				console.log("wikiEditor does not exist");
			}
			return;
		}
		var conf = mw.config.get(['wgCanonicalNamespace', 'wgServer', 'wgScriptPath']);
		var iconUrlBase = conf.wgServer + conf.wgScriptPath + '/extensions/Wenlin/modules/pinyin/';
		var namespace = conf.wgCanonicalNamespace;
		if (pyVerbose) {
			console.log("addPinyinButton got namespace " + namespace); // empty string means Main namespace
			console.log("addPinyinButton got wgServer " + conf.wgServer + " and wgScriptPath " + conf.wgScriptPath);
			// wgServer http://localhost and wgScriptPath /wanwu
		}
		/* Automatic digit-to-tone conversion is on by default, except for Cantonese (Jyut).
			Cantonese uses tone digits as in "aa1", must not convert to "aā"! */
		pyConversionOn = (namespace !== 'Jyut' && namespace !== 'Jyut_talk');

		const toolLabel = 'Typing 1-4 Adds Pinyin Tone Marks';

		/* We want our button to change its appearance to show whether conversion is on or off.
			It's not easy to change an already-added button in the wikiEditor API. Instead, we create
			two buttons, and we hide one or the other depending on pyConversionOn. */
		['on', 'off'].forEach(function(onOrOff) {
			textArea.wikiEditor('addToToolbar', buildPinyinTool(toolLabel, onOrOff, iconUrlBase));
		});
		showAndHidePinyinTools(toolLabel, pyConversionOn);

		/* When key '0' through '4' is pressed, add the corresponding tone mark to the text.
			'0' means to remove an existing tone mark. */
		textArea.keypress(function(event) {
			/* 0x30 and 0x34 are the ASCII codes (and Unicode Scalar Values) of '0' and '4' (U+0030 and U+0034). */
			if (pyConversionOn && event.which >= 0x30 && event.which <= 0x34) {
				/* addToneToTextArea returns a boolean value indicating whether a tone mark was added.
					Prevent normal insertion of the digit if, and only if, a tone mark was added. */
				if (addToneToTextArea(parseInt(String.fromCodePoint(event.which)))) {
					event.preventDefault();
				}
			}
		});
	} // addPinyinButton

	/** buildPinyinTool:
		Build an object suitable for passing to wikiEditor('addToToolbar', ...).

		@param {string} toolLabel the label of the tool, like 'Typing 1-4 Adds Pinyin Tone Marks', without trailing 'on' or 'off'.
		@param {string} onOrOff the string 'on' or 'off', which will be part of both the complete tool label and the icon filename.
		@param {string} iconUrlBase the URL for the directory containing the icon image files, including trailing slash,
			e.g., 'http://localhost/wanwu/extensions/Wenlin/modules/pinyin/'
			or 'https://wenlin.co/wanwu/extensions/Wenlin/modules/pinyin/'
		@return {object} the object suitable for passing to 'addToToolbar'.
	*/
	function buildPinyinTool(toolLabel, onOrOff, iconUrlBase) {
		const completeLabel = toolLabel + ' ' + onOrOff;
		const iconUrl = iconUrlBase + 'pinyin_' + onOrOff + '.png'; // pinyin_on.png or pinyin_off.png
		return {
			section: 'main',
			group: 'format',
			tools: {
				pinyin: {
					label: completeLabel,
					type: 'button',
					icon: iconUrl,
					action: {
						type: 'callback',
						execute: function(context) {
							pyConversionOn = !pyConversionOn;
							showAndHidePinyinTools(toolLabel, pyConversionOn);
						}
					}
				}
			}
		};
	} // buildPinyinTool

	/** showAndHidePinyinTools:
		Hide the pinyin tool that should be hidden, and show the pinyin tool that should be shown,
		according to the current value of pyConversionOn. There are two pinyin tools, of which one
		should always be hidden and the other should always be shown. This is a work-around for the
		fact that we don't have a way to change the icon for tool when it gets clicked on.

		@param {number} label the label (html title) of the tool,
			like 'Typing 1-4 Adds Pinyin Tone Marks', without trailing 'on' or 'off'.
		@param {boolean} convOn true when conversion is enabled, false when disabled (same as pyConversionOn).
		@return {boolean} true.
	*/
	function showAndHidePinyinTools(label, convOn) {
		const onEl = $("[title='" + label + " on']");
		const offEl = $("[title='" + label + " off']");
		const hiddenEl = convOn ? offEl : onEl;
		const shownEl = convOn ? onEl : offEl;
		hiddenEl.hide();
		shownEl.show();
		return true;
	} // showAndHidePinyinTools

	/** addToneToTextArea:
		Add a pinyin tone mark to text that is selected or adjacent to insertion point.

		@param {number} tone 0 through 4.
		@return {boolean} true if we add/change/remove a tone mark, else false.
	*/
	function addToneToTextArea(tone) {
		/* textArea is a jQuery "array-like object", and val() is a jQuery function, see http://api.jquery.com/val/
			The [0] to get the element el is due to the jQuery object being array-like, although if the selector
			is based on id (unique) then there is only one matching element. */
		const textArea = $(pyTextSelector);
		const el = $(textArea)[0];
		const wpt = new WenlinPinyinTone(pyVerbose);
		const newValues = wpt.addTone(textArea.val(), el.selectionStart, el.selectionEnd, tone);
		if (newValues.changed) {
			textArea.val(newValues.text);
			textArea[0].setSelectionRange(newValues.insertionPoint, newValues.insertionPoint);
		}
		return newValues.changed;
	} // addToneToTextArea

}(jQuery, mediaWiki));
