/* Copyright (c) 2018 Wenlin Institute, Inc. SPC.
	This JavaScript file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop, Kerry Ngan, and other members of the Wenlin development team.
*/

'use strict';

/* This class has one function meant to be public: addTone.
	For unit tests, see the "test" folder. */

class WenlinPinyinTone {
	constructor(verbose) {
		this.pyVerbose = verbose;
		/* validPy is derived from WenlinTushuguan/Text/transcriptions.wenlin,
		   all lines matching the regex "/^([a-zü]+)\s.+/"; that is, all the neutral-tone
		   syllables. */
		this.validPy = {
			'a': 1,
			'ai': 1,
			'an': 1,
			'ang': 1,
			'ao': 1,
			'ba': 1,
			'bai': 1,
			'ban': 1,
			'bang': 1,
			'bao': 1,
			'bei': 1,
			'ben': 1,
			'beng': 1,
			'bi': 1,
			'bian': 1,
			'biang': 1,
			'biao': 1,
			'bie': 1,
			'bin': 1,
			'bing': 1,
			'bo': 1,
			'bu': 1,
			'ca': 1,
			'cai': 1,
			'can': 1,
			'cang': 1,
			'cao': 1,
			'ce': 1,
			'cei': 1,
			'cen': 1,
			'ceng': 1,
			'cha': 1,
			'chai': 1,
			'chan': 1,
			'chang': 1,
			'chao': 1,
			'che': 1,
			'chen': 1,
			'cheng': 1,
			'chi': 1,
			'chong': 1,
			'chou': 1,
			'chu': 1,
			'chua': 1,
			'chuai': 1,
			'chuan': 1,
			'chuang': 1,
			'chui': 1,
			'chun': 1,
			'chuo': 1,
			'ci': 1,
			'cong': 1,
			'cou': 1,
			'cu': 1,
			'cuan': 1,
			'cui': 1,
			'cun': 1,
			'cuo': 1,
			'da': 1,
			'dai': 1,
			'dan': 1,
			'dang': 1,
			'dao': 1,
			'de': 1,
			'dei': 1,
			'den': 1,
			'deng': 1,
			'di': 1,
			'dia': 1,
			'dian': 1,
			'diao': 1,
			'die': 1,
			'din': 1,
			'ding': 1,
			'diu': 1,
			'dong': 1,
			'dou': 1,
			'du': 1,
			'duan': 1,
			'dui': 1,
			'dun': 1,
			'duo': 1,
			'e': 1,
			'ei': 1,
			'en': 1,
			'eng': 1,
			'er': 1,
			'fa': 1,
			'fan': 1,
			'fang': 1,
			'fei': 1,
			'fen': 1,
			'feng': 1,
			'fiao': 1,
			'fo': 1,
			'fou': 1,
			'fu': 1,
			'ga': 1,
			'gai': 1,
			'gan': 1,
			'gang': 1,
			'gao': 1,
			'ge': 1,
			'gei': 1,
			'gen': 1,
			'geng': 1,
			'gong': 1,
			'gou': 1,
			'gu': 1,
			'gua': 1,
			'guai': 1,
			'guan': 1,
			'guang': 1,
			'gui': 1,
			'gun': 1,
			'guo': 1,
			'ha': 1,
			'hai': 1,
			'han': 1,
			'hang': 1,
			'hao': 1,
			'he': 1,
			'hei': 1,
			'hen': 1,
			'heng': 1,
			'hm': 1,
			'hng': 1,
			'hong': 1,
			'hou': 1,
			'hu': 1,
			'hua': 1,
			'huai': 1,
			'huan': 1,
			'huang': 1,
			'hui': 1,
			'hun': 1,
			'huo': 1,
			'ji': 1,
			'jia': 1,
			'jian': 1,
			'jiang': 1,
			'jiao': 1,
			'jie': 1,
			'jin': 1,
			'jing': 1,
			'jiong': 1,
			'jiu': 1,
			'ju': 1,
			'juan': 1,
			'jue': 1,
			'jun': 1,
			'ka': 1,
			'kai': 1,
			'kan': 1,
			'kang': 1,
			'kao': 1,
			'ke': 1,
			'kei': 1,
			'ken': 1,
			'keng': 1,
			'kong': 1,
			'kou': 1,
			'ku': 1,
			'kua': 1,
			'kuai': 1,
			'kuan': 1,
			'kuang': 1,
			'kui': 1,
			'kun': 1,
			'kuo': 1,
			'la': 1,
			'lai': 1,
			'lan': 1,
			'lang': 1,
			'lao': 1,
			'le': 1,
			'lei': 1,
			'len': 1,
			'leng': 1,
			'li': 1,
			'lia': 1,
			'lian': 1,
			'liang': 1,
			'liao': 1,
			'lie': 1,
			'lin': 1,
			'ling': 1,
			'liu': 1,
			'lo': 1,
			'long': 1,
			'lou': 1,
			'lu': 1,
			'luan': 1,
			'lun': 1,
			'luo': 1,
			'lü': 1,
			'lüe': 1,
			'm': 1,
			'ma': 1,
			'mai': 1,
			'man': 1,
			'mang': 1,
			'mao': 1,
			'me': 1,
			'mei': 1,
			'men': 1,
			'meng': 1,
			'mi': 1,
			'mian': 1,
			'miao': 1,
			'mie': 1,
			'min': 1,
			'ming': 1,
			'miu': 1,
			'mo': 1,
			'mou': 1,
			'mu': 1,
			'n': 1,
			'na': 1,
			'nai': 1,
			'nan': 1,
			'nang': 1,
			'nao': 1,
			'ne': 1,
			'nei': 1,
			'nen': 1,
			'neng': 1,
			'ng': 1,
			'ni': 1,
			'nia': 1,
			'nian': 1,
			'niang': 1,
			'niao': 1,
			'nie': 1,
			'nin': 1,
			'ning': 1,
			'niu': 1,
			'nong': 1,
			'nou': 1,
			'nu': 1,
			'nuan': 1,
			'nun': 1,
			'nuo': 1,
			'nü': 1,
			'nüe': 1,
			'o': 1,
			'ou': 1,
			'pa': 1,
			'pai': 1,
			'pan': 1,
			'pang': 1,
			'pao': 1,
			'pei': 1,
			'pen': 1,
			'peng': 1,
			'pi': 1,
			'pian': 1,
			'piao': 1,
			'pie': 1,
			'pin': 1,
			'ping': 1,
			'po': 1,
			'pou': 1,
			'pu': 1,
			'qi': 1,
			'qia': 1,
			'qian': 1,
			'qiang': 1,
			'qiao': 1,
			'qie': 1,
			'qin': 1,
			'qing': 1,
			'qiong': 1,
			'qiu': 1,
			'qu': 1,
			'quan': 1,
			'que': 1,
			'qun': 1,
			'r': 1,
			'ran': 1,
			'rang': 1,
			'rao': 1,
			're': 1,
			'ren': 1,
			'reng': 1,
			'ri': 1,
			'rong': 1,
			'rou': 1,
			'ru': 1,
			'rua': 1,
			'ruan': 1,
			'rui': 1,
			'run': 1,
			'ruo': 1,
			'sa': 1,
			'sai': 1,
			'san': 1,
			'sang': 1,
			'sao': 1,
			'se': 1,
			'sen': 1,
			'seng': 1,
			'sha': 1,
			'shai': 1,
			'shan': 1,
			'shang': 1,
			'shao': 1,
			'she': 1,
			'shei': 1,
			'shen': 1,
			'sheng': 1,
			'shi': 1,
			'shou': 1,
			'shu': 1,
			'shua': 1,
			'shuai': 1,
			'shuan': 1,
			'shuang': 1,
			'shui': 1,
			'shun': 1,
			'shuo': 1,
			'si': 1,
			'song': 1,
			'sou': 1,
			'su': 1,
			'suan': 1,
			'sui': 1,
			'sun': 1,
			'suo': 1,
			'ta': 1,
			'tai': 1,
			'tan': 1,
			'tang': 1,
			'tao': 1,
			'te': 1,
			'tei': 1,
			'teng': 1,
			'ti': 1,
			'tian': 1,
			'tiao': 1,
			'tie': 1,
			'ting': 1,
			'tong': 1,
			'tou': 1,
			'tu': 1,
			'tuan': 1,
			'tui': 1,
			'tun': 1,
			'tuo': 1,
			'wa': 1,
			'wai': 1,
			'wan': 1,
			'wang': 1,
			'wei': 1,
			'wen': 1,
			'weng': 1,
			'wo': 1,
			'wong': 1,
			'wu': 1,
			'xi': 1,
			'xia': 1,
			'xian': 1,
			'xiang': 1,
			'xiao': 1,
			'xie': 1,
			'xin': 1,
			'xing': 1,
			'xiong': 1,
			'xiu': 1,
			'xu': 1,
			'xuan': 1,
			'xue': 1,
			'xun': 1,
			'ya': 1,
			'yan': 1,
			'yang': 1,
			'yao': 1,
			'ye': 1,
			'yi': 1,
			'yin': 1,
			'ying': 1,
			'yo': 1,
			'yong': 1,
			'you': 1,
			'yu': 1,
			'yuan': 1,
			'yue': 1,
			'yun': 1,
			'za': 1,
			'zai': 1,
			'zan': 1,
			'zang': 1,
			'zao': 1,
			'ze': 1,
			'zei': 1,
			'zen': 1,
			'zeng': 1,
			'zha': 1,
			'zhai': 1,
			'zhan': 1,
			'zhang': 1,
			'zhao': 1,
			'zhe': 1,
			'zhei': 1,
			'zhen': 1,
			'zheng': 1,
			'zhi': 1,
			'zhong': 1,
			'zhou': 1,
			'zhu': 1,
			'zhua': 1,
			'zhuai': 1,
			'zhuan': 1,
			'zhuang': 1,
			'zhui': 1,
			'zhun': 1,
			'zhuo': 1,
			'zi': 1,
			'zong': 1,
			'zou': 1,
			'zu': 1,
			'zuan': 1,
			'zui': 1,
			'zun': 1,
			'zuo': 1,
		}; // validPy

		this.uniPrecomp = { // for tone marks, use 1-4; ü=v0, Ü=V0
			'À': 'A4', // U+00c0
			'Á': 'A2', // U+00c1
			'È': 'E4', // U+00c8
			'É': 'E2', // U+00c9
			'Ì': 'I4', // U+00cc
			'Í': 'I2', // U+00cd
			'Ò': 'O4', // U+00d2
			'Ó': 'O2', // U+00d3
			'Ù': 'U4', // U+00d9
			'Ú': 'U2', // U+00da
			'Ü': 'V0', // U+00dc
			'à': 'a4', // U+00e0
			'á': 'a2', // U+00e1
			'è': 'e4', // U+00e8
			'é': 'e2', // U+00e9
			'ì': 'i4', // U+00ec
			'í': 'i2', // U+00ed
			'ò': 'o4', // U+00f2
			'ó': 'o2', // U+00f3
			'ù': 'u4', // U+00f9
			'ú': 'u2', // U+00fa
			'ü': 'v0', // U+00fc
			'Ā': 'A1', // U+0100
			'ā': 'a1', // U+0101
			'Ē': 'E1', // U+0112
			'ē': 'e1', // U+0113
			'Ě': 'E3', // U+011a
			'ě': 'e3', // U+011b
			'Ī': 'I1', // U+012a
			'ī': 'i1', // U+012b
			'Ń': 'N2', // U+0143
			'ń': 'n2', // U+0144
			'Ň': 'N3', // U+0147
			'ň': 'n3', // U+0148
			'Ō': 'O1', // U+014c
			'ō': 'o1', // U+014d
			'Ū': 'U1', // U+016a
			'ū': 'u1', // U+016b
			'Ǎ': 'A3', // U+01cd
			'ǎ': 'a3', // U+01ce
			'Ǐ': 'I3', // U+01cf
			'ǐ': 'i3', // U+01d0
			'Ǒ': 'O3', // U+01d1
			'ǒ': 'o3', // U+01d2
			'Ǔ': 'U3', // U+01d3
			'ǔ': 'u3', // U+01d4
			'Ǖ': 'V1', // U+01d5
			'ǖ': 'v1', // U+01d6
			'Ǘ': 'V2', // U+01d7
			'ǘ': 'v2', // U+01d8
			'Ǚ': 'V3', // U+01d9
			'ǚ': 'v3', // U+01da
			'Ǜ': 'V4', // U+01db
			'ǜ': 'v4', // U+01dc
			'Ǹ': 'N4', // U+01f8
			'ǹ': 'n4', // U+01f9
			'ḿ': 'm2', // U+1e3f
		}; // uniPrecomp
	} // constructor

	/** addTone:
		Add a pinyin tone mark to text that is selected or adjacent to insertion point.

		@param {string} wholeText the whole text.
		@param {number} selStart the start of the selection (highlighted range or insertion point),
			measured in UTF-16 code units (that's what JavaScript uses even though the HTML may
			be UTF-8).
		@param {number} selEnd the end of the selection (highlighted range or insertion point).
		@param {number} tone 0 through 4. 0 means to remove an existing tone mark.
		@return {object} an object containing a member ('changed') which is true if we add
			(or remove/change) a tone mark, else false. If changed is true, then the object
			also contains the changed text ('text') and new insertion point ('insertionPoint').
	*/
	addTone(wholeText, selStart, selEnd, tone) {
		if (tone < 0 || tone > 4) {
			return { changed: false };
		}
		if (this.pyVerbose) {
			console.log("addTone: selStart = " + selStart + "; selEnd = " + selEnd);
		}
		var [pySyl, pyStart, pyEnd, endsWithR] = this.findPySyl(wholeText, selStart, selEnd);
		if (!pySyl) {
			return { changed: false };
		}
		if (this.pyVerbose) {
			console.log("addTone: pyStart = " + pyStart + "; pyEnd = " + pyEnd + "; pySyl = " + pySyl);
		}
		const pyToneless = this.removeToneMarks(pySyl);
		const tonePos = this.tonePosition(pyToneless.toLowerCase(), pyToneless.length);
		const charToGetTone = pyToneless.charAt(tonePos);
		const pyVowel = this.combineVowelAndTone(charToGetTone, tone);
		if (!pyVowel) {
			if (this.pyVerbose) {
				console.log("addTone: combineVowelAndTone(" + charToGetTone + ", " + tone + ") failed");
			}
			return { changed: false };
		}
		if (this.pyVerbose) {
			console.log("addTone: combineVowelAndTone(" + charToGetTone + ", " + tone + ") returned " + pyVowel);
		}
		const newPySyl = pySyl.substring(0, tonePos) + pyVowel + pySyl.substring(tonePos + 1, pySyl.length);
		if (this.pyVerbose) {
			console.log("addTone: pySyl=" + pySyl + "; tonePos=" + tonePos + "; pyVowel=" + pyVowel +
				"; newPySyl=" + newPySyl + "; endsWithR = " + endsWithR);
		}
		if (endsWithR) {
			--pyEnd;
		}
		return {
			changed: true,
			text: wholeText.substring(0, pyStart) + newPySyl + wholeText.substring(pyEnd),
			insertionPoint: pyStart + newPySyl.length + (endsWithR ? 1 : 0)
		};
	}; // addTone

	/** findPySyl:
		Find the start and end of the pinyin syllable that is selected or adjacent to the insertion point.

		@param {string} wholeText the whole text.
		@param {number} selStart the start of the selection (highlighted range or insertion point),
			measured in UTF-16 code units (that's what JavaScript uses even though the HTML may
			be UTF-8).
		@param {number} selEnd the end of the selection (highlighted range or insertion point).
		@return {object} an object containing 4 elements:
			the pinyin syllable; its starting position; its ending position; and a boolean, true if it ends with -r or -R.
	*/
	findPySyl(wholeText, selStart, selEnd) {
		var pySyl = '';
		var pyStart = selStart;
		var pyEnd = selEnd;
		var endsWithR = false;
		if (selStart !== selEnd) {
			pySyl = wholeText.substring(pyStart, pyEnd);
			var py = pySyl.toLowerCase();
			py = this.removeToneMarks(py);
			py = py.replace(/v/g, 'ü');
			if (!(py in this.validPy)) {
				if (py.substring(0, py.length - 1) in this.validPy && py.substring(py.length - 1) === 'r') {
					endsWithR = true;
					pySyl = pySyl.substring(0, pySyl.length - 1);
				}
				else {
					if (this.pyVerbose) {
						console.log("findPySyl: highlighted range [" + pySyl + "] is not recognized as a pinyin syllable");
					}
					return ['', 0, 0, false];
				}
			}
		}
		else {
			// if nothing is selected, find the start of the "word" on the left side of the insertion point;
			// move left past all could-be-pinyin chars, but not more than 7
			while (pyStart > 0 && pyStart + 7 >= selStart) {
				var c = wholeText.charAt(pyStart - 1).toLowerCase();
				if (c < 'a' || c > 'z') {
					var [ascii, toneDigit] = this.uniPyChar(c); // returns ascii='v' for 'ü'
					if (!ascii) {
						break;
					}
				}
				--pyStart;
			}
			// Look for a valid pinyin syl that starts at pyStart and ends >= selEnd;
			// if not found, ++pyStart and look again, until found or abort for pyStart >= selStart.
			while (pyStart < selStart) {
				var len;
				[len, endsWithR] = this.lengthOfPySyl(wholeText, pyStart);
				if (len > 0 && pyStart + len >= selStart) {
					pyEnd = pyStart + len;
					break;
				}
				++pyStart;
			}
			if (pyStart > selStart) {
				if (this.pyVerbose) {
					console.log("findPySyl: valid pinyin not found");
				}
				return ['', 0, 0, false];
			}
			pySyl = wholeText.substring(pyStart, pyEnd - (endsWithR ? 1 : 0));
			if (this.pyVerbose) {
				console.log("findPySyl: zero selection, pySyl = " + pySyl + "; pyEnd = " + pyEnd);
			}
		}
		return [pySyl, pyStart, pyEnd, endsWithR];
	}; // findPySyl

	/** lengthOfPySyl:
		Find the length of the pinyin syllable (if any) that starts at the given position in the given text.

		@param {string} text the whole text.
		@param {number} pyStart the position in text of the start of what may be a pinyin syllable.
		@return {object} an object containing 2 elements:
			the length of the pinyin syllable, or 0 if there isn't one starting at that position;
			and a boolean, true if it ends with -r or -R.

		Called by addTone .
	*/
	lengthOfPySyl(text, pyStart) {
		text = text.substring(pyStart);
		if (text.length > 7) {
			text = text.substring(0, 7);
		}
		text = this.removeToneMarks(text.toLowerCase()).replace(/v/g, 'ü');
		for (var len = 7; len > 0; len--) {
			var py = text.substring(0, len);
			if (py in this.validPy) {
				return [len, false];
			}
			if (py.substring(0, len - 1) in this.validPy && py.substring(len - 1) === 'r') {
				return [len, true];
			}
		}
		return [0, false];
	}; // lengthOfPySyl

	/** removeToneMarks:
		Remove any tone marks in the given text. E.g., return 'hao' for 'hǎo'.

		@param {string} text the text that may include tone marks.
		@return {string} the text without tone marks.

		Called by addTone and lengthOfPySyl.
	*/
	removeToneMarks(text) {
		for (var i = 0; i < text.length; i++) {
			var c = text.charAt(i);
			if (c in this.uniPrecomp) {
				text = text.substring(0, i) + this.uniPyChar(c)[0] + text.substring(i + 1, text.length);
			}
		}
		return text;
	}; // removeToneMarks

	/** uniPyChar:
		Translate a Unicode pinyin vowel or 'm' or 'n' with tonemark
		into an ASCII letter, plus a number for the tone.

		@param {string} c the character.
		@return {Array<string>} a list of two items:
			(1) ASCII letter, or '' (empty string) if input isn't pinyin letter with tone mark.
			(2) tone: '0' (ü=v0), '1', '2', '3', '4', or '' (not found).

		Called by findPySyl and removeToneMarks.
	*/
	uniPyChar(c) {
		if (!(c in this.uniPrecomp)) {
			return ['', ''];
		}
		var p = this.uniPrecomp[c];
		var a = p.substring(0, 1);
		var b = parseInt(p.substring(1, 2));
		return [a, b];
	}; // uniPyChar

	/** combineVowelAndTone:
		Combine a letter and a tone number to make a letter with a tone diacritic.

		@param {string} vowel - the letter.
		@param {number} tone - the tone number (0-4).
		@return {string} the combined letter with a tone diacritic.

		Called by addTone.
	*/
	combineVowelAndTone(vowel, tone) {
		if (tone < 0 || tone > 4) {
			return '';
		}
		switch (vowel) {
		case 'A':
			return ['A', 'Ā', 'Á', 'Ǎ', 'À'][tone];
		case 'a':
			return ['a', 'ā', 'á', 'ǎ', 'à'][tone];
		case 'E':
			return ['E', 'Ē', 'É', 'Ě', 'È'][tone];
		case 'e':
			return ['e', 'ē', 'é', 'ě', 'è'][tone];
		case 'I':
			return ['I', 'Ī', 'Í', 'Ǐ', 'Ì'][tone];
		case 'i':
			return ['i', 'ī', 'í', 'ǐ', 'ì'][tone];
		case 'O':
			return ['O', 'Ō', 'Ó', 'Ǒ', 'Ò'][tone];
		case 'o':
			return ['o', 'ō', 'ó', 'ǒ', 'ò'][tone];
		case 'U':
			return ['U', 'Ū', 'Ú', 'Ǔ', 'Ù'][tone];
		case 'u':
			return ['u', 'ū', 'ú', 'ǔ', 'ù'][tone];
		case 'Ü': case 'V':
			return ['Ü', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ'][tone];
		case 'ü': case 'v':
			return ['ü', 'ǖ', 'ǘ', 'ǚ', 'ǜ'][tone];
		case 'N':
			return ['N', 'N̄', 'Ń', 'Ň', 'Ǹ'][tone];
		case 'n':
			return ['n', 'n̄', 'ń', 'ň', 'ǹ'][tone];
		case 'M':
			return ['M', 'M̄', 'Ḿ', 'M̌', 'M̀'][tone];
		case 'm':
			return ['m', 'm̄', 'ḿ', 'm̌', 'm̀'][tone];
		default:
			return '';
		}
	}; // combineVowelAndTone

	/** tonePosition:
		Find the tone position within the given pinyin syllable.

		@param {string} py the lowercase ascii pinyin syllable (no tone v represents umlaut u).
		@param {number} pyLen the length of py.
		@return {number} 0 for first letter, 1 for second, etc.

		Called by addTone.
	*/
	tonePosition(py, pyLen) {
		while (--pyLen > 0) {
			switch (py.charAt(pyLen)) {
				case 'i':
				case 'o':
				case 'u':
					switch (py.charAt(pyLen - 1)) {
						case 'a':
						case 'e':
						case 'o':
							return pyLen - 1;
					}
				case 'a':
				case 'e':
				case 'm':
				case 'ü':
				case 'v':
					return pyLen;
			}
		}
		return (py.charAt(0) == 'h' && py.charAt(1) == 'n') ? 1 : 0;
	}; // tonePosition

}; // class WenlinPinyinTone

/* "module.exports" is needed for mocha (node) test, but not defined in browser. See
	http://www.matteoagosti.com/blog/2013/02/24/writing-javascript-modules-for-both-browser-and-node/
	https://stackoverflow.com/questions/37895664/using-es6-classes-or-object-literals-in-controllers-for-an-express-nodejs-app
*/
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
	module.exports = WenlinPinyinTone;
}
