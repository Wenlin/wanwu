To install mocha:

First install npm, then

npm install -g mocha


To run the tests:

cd extensions/Wenlin/modules/pinyin/test

mocha WenlinPinyinToneTest.js


Expected output is like this:

  WenlinPinyinTone.addTone
    ✓ should return changed false for x4
    ✓ should return hǎo for hao3
    ✓ should return ň for n3
    ✓ should return ǎo for ao3
    ✓ should return nǚ for nv3
    ✓ should return háng for hang2
    ✓ should return ... nǚ ... for ... nv3 ...

  7 passing (13ms)
