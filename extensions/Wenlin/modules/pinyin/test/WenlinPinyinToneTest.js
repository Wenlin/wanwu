'use strict';
const assert = require('assert');
const WenlinPinyinTone = require('../WenlinPinyinTone.js');

describe('WenlinPinyinTone.addTone', function() {
	const wpt = new WenlinPinyinTone(false);
	it('should return changed false for x4', function() {
		const actualOutput = wpt.addTone('x', 0, 1, 4);
		const expectedOutput = {changed: false};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return hǎo for hao3', function() {
		const actualOutput = wpt.addTone('hao', 0, 3, 3);
		const expectedOutput = {changed: true, text: 'hǎo', insertionPoint: 3};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return HǍO for HAO3', function() {
		const actualOutput = wpt.addTone('HAO', 0, 3, 3);
		const expectedOutput = {changed: true, text: 'HǍO', insertionPoint: 3};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return ň for n3', function() {
		const actualOutput = wpt.addTone('n', 0, 1, 3);
		const expectedOutput = {changed: true, text: 'ň', insertionPoint: 1};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return ǎo for ao3', function() {
		const actualOutput = wpt.addTone('ao', 0, 2, 3);
		const expectedOutput = {changed: true, text: 'ǎo', insertionPoint: 2};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return nǚ for nv3', function() {
		const actualOutput = wpt.addTone('nv', 0, 2, 3);
		const expectedOutput = {changed: true, text: 'nǚ', insertionPoint: 2};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return háng for hang2', function() {
		const actualOutput = wpt.addTone('hang', 4, 4, 2);
		const expectedOutput = {changed: true, text: 'háng', insertionPoint: 4};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return ... nǚ ... for ... nv3 ...', function() {
		const actualOutput = wpt.addTone('blah blah nv blah blah', 12, 12, 3);
		const expectedOutput = {changed: true, text: 'blah blah nǚ blah blah', insertionPoint: 12};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return ǎr for ar3', function() {
		const actualOutput = wpt.addTone('ar', 2, 2, 3);
		const expectedOutput = {changed: true, text: 'ǎr', insertionPoint: 2};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return NǍR for NAR3', function() {
		const actualOutput = wpt.addTone('NAR', 3, 3, 3);
		const expectedOutput = {changed: true, text: 'NǍR', insertionPoint: 3};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return rángr for rangr2', function() {
		const actualOutput = wpt.addTone('rangr', 5, 5, 2);
		const expectedOutput = {changed: true, text: 'rángr', insertionPoint: 5};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return xiào for xiao4 and move insertion point from middle to end', function() {
		const actualOutput = wpt.addTone('xiao', 2, 2, 4);
		const expectedOutput = {changed: true, text: 'xiào', insertionPoint: 4};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return bàngr for bangr4 and move insertion point from middle to end', function() {
		const actualOutput = wpt.addTone('bangr', 3, 3, 4);
		const expectedOutput = {changed: true, text: 'bàngr', insertionPoint: 5};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return BÀNGR for BANGR4 and move insertion point from middle to end', function() {
		const actualOutput = wpt.addTone('BANGR', 3, 3, 4);
		const expectedOutput = {changed: true, text: 'BÀNGR', insertionPoint: 5};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return BÀNGR for BÁNGR4 and move insertion point from middle to end', function() {
		const actualOutput = wpt.addTone('BÁNGR', 3, 3, 4);
		const expectedOutput = {changed: true, text: 'BÀNGR', insertionPoint: 5};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return ń for n2', function() {
		const actualOutput = wpt.addTone('n', 1, 1, 2);
		const expectedOutput = {changed: true, text: 'ń', insertionPoint: 1};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return m̀ for m4', function() { // m̀ is two code points: U+006D plus U+0300
		const actualOutput = wpt.addTone('m', 1, 1, 4);
		const expectedOutput = {changed: true, text: 'm̀', insertionPoint: 2};
		assert.deepEqual(actualOutput, expectedOutput);
	});
	it('should return shǔ for shu3', function() {
		const actualOutput = wpt.addTone('shu', 3, 3, 3);
		const expectedOutput = {changed: true, text: 'shǔ', insertionPoint: 3};
		assert.deepEqual(actualOutput, expectedOutput);
	});

	/* TODO: see WenlinTest.php
	'dạ̌o' => 'dao, 3', // support dot/line below (tone-change notation)?
	'shuị̌' => 'shui, 3',
	'yǔ' => 'yu, 3',
	'yī̠' => 'yi, 1',
	// TODO: 'nǎr' => 'nar, 3' ?
	'Nǐhǎo' => 'Ni3hao3',
	'kẹ̌yǐ' => 'ke3yi3',
	'nǚ' => 'nv3',
	'nǚyǔhángyuán' => 'nv3yu3hang2yuan2',
	'lụ̈̌guǎn*' => 'lv3guan3*',
	'cídiǎn zhèngwén' => 'ci2dian3 zheng4wen2',
	'lüèwēi' => 'lve4wei1',
	'kèrén' => 'ke4ren2',
	'ér' => 'er2',
	'érhuà' => 'er2hua4',
	*/
});
