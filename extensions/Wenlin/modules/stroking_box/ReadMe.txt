The files cdl-utils.js and cdl-utils-ui.js should not be edited directly. They are copies of files by the same names in the Wenlin CDL JavaScript API, which is in the two separate projects cdlutils and cdlutils_ui.

Keep the copies up to date.

See:

http://wenlincdl.com/docs/getting-started

https://www.npmjs.com/package/cdl-utils

https://www.npmjs.com/package/cdl-utils-ui
