/* This javascript file gets loaded by onBeforePageDisplay in Wenlin.hooks.php,
	only if the page is an article in namespace 'Zi'. We could also get the
	namespace and title here in the js, like this:
		var conf = mw.config.get(['wgCanonicalNamespace', 'wgTitle']);
		if (conf.wgCanonicalNamespace === 'Zi') {
			var zi = conf.wgTitle;
	but we currently don't need to.

	The function WenlinRender::makeStrokeButtonForZidianEntry creates an element
	with id='strokingStuff' title='$zi'. Here we convert that element into a "button" (really a
	link, not an html button) that will open the stroking box, and we add the numeric
	stroke count and a "fanned" static stroke diagram.

	There are two related extensions declared in the ResourceModules part of extension.json:
	'ext.strokebutton' and 'ext.strokebox'.
	This file strokebutton.js is the one and only js file in 'ext.strokebutton'.
	Most of the CDL JS API is in 'ext.strokebox'.
	Currently, Wenlin.hooks.php loads 'ext.strokebutton', and then we load 'ext.strokebox' here
	with mw.loader.using(). What's the point of that? Originally, the "button" didn't include a
	fanned stroke diagram, and therefore it was possible in principle to show the button without
	loading 'ext.strokebox', and we could wait until the button was pressed (if ever) to load it,
	saving time. I don't think it really worked that way, though, since we always loaded it (by calling
	mw.loader.using) when we created the button. Anyway, now that we have a fanned stroke diagram,
	even before the button is pressed, there doesn't seem to be any benefit to the division between
	'ext.strokebutton' and 'ext.strokebox'. Maybe we should combine them, to simplify the code and
	possibly achieve a little speed-up since there's probably some overhead in loading them separately.
*/
mw.loader.using('ext.strokebox', function() {
	$(document).ready(function() {
		var strokeEl = $('#strokingStuff');
		if (!strokeEl) {
			return;
		}
		var zi = strokeEl.attr('title');
		if (!zi) {
			return;
		}
		var linkWidth = strokeEl.width();
		var linkHeight = strokeEl.height();
		strokeEl.attr('onclick', "StrokingBox.open('" + zi + "');");

		/* Set the width and height of each of the partially or fully stroked characters.
			Base the height on linkHeight. When linkHeight = 22, then HEIGHT = 18 seems about right. */
		var HEIGHT = Math.round(linkHeight * 0.818); // 18/22 = 0.818181818181...
		var WIDTH = HEIGHT; // a Hanzi fits in a square, width = height

		/*
		 * Same dataLoader code as in stroking_box.js
		 * Use this dataLoader instead of apiKey for fetchCdlChar
		 * Make sure to define dataLoader before using it in options for getSvgPathStrings.
		 */
		var dataLoader = function(unicode, variant) {
			// compare getUnicodeHex in cdl-utils.js and CdlParser.js
			if (typeof unicode === 'string' && !unicode.match(/^[0-9A-Fa-f]+$/)) { // not all hex digits
				unicode = unicode.codePointAt(0); // codePointAt NOT charCodeAt (which fails for U+10000 and beyond)
			}
			const id = 'strokingBoxCdl-' + unicode.toString(16).toUpperCase() + '-' + variant;
			const el = document.getElementById(id);
			if (el == null) {
				// alert('dataLoader Null: ' + id);
				return null;
			}
			var cdl = el.innerHTML;
			cdl = cdl.replace(/\&lt;/g, '<'); // replace '&lt;' with '<'
			cdl = cdl.replace(/\&gt;/g, '>'); // replace '&gt;' with '>'
			// alert(cdl);
			return cdl;
		};
		var svgPathRenderOptions = {
			width: WIDTH,
			height: HEIGHT,
			// hengRadius: 5,
			dataLoader: dataLoader
		}
		// Get the SVG path for the complete character.
		// Caution: distinguish between CdlUtils.getSvgPathStrings and CharacterRenderer.getSvgPathStrings, different calling conventions!
		CdlUtils.getSvgPathStrings(zi, svgPathRenderOptions).then(function(pathStrings) {
			var numStrokes = pathStrings.length;

			/* Write the number of strokes, replacing "..." that was used as a placeholder for the
				content of the element with id 'strokeCount' in makeStrokeButtonForZidianEntry. */
			$('#strokeCount').html(numStrokes);

			// Draw the fanned character.
			// The style "bottom" is to align the svg, otherwise it's too high.
			strokeEl.append("<span id='fannedChar' style='position:relative; bottom:-.6em; margin:0px;'></span>");
			drawFannedChar('fannedChar', pathStrings, numStrokes);
		});

		/*
		 * Draw the Fanned Character to a target HTML item, using SVG.
		 * (It would be interesting to try it with HTML5 "canvas", see if that's faster.)
		 */
		var drawFannedChar = function(targetId, pathStrings, numStrokes) {
			// Detect the available width to determine how many strokes will fit on the remainder of the line.
			// If the character has too many strokes, only display the last strokes in the fanning.
			var availableWidth = $('#bodyContent').width() - linkWidth;
			// alert("Avail wid = " + availableWidth);
			var strokesToDisplay = numStrokes;
			var horizontalSpaceBetween = WIDTH / 2;
			var fanWidth = (WIDTH + horizontalSpaceBetween) * strokesToDisplay - horizontalSpaceBetween;
			if (fanWidth > availableWidth) {
				strokesToDisplay = Math.round(availableWidth / (WIDTH + horizontalSpaceBetween) - 0.5)
				fanWidth = (WIDTH + horizontalSpaceBetween) * strokesToDisplay - horizontalSpaceBetween;
			}
			// console.log("In strokebutton.js: linkWidth = " + linkWidth + "; linkHeight = " + linkHeight);
			// console.log("In strokebutton.js: WIDTH = HEIGHT = " + WIDTH);
			// console.log("In strokebutton.js: availableWidth = " + availableWidth + "; fanWidth = " + fanWidth);
			var strokesToSkip = numStrokes - strokesToDisplay;
			var svg = SVG(targetId); // SVG function is in svg.js
			svg.width(fanWidth);
			svg.height(linkHeight);
			var pathStringsUpToNow = [];
			var n = 0;
			pathStrings.forEach(function(pathString, i) {
				pathStringsUpToNow.push(pathString);
				if (++n > strokesToSkip) {
					var group = svg.group();
					pathStringsUpToNow.forEach(function(currentPathString) {
						group.path(currentPathString).fill('#333');
					});
					group.translate((WIDTH + horizontalSpaceBetween) * (i - strokesToSkip), 0);
				};
			});
		};
	});
}, function(e) { console.error('strokebutton not loaded:' + e); });
