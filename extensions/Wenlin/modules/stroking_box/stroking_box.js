(function() {

  ////////// HTML structure //////////

  var html = "" +
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +
    "<div class='StrokingBox'>" +
      "<div class='StrokingBox-overlay'></div>" +
      "<div class='StrokingBox-modalContainer'>" +
        "<div id='draggable' class='StrokingBox-modal'>" + 
          "<script>\n" +
            "$(function() {\n" +
              "$('#draggable').draggable({cancel:\".undraggable\"});\n" +
            "});\n" +
          "</script>" +

          "<a href='#' class='StrokingBox-closeButton'>&times;</a>" +
          "<div class='StrokingBox-status'>" +
            "<div class='StrokingBox-miniCharacter'><svg></svg></div>" +
            "<span class='StrokingBox-strokeStatus'></span>" +
            "<a class='StrokingBox-toggleSettings' href='#'>" +
              "<span class='StrokingBox-gear self icon-gear undraggable' aria-hidden='true'></span>" +
            "</a>" +
          "</div>" +
          "<div class='StrokingBox-character undraggable' id='strokingQuizzing' style='display:none'>" +
            "<svg>" +
              "<g class='QuizzingBox-renderTarget'></g>" +
            "</svg>" +
          "</div>" +
          "<div class='StrokingBox-character' id='strokingAnimation'>" +
            "<svg>" +
              "<g class='StrokingBox-renderTarget'></g>" +
            "</svg>" +
          "</div>" +
          "<div class='StrokingBox-animationControls'>" +
            "<a class='StrokingBox-animationButton StrokingBox-play' href='#'><span class='self icon-play' aria-hidden='true'></span></a>" +
            "<a class='StrokingBox-animationButton StrokingBox-stop' href='#'><span class='self icon-stop' aria-hidden='true'></span></a>" +
            "<a class='StrokingBox-animationButton StrokingBox-stepBackwards' href='#'><span class='self icon-left' aria-hidden='true'></span></a>" +
            "<a class='StrokingBox-animationButton StrokingBox-stepForward' href='#'><span class='self icon-right' aria-hidden='true'></span></a>" +
            "<div class='StrokingBox-clear'></div>" +
          "</div>" +
          "<div class='StrokingBox-quizzingControls' style='display:none'>" +
            "<a class='StrokingBox-quizzingButton StrokingBox-restartQuiz' href='#'>Restart</a>" +
            "<a class='StrokingBox-quizzingButton StrokingBox-endQuiz' href='#'>End Quiz</a>" +
            "<div class='StrokingBox-clear'></div>" +
          "</div>" +
          "<div class='StrokingBox-controls undraggable'>" +
            "<div class='StrokingBox-animationSpeedControls'>" +
              "<span class='StrokingBox-slowLabel'>màn</span>" +
              "<span class='StrokingBox-fastLabel'>kuài</span>" +
              "<input type='range' class='StrokingBox-animationSpeed' min='-4' max='4' />" +
            "</div>" +
            "<div class='StrokingBox-colorControls'>" +
              "<input type='text' class='StrokingBox-charColor' />" +
              "<input type='text' class='StrokingBox-backgroundColor' />" +
            "</div>" +
            "<div class='StrokingBox-toggles'>" +
              "<label>" +
                "<input type='checkbox' class='StrokingBox-loop' />" +
                "Loop" +
              "</label>" +
              "<label>" +
                "<input type='checkbox' class='StrokingBox-grid8' />" +
                "Grid 8" +
              "</label>" +
              "<label>" +
                "<input type='checkbox' class='StrokingBox-trace' />" +
                "Trace" +
              "</label>" +
              "<label>" +
                "<input type='checkbox' class='StrokingBox-grid9' />" +
                "Grid 9" +
              "</label>" +
              "<label>" +
                "<input type='checkbox' class='StrokingBox-plain' />" +
                "Plain" +
              "</label>" +
              "<div class='StrokingBox-clear'></div>" +
            "</div>" +
            "<div class='StrokingBox-quizzingStartControls'>" +
              "<a class='StrokingBox-startQuiz' href='#'>Start Quiz</a>" +
            "</div>" +
          "</div>" +
          "<div class='StrokingBox-dragControlBottomRight undraggable'></div>" +
        "</div>"+
      "</div>" +
    "</div>";

  ////////// sizing constants //////////

  const strokingMarginX = 10;
  const strokingMarginY = strokingMarginX;
  const strokingInitialWidth = 200;
  const strokingInitialHeight = strokingInitialWidth;
  const strokingSquarePlusMargins = strokingInitialWidth + 2 * strokingMarginX; // 220

  ////////// grid helpers //////////

  var drawSvgLine = function(svg, point1, point2) {
    var line = document.createElementNS("http://www.w3.org/2000/svg","line");
    line.setAttribute('x1', point1.x);
    line.setAttribute('y1', point1.y);
    line.setAttribute('x2', point2.x);
    line.setAttribute('y2', point2.y);
    line.setAttribute('stroke', '#CCC');
    svg.insertBefore(line, svg.firstChild);
    return line;
  };

  var grid9 = [];
  var drawGrid9 = function(svg1, svg2, size) {
    grid9 = [];
    grid9.push(drawSvgLine(svg1, {x: size / 3, y: 0}, {x: size / 3, y: size}));
    grid9.push(drawSvgLine(svg1, {x: 2 * size / 3, y: 0}, {x: 2 * size / 3, y: size}));
    grid9.push(drawSvgLine(svg1, {x: 0, y: size / 3}, {x: size, y: size / 3}));
    grid9.push(drawSvgLine(svg1, {x: 0, y: 2 * size / 3}, {x: size, y: 2 * size / 3}));

    grid9.push(drawSvgLine(svg2, {x: size / 3, y: 0}, {x: size / 3, y: size}));
    grid9.push(drawSvgLine(svg2, {x: 2 * size / 3, y: 0}, {x: 2 * size / 3, y: size}));
    grid9.push(drawSvgLine(svg2, {x: 0, y: size / 3}, {x: size, y: size / 3}));
    grid9.push(drawSvgLine(svg2, {x: 0, y: 2 * size / 3}, {x: size, y: 2 * size / 3}));

  };

  var grid8 = [];
  var drawGrid8 = function(svg1, svg2, size) {
    grid8 = [];
    grid8.push(drawSvgLine(svg1, {x: 0, y: 0}, {x: size, y: size}));
    grid8.push(drawSvgLine(svg1, {x: 0, y: size}, {x: size, y: 0}));
    grid8.push(drawSvgLine(svg1, {x: size / 2, y: 0}, {x: size / 2, y: size}));
    grid8.push(drawSvgLine(svg1, {x: 0, y: size / 2}, {x: size, y: size / 2}));

    grid8.push(drawSvgLine(svg2, {x: 0, y: 0}, {x: size, y: size}));
    grid8.push(drawSvgLine(svg2, {x: 0, y: size}, {x: size, y: 0}));
    grid8.push(drawSvgLine(svg2, {x: size / 2, y: 0}, {x: size / 2, y: size}));
    grid8.push(drawSvgLine(svg2, {x: 0, y: size / 2}, {x: size, y: size / 2}));
  };

  var removeGrid = function(grid) {
    grid.forEach(function(line) {
      line.remove();
    });
  };

  var showGrid = function(grid) {
    grid.forEach(function(line) {
      line.setAttribute('visibility', 'visible');
    });
  };

  var hideGrid = function(grid) {
    grid.forEach(function(line) {
      line.setAttribute('visibility', 'hidden');
    });
  };

  ////////// resize helpers ///////////////

  var isResizing = false;
  var resizingOpts = {};
  var resizeStartPoint = null;
  var size = strokingSquarePlusMargins;
  var svgScale = 1.0;
  var resizeStartSize = null;
  
  var beginResize = function(evt) {
    evt.preventDefault();
    isResizing = true;
    resizeStartSize = size;
    resizeStartPoint = extractPoint(evt);
  };

  var continueResize = function(evt) {
    if (!isResizing) {
      return;
    }
    var point = extractPoint(evt);
    // multiply x by 2 since it expands in both x directions simultaneously
    var xDelta = 2 * (point.x - resizeStartPoint.x);
    var yDelta = point.y - resizeStartPoint.y;
    var deltas = [xDelta, yDelta];

    var newSize = resizeStartSize + Math.min.apply(null, deltas);
    setSize(newSize);
  };

  var endResize = function(evt) {
    isResizing = false;
  };

  var extractPoint = function(evt) {
    if (evt.originalEvent.touches && evt.originalEvent.touches.length > 0) {
      return {x: evt.originalEvent.touches[0].clientX, y: evt.originalEvent.touches[0].clientY}
    } else {
      return {x: evt.clientX, y: evt.clientY}
    }
  }

  var setSize = function(newSize) {
    size = Math.max(newSize, strokingSquarePlusMargins);
    svgScale = size / strokingSquarePlusMargins;
    $domRoot.find('.StrokingBox-modal').css('width', size);
    $domRoot.find('.StrokingBox-character').css('height', size - 2);
    $domRoot.find('.StrokingBox-renderTarget').attr('transform', 'scale(' + svgScale + ')');
    $domRoot.find('.QuizzingBox-renderTarget').attr('transform', 'scale(' + svgScale + ')');
  };


  ////////// other helpers ////////////////

  var currentStrokeNum = 0;
  var updateCurrentStrokeNum = function(strokeNum) {
    currentStrokeNum = strokeNum;
    var totalStrokes = animator.strokes.length;
    $domRoot.find('.StrokingBox-strokeStatus').text('' + strokeNum + ' / ' + totalStrokes);
  };

  //////// init code ///////////

  var initHasRun = false;
  var init = function() {
    if (initHasRun) {
      return;
    }
    initHasRun = true;
    setupDom();
  };

  var $domRoot;
  var setupDom = function() {
    $domRoot = $(html).appendTo(document.body);

    // closing
    $domRoot.on('click', '.StrokingBox-overlay', closeStrokingBox);
    $domRoot.on('click', '.StrokingBox-closeButton', function(evt) {
      evt.preventDefault();
      closeStrokingBox();
    });

    // toggle extra settings
    $domRoot.on('click', '.StrokingBox-toggleSettings', function(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      $domRoot.toggleClass('is-showingSettings');
    });
    $domRoot.on('click', '.StrokingBox-modal', function() {
      $domRoot.removeClass('is-showingSettings');
    });
    $domRoot.on('click', '.StrokingBox-controls', function(evt) {
      evt.stopPropagation();
    });

    // animation controls
    $domRoot.on('click', '.StrokingBox-stop', function(evt) {
      evt.preventDefault();
      animator.stopAnimation();
      animator.hide();
      updateCurrentStrokeNum(0);
    });
    $domRoot.on('click', '.StrokingBox-play', function(evt) {
      evt.preventDefault();
      var animate = function() {
        if (currentStrokeNum >= animator.strokes.length) {
          updateCurrentStrokeNum(0);
        }
        animator.animate({
          beginAnimationStrokeNum: currentStrokeNum,
          onBeginStrokeAnimation: function(info) {
            updateCurrentStrokeNum(info.strokeNum + 1);
          }
        });
      };
      animate();
    });
    $domRoot.on('click', '.StrokingBox-stepForward', function(evt) {
      evt.preventDefault();
      animator.stopAnimation();
      animator.strokes.slice(0, currentStrokeNum).forEach(function(stroke) {
        stroke.stop();
        stroke.show();
      });
      if (currentStrokeNum >= animator.strokes.length) {
        animator.hide();
        updateCurrentStrokeNum(0);
      }
      updateCurrentStrokeNum(currentStrokeNum + 1);
      animator.strokes[currentStrokeNum - 1].animate();
    });
    $domRoot.on('click', '.StrokingBox-stepBackwards', function(evt) {
      evt.preventDefault();
      animator.stopAnimation();
      updateCurrentStrokeNum(currentStrokeNum - 1);
      if (currentStrokeNum < 0) {
        updateCurrentStrokeNum(animator.strokes.length);
      }
      animator.strokes.slice(currentStrokeNum).forEach(function(stroke) {
        stroke.hide();
      });
      animator.strokes.slice(0, currentStrokeNum).forEach(function(stroke) {
        stroke.show();
      });
    });

    // quizzing controls
    $domRoot.on('click', '.StrokingBox-startQuiz', function(evt) {
      $domRoot.removeClass('is-showingSettings');
      updateQuizMode(true);
    });
    $domRoot.on('click', '.StrokingBox-restartQuiz', function(evt) {
      quizzer.resetQuiz();
      // note: updateQuizMode(true) isn't needed, and would have no effect since isQuizMode is already true
    });
    $domRoot.on('click', '.StrokingBox-endQuiz', function(evt) {
      updateQuizMode(false);
    });

    // animation speed
    $domRoot.on('change', '.StrokingBox-animationSpeed', updateAnimationSpeed);

    // color selection
    $domRoot.find('.StrokingBox-charColor').spectrum({
      replacerClassName: 'StrokingBox-charColor',
      color: '#000000',
      showInitial: true,
      change: updateCharacterColor
    });
    $domRoot.find('.StrokingBox-backgroundColor').spectrum({
      replacerClassName: 'StrokingBox-backgroundColor',
      color: '#FFFFFF',
      showInitial: true,
      change: updateBackgroundColor
    });

    // bottom toggles
    $domRoot.on('click', '.StrokingBox-grid9', updateGrid9);
    $domRoot.on('click', '.StrokingBox-grid8', updateGrid8);
    $domRoot.on('click', '.StrokingBox-trace', updateTrace);
    $domRoot.on('click', '.StrokingBox-loop', updateLoop);
    $domRoot.on('click', '.StrokingBox-plain', updateFontStyle);

    // resize handlers
    $domRoot.on('mousedown', '.StrokingBox-dragControlBottomRight', beginResize);
    $domRoot.on('touchstart', '.StrokingBox-dragControlBottomRight', beginResize);
    $domRoot.on('mousemove', continueResize);
    $domRoot.on('touchmove', continueResize);
    $domRoot.on('mouseup', endResize);
    $domRoot.on('touchend', endResize);
    $domRoot.on('mouseleave', endResize);
  };

  // make sure the character reflects what the controls say
  var synchronizeInitialState = function() {
    updateAnimationSpeed();
    updateGrid9();
    updateGrid8();
    updateBackgroundColor();
    updateCharacterColor();
    updateLoop();
    updateTrace();
    isPlain = false;
    updateFontStyle();
    updateQuizMode(false);
  };

  ///////////// event handlers ///////////

  var updateGrid8 = function() {
    $domRoot.find('.StrokingBox-grid8').is(':checked') ? showGrid(grid8) : hideGrid(grid8);
  };

  var updateGrid9 = function() {
    $domRoot.find('.StrokingBox-grid9').is(':checked') ? showGrid(grid9) : hideGrid(grid9);
  };

  var updateTrace = function() {
    var showTrace = $domRoot.find('.StrokingBox-trace').is(':checked');
    showTrace ? animator.showOutline() : animator.hideOutline();
    animator.updateOptions({ showOutline: showTrace });
  };

  var updateLoop = function() {
    animator.updateOptions({ loopAnimation: $domRoot.find('.StrokingBox-loop').is(':checked') });
  };

  var updateCharacterColor = function() {
    var color = $domRoot.find('.StrokingBox-charColor').spectrum('get').toHexString();
    var strokeStyles = { fill: isPlain ? 'none' : color, stroke: color };
    animator.setStrokeStyles(strokeStyles);
    animator.updateOptions({ strokeStyles: strokeStyles })
  };

  var updateAnimationSpeed = function() {
    var relativeSpeed = parseInt($domRoot.find('.StrokingBox-animationSpeed').val(), 10);
    var speedIndex = relativeSpeed + 4; // 0 - 8
    var strokeAnimationVelocites = [0.3, 0.5, 0.7, 0.9, 1, 1.5, 1.9, 2.4, 3]
    var delaysBetweenStrokes = [1000, 1000, 900, 700, 500, 300, 100, 50, 10];
    var delaysBetweenLoops = [2000, 2000, 1800, 1400, 1000, 600, 200, 100, 20];
    animator.updateOptions({
      strokeAnimationVelocity: strokeAnimationVelocites[speedIndex],
      delayBetweenStrokes: delaysBetweenStrokes[speedIndex],
      delayBetweenLoops: delaysBetweenLoops[speedIndex]
    });
  };

  var isPlain = false; // font
  var updateFontStyle = function(evt) {
    var newIsPlain = $domRoot.find('.StrokingBox-plain').is(':checked');
    if (newIsPlain === isPlain) return;
    isPlain = newIsPlain;
    animator.updateOptions({
      font: isPlain ? 'plain' : 'song'
    });
    animator.render().then(function() {
      animator.show();
      animator.setStrokeStyles({ 'stroke-width': isPlain ? '2' : '0' });
      animator.setOutlineStyles({ 'stroke-width': isPlain ? '2' : '0' });
      updateCharacterColor();
      updateCurrentStrokeNum(animator.strokes.length);
    });
  };

  var isQuizMode = false;
  var updateQuizMode = function(turnedOn) {
    if (turnedOn !== isQuizMode) {
      isQuizMode = !isQuizMode;
      var quizDisplay = isQuizMode ? 'block' : 'none';
      var animDisplay = isQuizMode ? 'none' : 'block';
      $('#strokingAnimation').css('display', animDisplay);
      $('#strokingQuizzing').css('display', quizDisplay);
      $('.StrokingBox-animationControls').css('display', animDisplay);
      $('.StrokingBox-quizzingControls').css('display', quizDisplay);
      $('.StrokingBox-quizzingStartControls').css('display', animDisplay); // display "Start" iff not already quizzing
      updateCurrentStrokeNum(0);
      if (animator) {
        animator.stopAnimation();
        animator.hide();
      }
      if (isQuizMode) {
        quizzer.startQuiz();
      }
    }
  };

  var updateBackgroundColor = function() {
    var color = $domRoot.find('.StrokingBox-backgroundColor').spectrum('get').toHexString();
    $domRoot.find('.StrokingBox-character').css('background', color);
  };

  ///////// public-facing functions ////////

  var closeStrokingBox = function() {
    $domRoot.addClass('is-closed');
  };

  var animator;
  var quizzer;
  var openStrokingBox = function(character) {
    init();
    $domRoot.removeClass('is-closed');
    if (animator) {
      animator.remove();
    }
    if (quizzer) {
      quizzer.remove();
    }
    removeGrid(grid8);
    removeGrid(grid9);

    var animatorTarget = $domRoot.find('.StrokingBox-renderTarget')[0];
    var quizzingTarget = $domRoot.find('.QuizzingBox-renderTarget')[0];

    animator = new CdlUtils.Animator(animatorTarget, {
      x: strokingMarginX, // Nonzero x offset is for margin.
      y: strokingMarginY, // Nonzero y offset is for margin, needed to prevent top of 林 being cut off.
      width: strokingInitialWidth,
      height: strokingInitialHeight,
      // font: 'plain', // TODO: Results aren't good, not same as checking 'plain' in the gear menu.
      strokeStyles: {
        'stroke-linejoin': 'round',
        'stroke-linecap': 'round'
      },
      strokeOutlineStyles: {
        'stroke-linejoin': 'round',
        'stroke-linecap': 'round'
      }
    });

    var quizzerOnCorrectStroke = function(result) {
      // Update the stroke number status indicator, and add a smiley face if the learner finished all the strokes.
      // We could also somehow show result.mistakesOnStroke and/or result.totalMistakes; currently we don't.
      updateCurrentStrokeNum(result.strokeNum + 1);
      if (result.strokesRemaining === 0) {
        const statusEl = $('.StrokingBox-strokeStatus');
        statusEl.html('☺ ' + statusEl.html());
      }
    };

    quizzer = new CdlUtils.Quizzer(quizzingTarget, {
      x: strokingMarginX,
      y: strokingMarginY,
      width: strokingInitialWidth,
      height: strokingInitialHeight,
      showHintAfterMisses: 1,
      showOutline: true,
      onCorrectStroke: quizzerOnCorrectStroke
    });

    /* Use this dataLoader instead of apiKey for fetchCdlChar.
        Assume the PHP code has put the cdl inside a hidden span,
        with &lt; and &gt; for < and >, including the descriptions of all
        components, recursively, without doing any coordinate arithmetic.
        Each component has its own hidden span, with id=usv-variant.
        This function is called in various ways: "unicode" might be a hex string
        like "4E00", or a Hanzi string like "𩨎", or maybe a number like 0x4E00? */
    var dataLoader = function(unicode, variant) {
      // compare getUnicodeHex in cdl-utils.js and CdlParser.js
      if (typeof unicode === 'string' && !unicode.match(/^[0-9A-Fa-f]+$/)) { // not all hex digits
        unicode = unicode.codePointAt(0); // codePointAt NOT charCodeAt (which fails for U+10000 and beyond)
      }
      const id = 'strokingBoxCdl-' + unicode.toString(16).toUpperCase() + '-' + variant;
      const el = document.getElementById(id);
      if (el == null) {
        alert('dataLoader Null: ' + id);
        return null;
      }
      var cdl = el.innerHTML;
      cdl = cdl.replace(/\&lt;/g, '<'); // replace '&lt;' with '<'
      cdl = cdl.replace(/\&gt;/g, '>'); // replace '&gt;' with '>'
      // alert(cdl);
      return cdl;
    };

    CdlUtils.fetchCdlChar(character,
      { dataLoader: dataLoader }
      /* { apiKey: '...' } */
      ).then(function(cdlChar) {
      animator.renderCharacter(cdlChar).then(function() {
        animator.show();
        hideGrid(grid8);
        hideGrid(grid9);
        updateCurrentStrokeNum(animator.strokes.length);
        synchronizeInitialState();
      });
      quizzer.renderCharacter(cdlChar).then(function() {
        quizzer.show();
        hideGrid(grid8);
        hideGrid(grid9);
        quizzer.startQuiz();
      });
      drawGrid8(animator.svg, quizzer.svg, strokingSquarePlusMargins);
      drawGrid9(animator.svg, quizzer.svg, strokingSquarePlusMargins);

      // render the mini character
      CdlUtils.getSvgPathStrings(cdlChar, { height: 16, width: 16 }).then(function(pathStrings) {
        $domRoot.find('.StrokingBox-miniCharacter svg').empty();
        var svg = $domRoot.find('.StrokingBox-miniCharacter svg')[0];
        pathStrings.forEach(function(pathString) {
          var path = document.createElementNS("http://www.w3.org/2000/svg","path");
          path.setAttribute("d", pathString);
          path.setAttribute("fill", "#999");
          svg.appendChild(path);
        });
      });

    });    
  };

  window.StrokingBox = {
    close: closeStrokingBox,
    open: openStrokingBox
  };
})();
