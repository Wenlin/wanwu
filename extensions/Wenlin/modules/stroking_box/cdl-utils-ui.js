/*!
 * CdlUtils-UI | Copyright (C) 2018 Wenlin Institute, Inc. SPC. All Rights Reserved.
 * 	This JavaScript file is offered for licensing under the GNU Affero General Public License v.3:
 * 		https://www.gnu.org/licenses/agpl.html
 * 	Use of the Wenlin CDL JavaScript API is subject to the Terms of Service:
 * 		https://wenlincdl.com/terms
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var velocity = __webpack_require__(1);
	var CharacterAnimator = __webpack_require__(2);
	var CharacterQuizzer = __webpack_require__(11);
	var StrokeAnimator = __webpack_require__(5);
	var CdlUtils = global.CdlUtils;

	CdlUtils.velocity = velocity;
	CdlUtils.Animator = CharacterAnimator;
	CdlUtils.Quizzer = CharacterQuizzer;
	CdlUtils.StrokeAnimator = StrokeAnimator;

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! VelocityJS.org (1.2.3). (C) 2014 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */
	/*! VelocityJS.org jQuery Shim (1.0.1). (C) 2014 The jQuery Foundation. MIT @license: en.wikipedia.org/wiki/MIT_License. */
	!function(a){function b(a){var b=a.length,d=c.type(a);return"function"===d||c.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===d||0===b||"number"==typeof b&&b>0&&b-1 in a}if(!a.jQuery){var c=function(a,b){return new c.fn.init(a,b)};c.isWindow=function(a){return null!=a&&a==a.window},c.type=function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?e[g.call(a)]||"object":typeof a},c.isArray=Array.isArray||function(a){return"array"===c.type(a)},c.isPlainObject=function(a){var b;if(!a||"object"!==c.type(a)||a.nodeType||c.isWindow(a))return!1;try{if(a.constructor&&!f.call(a,"constructor")&&!f.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(d){return!1}for(b in a);return void 0===b||f.call(a,b)},c.each=function(a,c,d){var e,f=0,g=a.length,h=b(a);if(d){if(h)for(;g>f&&(e=c.apply(a[f],d),e!==!1);f++);else for(f in a)if(e=c.apply(a[f],d),e===!1)break}else if(h)for(;g>f&&(e=c.call(a[f],f,a[f]),e!==!1);f++);else for(f in a)if(e=c.call(a[f],f,a[f]),e===!1)break;return a},c.data=function(a,b,e){if(void 0===e){var f=a[c.expando],g=f&&d[f];if(void 0===b)return g;if(g&&b in g)return g[b]}else if(void 0!==b){var f=a[c.expando]||(a[c.expando]=++c.uuid);return d[f]=d[f]||{},d[f][b]=e,e}},c.removeData=function(a,b){var e=a[c.expando],f=e&&d[e];f&&c.each(b,function(a,b){delete f[b]})},c.extend=function(){var a,b,d,e,f,g,h=arguments[0]||{},i=1,j=arguments.length,k=!1;for("boolean"==typeof h&&(k=h,h=arguments[i]||{},i++),"object"!=typeof h&&"function"!==c.type(h)&&(h={}),i===j&&(h=this,i--);j>i;i++)if(null!=(f=arguments[i]))for(e in f)a=h[e],d=f[e],h!==d&&(k&&d&&(c.isPlainObject(d)||(b=c.isArray(d)))?(b?(b=!1,g=a&&c.isArray(a)?a:[]):g=a&&c.isPlainObject(a)?a:{},h[e]=c.extend(k,g,d)):void 0!==d&&(h[e]=d));return h},c.queue=function(a,d,e){function f(a,c){var d=c||[];return null!=a&&(b(Object(a))?!function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;)a[e++]=b[d++];if(c!==c)for(;void 0!==b[d];)a[e++]=b[d++];return a.length=e,a}(d,"string"==typeof a?[a]:a):[].push.call(d,a)),d}if(a){d=(d||"fx")+"queue";var g=c.data(a,d);return e?(!g||c.isArray(e)?g=c.data(a,d,f(e)):g.push(e),g):g||[]}},c.dequeue=function(a,b){c.each(a.nodeType?[a]:a,function(a,d){b=b||"fx";var e=c.queue(d,b),f=e.shift();"inprogress"===f&&(f=e.shift()),f&&("fx"===b&&e.unshift("inprogress"),f.call(d,function(){c.dequeue(d,b)}))})},c.fn=c.prototype={init:function(a){if(a.nodeType)return this[0]=a,this;throw new Error("Not a DOM node.")},offset:function(){var b=this[0].getBoundingClientRect?this[0].getBoundingClientRect():{top:0,left:0};return{top:b.top+(a.pageYOffset||document.scrollTop||0)-(document.clientTop||0),left:b.left+(a.pageXOffset||document.scrollLeft||0)-(document.clientLeft||0)}},position:function(){function a(){for(var a=this.offsetParent||document;a&&"html"===!a.nodeType.toLowerCase&&"static"===a.style.position;)a=a.offsetParent;return a||document}var b=this[0],a=a.apply(b),d=this.offset(),e=/^(?:body|html)$/i.test(a.nodeName)?{top:0,left:0}:c(a).offset();return d.top-=parseFloat(b.style.marginTop)||0,d.left-=parseFloat(b.style.marginLeft)||0,a.style&&(e.top+=parseFloat(a.style.borderTopWidth)||0,e.left+=parseFloat(a.style.borderLeftWidth)||0),{top:d.top-e.top,left:d.left-e.left}}};var d={};c.expando="velocity"+(new Date).getTime(),c.uuid=0;for(var e={},f=e.hasOwnProperty,g=e.toString,h="Boolean Number String Function Array Date RegExp Object Error".split(" "),i=0;i<h.length;i++)e["[object "+h[i]+"]"]=h[i].toLowerCase();c.fn.init.prototype=c.fn,a.Velocity={Utilities:c}}}(window),function(a){"object"==typeof module&&"object"==typeof module.exports?module.exports=a(): true?!(__WEBPACK_AMD_DEFINE_FACTORY__ = (a), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):a()}(function(){return function(a,b,c,d){function e(a){for(var b=-1,c=a?a.length:0,d=[];++b<c;){var e=a[b];e&&d.push(e)}return d}function f(a){return p.isWrapped(a)?a=[].slice.call(a):p.isNode(a)&&(a=[a]),a}function g(a){var b=m.data(a,"velocity");return null===b?d:b}function h(a){return function(b){return Math.round(b*a)*(1/a)}}function i(a,c,d,e){function f(a,b){return 1-3*b+3*a}function g(a,b){return 3*b-6*a}function h(a){return 3*a}function i(a,b,c){return((f(b,c)*a+g(b,c))*a+h(b))*a}function j(a,b,c){return 3*f(b,c)*a*a+2*g(b,c)*a+h(b)}function k(b,c){for(var e=0;p>e;++e){var f=j(c,a,d);if(0===f)return c;var g=i(c,a,d)-b;c-=g/f}return c}function l(){for(var b=0;t>b;++b)x[b]=i(b*u,a,d)}function m(b,c,e){var f,g,h=0;do g=c+(e-c)/2,f=i(g,a,d)-b,f>0?e=g:c=g;while(Math.abs(f)>r&&++h<s);return g}function n(b){for(var c=0,e=1,f=t-1;e!=f&&x[e]<=b;++e)c+=u;--e;var g=(b-x[e])/(x[e+1]-x[e]),h=c+g*u,i=j(h,a,d);return i>=q?k(b,h):0==i?h:m(b,c,c+u)}function o(){y=!0,(a!=c||d!=e)&&l()}var p=4,q=.001,r=1e-7,s=10,t=11,u=1/(t-1),v="Float32Array"in b;if(4!==arguments.length)return!1;for(var w=0;4>w;++w)if("number"!=typeof arguments[w]||isNaN(arguments[w])||!isFinite(arguments[w]))return!1;a=Math.min(a,1),d=Math.min(d,1),a=Math.max(a,0),d=Math.max(d,0);var x=v?new Float32Array(t):new Array(t),y=!1,z=function(b){return y||o(),a===c&&d===e?b:0===b?0:1===b?1:i(n(b),c,e)};z.getControlPoints=function(){return[{x:a,y:c},{x:d,y:e}]};var A="generateBezier("+[a,c,d,e]+")";return z.toString=function(){return A},z}function j(a,b){var c=a;return p.isString(a)?t.Easings[a]||(c=!1):c=p.isArray(a)&&1===a.length?h.apply(null,a):p.isArray(a)&&2===a.length?u.apply(null,a.concat([b])):p.isArray(a)&&4===a.length?i.apply(null,a):!1,c===!1&&(c=t.Easings[t.defaults.easing]?t.defaults.easing:s),c}function k(a){if(a){var b=(new Date).getTime(),c=t.State.calls.length;c>1e4&&(t.State.calls=e(t.State.calls));for(var f=0;c>f;f++)if(t.State.calls[f]){var h=t.State.calls[f],i=h[0],j=h[2],n=h[3],o=!!n,q=null;n||(n=t.State.calls[f][3]=b-16);for(var r=Math.min((b-n)/j.duration,1),s=0,u=i.length;u>s;s++){var w=i[s],y=w.element;if(g(y)){var z=!1;if(j.display!==d&&null!==j.display&&"none"!==j.display){if("flex"===j.display){var A=["-webkit-box","-moz-box","-ms-flexbox","-webkit-flex"];m.each(A,function(a,b){v.setPropertyValue(y,"display",b)})}v.setPropertyValue(y,"display",j.display)}j.visibility!==d&&"hidden"!==j.visibility&&v.setPropertyValue(y,"visibility",j.visibility);for(var B in w)if("element"!==B){var C,D=w[B],E=p.isString(D.easing)?t.Easings[D.easing]:D.easing;if(1===r)C=D.endValue;else{var F=D.endValue-D.startValue;if(C=D.startValue+F*E(r,j,F),!o&&C===D.currentValue)continue}if(D.currentValue=C,"tween"===B)q=C;else{if(v.Hooks.registered[B]){var G=v.Hooks.getRoot(B),H=g(y).rootPropertyValueCache[G];H&&(D.rootPropertyValue=H)}var I=v.setPropertyValue(y,B,D.currentValue+(0===parseFloat(C)?"":D.unitType),D.rootPropertyValue,D.scrollData);v.Hooks.registered[B]&&(g(y).rootPropertyValueCache[G]=v.Normalizations.registered[G]?v.Normalizations.registered[G]("extract",null,I[1]):I[1]),"transform"===I[0]&&(z=!0)}}j.mobileHA&&g(y).transformCache.translate3d===d&&(g(y).transformCache.translate3d="(0px, 0px, 0px)",z=!0),z&&v.flushTransformCache(y)}}j.display!==d&&"none"!==j.display&&(t.State.calls[f][2].display=!1),j.visibility!==d&&"hidden"!==j.visibility&&(t.State.calls[f][2].visibility=!1),j.progress&&j.progress.call(h[1],h[1],r,Math.max(0,n+j.duration-b),n,q),1===r&&l(f)}}t.State.isTicking&&x(k)}function l(a,b){if(!t.State.calls[a])return!1;for(var c=t.State.calls[a][0],e=t.State.calls[a][1],f=t.State.calls[a][2],h=t.State.calls[a][4],i=!1,j=0,k=c.length;k>j;j++){var l=c[j].element;if(b||f.loop||("none"===f.display&&v.setPropertyValue(l,"display",f.display),"hidden"===f.visibility&&v.setPropertyValue(l,"visibility",f.visibility)),f.loop!==!0&&(m.queue(l)[1]===d||!/\.velocityQueueEntryFlag/i.test(m.queue(l)[1]))&&g(l)){g(l).isAnimating=!1,g(l).rootPropertyValueCache={};var n=!1;m.each(v.Lists.transforms3D,function(a,b){var c=/^scale/.test(b)?1:0,e=g(l).transformCache[b];g(l).transformCache[b]!==d&&new RegExp("^\\("+c+"[^.]").test(e)&&(n=!0,delete g(l).transformCache[b])}),f.mobileHA&&(n=!0,delete g(l).transformCache.translate3d),n&&v.flushTransformCache(l),v.Values.removeClass(l,"velocity-animating")}if(!b&&f.complete&&!f.loop&&j===k-1)try{f.complete.call(e,e)}catch(o){setTimeout(function(){throw o},1)}h&&f.loop!==!0&&h(e),g(l)&&f.loop===!0&&!b&&(m.each(g(l).tweensContainer,function(a,b){/^rotate/.test(a)&&360===parseFloat(b.endValue)&&(b.endValue=0,b.startValue=360),/^backgroundPosition/.test(a)&&100===parseFloat(b.endValue)&&"%"===b.unitType&&(b.endValue=0,b.startValue=100)}),t(l,"reverse",{loop:!0,delay:f.delay})),f.queue!==!1&&m.dequeue(l,f.queue)}t.State.calls[a]=!1;for(var p=0,q=t.State.calls.length;q>p;p++)if(t.State.calls[p]!==!1){i=!0;break}i===!1&&(t.State.isTicking=!1,delete t.State.calls,t.State.calls=[])}var m,n=function(){if(c.documentMode)return c.documentMode;for(var a=7;a>4;a--){var b=c.createElement("div");if(b.innerHTML="<!--[if IE "+a+"]><span></span><![endif]-->",b.getElementsByTagName("span").length)return b=null,a}return d}(),o=function(){var a=0;return b.webkitRequestAnimationFrame||b.mozRequestAnimationFrame||function(b){var c,d=(new Date).getTime();return c=Math.max(0,16-(d-a)),a=d+c,setTimeout(function(){b(d+c)},c)}}(),p={isString:function(a){return"string"==typeof a},isArray:Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},isFunction:function(a){return"[object Function]"===Object.prototype.toString.call(a)},isNode:function(a){return a&&a.nodeType},isNodeList:function(a){return"object"==typeof a&&/^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(a))&&a.length!==d&&(0===a.length||"object"==typeof a[0]&&a[0].nodeType>0)},isWrapped:function(a){return a&&(a.jquery||b.Zepto&&b.Zepto.zepto.isZ(a))},isSVG:function(a){return b.SVGElement&&a instanceof b.SVGElement},isEmptyObject:function(a){for(var b in a)return!1;return!0}},q=!1;if(a.fn&&a.fn.jquery?(m=a,q=!0):m=b.Velocity.Utilities,8>=n&&!q)throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");if(7>=n)return void(jQuery.fn.velocity=jQuery.fn.animate);var r=400,s="swing",t={State:{isMobile:/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),isAndroid:/Android/i.test(navigator.userAgent),isGingerbread:/Android 2\.3\.[3-7]/i.test(navigator.userAgent),isChrome:b.chrome,isFirefox:/Firefox/i.test(navigator.userAgent),prefixElement:c.createElement("div"),prefixMatches:{},scrollAnchor:null,scrollPropertyLeft:null,scrollPropertyTop:null,isTicking:!1,calls:[]},CSS:{},Utilities:m,Redirects:{},Easings:{},Promise:b.Promise,defaults:{queue:"",duration:r,easing:s,begin:d,complete:d,progress:d,display:d,visibility:d,loop:!1,delay:!1,mobileHA:!0,_cacheValues:!0},init:function(a){m.data(a,"velocity",{isSVG:p.isSVG(a),isAnimating:!1,computedStyle:null,tweensContainer:null,rootPropertyValueCache:{},transformCache:{}})},hook:null,mock:!1,version:{major:1,minor:2,patch:2},debug:!1};b.pageYOffset!==d?(t.State.scrollAnchor=b,t.State.scrollPropertyLeft="pageXOffset",t.State.scrollPropertyTop="pageYOffset"):(t.State.scrollAnchor=c.documentElement||c.body.parentNode||c.body,t.State.scrollPropertyLeft="scrollLeft",t.State.scrollPropertyTop="scrollTop");var u=function(){function a(a){return-a.tension*a.x-a.friction*a.v}function b(b,c,d){var e={x:b.x+d.dx*c,v:b.v+d.dv*c,tension:b.tension,friction:b.friction};return{dx:e.v,dv:a(e)}}function c(c,d){var e={dx:c.v,dv:a(c)},f=b(c,.5*d,e),g=b(c,.5*d,f),h=b(c,d,g),i=1/6*(e.dx+2*(f.dx+g.dx)+h.dx),j=1/6*(e.dv+2*(f.dv+g.dv)+h.dv);return c.x=c.x+i*d,c.v=c.v+j*d,c}return function d(a,b,e){var f,g,h,i={x:-1,v:0,tension:null,friction:null},j=[0],k=0,l=1e-4,m=.016;for(a=parseFloat(a)||500,b=parseFloat(b)||20,e=e||null,i.tension=a,i.friction=b,f=null!==e,f?(k=d(a,b),g=k/e*m):g=m;;)if(h=c(h||i,g),j.push(1+h.x),k+=16,!(Math.abs(h.x)>l&&Math.abs(h.v)>l))break;return f?function(a){return j[a*(j.length-1)|0]}:k}}();t.Easings={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},spring:function(a){return 1-Math.cos(4.5*a*Math.PI)*Math.exp(6*-a)}},m.each([["ease",[.25,.1,.25,1]],["ease-in",[.42,0,1,1]],["ease-out",[0,0,.58,1]],["ease-in-out",[.42,0,.58,1]],["easeInSine",[.47,0,.745,.715]],["easeOutSine",[.39,.575,.565,1]],["easeInOutSine",[.445,.05,.55,.95]],["easeInQuad",[.55,.085,.68,.53]],["easeOutQuad",[.25,.46,.45,.94]],["easeInOutQuad",[.455,.03,.515,.955]],["easeInCubic",[.55,.055,.675,.19]],["easeOutCubic",[.215,.61,.355,1]],["easeInOutCubic",[.645,.045,.355,1]],["easeInQuart",[.895,.03,.685,.22]],["easeOutQuart",[.165,.84,.44,1]],["easeInOutQuart",[.77,0,.175,1]],["easeInQuint",[.755,.05,.855,.06]],["easeOutQuint",[.23,1,.32,1]],["easeInOutQuint",[.86,0,.07,1]],["easeInExpo",[.95,.05,.795,.035]],["easeOutExpo",[.19,1,.22,1]],["easeInOutExpo",[1,0,0,1]],["easeInCirc",[.6,.04,.98,.335]],["easeOutCirc",[.075,.82,.165,1]],["easeInOutCirc",[.785,.135,.15,.86]]],function(a,b){t.Easings[b[0]]=i.apply(null,b[1])});var v=t.CSS={RegEx:{isHex:/^#([A-f\d]{3}){1,2}$/i,valueUnwrap:/^[A-z]+\((.*)\)$/i,wrappedValueAlreadyExtracted:/[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,valueSplit:/([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi},Lists:{colors:["fill","stroke","stopColor","color","backgroundColor","borderColor","borderTopColor","borderRightColor","borderBottomColor","borderLeftColor","outlineColor"],transformsBase:["translateX","translateY","scale","scaleX","scaleY","skewX","skewY","rotateZ"],transforms3D:["transformPerspective","translateZ","scaleZ","rotateX","rotateY"]},Hooks:{templates:{textShadow:["Color X Y Blur","black 0px 0px 0px"],boxShadow:["Color X Y Blur Spread","black 0px 0px 0px 0px"],clip:["Top Right Bottom Left","0px 0px 0px 0px"],backgroundPosition:["X Y","0% 0%"],transformOrigin:["X Y Z","50% 50% 0px"],perspectiveOrigin:["X Y","50% 50%"]},registered:{},register:function(){for(var a=0;a<v.Lists.colors.length;a++){var b="color"===v.Lists.colors[a]?"0 0 0 1":"255 255 255 1";v.Hooks.templates[v.Lists.colors[a]]=["Red Green Blue Alpha",b]}var c,d,e;if(n)for(c in v.Hooks.templates){d=v.Hooks.templates[c],e=d[0].split(" ");var f=d[1].match(v.RegEx.valueSplit);"Color"===e[0]&&(e.push(e.shift()),f.push(f.shift()),v.Hooks.templates[c]=[e.join(" "),f.join(" ")])}for(c in v.Hooks.templates){d=v.Hooks.templates[c],e=d[0].split(" ");for(var a in e){var g=c+e[a],h=a;v.Hooks.registered[g]=[c,h]}}},getRoot:function(a){var b=v.Hooks.registered[a];return b?b[0]:a},cleanRootPropertyValue:function(a,b){return v.RegEx.valueUnwrap.test(b)&&(b=b.match(v.RegEx.valueUnwrap)[1]),v.Values.isCSSNullValue(b)&&(b=v.Hooks.templates[a][1]),b},extractValue:function(a,b){var c=v.Hooks.registered[a];if(c){var d=c[0],e=c[1];return b=v.Hooks.cleanRootPropertyValue(d,b),b.toString().match(v.RegEx.valueSplit)[e]}return b},injectValue:function(a,b,c){var d=v.Hooks.registered[a];if(d){var e,f,g=d[0],h=d[1];return c=v.Hooks.cleanRootPropertyValue(g,c),e=c.toString().match(v.RegEx.valueSplit),e[h]=b,f=e.join(" ")}return c}},Normalizations:{registered:{clip:function(a,b,c){switch(a){case"name":return"clip";case"extract":var d;return v.RegEx.wrappedValueAlreadyExtracted.test(c)?d=c:(d=c.toString().match(v.RegEx.valueUnwrap),d=d?d[1].replace(/,(\s+)?/g," "):c),d;case"inject":return"rect("+c+")"}},blur:function(a,b,c){switch(a){case"name":return t.State.isFirefox?"filter":"-webkit-filter";case"extract":var d=parseFloat(c);if(!d&&0!==d){var e=c.toString().match(/blur\(([0-9]+[A-z]+)\)/i);d=e?e[1]:0}return d;case"inject":return parseFloat(c)?"blur("+c+")":"none"}},opacity:function(a,b,c){if(8>=n)switch(a){case"name":return"filter";case"extract":var d=c.toString().match(/alpha\(opacity=(.*)\)/i);return c=d?d[1]/100:1;case"inject":return b.style.zoom=1,parseFloat(c)>=1?"":"alpha(opacity="+parseInt(100*parseFloat(c),10)+")"}else switch(a){case"name":return"opacity";case"extract":return c;case"inject":return c}}},register:function(){9>=n||t.State.isGingerbread||(v.Lists.transformsBase=v.Lists.transformsBase.concat(v.Lists.transforms3D));for(var a=0;a<v.Lists.transformsBase.length;a++)!function(){var b=v.Lists.transformsBase[a];v.Normalizations.registered[b]=function(a,c,e){switch(a){case"name":return"transform";case"extract":return g(c)===d||g(c).transformCache[b]===d?/^scale/i.test(b)?1:0:g(c).transformCache[b].replace(/[()]/g,"");case"inject":var f=!1;switch(b.substr(0,b.length-1)){case"translate":f=!/(%|px|em|rem|vw|vh|\d)$/i.test(e);break;case"scal":case"scale":t.State.isAndroid&&g(c).transformCache[b]===d&&1>e&&(e=1),f=!/(\d)$/i.test(e);break;case"skew":f=!/(deg|\d)$/i.test(e);break;case"rotate":f=!/(deg|\d)$/i.test(e)}return f||(g(c).transformCache[b]="("+e+")"),g(c).transformCache[b]}}}();for(var a=0;a<v.Lists.colors.length;a++)!function(){var b=v.Lists.colors[a];v.Normalizations.registered[b]=function(a,c,e){switch(a){case"name":return b;case"extract":var f;if(v.RegEx.wrappedValueAlreadyExtracted.test(e))f=e;else{var g,h={black:"rgb(0, 0, 0)",blue:"rgb(0, 0, 255)",gray:"rgb(128, 128, 128)",green:"rgb(0, 128, 0)",red:"rgb(255, 0, 0)",white:"rgb(255, 255, 255)"};/^[A-z]+$/i.test(e)?g=h[e]!==d?h[e]:h.black:v.RegEx.isHex.test(e)?g="rgb("+v.Values.hexToRgb(e).join(" ")+")":/^rgba?\(/i.test(e)||(g=h.black),f=(g||e).toString().match(v.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g," ")}return 8>=n||3!==f.split(" ").length||(f+=" 1"),f;case"inject":return 8>=n?4===e.split(" ").length&&(e=e.split(/\s+/).slice(0,3).join(" ")):3===e.split(" ").length&&(e+=" 1"),(8>=n?"rgb":"rgba")+"("+e.replace(/\s+/g,",").replace(/\.(\d)+(?=,)/g,"")+")"}}}()}},Names:{camelCase:function(a){return a.replace(/-(\w)/g,function(a,b){return b.toUpperCase()})},SVGAttribute:function(a){var b="width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";return(n||t.State.isAndroid&&!t.State.isChrome)&&(b+="|transform"),new RegExp("^("+b+")$","i").test(a)},prefixCheck:function(a){if(t.State.prefixMatches[a])return[t.State.prefixMatches[a],!0];for(var b=["","Webkit","Moz","ms","O"],c=0,d=b.length;d>c;c++){var e;if(e=0===c?a:b[c]+a.replace(/^\w/,function(a){return a.toUpperCase()}),p.isString(t.State.prefixElement.style[e]))return t.State.prefixMatches[a]=e,[e,!0]}return[a,!1]}},Values:{hexToRgb:function(a){var b,c=/^#?([a-f\d])([a-f\d])([a-f\d])$/i,d=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;return a=a.replace(c,function(a,b,c,d){return b+b+c+c+d+d}),b=d.exec(a),b?[parseInt(b[1],16),parseInt(b[2],16),parseInt(b[3],16)]:[0,0,0]},isCSSNullValue:function(a){return 0==a||/^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(a)},getUnitType:function(a){return/^(rotate|skew)/i.test(a)?"deg":/(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(a)?"":"px"},getDisplayType:function(a){var b=a&&a.tagName.toString().toLowerCase();return/^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(b)?"inline":/^(li)$/i.test(b)?"list-item":/^(tr)$/i.test(b)?"table-row":/^(table)$/i.test(b)?"table":/^(tbody)$/i.test(b)?"table-row-group":"block"},addClass:function(a,b){a.classList?a.classList.add(b):a.className+=(a.className.length?" ":"")+b},removeClass:function(a,b){a.classList?a.classList.remove(b):a.className=a.className.toString().replace(new RegExp("(^|\\s)"+b.split(" ").join("|")+"(\\s|$)","gi")," ")}},getPropertyValue:function(a,c,e,f){function h(a,c){function e(){j&&v.setPropertyValue(a,"display","none")}var i=0;if(8>=n)i=m.css(a,c);else{var j=!1;if(/^(width|height)$/.test(c)&&0===v.getPropertyValue(a,"display")&&(j=!0,v.setPropertyValue(a,"display",v.Values.getDisplayType(a))),!f){if("height"===c&&"border-box"!==v.getPropertyValue(a,"boxSizing").toString().toLowerCase()){var k=a.offsetHeight-(parseFloat(v.getPropertyValue(a,"borderTopWidth"))||0)-(parseFloat(v.getPropertyValue(a,"borderBottomWidth"))||0)-(parseFloat(v.getPropertyValue(a,"paddingTop"))||0)-(parseFloat(v.getPropertyValue(a,"paddingBottom"))||0);return e(),k}if("width"===c&&"border-box"!==v.getPropertyValue(a,"boxSizing").toString().toLowerCase()){var l=a.offsetWidth-(parseFloat(v.getPropertyValue(a,"borderLeftWidth"))||0)-(parseFloat(v.getPropertyValue(a,"borderRightWidth"))||0)-(parseFloat(v.getPropertyValue(a,"paddingLeft"))||0)-(parseFloat(v.getPropertyValue(a,"paddingRight"))||0);return e(),l}}var o;o=g(a)===d?b.getComputedStyle(a,null):g(a).computedStyle?g(a).computedStyle:g(a).computedStyle=b.getComputedStyle(a,null),"borderColor"===c&&(c="borderTopColor"),i=9===n&&"filter"===c?o.getPropertyValue(c):o[c],(""===i||null===i)&&(i=a.style[c]),e()}if("auto"===i&&/^(top|right|bottom|left)$/i.test(c)){var p=h(a,"position");("fixed"===p||"absolute"===p&&/top|left/i.test(c))&&(i=m(a).position()[c]+"px")}return i}var i;if(v.Hooks.registered[c]){var j=c,k=v.Hooks.getRoot(j);e===d&&(e=v.getPropertyValue(a,v.Names.prefixCheck(k)[0])),v.Normalizations.registered[k]&&(e=v.Normalizations.registered[k]("extract",a,e)),i=v.Hooks.extractValue(j,e)}else if(v.Normalizations.registered[c]){var l,o;l=v.Normalizations.registered[c]("name",a),"transform"!==l&&(o=h(a,v.Names.prefixCheck(l)[0]),v.Values.isCSSNullValue(o)&&v.Hooks.templates[c]&&(o=v.Hooks.templates[c][1])),i=v.Normalizations.registered[c]("extract",a,o)}if(!/^[\d-]/.test(i))if(g(a)&&g(a).isSVG&&v.Names.SVGAttribute(c))if(/^(height|width)$/i.test(c))try{i=a.getBBox()[c]}catch(p){i=0}else i=a.getAttribute(c);else i=h(a,v.Names.prefixCheck(c)[0]);return v.Values.isCSSNullValue(i)&&(i=0),t.debug>=2&&console.log("Get "+c+": "+i),i},setPropertyValue:function(a,c,d,e,f){var h=c;if("scroll"===c)f.container?f.container["scroll"+f.direction]=d:"Left"===f.direction?b.scrollTo(d,f.alternateValue):b.scrollTo(f.alternateValue,d);else if(v.Normalizations.registered[c]&&"transform"===v.Normalizations.registered[c]("name",a))v.Normalizations.registered[c]("inject",a,d),h="transform",d=g(a).transformCache[c];else{if(v.Hooks.registered[c]){var i=c,j=v.Hooks.getRoot(c);e=e||v.getPropertyValue(a,j),d=v.Hooks.injectValue(i,d,e),c=j}if(v.Normalizations.registered[c]&&(d=v.Normalizations.registered[c]("inject",a,d),c=v.Normalizations.registered[c]("name",a)),h=v.Names.prefixCheck(c)[0],8>=n)try{a.style[h]=d}catch(k){t.debug&&console.log("Browser does not support ["+d+"] for ["+h+"]")}else g(a)&&g(a).isSVG&&v.Names.SVGAttribute(c)?a.setAttribute(c,d):a.style[h]=d;t.debug>=2&&console.log("Set "+c+" ("+h+"): "+d)}return[h,d]},flushTransformCache:function(a){function b(b){return parseFloat(v.getPropertyValue(a,b))}var c="";if((n||t.State.isAndroid&&!t.State.isChrome)&&g(a).isSVG){var d={translate:[b("translateX"),b("translateY")],skewX:[b("skewX")],skewY:[b("skewY")],scale:1!==b("scale")?[b("scale"),b("scale")]:[b("scaleX"),b("scaleY")],rotate:[b("rotateZ"),0,0]};m.each(g(a).transformCache,function(a){/^translate/i.test(a)?a="translate":/^scale/i.test(a)?a="scale":/^rotate/i.test(a)&&(a="rotate"),d[a]&&(c+=a+"("+d[a].join(" ")+") ",delete d[a])})}else{var e,f;m.each(g(a).transformCache,function(b){return e=g(a).transformCache[b],"transformPerspective"===b?(f=e,!0):(9===n&&"rotateZ"===b&&(b="rotate"),void(c+=b+e+" "))}),f&&(c="perspective"+f+" "+c)}v.setPropertyValue(a,"transform",c)}};v.Hooks.register(),v.Normalizations.register(),t.hook=function(a,b,c){var e=d;return a=f(a),m.each(a,function(a,f){if(g(f)===d&&t.init(f),c===d)e===d&&(e=t.CSS.getPropertyValue(f,b));else{var h=t.CSS.setPropertyValue(f,b,c);"transform"===h[0]&&t.CSS.flushTransformCache(f),e=h}}),e};var w=function(){function a(){return h?B.promise||null:i}function e(){function a(){function a(a,b){var c=d,e=d,g=d;return p.isArray(a)?(c=a[0],!p.isArray(a[1])&&/^[\d-]/.test(a[1])||p.isFunction(a[1])||v.RegEx.isHex.test(a[1])?g=a[1]:(p.isString(a[1])&&!v.RegEx.isHex.test(a[1])||p.isArray(a[1]))&&(e=b?a[1]:j(a[1],h.duration),a[2]!==d&&(g=a[2]))):c=a,b||(e=e||h.easing),p.isFunction(c)&&(c=c.call(f,y,x)),p.isFunction(g)&&(g=g.call(f,y,x)),[c||0,e,g]}function l(a,b){var c,d;return d=(b||"0").toString().toLowerCase().replace(/[%A-z]+$/,function(a){return c=a,""}),c||(c=v.Values.getUnitType(a)),[d,c]}function n(){var a={myParent:f.parentNode||c.body,position:v.getPropertyValue(f,"position"),fontSize:v.getPropertyValue(f,"fontSize")},d=a.position===I.lastPosition&&a.myParent===I.lastParent,e=a.fontSize===I.lastFontSize;I.lastParent=a.myParent,I.lastPosition=a.position,I.lastFontSize=a.fontSize;var h=100,i={};if(e&&d)i.emToPx=I.lastEmToPx,i.percentToPxWidth=I.lastPercentToPxWidth,i.percentToPxHeight=I.lastPercentToPxHeight;else{var j=g(f).isSVG?c.createElementNS("http://www.w3.org/2000/svg","rect"):c.createElement("div");t.init(j),a.myParent.appendChild(j),m.each(["overflow","overflowX","overflowY"],function(a,b){t.CSS.setPropertyValue(j,b,"hidden")}),t.CSS.setPropertyValue(j,"position",a.position),t.CSS.setPropertyValue(j,"fontSize",a.fontSize),t.CSS.setPropertyValue(j,"boxSizing","content-box"),m.each(["minWidth","maxWidth","width","minHeight","maxHeight","height"],function(a,b){t.CSS.setPropertyValue(j,b,h+"%")}),t.CSS.setPropertyValue(j,"paddingLeft",h+"em"),i.percentToPxWidth=I.lastPercentToPxWidth=(parseFloat(v.getPropertyValue(j,"width",null,!0))||1)/h,i.percentToPxHeight=I.lastPercentToPxHeight=(parseFloat(v.getPropertyValue(j,"height",null,!0))||1)/h,i.emToPx=I.lastEmToPx=(parseFloat(v.getPropertyValue(j,"paddingLeft"))||1)/h,a.myParent.removeChild(j)}return null===I.remToPx&&(I.remToPx=parseFloat(v.getPropertyValue(c.body,"fontSize"))||16),null===I.vwToPx&&(I.vwToPx=parseFloat(b.innerWidth)/100,I.vhToPx=parseFloat(b.innerHeight)/100),i.remToPx=I.remToPx,i.vwToPx=I.vwToPx,i.vhToPx=I.vhToPx,t.debug>=1&&console.log("Unit ratios: "+JSON.stringify(i),f),i}if(h.begin&&0===y)try{h.begin.call(o,o)}catch(r){setTimeout(function(){throw r},1)}if("scroll"===C){var u,w,z,A=/^x$/i.test(h.axis)?"Left":"Top",D=parseFloat(h.offset)||0;h.container?p.isWrapped(h.container)||p.isNode(h.container)?(h.container=h.container[0]||h.container,u=h.container["scroll"+A],z=u+m(f).position()[A.toLowerCase()]+D):h.container=null:(u=t.State.scrollAnchor[t.State["scrollProperty"+A]],w=t.State.scrollAnchor[t.State["scrollProperty"+("Left"===A?"Top":"Left")]],z=m(f).offset()[A.toLowerCase()]+D),i={scroll:{rootPropertyValue:!1,startValue:u,currentValue:u,endValue:z,unitType:"",easing:h.easing,scrollData:{container:h.container,direction:A,alternateValue:w}},element:f},t.debug&&console.log("tweensContainer (scroll): ",i.scroll,f)}else if("reverse"===C){if(!g(f).tweensContainer)return void m.dequeue(f,h.queue);"none"===g(f).opts.display&&(g(f).opts.display="auto"),"hidden"===g(f).opts.visibility&&(g(f).opts.visibility="visible"),g(f).opts.loop=!1,g(f).opts.begin=null,g(f).opts.complete=null,s.easing||delete h.easing,s.duration||delete h.duration,h=m.extend({},g(f).opts,h);var E=m.extend(!0,{},g(f).tweensContainer);for(var F in E)if("element"!==F){var G=E[F].startValue;E[F].startValue=E[F].currentValue=E[F].endValue,E[F].endValue=G,p.isEmptyObject(s)||(E[F].easing=h.easing),t.debug&&console.log("reverse tweensContainer ("+F+"): "+JSON.stringify(E[F]),f)}i=E}else if("start"===C){var E;g(f).tweensContainer&&g(f).isAnimating===!0&&(E=g(f).tweensContainer),m.each(q,function(b,c){if(RegExp("^"+v.Lists.colors.join("$|^")+"$").test(b)){var e=a(c,!0),f=e[0],g=e[1],h=e[2];if(v.RegEx.isHex.test(f)){for(var i=["Red","Green","Blue"],j=v.Values.hexToRgb(f),k=h?v.Values.hexToRgb(h):d,l=0;l<i.length;l++){var m=[j[l]];g&&m.push(g),k!==d&&m.push(k[l]),q[b+i[l]]=m}delete q[b]}}});for(var H in q){var K=a(q[H]),L=K[0],M=K[1],N=K[2];H=v.Names.camelCase(H);var O=v.Hooks.getRoot(H),P=!1;if(g(f).isSVG||"tween"===O||v.Names.prefixCheck(O)[1]!==!1||v.Normalizations.registered[O]!==d){(h.display!==d&&null!==h.display&&"none"!==h.display||h.visibility!==d&&"hidden"!==h.visibility)&&/opacity|filter/.test(H)&&!N&&0!==L&&(N=0),h._cacheValues&&E&&E[H]?(N===d&&(N=E[H].endValue+E[H].unitType),P=g(f).rootPropertyValueCache[O]):v.Hooks.registered[H]?N===d?(P=v.getPropertyValue(f,O),N=v.getPropertyValue(f,H,P)):P=v.Hooks.templates[O][1]:N===d&&(N=v.getPropertyValue(f,H));var Q,R,S,T=!1;if(Q=l(H,N),N=Q[0],S=Q[1],Q=l(H,L),L=Q[0].replace(/^([+-\/*])=/,function(a,b){return T=b,""}),R=Q[1],N=parseFloat(N)||0,L=parseFloat(L)||0,"%"===R&&(/^(fontSize|lineHeight)$/.test(H)?(L/=100,R="em"):/^scale/.test(H)?(L/=100,R=""):/(Red|Green|Blue)$/i.test(H)&&(L=L/100*255,R="")),/[\/*]/.test(T))R=S;else if(S!==R&&0!==N)if(0===L)R=S;else{e=e||n();var U=/margin|padding|left|right|width|text|word|letter/i.test(H)||/X$/.test(H)||"x"===H?"x":"y";switch(S){case"%":N*="x"===U?e.percentToPxWidth:e.percentToPxHeight;break;case"px":break;default:N*=e[S+"ToPx"]}switch(R){case"%":N*=1/("x"===U?e.percentToPxWidth:e.percentToPxHeight);break;case"px":break;default:N*=1/e[R+"ToPx"]}}switch(T){case"+":L=N+L;break;case"-":L=N-L;break;case"*":L=N*L;break;case"/":L=N/L}i[H]={rootPropertyValue:P,startValue:N,currentValue:N,endValue:L,unitType:R,easing:M},t.debug&&console.log("tweensContainer ("+H+"): "+JSON.stringify(i[H]),f)}else t.debug&&console.log("Skipping ["+O+"] due to a lack of browser support.")}i.element=f}i.element&&(v.Values.addClass(f,"velocity-animating"),J.push(i),""===h.queue&&(g(f).tweensContainer=i,g(f).opts=h),g(f).isAnimating=!0,y===x-1?(t.State.calls.push([J,o,h,null,B.resolver]),t.State.isTicking===!1&&(t.State.isTicking=!0,k())):y++)}var e,f=this,h=m.extend({},t.defaults,s),i={};switch(g(f)===d&&t.init(f),parseFloat(h.delay)&&h.queue!==!1&&m.queue(f,h.queue,function(a){t.velocityQueueEntryFlag=!0,g(f).delayTimer={setTimeout:setTimeout(a,parseFloat(h.delay)),next:a}}),h.duration.toString().toLowerCase()){case"fast":h.duration=200;break;case"normal":h.duration=r;break;case"slow":h.duration=600;break;default:h.duration=parseFloat(h.duration)||1}t.mock!==!1&&(t.mock===!0?h.duration=h.delay=1:(h.duration*=parseFloat(t.mock)||1,h.delay*=parseFloat(t.mock)||1)),h.easing=j(h.easing,h.duration),h.begin&&!p.isFunction(h.begin)&&(h.begin=null),h.progress&&!p.isFunction(h.progress)&&(h.progress=null),h.complete&&!p.isFunction(h.complete)&&(h.complete=null),h.display!==d&&null!==h.display&&(h.display=h.display.toString().toLowerCase(),"auto"===h.display&&(h.display=t.CSS.Values.getDisplayType(f))),h.visibility!==d&&null!==h.visibility&&(h.visibility=h.visibility.toString().toLowerCase()),h.mobileHA=h.mobileHA&&t.State.isMobile&&!t.State.isGingerbread,h.queue===!1?h.delay?setTimeout(a,h.delay):a():m.queue(f,h.queue,function(b,c){return c===!0?(B.promise&&B.resolver(o),!0):(t.velocityQueueEntryFlag=!0,void a(b))}),""!==h.queue&&"fx"!==h.queue||"inprogress"===m.queue(f)[0]||m.dequeue(f)}var h,i,n,o,q,s,u=arguments[0]&&(arguments[0].p||m.isPlainObject(arguments[0].properties)&&!arguments[0].properties.names||p.isString(arguments[0].properties));if(p.isWrapped(this)?(h=!1,n=0,o=this,i=this):(h=!0,n=1,o=u?arguments[0].elements||arguments[0].e:arguments[0]),o=f(o)){u?(q=arguments[0].properties||arguments[0].p,s=arguments[0].options||arguments[0].o):(q=arguments[n],s=arguments[n+1]);var x=o.length,y=0;if(!/^(stop|finish|finishAll)$/i.test(q)&&!m.isPlainObject(s)){var z=n+1;s={};for(var A=z;A<arguments.length;A++)p.isArray(arguments[A])||!/^(fast|normal|slow)$/i.test(arguments[A])&&!/^\d/.test(arguments[A])?p.isString(arguments[A])||p.isArray(arguments[A])?s.easing=arguments[A]:p.isFunction(arguments[A])&&(s.complete=arguments[A]):s.duration=arguments[A]}var B={promise:null,resolver:null,rejecter:null};h&&t.Promise&&(B.promise=new t.Promise(function(a,b){B.resolver=a,B.rejecter=b}));var C;switch(q){case"scroll":C="scroll";break;case"reverse":C="reverse";break;case"finish":case"finishAll":case"stop":m.each(o,function(a,b){g(b)&&g(b).delayTimer&&(clearTimeout(g(b).delayTimer.setTimeout),g(b).delayTimer.next&&g(b).delayTimer.next(),delete g(b).delayTimer),"finishAll"!==q||s!==!0&&!p.isString(s)||(m.each(m.queue(b,p.isString(s)?s:""),function(a,b){p.isFunction(b)&&b()}),m.queue(b,p.isString(s)?s:"",[]))});var D=[];return m.each(t.State.calls,function(a,b){b&&m.each(b[1],function(c,e){var f=s===d?"":s;return f===!0||b[2].queue===f||s===d&&b[2].queue===!1?void m.each(o,function(c,d){d===e&&((s===!0||p.isString(s))&&(m.each(m.queue(d,p.isString(s)?s:""),function(a,b){p.isFunction(b)&&b(null,!0)
	}),m.queue(d,p.isString(s)?s:"",[])),"stop"===q?(g(d)&&g(d).tweensContainer&&f!==!1&&m.each(g(d).tweensContainer,function(a,b){b.endValue=b.currentValue}),D.push(a)):("finish"===q||"finishAll"===q)&&(b[2].duration=1))}):!0})}),"stop"===q&&(m.each(D,function(a,b){l(b,!0)}),B.promise&&B.resolver(o)),a();default:if(!m.isPlainObject(q)||p.isEmptyObject(q)){if(p.isString(q)&&t.Redirects[q]){var E=m.extend({},s),F=E.duration,G=E.delay||0;return E.backwards===!0&&(o=m.extend(!0,[],o).reverse()),m.each(o,function(a,b){parseFloat(E.stagger)?E.delay=G+parseFloat(E.stagger)*a:p.isFunction(E.stagger)&&(E.delay=G+E.stagger.call(b,a,x)),E.drag&&(E.duration=parseFloat(F)||(/^(callout|transition)/.test(q)?1e3:r),E.duration=Math.max(E.duration*(E.backwards?1-a/x:(a+1)/x),.75*E.duration,200)),t.Redirects[q].call(b,b,E||{},a,x,o,B.promise?B:d)}),a()}var H="Velocity: First argument ("+q+") was not a property map, a known action, or a registered redirect. Aborting.";return B.promise?B.rejecter(new Error(H)):console.log(H),a()}C="start"}var I={lastParent:null,lastPosition:null,lastFontSize:null,lastPercentToPxWidth:null,lastPercentToPxHeight:null,lastEmToPx:null,remToPx:null,vwToPx:null,vhToPx:null},J=[];m.each(o,function(a,b){p.isNode(b)&&e.call(b)});var K,E=m.extend({},t.defaults,s);if(E.loop=parseInt(E.loop),K=2*E.loop-1,E.loop)for(var L=0;K>L;L++){var M={delay:E.delay,progress:E.progress};L===K-1&&(M.display=E.display,M.visibility=E.visibility,M.complete=E.complete),w(o,"reverse",M)}return a()}};t=m.extend(w,t),t.animate=w;var x=b.requestAnimationFrame||o;return t.State.isMobile||c.hidden===d||c.addEventListener("visibilitychange",function(){c.hidden?(x=function(a){return setTimeout(function(){a(!0)},16)},k()):x=b.requestAnimationFrame||o}),a.Velocity=t,a!==b&&(a.fn.velocity=w,a.fn.velocity.defaults=t.defaults),m.each(["Down","Up"],function(a,b){t.Redirects["slide"+b]=function(a,c,e,f,g,h){var i=m.extend({},c),j=i.begin,k=i.complete,l={height:"",marginTop:"",marginBottom:"",paddingTop:"",paddingBottom:""},n={};i.display===d&&(i.display="Down"===b?"inline"===t.CSS.Values.getDisplayType(a)?"inline-block":"block":"none"),i.begin=function(){j&&j.call(g,g);for(var c in l){n[c]=a.style[c];var d=t.CSS.getPropertyValue(a,c);l[c]="Down"===b?[d,0]:[0,d]}n.overflow=a.style.overflow,a.style.overflow="hidden"},i.complete=function(){for(var b in n)a.style[b]=n[b];k&&k.call(g,g),h&&h.resolver(g)},t(a,l,i)}}),m.each(["In","Out"],function(a,b){t.Redirects["fade"+b]=function(a,c,e,f,g,h){var i=m.extend({},c),j={opacity:"In"===b?1:0},k=i.complete;i.complete=e!==f-1?i.begin=null:function(){k&&k.call(g,g),h&&h.resolver(g)},i.display===d&&(i.display="In"===b?"auto":"none"),t(this,j,i)}}),t}(window.jQuery||window.Zepto||window,window,document)});


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var CdlUtils = global.CdlUtils;
	var extend = __webpack_require__(3);
	var invoke = __webpack_require__(4);
	var StrokeAnimator = __webpack_require__(5);
	var StrokeOutlineRenderer = __webpack_require__(8);
	var CharacterAnimationManager = __webpack_require__(9);
	var setSvgAttr = __webpack_require__(6);

	var CharacterAnimator = function(targetElm, options) {
	  this.options = options || {};
	  this.options.x = this.options.x || 0;
	  this.options.y = this.options.y || 0;
	  this.options.width = this.options.width || 400;
	  this.options.height = this.options.height || 400;

	  if (typeof targetElm === 'string') {
	    this.targetElm = document.getElementById(targetElm);
	  } else {
	    this.targetElm = targetElm;
	  }
	  if (!targetElm) {
	    throw new Error('Invalid element passed in to CharacterAnimator');
	  }
	  var targetNodeType = this.targetElm.nodeName.toUpperCase();
	  if (targetNodeType === 'SVG' || targetNodeType === 'G') {
	    this.svg = this.targetElm;
	  } else {
	    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	    this.targetElm.appendChild(this.svg);
	  }
	  this.reset();
	};

	CharacterAnimator.prototype.renderCharacter = function(charOrUnicode, options) {
	  this.reset();
	  var opts = extend(this.options, options);
	  return this.loadCharacter(charOrUnicode, opts).then(function() {
	    return this.render(opts);
	  }.bind(this));
	};

	CharacterAnimator.prototype.loadCharacter = function(charOrUnicode, options) {
	  return CdlUtils.fetchCdlChar(charOrUnicode, extend(this.options, options)).then(function(cdlChar) {
	    this.cdlChar = cdlChar;
	    return this;
	  }.bind(this));
	};

	CharacterAnimator.prototype.render = function(options) {
	  this.reset();
	  if (!this.cdlChar) return;
	  this.defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
	  this.svg.appendChild(this.defs);
	  var baseOptions = extend(this.options, options);
	  setSvgAttr(this.svg, 'width', baseOptions.width + baseOptions.x);
	  setSvgAttr(this.svg, 'height', baseOptions.height + baseOptions.y);
	  // same options, but also need to get the plain font pathstrings.
	  // we use the plain stroke pathstring as a mask while animating
	  var plainFontOptions = extend(baseOptions, {font: 'plain'});
	  this._ensureCdlChar();
	  return Promise.all([
	    CdlUtils.getSvgPathStrings(this.cdlChar, baseOptions),
	    CdlUtils.getSvgPathStrings(this.cdlChar, plainFontOptions)
	  ]).then(function(mirroredPathStrings) {
	    var pathStrings = mirroredPathStrings[0]; // these are the actual pathstrings we need to draw
	    var maskPathStrings = mirroredPathStrings[1]; // these are the plain font versions of those pathstrings used as a mask
	    // draw all outlines first so they're behind all the strokes
	    pathStrings.forEach(function(pathString) {
	      this.strokeOutlines.push(new StrokeOutlineRenderer(this.svg, pathString, baseOptions));
	    }.bind(this));
	    for (var i = 0; i < pathStrings.length; i++) {
	      this.strokes.push(new StrokeAnimator(this.svg, this.defs, pathStrings[i], maskPathStrings[i], baseOptions));
	    }
	  }.bind(this));
	};

	CharacterAnimator.prototype._ensureCdlChar = function() {
	  if (!this.cdlChar) {
	    throw new Error('You need to load a character before calling methods on a CharacterAnimator');
	  }
	};

	CharacterAnimator.prototype.animate = function(animationOptions) {
	  this.stopAnimation();

	  var options = extend(this.options, animationOptions);
	  this.animationManager = new CharacterAnimationManager(this.strokes, options);
	  this.animationManager.run();
	};

	CharacterAnimator.prototype.stopAnimation = function() {
	  if (this.animationManager) {
	    this.animationManager.stop();
	    this.animationManager = null;
	  }
	};

	CharacterAnimator.prototype.updateOptions = function(options) {
	  this.options = extend(this.options, options);
	  invoke(this.strokes, 'updateOptions', options);
	  invoke(this.strokeOutlines, 'updateOptions', options);
	  if (this.animationManager) {
	    this.animationManager.updateOptions(options);
	  }
	};

	CharacterAnimator.prototype.reset = function() {
	  this.stopAnimation();
	  if (this.strokes) {
	    invoke(this.strokes, 'remove');
	    invoke(this.strokeOutlines, 'remove');
	  }
	  if (this.defs) {
	    this.defs.remove();
	  }
	  this.strokes = [];
	  this.strokeOutlines = [];
	  this.isDrawn = false;
	};

	CharacterAnimator.prototype.setStrokeStyles = function(attrs) {
	  invoke(this.strokes, 'setStyles', attrs);
	};

	CharacterAnimator.prototype.tweenStrokeStyles = function(attrs, options) {
	  invoke(this.strokes, 'tweenStyles', attrs, options);
	};

	CharacterAnimator.prototype.setOutlineStyles = function(attrs) {
	  invoke(this.strokeOutlines, 'setStyles', attrs);
	};

	CharacterAnimator.prototype.tweenOutlineStyles = function(attrs, options) {
	  invoke(this.strokeOutlines, 'tweenStyles', attrs, options);
	};

	CharacterAnimator.prototype.hide = function() {
	  invoke(this.strokes, 'hide');
	};

	CharacterAnimator.prototype.show = function() {
	  return Promise.all(invoke(this.strokes, 'show'));
	};

	CharacterAnimator.prototype.hideOutline = function() {
	  invoke(this.strokeOutlines, 'hide');
	};

	CharacterAnimator.prototype.showOutline = function() {
	  return Promise.all(invoke(this.strokeOutlines, 'show'));
	};

	CharacterAnimator.prototype.remove = function() {
	  this.stopAnimation();
	  invoke(this.strokes, 'remove');
	  invoke(this.strokeOutlines, 'remove');
	  this.defs.remove();
	  // only delete the svg node if we created it
	  if (this.targetElm !== this.svg) {
	    this.svg.remove();
	  }
	};

	// --------- Static convenience methods --------

	CharacterAnimator.animateCharacter = function(targetElm, charOrUnicode, options) {
	  var animator = new CharacterAnimator(targetElm, options);
	  animator.renderCharacter(charOrUnicode).then(function() {
	    animator.animate();
	  });
	  return animator;
	};

	module.exports = CharacterAnimator;

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 3 */
/***/ function(module, exports) {

	// modified from https://gomakethings.com/vanilla-javascript-version-of-jquery-extend/

	var extend = function() {
	  // Variables
	  var extended = {};

	  // Merge the object into the extended object
	  var merge = function(obj) {
	    for (var prop in obj) {
	      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
	        // If property is an object, merge properties
	        if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
	          extended[prop] = extend(extended[prop], obj[prop]);
	        } else {
	          extended[prop] = obj[prop];
	        }
	      }
	    }
	  };

	  // Loop through each object and conduct a merge
	  for (var i = 0; i < arguments.length; i++) {
	    var obj = arguments[i] || {};
	    merge(obj);
	  }

	  return extended;
	};

	module.exports = extend;


/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = function(arr, methodName) {
	  var args = Array.prototype.slice.call(arguments, 2);
	  return arr.map(function(elm) {
	    return elm[methodName].apply(elm, args);
	  });
	};


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var velocity = __webpack_require__(1);
	var extend = __webpack_require__(3);
	var setSvgAttr = __webpack_require__(6);
	var setSvgStyles = __webpack_require__(7);


	var maskIdCounter = 0;

	var StrokeAnimator = function(svg, defs, pathString, maskPathString, options) {
	  this.svg = svg;
	  this.defs = defs;
	  this.pathString = pathString;
	  this.maskPathString = maskPathString;
	  this.options = options;

	  this._render();
	};

	StrokeAnimator.prototype._render = function() {
	  this.maskPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  // mask needs to be wide enough to cover the whole stroke, but not too wide or weird stuff might happen
	  // tweak this value to find something that works well
	  var maskWidth = (this.options.width + this.options.height) / 10;
	  // experiment with this to make sure it fully covers all strokes still
	  var extensionAmount = (this.options.width + this.options.height) / 20;
	  var maskId = 'stroke-mask-' + maskIdCounter;
	  maskIdCounter += 1;
	  setSvgAttr(this.maskPath, 'd', this.maskPathString);
	  setSvgAttr(this.maskPath, 'fill', 'none');
	  setSvgAttr(this.maskPath, 'stroke', '#FFF');
	  setSvgAttr(this.maskPath, 'stroke-width', maskWidth);
	  extendPath(this.maskPath, extensionAmount);
	  // determine length after extending the path
	  this.strokeLength = this.maskPath.getTotalLength();
	  setSvgAttr(this.maskPath, 'stroke-dasharray', '' + this.strokeLength + ',' + this.strokeLength);
	  // animating the stroke is accomplished by animating the stroke-dashoffset
	  setSvgStyles(this.maskPath, { 'stroke-dashoffset': this.strokeLength });

	  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  // there's a bug in some browsers rendering vertical paths with masks. This is a trick to fix that bug.
	  // http://stackoverflow.com/questions/19708943/svg-straight-path-with-clip-path-not-visible-in-chrome
	  var modifiedPathString = 'M -10,-10 ' + this.pathString;
	  setSvgAttr(this.path, 'd', modifiedPathString);
	  setSvgAttr(this.path, 'mask', 'url(#' + maskId + ')');
	  var defaultStrokeStyles = this.options.font === 'plain' ? { fill: 'none', stroke: '#000' } : { fill: '#000' };
	  setSvgStyles(this.path, extend(defaultStrokeStyles, this.options.strokeStyles));

	  this.mask = document.createElementNS('http://www.w3.org/2000/svg', 'mask');
	  setSvgAttr(this.mask, 'id', maskId);

	  this.defs.appendChild(this.mask);
	  this.mask.appendChild(this.maskPath);
	  this.svg.appendChild(this.path);
	};

	StrokeAnimator.prototype.updateOptions = function(options) {
	  this.options = extend(this.options, options);
	};

	StrokeAnimator.prototype.show = function() {
	  setSvgStyles(this.maskPath, { 'stroke-dashoffset': 0 });
	};

	StrokeAnimator.prototype.hide = function() {
	  this.stopStrokeAnimation();
	  setSvgStyles(this.maskPath, { 'stroke-dashoffset': this.maskPath.getTotalLength() });
	};

	StrokeAnimator.prototype.animate = function(animationOptions) {
	  var options = extend(this.options, animationOptions);
	  var strokeVelocity = options.strokeAnimationVelocity || 1;
	  var duration = 2000 * this.strokeLength / (this.options.width + this.options.height) / strokeVelocity;
	  this.hide();
	  var animationPromises = [];
	  if (options.animateStyles && options.animateStyles.start) {
	    setSvgStyles(this.path, options.animateStyles.start);
	  }
	  animationPromises.push(velocity(this.maskPath, { 'stroke-dashoffset': 0 }, { duration: duration }));
	  if (options.animateStyles && options.animateStyles.end) {
	    animationPromises.push(velocity(this.path, options.animateStyles.end, { duration: duration }));
	  }
	  return Promise.all(animationPromises);
	};

	StrokeAnimator.prototype.setStyles = function(attrs) {
	  setSvgStyles(this.path, attrs);
	};

	StrokeAnimator.prototype.tweenStyles = function(attrs, options) {
	  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000});
	};

	StrokeAnimator.prototype.stopStrokeAnimation = function() {
	  velocity(this.maskPath, 'stop', true);
	};

	StrokeAnimator.prototype.stop = function() {
	  velocity(this.path, 'stop', true);
	  velocity(this.maskPath, 'stop', true);
	};

	StrokeAnimator.prototype.remove = function() {
	  this.maskPath.remove();
	  this.mask.remove();
	  this.path.remove();
	};

	StrokeAnimator.prototype.clone = function() {
	  return new StrokeAnimator(this.svg, this.defs, this.pathString, this.maskPathString, this.options);
	};

	// extend a non-closed SVG path
	var extendPath = function(path, extensionAmount) {
	  var pathLength = path.getTotalLength();
	  var delta = pathLength / 100; // approximate a tangent line by looking at points spaced very close together

	  var newStartPoint = extrapolatePoint(path.getPointAtLength(delta), path.getPointAtLength(0), extensionAmount);
	  var newEndPoint = extrapolatePoint(path.getPointAtLength(pathLength - delta), path.getPointAtLength(pathLength), extensionAmount);
	  var pathString = path.getAttribute('d');
	  var newPathStart = 'M' + newStartPoint.x + ' ' + newStartPoint.y + ' L';
	  var newPathEnd = ' L' + newEndPoint.x +  ' ' + newEndPoint.y;
	  var newPathString = pathString.replace(/^M/, newPathStart) + newPathEnd;
	  setSvgAttr(path, 'd', newPathString);
	};

	// given 2 points find another point <distance> pixels away on the same line
	var extrapolatePoint = function(pointA, pointB, distance) {
	  if (pointA.x === pointB.x) {
	    var sign = pointB.y > pointA.y ? 1 : -1;
	    return {
	      x: pointA.x,
	      y: pointB.y + sign * distance
	    };
	  }
	  var slope = (pointB.y - pointA.y) / (pointB.x - pointA.x);
	  var intercept = pointA.y - pointA.x * slope;
	  var distBA = Math.sqrt(Math.pow(pointB.y - pointA.y, 2) + Math.pow(pointB.x - pointA.x, 2));
	  var xDistBA = pointB.x - pointA.x;
	  var xDist = xDistBA * distance / distBA;

	  var x = pointB.x + xDist;
	  var y = slope * x + intercept;
	  return {x: x, y: y};
	};

	module.exports = StrokeAnimator;


/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = function(elm, attrName, attrValue) {
	  elm.setAttributeNS(null, attrName, attrValue);
	};


/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = function(elm, styles) {
	  for (var styleName in styles) {
	    if (Object.prototype.hasOwnProperty.call(styles, styleName)) {
	      elm.style[styleName] = styles[styleName];
	    }
	  }
	};


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var velocity = __webpack_require__(1);
	var extend = __webpack_require__(3);
	var setSvgAttr = __webpack_require__(6);
	var setSvgStyles = __webpack_require__(7);

	var StrokeOutlineRenderer = function(svg, pathString, options) {
	  this.svg = svg;
	  this.pathString = pathString;
	  this.options = options;

	  this._render();
	};

	StrokeOutlineRenderer.prototype._render = function() {
	  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  setSvgAttr(this.path, 'd', this.pathString);
	  var defaultStyles = this.options.font === 'plain' ? { stroke: '#DDD', fill: 'none' } : { fill: '#DDD' };
	  var styles = extend(defaultStyles, this.options.strokeOutlineStyles);
	  setSvgStyles(this.path, styles);
	  if (this.options.showOutline) {
	    this.show();
	  } else {
	    this.hide();
	  }
	  this.svg.appendChild(this.path);
	};

	StrokeOutlineRenderer.prototype.updateOptions = function(options) {
	  this.options = extend(this.options, options);
	};

	StrokeOutlineRenderer.prototype.show = function() {
	  this.setStyles({ 'visibility': 'visible' });
	};

	StrokeOutlineRenderer.prototype.hide = function() {
	  this.setStyles({ 'visibility': 'hidden' });
	};

	StrokeOutlineRenderer.prototype.setStyles = function(attrs) {
	  setSvgStyles(this.path, attrs);
	};

	StrokeOutlineRenderer.prototype.tweenStyles = function(attrs, options) {
	  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000 });
	};

	StrokeOutlineRenderer.prototype.remove = function() {
	  this.path.remove();
	};

	StrokeOutlineRenderer.prototype.clone = function() {
	  return new StrokeOutlineRenderer(this.svg, this.pathString, this.options);
	};

	module.exports = StrokeOutlineRenderer;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var invoke = __webpack_require__(4);
	var callIfExists = __webpack_require__(10);
	var extend = __webpack_require__(3);

	var CharacterAnimationManager = function(strokes, options) {
	  this.strokes = strokes;
	  this.options = options;
	  this.isRunning = false;
	};

	CharacterAnimationManager.prototype.run = function() {
	  this.strokeNum = this.options.beginAnimationStrokeNum || 0;
	  this._runAnimation();
	};

	CharacterAnimationManager.prototype.stop = function() {
	  this.isRunning = false;
	  if (this.animationTimer) {
	    clearTimeout(this.animationTimer);
	  }
	  invoke(this.strokes, 'stopStrokeAnimation');
	};

	CharacterAnimationManager.prototype.updateOptions = function(options) {
	  this.options = extend(this.options, options);
	};

	CharacterAnimationManager.prototype._runAnimation = function() {
	  this.isRunning = true;
	  invoke(this.strokes.slice(0, this.strokeNum), 'show');
	  invoke(this.strokes.slice(this.strokeNum), 'hide');
	  this._animateNextStroke();
	};

	CharacterAnimationManager.prototype._animateNextStroke = function() {
	  if (!this.isRunning) return;
	  callIfExists(this.options.onBeginStrokeAnimation, { strokeNum: this.strokeNum });

	  this.strokes[this.strokeNum].animate(this.options).then(function() {
	    callIfExists(this.options.onFinishStrokeAnimation, { strokeNum: this.strokeNum });
	    this.strokeNum += 1;
	    var isFinished = this.strokeNum >= this.strokes.length;
	    if (isFinished && this.options.loopAnimation) {
	      this.strokeNum = 0;
	      this.animationTimer = setTimeout(this._runAnimation.bind(this), this.options.delayBetweenLoops || 2000);
	    } else if (isFinished) {
	      this.isRunning = false;
	      callIfExists(this.options.onFinishAnimation);
	    } else {
	      this.animationTimer = setTimeout(this._animateNextStroke.bind(this), this.options.delayBetweenStrokes || 1000);
	    }
	  }.bind(this));
	};

	module.exports = CharacterAnimationManager;


/***/ },
/* 10 */
/***/ function(module, exports) {

	// taken from compiled hanzi-writer code
	module.exports = function(callback) {
	  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	    args[_key - 1] = arguments[_key];
	  }

	  if (callback) callback.apply(this, args);
	};


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var CharacterAnimator = __webpack_require__(2);
	var StrokeMatcher = __webpack_require__(12);
	var UserStrokeRenderer = __webpack_require__(17);
	var callIfExists = __webpack_require__(10);
	var invoke = __webpack_require__(4);
	var extend = __webpack_require__(3);

	var CharacterQuizzer = function(targetElm, options) {
	  CharacterAnimator.call(this, targetElm, options);
	  this._setupListeners();
	};

	// convenience method for calling superclass methods
	var superclass = function() {
	  return Object.getPrototypeOf(CharacterQuizzer.prototype);
	};

	// extends animator
	CharacterQuizzer.prototype = Object.create(CharacterAnimator.prototype);
	CharacterQuizzer.prototype.constructor = CharacterQuizzer;

	CharacterQuizzer.prototype._setupListeners = function() {
	  /* The 'svg' node may contain a 'g' node. If so, make sure to add the event listeners
	     to the 'svg' node, which is the parent of the 'g' node.
	     Typically, this.svg.nodeName = 'g' and this.svg.parentNode.nodeName = 'svg'. */
	  var svgNode = this.svg.nodeName.toUpperCase() === 'G' ? this.svg.parentNode : this.svg;
	  svgNode.addEventListener('mousedown', function(evt) {
	    if (!this._isActive) return;
	    evt.preventDefault();
	    this._startUserStroke(this._getMousePoint(evt));
	  }.bind(this));
	  svgNode.addEventListener('touchstart', function(evt) {
	    if (!this._isActive) return;
	    evt.preventDefault();
	    this._startUserStroke(this._getTouchPoint(evt));
	  }.bind(this));
	  svgNode.addEventListener('mousemove', function(evt) {
	    if (!this._isActive) return;
	    evt.preventDefault();
	    this._continueUserStroke(this._getMousePoint(evt));
	  }.bind(this));
	  svgNode.addEventListener('touchmove', function(evt) {
	    if (!this._isActive) return;
	    evt.preventDefault();
	    this._continueUserStroke(this._getTouchPoint(evt));
	  }.bind(this));

	  // TODO: fix
	  global.document.addEventListener('mouseup', this._endUserStroke.bind(this));
	  global.document.addEventListener('touchend', this._endUserStroke.bind(this));
	};

	CharacterQuizzer.prototype._getMousePoint = function(evt) {
	  var box = this.svg.getBoundingClientRect();
	  return {
	    x: evt.clientX - box.left,
	    y: evt.clientY - box.top
	  };
	};

	CharacterQuizzer.prototype._getTouchPoint = function(evt) {
	  var box = this.svg.getBoundingClientRect();
	  return {
	    x: evt.touches[0].clientX - box.left,
	    y: evt.touches[0].clientY - box.top
	  };
	};

	CharacterQuizzer.prototype.render = function(options) {
	  var baseOptions = extend(this.options, options);
	  this._strokeMatcher = new StrokeMatcher(baseOptions.width, baseOptions.height);
	  return superclass().render.call(this, options).then(function() {
	    this.highlightStrokes = invoke(this.strokes, 'clone');
	    var highlightStrokeStyles = extend({ fill: '#AAF' }, this.options.highlightStrokeStyles);
	    invoke(this.highlightStrokes, 'hide');
	    invoke(this.highlightStrokes, 'setStyles', highlightStrokeStyles);
	  }.bind(this));
	};

	CharacterQuizzer.prototype.startQuiz = function(options) {
	  this.options = extend(this.options, options);
	  this.resetQuiz();
	  this._isActive = true;
	};

	CharacterQuizzer.prototype.resetQuiz = function() {
	  this._currentStrokeIndex = 0;
	  this._numRecentMistakes = 0;
	  this._totalMistakes = 0;

	  if (this._userStrokeRenderer) {
	    this._userStrokeRenderer.remove();
	  }
	  invoke(this.strokes, 'hide');
	  this._userStrokeRenderer = null;
	  this._userStrokePoints = null;
	};

	CharacterQuizzer.prototype.animate = function(options) {
	  invoke(this.highlightStrokes, 'hide');
	  return superclass().animate(options);
	};

	CharacterQuizzer.prototype.reset = function() {
	  superclass().reset.call(this);
	  this.resetQuiz();
	  invoke(this.highlightStrokes || [], 'remove');
	  this.highlightStrokes = [];
	};

	CharacterQuizzer.prototype._startUserStroke = function(point) {
	  if (!this._isActive) return null;
	  if (this._userStrokePoints) return this._endUserStroke();

	  // Reverse svg scale to capture correct coordinates
	  this._reverseSvgScale(point);

	  this._userStrokePoints = [point];
	  this._userStrokeRenderer = new UserStrokeRenderer(this.svg, this._userStrokePoints, this.options);
	  return null;
	};

	CharacterQuizzer.prototype._continueUserStroke = function(point) {
	  if (!this._userStrokePoints) return;

	  // Reverse svg scale to capture correct coordinates
	  this._reverseSvgScale(point);

	  this._userStrokePoints.push(point);
	  this._userStrokeRenderer.updatePath();
	};

	CharacterQuizzer.prototype._reverseSvgScale = function(point) {
	  svgScaleAni = this.svg.transform.animVal;
	  svgScale = 1;
	  // check matrix defined and not null, to avoid error on Safari
	  if (svgScaleAni.length > 0 && svgScaleAni[0].matrix) {
	    svgScale = svgScaleAni[0].matrix.a;
	  }
	  // console.log(svgScale);
	  point.x = point.x / svgScale - this.options.x; // options.x is for margin
	  point.y = point.y / svgScale - this.options.y; // options.y is for margin
	};

	CharacterQuizzer.prototype._getStrokesPoints = function() {
	  return this.strokes.map(function(stroke) {
	    return this._extractPointsFromStrokeAnimator(stroke);
	  }.bind(this));
	};

	CharacterQuizzer.prototype._endUserStroke = function() {
	  if (!this._userStrokePoints) return Promise.resolve();
	  var promises = [];
	  var matchingStrokeIndex = this._strokeMatcher.getMatchingStrokeIndex(this._userStrokePoints, this._getStrokesPoints());

	  promises.push(this._userStrokeRenderer.fadeAndRemove());
	  this._userStrokePoints = null;
	  this._userStrokeRenderer = null;
	  if (!this._isActive) return Promise.resolve();

	  if (this._isValidStroke(matchingStrokeIndex)) {
	    this._handleSuccess(matchingStrokeIndex);
	  } else {
	    this._handleFaiulure();
	    if (this.options.showHintAfterMisses !== false && this._numRecentMistakes >= (this.options.showHintAfterMisses || 3)) {
	      promises.push(this._highlightCorrectStroke());
	    }
	  }
	  return Promise.all(promises);
	};

	CharacterQuizzer.prototype._handleSuccess = function(strokeIndex) {
	  callIfExists(this.options.onCorrectStroke, {
	    strokeNum: this._currentStrokeIndex,
	    mistakesOnStroke: this._numRecentMistakes,
	    totalMistakes: this._totalMistakes,
	    strokesRemaining: this.strokes.length - this._currentStrokeIndex - 1
	  });
	  this._currentStrokeIndex += 1;
	  this._numRecentMistakes = 0;
	  var promise = this._fadeInStroke(strokeIndex);
	  if (this._currentStrokeIndex === this.strokes.length) {
	    callIfExists(this.options.onComplete, {
	      totalMistakes: this._totalMistakes
	    });
	    if (this.options.highlightOnComplete) {
	      var duration = this.options.highlightOnCompleteDuration || 1000;
	      promise = promise.then(this._highlightCharacter.bind(this, duration));
	    }
	  }
	  return promise;
	};

	CharacterQuizzer.prototype._fadeInStroke = function(strokeIndex) {
	  var stroke = this.strokes[strokeIndex];
	  var duration = this.options.strokeFadeInDuration || 300;
	  stroke.setStyles({ opacity: 0 });
	  stroke.show();
	  return stroke.tweenStyles({ opacity: 1 }, { duration: duration });
	};

	CharacterQuizzer.prototype._highlightCharacter = function(duration) {
	  return Promise.all(this.highlightStrokes.map(function(highlightStroke) {
	    highlightStroke.setStyles({ opacity: 0 });
	    highlightStroke.show();
	    return highlightStroke.tweenStyles({ opacity: 1 }, { duration: duration / 2 }).then(function() {
	      return highlightStroke.tweenStyles({ opacity: 0 }, { duration: duration / 2 });
	    });
	  }));
	};

	CharacterQuizzer.prototype._handleFaiulure = function() {
	  this._numRecentMistakes += 1;
	  this._totalMistakes += 1;
	  callIfExists(this.options.onMissedStroke, {
	    strokeNum: this._currentStrokeIndex,
	    mistakesOnStroke: this._numRecentMistakes,
	    totalMistakes: this._totalMistakes,
	    strokesRemaining: this.strokes.length - this._currentStrokeIndex
	  });
	};

	CharacterQuizzer.prototype._highlightCorrectStroke = function() {
	  return this.highlightStrokes[this._currentStrokeIndex].animate({
	    strokeAnimationVelocity: this.options.highlightStrokeVelocity || 1.5,
	    animateStyles: {
	      start: {
	        opacity: 1
	      },
	      end: {
	        opacity: 0
	      }
	    }
	  });
	};

	CharacterQuizzer.prototype._isValidStroke = function(strokeIndex) {
	  return strokeIndex === this._currentStrokeIndex;
	};

	CharacterQuizzer.prototype.stopQuiz = function() {
	  this._isActive = false;
	};

	var POINTS_PER_STROKE = 20; // estimate a stroke using this many points. More is probably better but slower.
	CharacterQuizzer.prototype._extractPointsFromStrokeAnimator = function(strokeAnimator) {
	  var maskPath = strokeAnimator.maskPath;
	  var length = maskPath.getTotalLength();
	  var points = [];
	  for (var i = 0; i < POINTS_PER_STROKE; i++) {
	    points.push(maskPath.getPointAtLength(i * length / (POINTS_PER_STROKE - 1)));
	  }
	  return points;
	};

	// --------- Static convenience methods --------

	// convenience method around new CharacterQuizzer, renderCharacter, startQuiz
	CharacterQuizzer.setupQuiz = function(targetElm, charOrUnicode, options) {
	  var quizzer = new CharacterQuizzer(targetElm, options);
	  quizzer.renderCharacter(charOrUnicode).then(function() {
	    quizzer.startQuiz();
	  });
	  return quizzer;
	};

	module.exports = CharacterQuizzer;

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var average = __webpack_require__(13);
	var pointUtils = __webpack_require__(14);

	var AVG_DIST_THRESHOLD = 0.2; // 0 to 1, bigger = more lenient
	var LENGTH_RATIO_THRESHOLD = 0.7; // 0 to 1, bigger = more lenient
	var COSINE_SIMILARITY_THRESHOLD = 0;  // -1 to 1, smaller = more lenient
	var START_AND_END_DIST_THRESHOLD = 0.3; // 0 to 1, bigger = more lenient

	var StrokeMatcher = function(width, height) {
	  this.size = (width + height) / 2;
	};

	StrokeMatcher.prototype.getMatchingStrokeIndex = function(userStrokePoints, strokesPoints) {
	  var points = this._stripDuplicates(userStrokePoints);
	  if (points.length < 2) return null;

	  var closestStrokePoints = null;
	  var closestStrokeIndex = 0;
	  var bestAvgDist = 0;
	  strokesPoints.forEach(function(strokePoints, i) {
	    var avgDist = pointUtils.getAvgMinDist(points, strokePoints);
	    if (avgDist < bestAvgDist || !closestStrokePoints) {
	      closestStrokePoints = strokePoints;
	      closestStrokeIndex = i;
	      bestAvgDist = avgDist;
	    }
	  });

	  var withinDistThresh = bestAvgDist < (this.size * AVG_DIST_THRESHOLD);
	  var lengthAdjustFactor = (this.size / 10); // make ratio more fair for tiny strokes
	  var lengthRatio = (this._getLength(points) + lengthAdjustFactor) / (this._getLength(closestStrokePoints) + lengthAdjustFactor);
	  var withinLengthThresh = (lengthRatio > (1 - LENGTH_RATIO_THRESHOLD)) && (lengthRatio < (1 + LENGTH_RATIO_THRESHOLD));
	  var startAndEndMatch = this._startAndEndMatches(points, closestStrokePoints);
	  var directionMatches = this._directionMatches(points, closestStrokePoints);
	  if (withinDistThresh && withinLengthThresh && startAndEndMatch && directionMatches) {
	    return closestStrokeIndex;
	  }
	  return null;
	};

	StrokeMatcher.prototype._startAndEndMatches = function(points, closestStrokePoints) {
	  var startingDist = pointUtils.getDistance(closestStrokePoints[0], points[0]);
	  var endingDist = pointUtils.getDistance(closestStrokePoints[closestStrokePoints.length - 1], points[points.length - 1]);
	  return startingDist < (this.size * START_AND_END_DIST_THRESHOLD) && endingDist < (this.size * START_AND_END_DIST_THRESHOLD);
	};

	StrokeMatcher.prototype._directionMatches = function(points, strokePoints) {
	  var edgeVectors = this._getEdgeVectors(points);
	  var strokeVectors = this._getEdgeVectors(strokePoints);
	  var similarities = [];
	  for (var i = 0; i < edgeVectors.length; i++) {
	    var edgeVector = edgeVectors[i];
	    var strokeSimilarities = strokeVectors.map(function(strokeVector) {
	      return pointUtils.cosineSimilarity(strokeVector, edgeVector);
	    });
	    var maxSimilarity = Math.max.apply(Math, strokeSimilarities);
	    similarities.push(maxSimilarity);
	  }
	  var avgSimilarity = average(similarities);
	  return avgSimilarity > COSINE_SIMILARITY_THRESHOLD;
	};

	StrokeMatcher.prototype._stripDuplicates = function(points) {
	  if (points.length < 2) return points;
	  var dedupedPoints = [points[0]];
	  points.slice(1).forEach(function(point) {
	    if (!pointUtils.equals(point, dedupedPoints[dedupedPoints.length - 1])) {
	      dedupedPoints.push(point);
	    }
	  });
	  return dedupedPoints;
	};

	StrokeMatcher.prototype._getLength = function(points) {
	  var length = 0;
	  var lastPoint = points[0];
	  for (var i = 0; i < points.length; i++) {
	    var point = points[i];
	    length += pointUtils.getDistance(point, lastPoint);
	    lastPoint = point;
	  }
	  return length;
	};

	// returns a list of the direction of all segments in the line connecting the points
	StrokeMatcher.prototype._getEdgeVectors = function(points) {
	  var vectors = [];
	  var lastPoint = points[0];
	  points.slice(1).forEach(function(point) {
	    vectors.push(pointUtils.subtract(point, lastPoint));
	    lastPoint = point;
	  });
	  return vectors;
	};

	module.exports = StrokeMatcher;


/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = function(arr) {
	  var sum = 0;
	  for (var i = 0; i < arr.length; i++) {
	    sum += arr[i];
	  }
	  return sum / arr.length;
	};


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var arrayMin = __webpack_require__(15);
	var arrayMax = __webpack_require__(16);
	var average = __webpack_require__(13);

	var pointUtils = {};

	// return a new point subtracting point from this
	pointUtils.subtract = function(pointA, pointB) {
	  return { x: pointA.x - pointB.x, y: pointA.y - pointB.y };
	};

	pointUtils.getMagnitude = function(point) {
	  return Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2));
	};

	pointUtils.equals = function(pointA, pointB) {
	  if (!pointA || !pointB) return false;
	  return pointB.x === pointA.x && pointB.y === pointA.y;
	};

	pointUtils.getBounds = function(points) {
	  var xs = points.map(function(point) { return point.x; });
	  var ys = points.map(function(point) { return point.y; });
	  var maxX = arrayMax(xs);
	  var maxY = arrayMax(ys);
	  var minX = arrayMin(xs);
	  var minY = arrayMin(ys);
	  return [{x: minX, y: minY}, {x: maxX, y: maxY}];
	};

	pointUtils.getDistance = function(point1, point2) {
	  var difference = pointUtils.subtract(point1, point2);
	  return pointUtils.getMagnitude(difference);
	};

	pointUtils.cosineSimilarity = function(point1, point2) {
	  var rawDotProduct = point1.x * point2.x + point1.y * point2.y;
	  return rawDotProduct / pointUtils.getMagnitude(point1) / pointUtils.getMagnitude(point2);
	};

	// get for each point in pointsA, find the min dist to a point in pointsB, then average
	// TODO: consider also running the reverse calculation and averaging
	pointUtils.getAvgMinDist = function(pointsA, pointsB) {
	  var dists = pointsA.map(function(pointA) {
	    var distsToB = pointsB.map(function(pointB) {
	      return pointUtils.getDistance(pointA, pointB);
	    });
	    return arrayMin(distsToB);
	  });
	  return average(dists);
	};

	module.exports = pointUtils;


/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = function(numArray) {
	  return Math.min.apply(null, numArray);
	};


/***/ },
/* 16 */
/***/ function(module, exports) {

	module.exports = function(numArray) {
	  return Math.max.apply(null, numArray);
	};


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var velocity = __webpack_require__(1);
	var extend = __webpack_require__(3);
	var setSvgAttr = __webpack_require__(6);
	var setSvgStyles = __webpack_require__(7);

	var UserStrokeRenderer = function(svg, points, options) {
	  this.svg = svg;
	  this.points = points;
	  this.options = options;

	  this._render();
	};

	UserStrokeRenderer.prototype._render = function() {
	  this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	  this.updatePath();
	  var styles = extend({
	    fill: 'none',
	    stroke: '#555',
	    'stroke-width': 2,
	    opacity: 1
	  }, this.options.userStrokeStyles);
	  setSvgStyles(this.path, styles);
	  this.svg.appendChild(this.path);
	};

	UserStrokeRenderer.prototype.updateOptions = function(options) {
	  this.options = extend(this.options, options);
	};

	UserStrokeRenderer.prototype.updatePath = function() {
	  setSvgAttr(this.path, 'd', this.getPathString());
	};

	UserStrokeRenderer.prototype.setStyles = function(attrs) {
	  setSvgStyles(this.path, attrs);
	};

	UserStrokeRenderer.prototype.tweenStyles = function(attrs, options) {
	  return velocity(this.path, attrs, { duration: (options && options.duration) || 1000 });
	};

	UserStrokeRenderer.prototype.fadeAndRemove = function(options) {
	  var opts = extend(this.options, options);
	  var duration = opts.userStrokeFadeDuration || 500;
	  return this.tweenStyles({ opacity: 0 }, { duration: duration }).then(function() {
	    this.remove();
	  }.bind(this));
	};

	UserStrokeRenderer.prototype.remove = function() {
	  this.path.remove();
	};

	UserStrokeRenderer.prototype.getPathString = function() {
	  var start = this.points[0];
	  var remainingPoints = this.points.slice(1);
	  var pathString = 'M ' + start.x + ' ' + start.y;
	  remainingPoints.forEach(function(point) {
	    pathString += ' L ' + point.x + ' ' + point.y;
	  });
	  return pathString;
	};

	module.exports = UserStrokeRenderer;


/***/ }
/******/ ]);