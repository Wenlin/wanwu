<?php
/* Copyright (c) 2024 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/

/* To upload to wenlin.co: sh rsync_upload_wenlinco_wow

	References:
	https://www.mediawiki.org/wiki/Manual:Developing_extensions
	https://www.mediawiki.org/wiki/List_of_simple_extensions

	http://www.mediawiki.org/wiki/Manual:Tag_extensions
	http://www.mediawiki.org/wiki/Manual:Tag_extensions/Example

	http://www.mediawiki.org/wiki/Manual:How_to_debug
	http://www.mediawiki.org/wiki/MediaWiki-Vagrant
	http://www.mediawiki.org/wiki/Debugging_toolbar
	http://www.organicdesign.co.nz/MediaWiki_code_snippets#Getting_in_to_MediaWiki_coding
*/

/** WenlinHooks:
	Hooks for the Wenlin extension.
	@file
	@ingroup Extensions

	All the hooks get loaded by means of the Hooks section of extension.json.
	The code for each hook-handling method in this module should be brief and call
	other modules to do most of the work. Order the methods alphabetically, and likewise
	use alphabetical order in extension.json.
*/
class WenlinHooks
{
	/* TODO: hooks for "undelete" and similar -- handle similarly to "save". See hooks.txt.
		Possible hooks: ArticleRollbackComplete (After an article rollback is completed -- called by commitRollback in WikiPage.php)
					ArticleRevisionUndeleted (After an article revision is restored -- called by undeleteRevisions in SpecialUndelete.php)
					ArticleUndelete (When one or more revisions of an article are restored) -- called by undeleteRevisions in SpecialUndelete.php)
			Difference between ArticleRevisionUndeleted and ArticleUndelete: ArticleRevisionUndeleted is called in a loop for each revision (not
			only the current revision); ArticleUndelete is called only once. Probably sufficient for us to hook ArticleUndelete, since we only
			index the current revision. This may all become much more complicated due to FlaggedRevisions, though... */

	/** onApiCheckCanExecute:
		Hook for ApiCheckCanExecute.
		For Wenlin, only allow a special group of users to use api.php, such as for web-sync between Wenlin Desktop and the wiki.
		However, to log in to the API, you have to use the API. So, make an exception: if you’re not logged in, you can use the API
		-- but only as needed for logging in.

		@param $module: Module object.
		@param $user: Current user
		@param &$message: API usage message to die with. Since 1.27, it is preferred to use an ApiMessage object here.
			A string or array is currently passed to ApiBase::dieUsageMsg(), but this is planned to change in 1.29.
		@return true to allow API execution, or false to disallow.
	*/
	public static function onApiCheckCanExecute($module, $user, &$message)
	{
		if ($user && in_array('bureaucrat', $user->getGroups())) {
			return true;
		}
		$moduleName = $module->getModuleName();
		if ($moduleName == "clientlogin" || $moduleName == "login") {
			return true;
		}
		if (self::gettingLoginToken($module)) {
			return true;
		}
		$message = "API access is restricted";
		return false;
	} // onApiCheckCanExecute

	private static function gettingLoginToken($module) {
		global $wgRequest;
		if ($module->getModuleName() == "query") {
			$params = $module->extractRequestParams();
			if (in_array("tokens", $params["meta"])) {
				$names = $wgRequest->getValueNames();
				// allow only these names in query; otherwise any query with "meta=tokens" would be allowed
				$goodNames = ["action", "meta", "type", "format"];
				foreach ($names as $name) {
					if (!in_array($name, $goodNames)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	/** onArticleDeleteComplete:
		Hook for ArticleDeleteComplete.
		Occurs after the delete article request has been processed.
		Triggered both by edits made through the edit page, and by edits made through the API. (? Test!)
		Update our custom database tables whenever an entry gets deleted.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/ArticleDeleteComplete
		@see https://www.mediawiki.org/wiki/Category:ArticleDeleteComplete_extensions

		@param WikiPage $article: the article that was deleted. WikiPage in MW >= 1.18, Article in 1.17.x and earlier.
		@param User $user: the user that deleted the article.
		@param $reason: the reason the article was deleted.
		@param $id: id of the article that was deleted (added in 1.13).
		@param Content $content: the content of the deleted article, or null in case of an error (added in 1.21).
		@param $logEntry: the log entry used to record the deletion (added in 1.21).
		@return true to continue hook processing, or false to abort.
	*/
	public static function onArticleDeleteComplete(WikiPage $article, User $user, $reason, $id, Content $content, $logEntry)
	{
		/* Update indexes. */
		WenlinIndex::updateIndexesOnDelete($article, $content, $id);
		return true; // continue hook processing
	} // onArticleDeleteComplete

	/** onArticleUndelete:
		Hook for ArticleUndelete.
		Occurs when one or more revisions of an article are restored.
		Triggered both by edits made through the edit page, and by edits made through the API. (? Test!)
		Update our custom database tables whenever an entry gets undeleted.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/ArticleUndelete
		@see https://www.mediawiki.org/wiki/Category:ArticleUndelete_extensions

		@param $titleObj: Title corresponding to the article restored
		@param $create: Whether or not the restoration caused the page to be created (i.e. it didn't exist before)
		@param $comment: Comment explaining the undeletion
		@param $oldPageId: ID of page previously deleted (from archive table) (since gerrit:133631)
		@return true to continue hook processing, or false to abort.
	*/
	public static function onArticleUndelete(Title $titleObj, $create, $comment, $oldPageId)
	{
		/* Update indexes. */
		WenlinIndex::updateIndexesOnUndelete($titleObj, $create, $comment, $oldPageId);
		return true; // continue hook processing
	} // onArticleUndelete

	/** onAuthChangeFormFields:
		Modify the login page by hooking AuthChangeFormFields.

		For Wenlin we add a link to wenlinshangdian.com for creating an account.

		@param array $requests: array of AuthenticationRequests the fields are created from.
		@param array $fieldInfo: field information array (union of all AuthenticationRequest::getFieldInfo() responses).
		@param array &$formDescriptor: HTMLForm descriptor. The special key weight can be set to change the order of the fields.
		@param string $action: one of the AuthManager::ACTION_* constants.
		@return true.
	*/
	public static function onAuthChangeFormFields($requests, $fieldInfo, array &$formDescriptor, $action)
	{
		// die(var_dump($formDescriptor)); // uncomment this to find out what it contains

		/* Added a field that resembles the ordinary one that says "Forgot your password?",
			which is in $formDescriptor['passwordReset']. */
		$url = 'https://wenlinshangdian.com';
		$text = 'Create or modify an account on wenlinshangdian.com';
		$formDescriptor['wenlinshangdian']['default'] = "<a href='$url'>$text</a>";
		$formDescriptor['wenlinshangdian']['type'] = 'info';
		$formDescriptor['wenlinshangdian']['raw'] = true;
		$formDescriptor['wenlinshangdian']['cssclass'] = 'mw-form-related-link-container';
		$formDescriptor['wenlinshangdian']['weight'] = 250; // compare passwordReset which is 230

		/* Make the Help item ('linkcontainer') point to our own page. Normally it says,
			<a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Logging_in">Help with logging in</a>'
			Replace that with our own wiki's Help:Logging_in. */
		if (isset($formDescriptor['linkcontainer'])) {
			$base = WenlinGongju::articlePathBase();
			$url = "$base/Help:Logging_in";
			$text = 'Help with logging in';
			$formDescriptor['linkcontainer']['default'] = "<a href='$url'>$text</a>";
		}
		return true;
	} // onAuthChangeFormFields

	/** onBeforePageDisplay:
		Hook for BeforePageDisplay.
		Allow last-minute changes to the output page, e.g. adding of CSS or JavaScript by extensions.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/BeforePageDisplay

		@param $out: the OutputPage.
		@param $skin: the Skin.
		@return true to continue hook processing.
	*/
	public static function onBeforePageDisplay(OutputPage $out, Skin &$skin)
	{
		/* Make the call to addModuleScripts('ext.strokebutton') conditional, only for zidian entries.
			Currently stroke buttons are only included in zidian entries. Calling addModuleScripts for
			other pages might needlessly slow their loading. */
		if ($out->isArticle()) {
			$titleObj = $out->getContext()->getTitle();
			if ($titleObj && $titleObj->getNsText() === 'Zi') {
				/* Enable the button for the stroking box.
					The name 'ext.strokebutton' needs to match extensions.json, where
					it gets associated with the script 'strokebutton.js'.
					It was necessary to change addModuleScripts to addModules for compatibility with
					MediaWiki 1.32.0, to prevent "TypeError: mw.loader.using is not a function". */
				$out->addModules('ext.strokebutton');
			}
		}
		/* Enable pinyin tone conversion. The name 'ext.pinyin' needs to match extensions.json, where
			it gets associated with the script 'pinyin.js'.
			It was necessary to change addModuleScripts to addModules for compatibility with
			MediaWiki 1.32.0, to prevent "jQuery is not defined". */
		$out->addModules('ext.pinyin');

		return true;
	} // onBeforePageDisplay

	/** onChangesListInsertArticleLink:
		Override or augment link to article in Special:RecentChanges or Special:Watchlist.
		For Wenlin we want to display headwords rather than just serial numbers.
		Compare onShowSearchHitTitle which serves a similar purpose for Special:Search.

		@param $changesList: ChangesList instance.
		@param &$articleLink: HTML of link to article (already filled-in).
		@param &$s: HTML of row that is being constructed.
		@param &$rc: RecentChange instance.
		@param $unpatrolled: Whether or not we are showing unpatrolled changes.
		@param $watched: Whether or not the change is watched by the user.

		References:
			https://gitlab.com/Wenlin/wanwu/issues/37
			https://www.mediawiki.org/wiki/Manual:Hooks/ChangesListInsertArticleLink
			https://www.mediawiki.org/wiki/Manual:Hooks/EnhancedChangesListModifyBlockLineData
			https://www.mediawiki.org/wiki/Manual:Hooks/EnhancedChangesListModifyLineData

		We don't need to hook EnhancedChangesListModifyBlockLineData or EnhancedChangesListModifyLineData.
		We could use EnhancedChangesListModifyBlockLineDataCompare to accomplish most of what we need, but
		it would fail (not be called) on "grouped entries". Compare:
			EnhancedChangesListModifyBlockLineData - Modify data used to build a non-grouped entry in Special:RecentChanges
			EnhancedChangesListModifyLineData      - Modify data used to build a grouped recent change inner line in Special:RecentChanges
		"Grouped entries" can occur when there have been multiple changes to the same entry, maybe close together and by the same user;
		such a "group" may be "collapsible". EnhancedChangesListModifyLineData is for "cur|prev" lines that get revealed when you
		uncollapse a group. We don't need to change "cur|prev" lines. Hooking ChangesListInsertArticleLink seems to do what we need for
		both grouped and ungrouped entries.
	*/
	public static function onChangesListInsertArticleLink($changesList, &$articleLink, &$s, &$rc, $unpatrolled, $watched)
	{
		// die(var_dump($articleLink)); // uncomment this to find out what an argument contains
		$titleObj = $rc->mTitle; // the Title object
		$namespaceStr = $titleObj->getNsText();
		if (in_array($namespaceStr, ['En', 'Ci', 'Jyut'])) {
			$isTalkPage = false;
			$entryNsStr = $namespaceStr;
		}
		elseif (in_array($namespaceStr, ['En_talk', 'Ci_talk', 'Jyut_talk'])) {
			$isTalkPage = true;
			$entryNsStr = str_replace('_talk', '', $namespaceStr);
		}
		else {
			return true;
		}
		/* $articleLink is a string like:
			<span class="mw-title"><a href="/wow/En:2000718292" class="mw-changeslist-title" title="En:2000718292">En:2000718292</a></span>
		*/
		if (!preg_match("/\>([^\<]+)\<\/a\>/u", $articleLink, $matches)) { // match content of "a" tag
			// die(var_dump($articleLink));
			return true;
		}
		$linkText = $matches[1];
		$titleStr = $titleObj->getText();
		if ($isTalkPage) {
			$titleObj = Title::newFromText("$entryNsStr:$titleStr");
		}
		$contentText = WenlinGongju::getContentTextFromTitleObject($titleObj);
		WenlinGongju::stripWLTag($contentText);
		/* $changesList->getOutput() is an OutputPage object, which getHeadwordFromContentText uses to get a WenlinParser.
			The OutputPage might not be needed here. We could pass null for $outputPage instead, since getHeadwordFromContentText
			only calls translateToDisplayFormat to get the headword which probably doesn't require a WenlinParser. It seems
			safer though not to assume that here. */
		$hw = WenlinBand::getHeadwordFromContentText($contentText, $entryNsStr, $changesList->getOutput());
		if ($isTalkPage) {
			$hw = "Talk:$hw";
		}
		// Include the closing "a" tag for str_replace, to avoid changing other occurrences of $linkText such as in the URL.
		$articleLink = str_replace("$linkText</a>", "$hw ($namespaceStr:$titleStr)</a>", $articleLink);
		return true;
	} // onChangesListInsertArticleLink

	/** onEditFilterMergedContent:
		Hook function for EditFilterMergedContent.
		Make sure the just-edited dictionary entry is valid, and reject it, with an explanation, if it's not valid.

		@param IContextSource $context (or RequestContext?)
		@param Content $content New content, as a Content object.
		@param Status $status
		@param string $summary
		@param User $user
		@param boolean $isMinor Whether or not the edit was marked as minor.
		@return true.

		Based on filterMergedContent in SpamBlacklistHooks.php

		Part of what we do here is the same as onPageContentSaveComplete. The differences are:
			(1) onPageContentSaveComplete gathers the info and updates the index; here we only
				gather the info without updating the index.
			(2) here we need to expect problems and report them to the user; onPageContentSaveComplete
				can assume we've been called, shouldn't have as much responsibility for error reporting.
	*/
	public static function onEditFilterMergedContent(IContextSource $context, Content $content,
									Status $status, $summary, User $user, $isMinor)
	{
		if ($status->isOK()) {
			if (!$context->canUseWikiPage()) {
				$status->fatal("onEditFilterMergedContent cannot use WikiPage");
				return true;
			}
			WenlinIndex::entryIsValidForSaving($context->getWikiPage(), $content, $status);
			// entryIsValidForSaving may call $status->fatal(....) with error messages.
		}
		// Always return true, EditPage will look at $status->isOk().
		return true;
	} // onEditFilterMergedContent

	/** onGetPreferences:
		Hook function for GetPreference.
		Enable the user to set Wenlin-specific preferences in a similar way to generic MediaWiki preferences.

		We might also change the ChangePassword link to go to wenlinshangdian.com, or just
		add an additional link to wenlinshangdian.com.

		@param User $user the current user, whose preferences we want.
		@param array $preferences called with preference configuration,
				to be filled in with our changed preference configuration.
		@return boolean true.

		Reference: https://www.mediawiki.org/wiki/Manual:Hooks/GetPreferences

		TODO: specify defaults here, or in DefaultUserOptions in extension.json, but not both?

		TODO: localize strings for user-interface languages other than English.
		Clarify the difference between 'label-message' and 'label', use of intl
		message files, etc. Lots of room for improvement!!
	*/
	public static function onGetPreferences(User $user, array &$preferences)
	{
		// Here's a way with wfDebug to see what $preferences contains.
		// wfDebug("\nWenlin Debug onGetPreferences:\n");
		// wfDebug(var_dump($preferences));
/*   'password' =>
    array (size=5)
      'type' => string 'info' (length=4)
      'raw' => boolean true
      'default' => string '<a href="/wanwu/index.php?title=Special:ChangePassword&amp;returnto=Special%3APreferences" title="Special:ChangePassword">Change password</a>' (length=141)
      'label-message' => string 'yourpassword' (length=12)
      'section' => string 'personal/info' (length=13)
*/
		if (false) {
			/* Modify the ChangePassword link to point to wenlinshangdian.com, which is
				in charge of all wiki account names and passwords.
				TODO: refine this URL -- maybe login page for wenlinshangdian.com? */
			$preferences['password'] = array(
				'type' => 'info',
				'raw' => true,
				'default' => '<a target="_blank" href="https://wenlinshangdian.com" title="Go to shangdian account">Change password, membership, subscription, etc., on wenlinshangdian.com</a>',
				/* 'label-message' => 'yourpassword', */
				'label' => 'Password, membership, etc.',
				'section' => 'personal/info'
			);
		}
		// unset($preferences['password']); // this removes the password stuff from preferences page
		// Compare: disable Special:PasswordReset
		/* Documentation about array keys:
			https://phabricator.wikimedia.org/source/mediawiki/browse/master/includes/htmlform/HTMLForm.php
		*/
		/* Add our Wenlin-specific preferences. Use names with "Wenlin" in them for our keys
			in the $preferences array, to avoid accidental collision with other MediaWiki code.
			The names of Wenlin-specific options like 'WenlinHideHanzi' must match those
			in WenlinDictionaryPreferences::loadFromDatabase (see WenlinBand.php), and in
			the DefaultUserOptions section of extensions/Wenlin/extension.json */
		/* "If your section value is foo/bar, this means your preference will appear on the foo
			tab (named by system message prefs-foo) within the bar section (named by system
			message prefs-bar). If no such tab or section exists, it is created automatically."
			https://www.mediawiki.org/wiki/Manual:Hooks/GetPreferences#Tabs_and_sections */
		/* Let's have our own 'Dictionary' section in our own 'Wenlin' tab!
			This assumes we have lines like this in extensions/Wenlin/i18n/en.json:
				"prefs-wenlin": "Wenlin",
				"prefs-dictionary": "Dictionary" */
		$wenlinTab = 'wenlin'; // en.json: "prefs-wenlin": "Wenlin"
		$dictionarySection = 'wenlin-dictionary'; // en.json: "prefs-wenlin-dictionary": "Dictionary"
		$subscriptionSection = 'wenlin-subscription'; // en.json: "prefs-wenlin-subscription": "Subscription"

		/* Defaults: see DefaultUserOptions in extensions/Wenlin/extension.json,
			which is equivalent to $wgDefaultUserOptions. When adding new options here,
			also add their default values in extension.json. Do NOT specify defaults here,
			(except for 'type' ==> 'info') since they would be redundant at best and contradictory
			at worst, and they would not take effect until the user actually went to the Preferences form. */

		/******* Dictionary section ***********/
		$preferences['WenlinPreferSimple'] = array(
			'type' => 'toggle',
			'label' => 'Prefer Simple Form Characters',
			'section' => "$wenlinTab/$dictionarySection"
		);
		// The following options match desktop Wenlin's "Dictionary Display Options"
		// (choose "Show/Hide Dictionary Items" from the Options menu).
		$preferences['WenlinHideRareinfo'] = array(
			'type' => 'toggle',
			'label' => 'Hide (abridge) rare/archaic/advanced information',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHidePinyin'] = array(
			'type' => 'toggle',
			'label' => 'Hide Pinyin (alphabetic Mandarin pronunciation)',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideIPA'] = array(
			'type' => 'toggle',
			'label' => 'Hide IPA (international phonetic alphabet, for English)',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideHanzi'] = array(
			'type' => 'toggle',
			'label' => 'Hide Hanzi (Chinese characters)',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideSFEquiv'] = array(
			'type' => 'toggle',
			'label' => 'Hide full/simple form equivalents of simple/full forms',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideGloss'] = array(
			'type' => 'toggle',
			'label' => 'Hide glosses (translations/definitions in the other language)',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideSubentries'] = array(
			'type' => 'toggle',
			'label' => 'Hide English subentries separated in alphabetical listing',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinHideToneChange'] = array(
			'type' => 'toggle',
			'label' => 'Hide tone-change notation (dots and lines below vowels) in dictionary entries',
			'section' => "$wenlinTab/$dictionarySection"
		);
		$preferences['WenlinReplaceTilde'] = array(
			'type' => 'toggle',
			'label' => 'Replace ∼ (tilde) with the headword in dictionary example sentences',
			'section' => "$wenlinTab/$dictionarySection"
		);

		// 'Always show Pīnyīn first in Cídiǎn entries' corresponds to one of the
		// Advanced Options in the desktop Wenlin.
		$preferences['WenlinPreferPinyinFirst'] = array(
			'type' => 'toggle',
			'label' => 'Always show Pīnyīn first in Cídiǎn entries',
			'section' => "$wenlinTab/$dictionarySection"
		);
		if ($user->isAllowed('WenlinRightToViewAllJyut')) {
			$preferences['WenlinJyutAndOrCi'] = array(
				'type' => 'select',
				'label' => 'Jyut, Ci:',
				'help' => 'See both Jyut (Cantonese) and Ci (Mandarin) entries, in either order, or only one or the other.',
				'options' => [
					/* The namespaces are separated by hyphens here, as required for
						WenlinDictionaryPreferences::getPreferredChineseNamespaces(). */
					'Jyut before Ci' => 'Jyut-Ci',
					'Ci before Jyut' => 'Ci-Jyut',
					'Jyut only' => 'Jyut',
					'Ci only' => 'Ci',
				],
				'section' => "$wenlinTab/$dictionarySection"
			);
		}
		/******* Subscription section ***********/
		// The string 'wenlin-subscription-xyz' is arbitrary, not referenced elsewhere.
		$preferences['wenlin-subscription-xyz'] = array(
			'type' => 'info',
			/* Without 'raw', the link html would be displayed with visible angle brackets, etc., instead of as a link. */
			'raw' => true,
			/* Formerly we used 'help' here, with no 'default', but in MediaWiki 1.32.0 that caused an error,
				"Content passed to HtmlSnippet must be a string". Very mysterious. To prevent it, we seem
				to need a 'default' here that maps to a string. An empty string would cause an empty line
				before the 'help' line. So, just use 'default' instead of 'help' and never mind that the style
				ideally might be the style used for 'help' (slightly smaller text size).
					If "Content passed to HtmlSnippet must be a string" ever occurs again, it's not obvious
				which preference is responsible. I tracked it down with MWDebug::log("...sectionName = $sectionName")
				in HTMLForm.php where displaySection calls $value->getDefault().
					Specifying 'default' here is correct for 'type' => 'info', but not for ordinary preferences,
				which should be in DefaultUserOptions, not here. */
			'default' => 'To change your subscription please visit <a href="https://wenlinshangdian.com">wenlinshangdian.com</a>',
			'section' => "$wenlinTab/$subscriptionSection"
		);
		return true;
	} // onGetPreferences

	/** onHtmlPageLinkRendererBegin:
		Used when generating internal and interwiki links in LinkRenderer, before processing starts.

		@param $linkRenderer: the LinkRenderer object.
		@param $target: the LinkTarget that the link is pointing to (typically a Title object).
		@param &$text: the contents that the "a" tag should have; either a plain, unescaped
			string or a HtmlArmor object; null means "default".
		@param &$customAttribs: the HTML attributes that the "a" tag should have, in
			associative array form, with keys and values unescaped. Should be merged
			with default values, with a value of false meaning to suppress the attribute.
		@param &$query: the query string to add to the generated URL (the bit after the "?"),
			in associative array form, with keys and values unescaped.
		@param &$ret: the value to return if your hook returns false.
		@return boolean -- false to skip default processing and return $ret, else true to continue default processing.

		For Wenlin, we want links to the main namespace (except Main_Page) to redirect
		to Special:Wenlin look-up queries, and not to be turned into "redlinks".
		Compare onShowMissingArticle.

		https://github.com/wikimedia/mediawiki/blob/master/docs/hooks.txt
		http://stackoverflow.com/questions/29264423/how-to-redirect-all-mediawiki-red-links-to-a-single-url
	*/
	public static function onHtmlPageLinkRendererBegin($linkRenderer, $target, &$text, &$customAttribs, &$query, &$ret)
	{
		/* We get called for "log in" links (for which $target->exists() is false), and who knows what.
			If target isn't a Title or namespace isn't NS_Main, or the page exists, leave the
			link as-is, just return true. */
		if (!($target instanceof Title && $target->inNamespace(NS_MAIN)) || $target->exists()) {
			return true;
		}
		/* TODO: since we know target is in NS_MAIN, there is no namespace string, so getFullText should return
			the same that getText would return. Verify this still works if change getFullText to getText, and
			if so rename pageTitle to titleStr for consistency with other code. */
		$pageTitle = $target->getFullText(); // "horse" for "[[horse]]" --- and "abc" for "[[abc|xyz]]" (object is a LinkTarget and/or a Title object)
		if (mb_substr($pageTitle, 0, 1) === '#') { // first character is '#'
			/* If the target is a link to an anchor in the same page, leave it as-is, just return true. */
			return true;
		}
		/* Link to a URL like "/wow/horse". Even though the page doesn't exist at that URL, it will be
			converted to an appropriate search by our hook onShowMissingArticle. */
		$base = WenlinGongju::articlePathBase();
		$displayText = HtmlArmor::getHtml($text); // "horse" for "[[horse]]" --- and "xyz" for "[[abc|xyz]]"
		$ret = "<a href='$base/$pageTitle' title='$pageTitle'>$displayText</a>";
		return false;
	} // onHtmlPageLinkRendererBegin

	/** onLoadExtensionSchemaUpdates:
		Hook for LoadExtensionSchemaUpdates. Gets called when run "update.php" in the maintenance folder.

		@param $updater .
		@return true.

		https://www.mediawiki.org/wiki/Manual:DatabaseUpdater.php
		https://doc.wikimedia.org/mediawiki-core/master/php/classDatabaseUpdater.html
		https://www.mediawiki.org/wiki/Manual:Hooks/LoadExtensionSchemaUpdates

		The "Example extension" uses addExtensionTable in Examples.hooks.php:
		https://git.wikimedia.org/blob/mediawiki%2Fextensions%2Fexamples/6f0437a85a26cbac2ae14777cc78043e115fd1df/Example%2FExample.hooks.php
	*/
	public static function onLoadExtensionSchemaUpdates($updater)
	{
		$a = [
			'wenlin_ci_pinyin',
			'wenlin_ci_hanzi_string',
			'wenlin_ci_hanzi_contained',
			'wenlin_en_collation',
			'wenlin_jyut_ping',
			'wenlin_jyut_hz_string',
			'wenlin_jyut_hz_contained',
			'wenlin_zi_pinyin',
			'wenlin_zi_bujian',
			'wenlin_zi_seal',
			'wenlin_zi_frequency',
			];
		foreach ($a as $s) {
			$updater->addExtensionTable($s, __DIR__ . "/sql/add-$s.sql");
		}
		return true;
	} // onLoadExtensionSchemaUpdates

	/** onMediaWikiPerformAction:
		Hook for MediaWikiPerformAction. Gets called for all kinds of actions.
		Use this to do something completely different, after the basic globals have been
		set up, but before ordinary actions take place.
		https://www.mediawiki.org/wiki/Manual:Hooks/MediaWikiPerformAction

		First use in Wenlin extension is to inhibit "View source" (or "Edit"), "History",
		etc., if user lacks permission to view -- or edit -- the page.

		@param $outputPage: $wgOut (OutputPage class).
		@param $article: $wgArticle (Article class).
		@param $titleObj: $wgTitle (Title class).
		@param $user: $wgUser (User class).
		@param $request: $wgRequest (WebRequest class).
		@param $wiki: MediaWiki object, added in 1.13.
		@return false to prevent the standard performAction class from doing anything; else true.
	*/
	public static function onMediaWikiPerformAction($outputPage, $article, $titleObj, $user, $request, $wiki)
	{
		$val = $request->getVal('action');
		$restrictedActions = array('edit', 'history', 'delete', 'move'); // TODO: what other actions should be restricted?
		if (!in_array($val, $restrictedActions)) {
			return true;
		}
		$namespaceStr = $titleObj->getNsText();

		/* Return false for users who don't have 'edit' permission (e.g., WenlinGroupFreeBasic/WenlinGroupFreeIntro).
			This restores the correct behavior for WenlinGroupFreeBasic, as before the 2017-6-19 bug fix (don't call getFullText). */
		if (!$user->isAllowed('edit')) {
			$msg = WenlinXuke::messagePleaseLogInOrSubscribe($user->isLoggedIn() ? 'subscribe' : 'login');
			$html = $outputPage->parse($msg, false /* not necessarily start of line */);
			$outputPage->addHTML($html);
			return false;
		}
		if ($namespaceStr == 'Jyut' && $val != 'history' && !$user->isAllowed('editprotected')) {
			return false; /* Temporary limit on editing Jyut in last stage of preparing first print edition */
		}
		$wenlinRightViewAllThisNamespace = WenlinXuke::rightToViewAllThisNamespace($namespaceStr);
		if ($user->isAllowed($wenlinRightViewAllThisNamespace)) {
			return true;
		}
		$titleStr = $titleObj->getText();
		$msg = '?';
		if (WenlinXuke::entryIsViewableByThisUser($user, $namespaceStr, $titleStr, $msg)) {
			return true;
		}
		$html = $outputPage->parse($msg, false /* not necessarily start of line */);
		$outputPage->addHTML($html);
		return false;
	} // onMediaWikiPerformAction

	/** onPageContentSave:
		Hook for PageContentSave.
		Called before an article is saved. Unlike PageContentSaveComplete, we can change the content.

		For Wenlin, we make sure the entry is surrounded by WL tags; add them if they're not already there.
		In the future this hook might also serve other purposes, such as converting the ipa band in En entries.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/PageContentSave

		@param WikiPage $wikiPage
		@param User $user: User performing the modification.
		@param Content $content New content, as a Content object.
		@param string $summary Edit summary/comment.
		@param boolean $isMinor Whether or not the edit was marked as minor.
		@param boolean $isWatch (no longer used).
		@param $section Deprecated (no longer used).
		@param integer $flags see WikiPage::doEditContent documentation.
		@param Status $status Status object.
		@return true.
	*/
	public static function onPageContentSave($wikiPage, &$user, &$content, &$summary,
			$isMinor, $isWatch, $section, &$flags, &$status)
	{
		$titleObj = $wikiPage->getTitle();
		if (in_array($titleObj->getNamespace(), [NS_WL_CI, NS_WL_ZI, NS_WL_EN, NS_WL_JYUT])) {
			$text = $content->getContentHandler()->serializeContent($content);
			$changed = false;
			if (!preg_match('/^\<WL\>/', $text)) { // make sure it begins with WL tag ...
				$text = "<WL>\n" . ltrim($text);
				$changed = true;
			}
			if (!preg_match('/\<\/WL\>/', $text)) { // ... and ends with /WL tag
				$text = rtrim($text) . "\n</WL>";
				$changed = true;
			}
			if ($changed) {
				$content = $content->getContentHandler()->unserializeContent($text);
			}
		}
		return true;
	} // onPageContentSave

	/** onPageContentSaveComplete:
		Hook for PageContentSaveComplete.
		Occurs after the save page request has been processed.
		Triggered both by edits made through the edit page, and by edits made through the API.
		Update our custom database tables whenever an entry gets modified.

		Compare onPageContentSave, above.

		@see https://www.mediawiki.org/wiki/Manual:Hooks/PageContentSaveComplete
		@see https://www.mediawiki.org/wiki/Category:PageContentSaveComplete_extensions

		@param WikiPage $article
		@param User $user: User performing the modification.
		@param Content $content New content, as a Content object.
		@param string $summary Edit summary/comment.
		@param boolean $isMinor Whether or not the edit was marked as minor.
		@param boolean $isWatch (no longer used)
		@param $section Deprecated (no longer used)
		@param integer $flags Flags passed to WikiPage::doEditContent().
		@param {Revision|null} $revision Revision object of the new article. This can be null for edits
					that change nothing. There are reports (see bugzilla:64573) that it may be null in other
					scenarios also, which have not been identified.
		@param Status $status Status object about to be returned by doEditContent()
		@param integer $baseRevId the rev ID (or false) this edit was based on.
		@return true to continue hook processing, or false to abort.
	*/
	public static function onPageContentSaveComplete(WikiPage $article, User $user, Content $content, $summary,
			$isMinor, $isWatch, $section, $flags, $revision, Status $status, $baseRevId)
	{
		/* Update indexes. */
		/* Difference between isGood and isOK:
			"Good" means the operation was completed with no warnings or errors.
			"OK" means the operation was partially or wholly completed.
			But, what does that really mean? The question for Wenlin is, did the new/revised
			article get saved? If it did, we should update our custom indexes. If it didn't, we should do nothing.
			For now let's update our custom indexes if and only if isOK. */
		if ($status->isOK()) {
			WenlinIndex::updateIndexesOnSave($article, $content, $status);
		}
		/* Make a log entry (this is originally for debugging; keep it, or not?
			Log entries look like this:
			 onPageContentSaveComplete date:2015-09-24 19:42:50; user:Tangmu; title:Test; summary:Carlyle again; isOK:1; isGood:1
		*/
		if (false) {
			$now = date('Y-m-d H:i:s'); // server time zone -- would Zulu be better?
			$fname = '/Users/tbishop/wanwu_images/wow_save_log.txt';
			// $fname = '/home/wenlinbiz/private_wanwu/wow_save_log.txt';
			$username = $user->getName();
			$titleObj = $article->getTitle();
			$titleStr = $titleObj->getText();
			$isOK = $status->isOK();
			$isGood = $status->isGood();
			$message = "onPageContentSaveComplete date:$now; user:$username; title:$titleStr; summary:$summary; isOK:$isOK; isGood:$isGood\n";
			file_put_contents($fname, $message, FILE_APPEND);
		}
		return true; // continue hook processing
	} // onPageContentSaveComplete

	/** onParserFirstCallInit:
		Register the extension for WL tags with the WikiText parser.

		@param Parser parser the Parser.
		@return true to continue hook processing, or false to abort.

		We surround each entire entry with <WL> tags, to enable conversion from the format in which
		a database entry is stored to the format in which it is displayed.

		Instead of using <WL> tags surrounding each entire entry, there might be a hook
		to use to call the callback at the appropriate time without needing the tags; but
		experimenting with BeforePageDisplay and ArticleAfterFetchContentObject was not
		entirely successful. Old notes:
			'ParserBeforeInternalParse works fine except for one problem. Not only the page content gets
		converted, but also the part of the footer that says "This page has been accessed ... times", and also some of the text
		shown for the "View History" tab (and probably other pieces of user-interface text that I'm not aware of).
		ArticleAfterFetchContentObject also works fine except for a different (similar) problem. The page content is converted
		even when the user selects the "Edit" tab. It's important that the "Edit" tab should bring up the notation as it's
		stored, unconverted.' */
	public static function onParserFirstCallInit(Parser $parser)
	{
		/* The first parameter to setHook is the name of the new tag.
			The second parameter to setHook is the callback function for processing the text between the tags.
			It is possible instead do $parser->setHook('WL', array('object', 'renderWenlin'); where the array contains the object and the
			method. Cf. the PHP function is_callable(). But here we're using a static function in a class, not an object.
			See <https://www.mediawiki.org/wiki/Manual:Hooks#Writing_an_event_handler> */
		$parser->setHook("WL", "WenlinRender::renderWL");
		return true; // continue hook processing
	} // onParserFirstCallInit

	/** onSearchableNamespaces:
		Modify which namespaces are searchable.
		Currently we use this to inhibit searching of NS_WL_CDL, since the results would
		not be helpful for most users.
		By default, all of our content namespaces (see $wgContentNamespaces) are searchable,
		so there's no need to add them here.

		@param &$arr: Array of namespaces ($nsId => $name) which will be used.
		@return ?

		NOTE: Making a namespace searchable doesn't mean it will be searched by DEFAULT.
		There's a different mechanism for that: $wgNamespacesToBeSearchedDefault.

		https://www.mediawiki.org/wiki/Manual:Hooks/SearchableNamespaces
	*/
	public static function onSearchableNamespaces(&$arr)
	{
		// die(print_r($arr, true));
		unset($arr[NS_WL_CDL]);
		return true; // return value is ignored
	} // onSearchableNamespaces

	/** onSearchGetNearMatchBefore:
		Perform exact-title-matches in "go" searches before the normal operations.
		For Wenlin that means, treat the search term as though it were entered for "Look up word",
		and perform the search using our custom database tables.
		Note: "go" searches contrast with "fulltext" searches.

		@param $allSearchTerms the array of the search terms in all content languages; the first
					element seems to be what the user entered, and the other elements, if any, may
					be "variants" like "colour" for "color", or "馬" for "马".
		@param $titleObj to be set to a Title object or null.
		@return false if we produce our own look-up result (fill in $titleObj), else true to continue hook
					processing (that is let MediaWiki do whatever it normally does with the search box).

		We're called through SpecialSearch, a special page that is created for the "search" box even
		before we make a title for our own special page. SpecialSearch knows how to "redirect" itself to
		an article page, or another special page:  $this->getOutput()->redirect(...).

		TODO: seriously consider making separate boxes for LOOK-UP and SEARCH. The code for subverting
		SEARCH to do LOOK-UP is a bunch of hacks that depend too much on MediaWiki internals, and also
		it's not obvious to user how to do search instead of look-up -- namely, surround the search string with
		quotation marks. Two boxes would be MUCH clearer to user, and cleaner to code. Alternatively, include
		a checkbox or something for changing the function of the one box.

		Call stack:

#0  WenlinHooks::onSearchGetNearMatchBefore(Array ([0] => hu), )
#1  call_user_func_array(WenlinHooks::onSearchGetNearMatchBefore, Array ([0] => Array ([0] => hu),[1] => )) called at [/Users/tbishop/Sites/wanwu/includes/Hooks.php:209]
#2  Hooks::run(SearchGetNearMatchBefore, Array ([0] => Array ([0] => hu),[1] => )) called at [/Users/tbishop/Sites/wanwu/includes/search/SearchEngine.php:174]
#3  SearchEngine::getNearMatchInternal(hu) called at [/Users/tbishop/Sites/wanwu/includes/search/SearchEngine.php:139]
#4  SearchEngine::getNearMatch(hu) called at [/Users/tbishop/Sites/wanwu/includes/specials/SpecialSearch.php:188]
#5  SpecialSearch->goResult(hu) called at [/Users/tbishop/Sites/wanwu/includes/specials/SpecialSearch.php:117]
#6  SpecialSearch->execute() called at [/Users/tbishop/Sites/wanwu/includes/specialpage/SpecialPage.php:384]
#7  SpecialPage->run() called at [/Users/tbishop/Sites/wanwu/includes/specialpage/SpecialPageFactory.php:582]
#8  SpecialPageFactory::executePath(...RequestContext Object ([] => WebRequest Object ([] => Array ([search] => hu,...))) called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:267]
#9  MediaWiki->performRequest() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:566]
#10 MediaWiki->main() called at [/Users/tbishop/Sites/wanwu/includes/MediaWiki.php:414]
#11 MediaWiki->run() called at [/Users/tbishop/Sites/wanwu/index.php:41]
	*/
	public static function onSearchGetNearMatchBefore($allSearchTerms, &$titleObj)
	{
		# debug_print_backtrace(); die("ouch!");
		/* Use $allSearchTerms[0], ignore the rest. When I test:
				 "global $wgContLang; if ($wgContLang->hasVariants())..."
			hasVariants returns false, anyway, and count($allSearchTerms) == 1. */
		$term = $allSearchTerms[0];

		/* Provide a way that the user can bypass our handling and get the default Mediawiki
			handling (e.g., searching). If the term starts with quotation mark ("), that works, since
			SearchEngine::getNearMatchInternal knows how to handle that. It will call us again,
			after stripping off the quotation marks, so we set a static boolean flag "$bypassing" to
			return true a second time.
				NOTE: now that ShiliangUseSimpleSearch is false in skins/Shiliang/skin.json,
			maybe this mechanism with quotation mark (") isn't needed. Consider getting rid of it. */
		static $bypassing = false;
		if ($bypassing) {
			$bypassing = false;
			return true;
		}
		$sub = substr($term, 0, 1);
		if ($sub == "\"") {
			$bypassing = true;
			return true;
		}
		/* Command "s" for "search" or "搜索".
			Note: Title::newFromText doesn't work with syntax 'Special:Wenlin/s?s=...'
			(because it urlencodes the ? and =), so use the special 'QQ' syntax. */
		$titleObj = Title::newFromText('Special:Wenlin/QQs?s=' . $term);
		if ($titleObj) {
			return false; // success
		}
		return true; // failure (caller should continue hook processing)
	} // onSearchGetNearMatchBefore

	/** onShowMissingArticle:
		Called when generating the output for a non-existent page.
		For Wenlin, we don't use the Main namespace for dictionary entries, or for
		anything except Main_Page and a few special exceptions. If the user looks for
		an article in the Main namespace that doesn't exist, behave as though they had
		entered that title in the search box and pressed the Go (look-up) button.

		This works for URLs like "wenlin.co/wow/你好".
		In conjunction with our onHtmlPageLinkRendererBegin hook, it also works for wiki links like "[[你好]]".

		@param $article the Article object.
		@return bool true (ignored?).
	*/
	public static function onShowMissingArticle($article)
	{
		$titleObj = $article->getTitle();
		if ($titleObj->getNamespace() !== NS_MAIN) {
			return true;
		}
		$outputPage = $article->getContext()->getOutput();
		$base = WenlinGongju::articlePathBase();

		/* For compatibility with MediaWiki 1.32.0, it's necessary to urlencode $titleStr here.
			Otherwise bytes such as 0x83 get turned into underscores, and we get luanma --
			url like "http://localhost/wow/Special:Wenlin/s?s=%E9__%E8%BF_"
			title like "é è¿ "! */
		$titleStr = urlencode($titleObj->getText());

		$outputPage->redirect("$base/Special:Wenlin/s?s=$titleStr");
		return true;
	} // onShowMissingArticle

	/** onShowSearchHitTitle:
		Customize display of search hit title/link.

		@param Title &$titleObj Title object to link to.
		@param string &$titleSnippet Label for the link representing the search result. Typically the article title; may be null.
		@param SearchResult $result the SearchResult object.
		@param array $terms the search terms entered.
		@param SpecialSearch $page the SpecialSearch object.
		@param array $query the string parameters for the link representing the search result; may be empty.
		@return true.
	*/
	public static function onShowSearchHitTitle(Title $titleObj, &$titleSnippet, $result, $terms, $page, &$query)
	{
		// die(var_dump($titleObj)); // uncomment this to find out what it contains
		$namespaceStr = $titleObj->getNsText();
		$titleStr = $titleObj->getText();
		if ($namespaceStr === 'Zi') {
			$titleSnippet = Linker::link($titleObj, "$titleStr ($namespaceStr)");
		}
		elseif (in_array($namespaceStr, ['En', 'Ci', 'Jyut'])) {
			$contentText = WenlinGongju::getContentTextFromTitleObject($titleObj);
			WenlinGongju::stripWLTag($contentText);
			$hw = WenlinBand::getHeadwordFromContentText($contentText, $namespaceStr, $page->getOutput());
			$titleSnippet = Linker::link($titleStr, $hw) . " ($namespaceStr)";
		}
		elseif (in_array($namespaceStr, ['En_talk', 'Ci_talk', 'Jyut_talk'])) {
			$entryNsStr = str_replace('_talk', '', $namespaceStr);
			$entryTitle = Title::newFromText("$entryNsStr:$titleStr");
			$contentText = WenlinGongju::getContentTextFromTitleObject($entryTitle);
			WenlinGongju::stripWLTag($contentText);
			$hw = WenlinBand::getHeadwordFromContentText($contentText, $entryNsStr, $page->getOutput());
			$hw = "Talk:$hw";
			$titleSnippet = Linker::link($titleStr, $hw) . " ($namespaceStr)";
		}
		return true;
	} // onShowSearchHitTitle

	/** onSkinTemplateOutputPageBeforeExec:
		Hook SkinTemplateOutputPageBeforeExec. Allow further setup of the template engine
		after all standard setup has been performed but before the skin has been rendered.

		For Wenlin, we use this to add footer items, as documented at:
				https://www.mediawiki.org/wiki/Manual:Footer

		This requires creation of three pages for each item:
			(1) Mediawiki:FooLabel (contains text to display in footer);
			(2) Mediawiki:FooPage (contains namespace:title of linked page, like 'Project:Foo');
			(3) Project:Foo (contains detailed page text).

		TODO: Use of SkinTemplateOutputPageBeforeExec hook (used in WenlinHooks::onSkinTemplateOutputPageBeforeExec) was deprecated in MediaWiki 1.35.

		@param $skin the skin.
		@param $template the skin template.
		@return true.
	*/
	public static function onSkinTemplateOutputPageBeforeExec($skin, &$template)
	{
		/* First additional footer item: Copyright
			(1) Mediawiki:CopyrightLabel (contains text to display in footer, like 'Copyright©');
			(2) Mediawiki:CopyrightPage (contains namespace:title of linked page, like 'Project:Copyrights');
			(3) Project:Copyrights (contains detailed copyright notice).
			Note: MediaWiki has 'Project:Copyrights' (plural) hard-coded (?) in the notice where
			you save changes to a page, so include the 's' in 'Project:Copyrights'. */
		$template->set('CopyrightLabel', $skin->footerLink('CopyrightLabel', 'CopyrightPage'));
		$template->data['footerlinks']['places'][] = 'CopyrightLabel';

		/* Second additional footer item: Terms of service
			(1) Mediawiki:TermsOfServiceLabel (contains text to display in footer, like 'Terms of service');
			(2) Mediawiki:TermsOfServicePage (contains namespace:title of linked page, like 'Project:Terms_of_Service');
			(3) Project:Terms_of_Service (contains detailed terms of service). */
		$template->set('TermsOfServiceLabel', $skin->footerLink('TermsOfServiceLabel', 'TermsOfServicePage'));
		$template->data['footerlinks']['places'][] = 'TermsOfServiceLabel';

		return true;
	} // onSkinTemplateOutputPageBeforeExec

	/** onSpecialRandomGetRandomTitle:
		Modify the selection criteria used in Special:Random and subclasses of it.

		For Wenlin we currently don't want "Random page" to go to the CDL namespaces (NS_WL_CDL, NS_WL_CDL_TALK).

		@param &$randstr: A random number string, generatated by wfRandom.
		@param &$isRedir: Boolean, whether or not the page to be selected should be a redirect.
		@param &$namespaces: An array of namespace indexes (integers, not strings) to get the page from.
		@param &$extra: An array of extra SQL statements added to the WHERE clause of the query.
		@param &$titleObj: A Title object to redirect to if the hook returns false (must be set if the hook returns false).

		Reference: https://www.mediawiki.org/wiki/Manual:Hooks/SpecialRandomGetRandomTitle
	*/
	public static function onSpecialRandomGetRandomTitle(&$randstr, &$isRedir, &$namespaces, &$extra, &$titleObj)
	{
		// die(var_dump($namespaces)); // uncomment this to find out what it contains
		foreach ([NS_WL_CDL, NS_WL_CDL_TALK] as $ns) {
			$key = array_search($ns, $namespaces);
			if ($key !== false) {
				unset($namespaces[$key]);
			}
		}
		return true;
	} // onSpecialRandomGetRandomTitle

	/** onGetUserPermissionsErrorsExpensive:
		Hook getUserPermissionsErrorsExpensive (formerly userCan), to allow or disallow certain actions on particular pages.
		For now, in Wenlin we use this to disallow all actions on Cdl namespace except for administrators.

		TODO: integrate this hook with WenlinXuke.php, clarify what happens if both this hook and WenlinXuke.php
		have something to say about accessing the same titles. Which takes effect first if they would both have effects?
		Per https://www.mediawiki.org/wiki/Security_issues_with_authorization_extensions there may be some ways to
		circumvent WenlinXuke.php that can be caught by userCan. However, userCan might not allow for the ways
		we suggest that users log in or subscribe for access...

		@param $titleObj reference to the title object in question (see the use in $IP/includes/Title.php).
		@param $user reference to the current user (see the use in $IP/includes/Title.php).
		@param $action action concerning the title in question.
		@param $result reference to the result propagated along the chain of hooks (see $IP/includes/Hooks.php).
			$result can be left untouched, or set to true or false.
			True means that the user is allowed.
			False means that the user is disallowed for the action concerning the title.
			Leaving untouched means that we have no opinion about the situation.
		@return true to continue processing the possibly nested list of hooks, or false to stop processing.

		https://www.mediawiki.org/wiki/Manual:Hooks/userCan

		Compare onMediaWikiPerformAction.
	*/
	public static function onGetUserPermissionsErrorsExpensive($titleObj, &$user, $action, &$result)
	{
		if ($titleObj->inNamespace(NS_WL_CDL) && !$user->isAllowed('editinterface')) {
			$result = false;
			return false;
		}
		/* Do nothing else, just return true. Calling entryIsViewableByThisUser here would be problematic
			since we don't have the opportunity to display a message advising the user to log in or
			subscribe. Also, seemingly it would be redundant, since we can control access elsewhere
			including onMediaWikiPerformAction.
			*/
		return true;
	} // onGetUserPermissionsErrorsExpensive

} // class WenlinHooks
