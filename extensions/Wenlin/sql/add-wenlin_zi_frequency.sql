--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_zi_frequency (

  -- The USV of a Hanzi (entry headword); unique in this table (each Hanzi has only one frequency).
  -- Need an index on hanzi, for quickly getting the frequency rank of a given hanzi.
  hanzi mediumint unsigned NOT NULL PRIMARY KEY,

  -- Frequency rank.
  frequency int unsigned NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on frequency, for quickly listing characters by frequency rank.
CREATE INDEX /*i*/frequency ON /*_*/wenlin_zi_frequency (frequency);
