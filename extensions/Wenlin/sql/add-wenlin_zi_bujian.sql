--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_zi_bujian (

  -- Unique column not related to anything, just so we have a unique column.
  -- Otherwise, phpMyAdmin warns, "This table does not contain a unique column. Grid edit, checkbox, Edit, Copy and Delete features are not available."
  id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,

  -- The USV of a Hanzi (entry headword); not unique in this table, since a Hanzi may have many components.
  hanzi mediumint unsigned NOT NULL,

  -- The USV of a bujian (component); not unique, since a component may occur in many Hanzi.
  bujian mediumint unsigned NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on bujian for quickly listing characters containing a given component.
CREATE INDEX /*i*/bujian ON /*_*/wenlin_zi_bujian (bujian);

-- TODO: May join with wenlin_zi_frequency for ordering by frequency; would index on hanzi speed that up?
