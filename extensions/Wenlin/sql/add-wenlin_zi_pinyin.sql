--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_zi_pinyin (

  -- Unique column not related to anything, just so we have a unique column.
  id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,

  -- The USV of the Hanzi (entry headword); not unique, since one Hanzi may have multiple readings.
  hanzi mediumint unsigned NOT NULL,

  -- Pinyin syllable, toneless, as a string.
  -- Longest is "chuang", 6 bytes. (Longest with "ü" is "lüe", 4 bytes in UTF-8, "ü" being 2 bytes.)
  pinyin varbinary(6) NOT NULL,

  -- Tone, as a number 0-4.
  tone tinyint(1) unsigned NOT NULL,

  -- Is the Hanzi with this reading valid as Jiantizi (simple form)?
  jianti boolean NOT NULL,

  -- Is the Hanzi with this reading valid as Fantizi (full form)?
  fanti boolean NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on pinyin for listing all the characters with a given reading.
CREATE INDEX /*i*/pinyin ON /*_*/wenlin_zi_pinyin (pinyin);

-- Maybe don't need an index on hanzi (unless it would help with joins?).
