--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
-- Note that this table definition is identical to wenlin_ci_hanzi_contained except for the name.
-- For code re-use, column names have "pinyin" and "hanzi" rather than "jyutping" and "honzi".
--

CREATE TABLE /*_*/wenlin_jyut_hz_contained (

  -- Unique column not related to anything, just so we have a unique column.
  -- Otherwise, phpMyAdmin warns, "This table does not contain a unique column. Grid edit, checkbox, Edit, Copy and Delete features are not available."
  id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,

  -- Non-unique ID, identifying the Cidian entry. Non-unique in this table since one entry may have many hanzi.
  -- Maybe same as page_id of WikiPage; maybe same as page_id in wenlin_ci_pinyin and other tables, for joins.
  page_id int unsigned NOT NULL,

  -- The USV of one Hanzi that is contained in the entry (like 林 in 文林).
  hanzi mediumint unsigned NOT NULL,

  -- Syllable number. E.g., 1 for 文 in 文林, 2 for 林 in 文林.
  syllable_number smallint unsigned NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on hanzi, so we can quickly list all the entries that contain a given hanzi.
CREATE INDEX /*i*/hanzi ON /*_*/wenlin_jyut_hz_contained (hanzi);

-- Want an index on syllable_number for quickly listing starting/ending with hanzi.
CREATE INDEX /*i*/syllable_number ON /*_*/wenlin_jyut_hz_contained (syllable_number);
