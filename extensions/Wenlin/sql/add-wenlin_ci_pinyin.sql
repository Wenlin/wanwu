--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_ci_pinyin (

  -- Unique ID, identifying the Cidian entry.
  -- Unique in this table since an entry has only one pinyin, one serial number, one frequency.
  -- Maybe same as page_id of WikiPage; maybe same as id in other tables, for joins.
  page_id int unsigned NOT NULL PRIMARY KEY,

  -- Serial number as a string; used as entry title. Compare MAX_SER_LENGTH = 20 in C code.
  serial_number varbinary(20) NOT NULL,

  -- Pinyin collation key as a string. Compare MAX_ABC_EXP_LEN = 300 in C code.
  pinyin_collation varbinary(300) NOT NULL,

  -- Syllable count as a number. E.g., 2 for Wénlín.
  syllable_count smallint unsigned NOT NULL,

  -- Frequency of usage as a number.
  frequency float NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on pinyin_collation for listing words alphabetically.
CREATE INDEX /*i*/pinyin_collation ON /*_*/wenlin_ci_pinyin (pinyin_collation);

-- Need an index on frequency for listing words by frequency.
CREATE INDEX /*i*/frequency ON /*_*/wenlin_ci_pinyin (frequency);

-- Maybe want an index on syllable_count, e.g., for join with wenlin_ci_hanzi_contained for listing words ending with hanzi?
-- CREATE INDEX /*i*/syllable_count ON /*_*/wenlin_ci_pinyin (syllable_count);

-- Probably don't need an index on serial_number, since the ordinary mediawiki tables already enable listing by serial number (entry title). 