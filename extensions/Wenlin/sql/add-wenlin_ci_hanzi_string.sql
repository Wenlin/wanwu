--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_ci_hanzi_string (

  -- Unique column not related to anything, just so we have a unique column.
  id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,

  -- Non-unique ID, identifying the Cidian entry.
  -- Not unique in this table since one entry may have many hanzi strings (like 学校 and 學校 for xuéxiào).
  -- Maybe same as page_id of WikiPage; maybe same as id in wenlin_ci_pinyin and other tables, for joins.
  page_id int unsigned NOT NULL,

  -- Hanzi string (maybe polysyllabic, like 学校), UTF-8. Compare C code MAX_CI_LEN = 80.
  -- Not unique since many entries can have the same hanzi string (like 重重 = ¹chóngchóng or zhòngzhòng).
  hanzi varbinary(80) NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on hanzi for quickly finding the entry (or entries) for a given hanzi string.
CREATE INDEX /*i*/hanzi ON /*_*/wenlin_ci_hanzi_string (hanzi);
