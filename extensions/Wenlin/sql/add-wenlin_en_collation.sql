--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_en_collation (

  -- Unique ID, identifying the Ying-Han entry. Unique in this table since one entry has only one serial number, one English collation key.
  -- Maybe same as page_id of WikiPage.
  page_id int unsigned NOT NULL PRIMARY KEY,

  -- Serial number as a string; used as entry title. Compare MAX_SER_LENGTH = 20 in C code.
  serial_number varbinary(20) NOT NULL,

  -- English collation key as a string. Compare MAX_ABC_EXP_LEN = 300 in C code.
  -- Compare 'pinyin_collation' in Ci and Jyut tables. It would have been better if we'd used a neutral
  -- name like 'collation' instead of 'english_collation' or 'pinyin_collation', to simplify some code that
  -- works for multiple namespaces.
  english_collation varbinary(300) NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on english_collation for quickly listing entries alphabetically.
CREATE INDEX /*i*/english_collation ON /*_*/wenlin_en_collation (english_collation);

-- Probably don't need an index on serial_number, since the ordinary mediawiki tables already enable listing by serial number (entry title). 
