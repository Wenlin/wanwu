--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
-- Note that this table definition is identical to wenlin_ci_pinyin except for the name.
-- For code re-use, column names have "pinyin" and "hanzi" rather than "jyutping" and "honzi".
-- It would have been even better to use more neutral names like "collation" instead of "pinyin_collation",
-- so that the same names would work for English, functions could be simpler.
--

CREATE TABLE /*_*/wenlin_jyut_ping (

  -- Unique ID, identifying the Jyut entry.
  -- Unique in this table since an entry has only one pinyin, one serial number, one frequency.
  -- Maybe same as page_id of WikiPage; maybe same as id in other tables, for joins.
  page_id int unsigned NOT NULL PRIMARY KEY,

  -- Serial number as a string; used as entry title. Compare MAX_SER_LENGTH = 20 in C code.
  serial_number varbinary(20) NOT NULL,

  -- Jyutping collation key as a string.
  pinyin_collation varbinary(300) NOT NULL,

  -- Syllable count as a number. E.g., 2 for Wénlín.
  syllable_count smallint unsigned NOT NULL,

  -- Frequency of usage as a number.
  frequency float NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on pinyin_collation for listing words alphabetically.
CREATE INDEX /*i*/pinyin_collation ON /*_*/wenlin_jyut_ping (pinyin_collation);

-- Need an index on frequency for listing words by frequency.
CREATE INDEX /*i*/frequency ON /*_*/wenlin_jyut_ping (frequency);
