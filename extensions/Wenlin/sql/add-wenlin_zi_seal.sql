--
-- Table for the Wenlin extension; see onLoadExtensionSchemaUpdates in Wenlin.hooks.php.
--

CREATE TABLE /*_*/wenlin_zi_seal (

  -- Unique column not related to anything, just so we have a unique column.
  id int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,

  -- The USV of a Hanzi (entry headword); not unique, since a Hanzi may have many Seal mappings.
  hanzi mediumint unsigned NOT NULL,

  -- The USV of a Shuowen Seal character; not unique, since a Seal character may be mapped from many Hanzi.
  seal mediumint unsigned NOT NULL,

  -- Distance: how closely related are Hanzi and Seal.
  distance smallint unsigned NOT NULL

) /*$wgDBTableOptions*/;

-- Need an index on hanzi for quickly getting its corresponding seal form(s).
CREATE INDEX /*i*/hanzi ON /*_*/wenlin_zi_seal (hanzi);
