<?php
/* Copyright (c) 2017 Wenlin Institute, Inc. SPC.
	This PHP file is offered for licensing under the GNU Affero General Public License v.3:
		https://www.gnu.org/licenses/agpl.html
	For other licensing options, please see:
		http://wenlin.com/developers
	Much of the Wenlin MediaWiki extension is derived from the C source code for
	the desktop (Mac/Windows/Linux) application Wenlin Software for Learning Chinese.
	Authors: Tom Bishop and other members of the Wenlin development team.
*/
/**
	SpecialWenlin SpecialPage for Wenlin extension.
	See <https://wenlin.biz/wow/Special:Wenlin>, which can be reached by "Special pages" in sidebar.

	Reference: <https://www.mediawiki.org/wiki/Manual:Special_pages>
	@file
	@ingroup Extensions
*/

class SpecialWenlin extends SpecialPage
{
	/* __construct:
		Construct a Wenlin special page.
		The name we pass to the parent here is "Name of the special page, as seen in links and URLs".
		It needs translations in the i18 folder, otherwise it shows up on <https://wenlin.biz/wow/Special:SpecialPages>
		enclosed in angle brackets and converted to lowercase.
		Also see extension.json, which has TWO sections related to special pages: "AutoloadClasses" and "SpecialPages":
			"AutoloadClasses": {
					...
				"SpecialWenlin": "specials/SpecialWenlin.php"
			},
			...
			"SpecialPages": {
				"Wenlin": "SpecialWenlin"
			},
		Seemingly the "Wenlin" following "SpecialPages" needs to match the name we pass to the parent here,
		while "SpecialWenlin" needs to match the name of this class.
	*/
	public function __construct()
	{
		parent::__construct('Wenlin'); // see i18/*.json for "wenlin".
	} // __construct

	/** getGroupName:
		Get the group name for this special page.
		Actually we might not want a link for the special pages group at all...
	*/
	protected function getGroupName()
	{
		return 'other';
	} // getGroupName

	/** execute:
		Show the page to the user.

		@param string $subPage The subpage string argument (if any), or null ([[Special:Wenlin/subpage]]).

		Example of an actual URL: "http://localhost/wow/Special:Wenlin/a?a=horse&ns=En". Here,
		we'll get $subPage = 'a', which is the "command". We'll get the arguments 'a=horse' and
		'ns=En' (which follow '?') from the WebRequest.
	*/
	public function execute($subPage)
	{
		$outputPage = $this->getOutput();
		if ($subPage !== null) {
			$wlParser = new WenlinParser(null, $outputPage);
			new WenlinZhixing($subPage /* command */, $this->getRequest(), $outputPage, $wlParser);
			return;
		}
		$outputPage->setPageTitle($this->msg('wenlin-helloworld'));
		$outputPage->addWikiMsg('wenlin-helloworld-intro');
		$outputPage->addHTML("<p>This special page expects a command following its name, like 'Special:Wenlin/zc?z=好'.</p>");
	} // execute

} // class SpecialWenlin
