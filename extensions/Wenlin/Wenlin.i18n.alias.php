<?php
/**
 * Aliases for special pages of the Wenlin extension
 *
 * @file
 * @ingroup Extensions
 */

$specialPageAliases = array();

/** English (English) */
$specialPageAliases['en'] = array(
	'Wenlin' => array( 'Wenlin' ),
);

/** Chinese (Zhongwen) */
$specialPageAliases['zh'] = array(
	'Wenlin' => array( '文林' ),
);
